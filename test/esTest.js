'use strict';

module.exports.handler = async event => {
    
    try {

        const result = await es.listDomainNames()
        console.log(result)
        
        return {
            statusCode: 200
        };

    } catch (e) {

        console.log(e)

        return {
            statusCode: 403
        };
    }
};
