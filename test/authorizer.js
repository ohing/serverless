'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)

module.exports.handler = async event => {
    
    try {

        const userInfo = await authorizer.userInfo('88fadd20-9980-4197-aa5a-ac9dadf6961e')
        console.log(userInfo)
        
        return {
            statusCode: 200
        };

    } catch (e) {

        console.log(e)

        return {
            statusCode: 403
        };
    }
};
