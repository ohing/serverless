'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingredis-${process.env.databaseStage}`)

module.exports.handler = async event => {
    
    try {

        const userInfo = await authorizer.userInfo('88fadd20-9980-4197-aa5a-ac9dadf6961e')
        console.log(userInfo)

        const writable = await redis.writable()

        await writable.set('aaa', 'ccc')
        const value = await writable.get('aaa')

        writable.quit()

        console.log(value)
        
        return {
            statusCode: 200
        };

    } catch (e) {

        console.log(e)

        return {
            statusCode: 403
        };
    }
};
