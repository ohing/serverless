'use strict';

const mysql = require(`@ohing/ohingredis-${process.env.databaseStage}`)

module.exports.handler = async event => {
    
    try {

        const writable = await redis.writable()
        
        await writable.set('aaa', 'bbb')
        const value = await writable.get('aaa')
        console.log(value)

        await writable.quit()
        
        return {
            statusCode: 200,
            body: value
        };

    } catch (e) {

        console.log(e)

        return {
            statusCode: 403
        };
    }
};
