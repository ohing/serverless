CREATE DEFINER=`ohing_master`@`%` PROCEDURE `IO_SP_WITHDRAW_USER_INFO_ALL_DELETE`()
    COMMENT '탈퇴회원의 모든 정보를 삭제'
BEGIN
   DECLARE v_finished  INTEGER DEFAULT 0;
   
   DECLARE $v_user_id        VARCHAR(20);
	DECLARE $v_remove_ymd     VARCHAR(12);
   
   
   /* 삭제기준일자 */
	select date_format(DATE_SUB(sysdate(), INTERVAL 1 DAY),  '%Y%m%d') as remove_ymd
	into   $v_remove_ymd;
	

	User_Block: BEGIN
		DECLARE user_cursor CURSOR FOR
		/**********************************************************************************
	    탈퇴대기회원중 1일이 경과하여 탈퇴 처리하는 대상자 가져오기.
		 **********************************************************************************/
		select user_id
		from   oi_user_info
		where  user_status_cd = 'WDSTANDBY'
		and    date_format(withdraw_expire_dt, '%Y%m%d') <= $v_remove_ymd;
	  
		
		-- NOT FOUND 핸들러를 정의한다.
	   DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
	   
		-- 커서 OPEN
		OPEN user_cursor;			
			
			-- 커서의 레코드 수 만큼 반복한다.
			get_user_info: LOOP
			-- 선택한 행의 값을 가져온다.
			FETCH user_cursor INTO $v_user_id;
			
			-- 처리할 레코드가 없는 경우 종료한다.
			IF v_finished = 1 THEN
				LEAVE get_user_info;
			END IF;  	   
			 
		   
		   /**********************************************************************************
			  SNS 관련 데이터 삭제하기.
          **********************************************************************************/
         -- 1. HOME 
         update oi_legend_top10 set user_login_id = '', school_nm = '' where user_id = $v_user_id;
         update oi_help_best set user_login_id = '', title = 'user_delete', content = 'user_delete' where user_id = $v_user_id;
         update oi_help_category_best set user_login_id = '', title = 'user_delete', content = 'user_delete' where user_id = $v_user_id;
--          update oi_fav_movie set user_login_id = '', content = 'user_delete' where user_id = $v_user_id;
         
                  
         -- 2. FEED 관련 삭제하기.
         delete from oi_feed_comment where user_id = $v_user_id and level = 1;
         update oi_feed_comment set content = '삭제된 댓글입니다.', del_yn = 'Y', upd_dt = now()
         where user_id = $v_user_id and level = 0;
         -- update oi_feed_comment set content = '삭제된 글입니다.' where user_id = $v_user_id;

         -- delete from oi_feed_comment_except where comment_id in (select comment_id from oi_feed_comment where user_id = $v_user_id);
         -- delete from oi_feed_info_except where feed_id in (select feed_id from oi_feed_info where reg_user_id = $v_user_id);
         -- delete from oi_feed_view_log where feed_id in (select feed_id from oi_feed_info where reg_user_id = $v_user_id);
         -- delete from oi_feed_like_log where user_id = $v_user_id;
         
         delete from oi_feed_tag where feed_id in (select feed_id from oi_feed_info where reg_user_id = $v_user_id);
         delete from oi_feed_link_url_mapping where feed_id in (select feed_id from oi_feed_info where reg_user_id = $v_user_id);

         -- delete from oi_feed_report where report_user_id = $v_user_id or feed_id in (select feed_id from oi_feed_info where reg_user_id = $v_user_id);
                  
                  
         delete from oi_media_info where reg_user_id = $v_user_id;
         -- delete from oi_media_like_log where user_id = $v_user_id;
         
         -- 관심사 배치 : 속도 관련 제외하기
         -- delete from oi_interest_feed where user_id = $v_user_id;
                  
         delete from oi_feed_info where reg_user_id = $v_user_id;
         
                  
         -- 3. 팔로워 정보 삭제하기.
         delete from oi_follower_info where user_id = $v_user_id;
         delete from oi_follower_info where follow_user_id = $v_user_id;
         
         
         -- 4. 첨부파일 삭제하기.
         update com_atch_file_detail set del_yn = 'Y' where reg_user_id = $v_user_id;
         update oi_file_detail_info set del_yn = 'Y' where reg_user_id = $v_user_id;
         
         -- 5. 사용자 정보 삭제하기.
         delete from oi_user_profile_image where user_id = $v_user_id;
         -- delete from oi_user_profile_view_log where user_id = $v_user_id;
         
         -- delete from oi_user_blocking_info where (user_id = $v_user_id or blocking_user_id = $v_user_id);
         -- delete from oi_user_report where user_id = $v_user_id;
         
         -- delete from oi_user_school_history where user_id = $v_user_id;
         delete from oi_user_tag_info where user_id = $v_user_id; 
         
         delete from oi_user_device_info where user_id = $v_user_id;
         delete from oi_user_address where user_id = $v_user_id;
         
         update oi_user_join_push set user_login_id = concat('알 수 없는 사용자::D', user_id) where user_id = $v_user_id;
         update oi_user_ask_info set answer_email = 'delete_user' where user_id = $v_user_id;
         
         
         -- 6. 채팅 참여 대상자 삭제.
         delete from oi_chat_user_info where user_id = $v_user_id;
         delete from oi_chat_user_room_setting where user_id = $v_user_id;
         delete from oi_chat_room_profile_info where user_id = $v_user_id;
         delete from oi_chat_profile_info where user_id = $v_user_id;
         
         /*
         delete from oi_chat_msg_receive_log where receive_user_id = $v_user_id;
         delete from oi_chat_msg_receive where receive_user_id = $v_user_id;
         */
         
         
         insert into ct_delete_user_info (
														bit_index,
														user_id,
														user_type_cd,
														user_login_id,
														user_pwd,
														user_nm,
														user_real_nm,
														user_email,
														user_hp,
														region_cd,
														sex,
														birth_ymd,
														user_school_id,
														profile_open_type_cd,
														tag_list,
														self_introduction,
														ci,
														di,
														user_status_cd,
														login_auth_no,
														login_auth_no_expire_dt,
														auth_no,
														auth_no_expire_dt,
														last_login_dt,
														online_update_dt,
														friend_tab_last_visit_dt,
														parent_hp,
														parent_nm,
														parent_birth_ymd,
														parent_sex,
														withdraw_expire_dt,
														withdraw_expire_reason,
														reg_dt,
														reg_user_id,
														upd_dt,
														upd_user_id,
														os_type_cd,
														remark
													)
			select 
				a.bit_index,
				a.user_id,
				a.user_type_cd,
				a.user_login_id,
				a.user_pwd,
				a.user_nm,
				a.user_real_nm,
				a.user_email,
				a.user_hp,
				a.region_cd,
				a.sex,
				a.birth_ymd,
				a.user_school_id,
				a.profile_open_type_cd,
				a.tag_list,
				a.self_introduction,
				a.ci,
				a.di,
				a.user_status_cd,
				a.login_auth_no,
				a.login_auth_no_expire_dt,
				a.auth_no,
				a.auth_no_expire_dt,
				a.last_login_dt,
				a.online_update_dt,
				a.friend_tab_last_visit_dt,
				a.parent_hp,
				a.parent_nm,
				a.parent_birth_ymd,
				a.parent_sex,
				a.withdraw_expire_dt,
				a.withdraw_expire_reason,
				a.reg_dt,
				a.reg_user_id,
				a.upd_dt,
				a.upd_user_id,
				a.os_type_cd,
				a.remark		 
			from oi_user_info a
			where  a.user_id = $v_user_id
		   and    a.user_id != 'USR_0000000000062429'
			; 
       
       
		/**********************************************************************************
			 제재회원 여부를 확인한다.
         **********************************************************************************/  
			update oi_user_info set user_login_id  = concat('알 수 없는 사용자::D', user_id),
			                        user_pwd       = '',
			                        user_nm        = '',
			                        user_real_nm   = '',
			                        user_email     = '',
			                        user_hp        = '',
			                        self_introduction = '',
			                        tag_list       = '',
			                        ci             = '',
			                        di             = '',
			                        parent_hp      = '',
			                        parent_nm      = '',
			                        parent_birth_ymd = '',
			                        parent_sex     = '',
			                        user_status_cd = 'WITHDRAW',
			                        upd_dt         = now()
			where  user_id = $v_user_id;

										 
			END LOOP get_user_info;
			
		-- 커서 CLOSE
		CLOSE user_cursor;
	
	END User_Block;
	
END