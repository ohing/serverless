

1. 어제까지의 탈퇴자 userId
select user_id from oi_user_info where user_status_cd = 'WDSTANDBY' and date_format(withdraw_expire_dt, '%Y%m%d') < date_format(sysdate(), '%Y%m%d');


2. 각종 활동내역 삭제

-- update oi_legend_top10 set user_login_id = '', school_nm = '' where user_id in ();
-- update oi_help_best set user_login_id = '', title = 'user_delete', content = 'user_delete' where user_id in ();
-- update oi_help_category_best set user_login_id = '', title = 'user_delete', content = 'user_delete' where user_id in ();
-- update oi_feed_comment set content = '삭제된 댓글입니다.', del_yn = 'Y', upd_dt = now() where user_id in () and level = 0
-- delete from oi_feed_comment where user_id in () and level = 1;
-- delete from oi_feed_tag where feed_id in (select feed_id from oi_feed_info where reg_user_id in ());
-- delete from oi_feed_link_url_mapping where feed_id in (select feed_id from oi_feed_info where reg_user_id in ());
-- delete from oi_media_info where reg_user_id in ();
-- delete from oi_feed_info where reg_user_id in ();
-- delete from oi_follower_info where user_id in ();
-- delete from oi_follower_info where follow_user_id in ();
-- update com_atch_file_detail set del_yn = 'Y' where reg_user_id in ();
-- update oi_file_detail_info set del_yn = 'Y' where reg_user_id in ();
-- delete from oi_user_profile_image where user_id in ();
-- delete from oi_user_tag_info where user_id in (); 
-- delete from oi_user_device_info where user_id in ();
-- delete from oi_user_address where user_id in ();
-- update oi_user_join_push set user_login_id = concat('알 수 없는 사용자::D', user_id) where user_id in ();
-- update oi_user_ask_info set answer_email = 'delete_user' where user_id in ();
-- delete from oi_chat_user_info where user_id in ();
-- delete from oi_chat_user_room_setting where user_id in ();
-- delete from oi_chat_room_profile_info where user_id in ();
-- delete from oi_chat_profile_info where user_id in ();

3. 삭제유저정보 백업. 근데 왜..?

insert into ct_delete_user_info ( 
    bit_index, user_id, user_type_cd, user_login_id, user_pwd, user_nm, user_real_nm, user_email, user_hp, region_cd,
    sex, birth_ymd, user_school_id, profile_open_type_cd, tag_list, self_introduction, ci, di, user_status_cd, login_auth_no,
    login_auth_no_expire_dt, auth_no, auth_no_expire_dt, last_login_dt, online_update_dt, friend_tab_last_visit_dt, parent_hp, parent_nm, parent_birth_ymd, parent_sex,
    withdraw_expire_dt, withdraw_expire_reason, reg_dt, reg_user_id, upd_dt, upd_user_id, os_type_cd, remark
)
    select 
        a.bit_index, a.user_id, a.user_type_cd, a.user_login_id, a.user_pwd, a.user_nm, a.user_real_nm, a.user_email, a.user_hp,
        a.region_cd, a.sex, a.birth_ymd, a.user_school_id, a.profile_open_type_cd, a.tag_list, a.self_introduction, a.ci, a.di, a.user_status_cd, a.login_auth_no,
        a.login_auth_no_expire_dt, a.auth_no, a.auth_no_expire_dt, a.last_login_dt, a.online_update_dt, a.friend_tab_last_visit_dt, a.parent_hp, a.parent_nm, a.parent_birth_ymd, a.parent_sex,
        a.withdraw_expire_dt, a.withdraw_expire_reason, a.reg_dt, a.reg_user_id, a.upd_dt, a.upd_user_id, a.os_type_cd, a.remark		 
    from
        oi_user_info a
    where
        a.user_id in ()
        and a.user_id != 'USR_0000000000062429'
; 

4. 삭제회원 마킹

update
    oi_user_info 
set 
    user_login_id = concat('알 수 없는 사용자::D', user_id),
    user_pwd = '', user_nm = '', user_real_nm = '', user_email = '', user_hp = '',
    self_introduction = '', tag_list = '', ci = '', di = '', parent_hp = '',
    parent_nm = '', parent_birth_ymd = '', parent_sex = '', user_status_cd = 'WITHDRAW', upd_dt = now()
where
    user_id in ();