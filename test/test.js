module.rds = async () => {
  return await require('./rds.js').handler()
}

module.redis = async () => {
  return await require('./redis.js').handler()
}

module.authorizer = async () => {
  return await require('./authorizer.js').handler()
}

module.authAndGet = async () => {
  return await require('./authAndGet.js').handler()
}

module.awsTest = async () => {
  return await require('./awsTest.js').handler()
}
module.rds2 = async () => {
  return await require('./rds2.js').handler()
}
module.attend_2020_last = async () => {
  return await require('./attend_2020_last.js').handler()
}



test = async () => {

  const environment = require('../environment.js')
  environment.setEnvironment()
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()