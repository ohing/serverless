const mysql = require('mysql2/promise')

const RDS = class {
    
    writable() {
        return new WritableClient()
    }

    readable() {
        return new ReadableClient()
    }
}

class RDSClient {

    host() { return '' }
    user() { return '' }
    password() { return '' }
    database() { return '' }
    
    #client = null

    currentClient() {
        return this.#client
    }

    async client() {
        try {
            return await mysql.createConnection({host: this.host(), user: this.user(), password: this.password(), database: this.database()})
        } catch(e) {
            console.log(e)
            throw e
        }
    }

    async execute(query, parameters = null) {
    
        try {
            
            if (this.#client == null) {
                this.#client = await this.client()
            }

            if (parameters == null) {
                const [rows, _] = await this.#client.execute(query)
                return rows
            } else {
                const [rows, _] = await this.#client.query(query, parameters)
                return rows
            }
            
        } catch(e) {
            
            console.log(e)
            throw e
        }
    }

    async escape(component) {

      try {
          if (this.#client == null) {
              this.#client = await this.client()
          }
      } catch (e) {
        console.log(e)
        throw e
      }

      return this.#client.escape(component)
    }

    async beginTransaction() {

        try {

            if (this.#client == null) {
                this.#client = await this.client()
            }

            await this.#client.beginTransaction()

        } catch(e) {
            
            console.log(e)
            throw e
        }
    }
    
    async commit() {

        try {

            if (this.#client == null) {
                return
            }

            await this.#client.commit()

        } catch(e) {
            
            console.log(e)
            throw e
        }
    }

    async rollback() {

        try {

            if (this.#client == null) {
                return
            }

            await this.#client.rollback()

        } catch(e) {
            
            console.log(e)
            throw e
        }
    }

    end() {
    
        if (this.#client != null) {
            this.#client.end()
            this.#client = null
        }
    }
}

class WritableClient extends RDSClient {

    host() { return 'ohing-migration-cluster.cluster-cztt5idwty9x.ap-northeast-2.rds.amazonaws.com' }
    user() { return 'ohing_master' }
    password() { return 'ohing123' }
    database() { return 'ohingmig' }
}

class ReadableClient extends RDSClient {

    host() { return 'ohing-migration-cluster.cluster-cztt5idwty9x.ap-northeast-2.rds.amazonaws.com' }
    user() { return 'ohing_master' }
    password() { return 'ohing123' }
    database() { return 'ohingmig' }
}

module.exports = new RDS()
