'use strict';

const AWS = require('aws-sdk')
const es = new AWS.ES({endpoint: 'https://vpc-dev-es-rwdcbq535w6kgusnpeooxmw3mm.ap-northeast-2.es.amazonaws.com'});

module.exports.handler = async event => {
    
    try {

        const result = await es.listDomainNames()
        console.log(result)
        
        return {
            statusCode: 200
        };

    } catch (e) {

        console.log(e)

        return {
            statusCode: 403
        };
    }
};
