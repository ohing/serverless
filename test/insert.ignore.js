const mysql = require('./rds.test').writable();

async function insertTest() {

    const roomId = '4977';
    const messageType = '';
    const text = '';
    const timestamp = '1645965576768';
    const userId = '300';
    const nickname = '';
    const profileFileName = '';
    const selfIntroduction = '';
    const masterYn = '';
    const targetUserId = '';
    const targetUserNickname = '';
    const inviteRoomId = '';
    const inviteRoomInfo = null;
    const mediaInfo = null;
    const encoded = '';
    const chatHash = '4977-300-1645965576768';
    const chatMessageInsert = `
        insert ignore into oi_chat_msg(
            room_id, msg_type_cd, msg_content, send_dt_int, send_user_id, 
            send_user_nick_nm, send_profile_image_url, chat_profile_intro_content, master_yn, target_user_id, 
            target_user_nick_nm, invite_room_id, invite_room_info, media_info, encoded_yn, 
            chat_hash
        ) values(
            ?, ?, ?, ?, ?, 
            ?, ?, ?, ?, ?, 
            ?, ?, ?, ?, ?,
            ?
        );
    `;

    const valueParameters = [
        roomId, messageType, text, timestamp, userId, nickname, profileFileName, selfIntroduction, masterYn,
        targetUserId, targetUserNickname, inviteRoomId, inviteRoomInfo, mediaInfo, encoded, chatHash
    ];

    const chatMessageResult = await mysql.execute(chatMessageInsert, valueParameters);

    console.log(chatMessageResult);

    // inseter ignore 되면 id는 0으로 올라옴
    if (chatMessageResult.insertId === 0) {
        const getChatMessageByChatHash = `
            select msg_id from oi_chat_msg where chat_hash = ? limit 1;
        `;

        const chatMsg = await mysql.execute(getChatMessageByChatHash, [chatHash]);
        console.log(chatMsg);

    }
}

insertTest();