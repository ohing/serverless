'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 도움말 목록 가져오기
 * @method GET getFAQs
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    // const parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }
    
    console.log(parameters)
    let code = parameters.code
    let searchText = parameters.searchText
    let rowCount = parseInt(parameters.rowCount || '20') || 20
    let lastId = (parameters.lastId || '0') || 0

    let where = `where onoff_yn = 'Y' and faq_id>${lastId}`
    if (searchText != null && searchText.length > 0) {
      where += ` and (title like '%${searchText}%' or content like '%${searchText}%')`
    } else if (code != null && code.length > 0) {
      where += ` and faq_type_cd = ${code}`
    }

    let query = `
      select
        faq_id, faq_type_cd, title, content
      from
        oi_faq
      ${where}
      order by
        faq_id asc
      limit 
        ${rowCount};
    `
    console.log(query)
    let result = await mysql.execute(query)
    mysql.end()

    return response.result(200, result)
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
