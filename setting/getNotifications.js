'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 알림 목록
 * @method GET getNotifications
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    // const parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }
    
    const lastId = parameters.lastId == null ? 2147483647 : parseInt(parameters.lastId)
    const rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)

    let searchText = (parameters.searchText || '').trim()
    let searchTextCondition = searchText.length == 0 ? '' : `
      and (
        title like '%${searchText}%'
        or body like '%${searchText}%'
      )
    `

    let query = `
      select
        notification_id, notification_type_cd notification_type,
        concat('https://${process.env.stage == 'dev' ? 'd2ro0vwgwjzuou' : 'd257x8iyw0ju12'}.cloudfront.net/icon/notification/',
        case 
          when notification_type_cd = 'BIRTHDAY' or notification_type_cd = 'BDGROUP' then 'birthday.png'
          when notification_type_cd = 'INVITECHAT' or notification_type_cd = 'RECOMCHAT' then 'chat.png'
          when notification_type_cd = 'COMMENT' or notification_type_cd = 'REPLY' then 'comment.png'
          when notification_type_cd = 'MENTION' then 'mention.png'
          when notification_type_cd = 'FEEDLIKE' or notification_type_cd = 'IMAGELIKE' or notification_type_cd = 'MOVIELIKE' 
            or notification_type_cd = 'COMTLIKE' or notification_type_cd = 'REPLYLIKE' then 'like.png'
          when notification_type_cd = 'FOLLOW' then 'follow.png'
          when notification_type_cd = 'FOLLOWREQ' then 'follow_request.png'
          when notification_type_cd = 'FOLLOWACPT' then 'follow_accept.png'
          when notification_type_cd = 'JJIM' then 'jjim.png'
          when notification_type_cd = 'PFIMAGE' or notification_type_cd = 'PROFILE' then 'profile.png'
          when notification_type_cd = 'SUREGION' or notification_type_cd = 'SUREGGROUP' then 'location.png'
          when notification_type_cd = 'SUSCHOOL' or notification_type_cd = 'SUSCGROUP' then 'school.png'
          when notification_type_cd = 'SUTAG' or notification_type_cd = 'SUTAGGROUP' then 'tag.png'
        end
        ) icon_image_url,
        target_type_cd target_type, target_id, target_url,
        title, body, 
        concat('img/', substring_index(profile_file_name, '.', 1), '/', profile_file_name) profile_image_url, profile_file_name,
        profile_target_type_cd profile_target_type, profile_target_id, link_info,
        read_yn is_read,
        reg_dt created_at
      from
        oi_notification_info
      where 
        user_id = ${userId}
        and notification_id < ${lastId}
        ${searchTextCondition}
      order by 
        notification_id desc
      limit 
        ${rowCount};
    `
    let result = await mysql.execute(query)
    mysql.end()

    let notifications = result.map(row => {
      row.is_read = row.is_read == 'Y'
      return row
    })

    return response.result(200, notifications)
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
