'use strict';

const mysql = require(`@ohing/ohingrds-prod`).writable()
const response = require('@ohing/ohingresponse')


module.exports.handler = async event => {

  console.log(event)

  try {

    let continueFetching = true
    let lastIndex = 0
    let tagCounts = {}
    while (continueFetching) {

      console.log('lastIndex', lastIndex)
      
      let query = `
        select 
          bit_index, tag_list 
        from 
          oi_user_info 
        where
          bit_index > ${lastIndex}
        order by 
          bit_index asc 
        limit 
          1000;
      `
      let result = await mysql.execute(query)
      if (result.length == 0) {
        continueFetching = false
      } else {

        result.forEach(row => {
          let tagList = row.tag_list
          let tags = []
          if (tagList.includes(' #')) {
            tags = tagList.split(' #')
          } else {
            tags = tagList.split('#')
          }
          tags.forEach(tag => {
            if (tag.length > 0) {
              if (tagCounts[tag] == null) {
                tagCounts[tag] = 1
              } else {
                tagCounts[tag] += 1
              }
            }
          })
        })

        lastIndex = result[result.length-1].bit_index
      }
    }

    let keys = Object.keys(tagCounts)
    let counts = []

    for (let i = 0; i < keys.length; i++) {
      let key = keys[i]
      let value = tagCounts[key]
      
      if (value < 100) {
        continue
      }

      counts.push({key: key, value: value})
    }

    counts.sort((lhs, rhs) => {
      return lhs.value < rhs.value ? 1 : lhs.value > rhs.value ? -1 : 0
    })

    console.log(counts)

    continueFetching = true
    lastIndex = 'FEED_000000000000000'
    tagCounts = {}
    while (continueFetching) {

      console.log('lastIndex', lastIndex)
      
      let query = `
        select 
          feed_id, tag_list 
        from 
          oi_feed_info 
        where
          feed_id > '${lastIndex}'
        order by 
          feed_id asc 
        limit 
          1000;
      `
      let result = await mysql.execute(query)
      if (result.length == 0) {
        continueFetching = false
      } else {

        result.forEach(row => {
          let tagList = row.tag_list
          let tags = []
          if (tagList.includes(' #')) {
            tags = tagList.split(' #')
          } else {
            tags = tagList.split('#')
          }
          tags.forEach(tag => {
            let trimmed = tag.trim().replace('#', '')
            if (trimmed.length > 0) {
              if (tagCounts[trimmed] == null) {
                tagCounts[trimmed] = 1
              } else {
                tagCounts[trimmed] += 1
              }
            }
          })
        })

        lastIndex = result[result.length-1].feed_id
      }
    }

    keys = Object.keys(tagCounts)
    counts = []

    for (let i = 0; i < keys.length; i++) {
      let key = keys[i]
      let value = tagCounts[key]
      
      if (value < 100) {
        continue
      }

      counts.push({key: key, value: value})
    }

    counts.sort((lhs, rhs) => {
      return lhs.value < rhs.value ? 1 : lhs.value > rhs.value ? -1 : 0
    })

    console.log(counts)

    mysql.end()
    
    return null
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
