'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 신고하기
 * @method PUT sendReport
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let targetType = parameters.targetType
    let targetId = parameters.targetId
    let codes = parameters.codes
    let content = parameters.content
    let images = parameters.images || []

    console.log(parameters, targetType == null, targetId == null, codes == null, !Array.isArray(codes), codes.length == 0, content == null)

    if (targetType == null || targetId == null || codes == null || !Array.isArray(codes) || content == null) {
      return response.result(400)
    }

    let query
    let result

    let logType = `RP${targetType}`
    let actionId = targetId
    let log = null

    query = `
      select
        sex
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let genderEmoji = result[0].sex == 'M' ? '🦸🏻‍♂️' : '🦸🏻‍♀️'

    switch (targetType) {
      case 'USER':
        log = `유해유저 신고하는 당신이 히어로${genderEmoji}`
        break
      case 'CHATROOM':
        log = `유해게시물 신고하는 당신이 히어로${genderEmoji}`
        break
      default:
        log = `유해채팅방 신고하는 당신이 히어로${genderEmoji}`
        break
    }

    query = `
      insert into oi_report (
        target_type_cd, target_id, report_type_cd, report_content, status_cd, report_user_id, report_dt
      ) values (
        '${targetType}', ${targetId}, '${codes.join('|')}', '${content}', 'NEW', ${userId}, now()
      );
    `
    result = await mysql.execute(query)
    let reportId = result.insertId

    if (images.length > 0) {

      let values = images.map(imageKey => {

        let finalKey = imageKey
        if (!imageKey.includes('/') && imageKey.includes('.')) {
          let fileNames = imageKey.split('.')
          let fileName = fileNames[0]
          let fileExtension = fileNames[1]
          finalKey = `report/${fileName}.${fileExtension}`
        }

        return `( 'IMAGE', '${finalKey}' )`
      })
      let valuesString = values.join(', ')

      query = `
        insert into oi_file_info (
          media_type_cd, org_file_nm
        ) values 
        ${valuesString};
      `

      result = await mysql.execute(query)
      let fileId = result.insertId
      
      values = []

      for (let i = 0; i < images.length; i++) {
        values.push(` ( ${reportId}, ${i+1}, ${fileId+i}, ${userId}) `)
      }
      valuesString = values.join(', ')

      query = `
        insert into oi_report_media_info (
          report_id, media_seq, file_id, reg_user_id
        ) values
        ${valuesString};
      `
      result = await mysql.execute(query)
    }
    
    mysql.end()

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: 1,
          type: logType,
          actionId: targetId,
          log: log
        })
      })
    }
    console.log(logParams)

    await lambda.invoke(logParams).promise()

    return response.result(200, {}, '신고가 접수되었습니다.')
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
