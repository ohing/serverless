'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 1:1문의하기
 * @method PUT sendAsk
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let code = (parameters.code || '').trim()
    let email = parameters.email
    let content = parameters.content
    let images = parameters.images || []

    if (email == null || content == null) {
      return response.result(400)
    }

    if (code.length == 0) {
      code = 'ASK000'
    }

    let query = `
      insert into oi_user_ask (
        answer_email, ask_type_cd, ask_status_cd, content, ask_user_id, ask_dt
      ) values (
        '${email}', '${code}', 'ASK', '${content}', ${userId}, now()
      );
    `
    let result = await mysql.execute(query)
    let askId = result.insertId

    if (images.length > 0) {

      let values = images.map(imageKey => {
        
        let finalKey = imageKey
        if (!imageKey.includes('/') && imageKey.includes('.')) {
          let fileNames = imageKey.split('.')
          let fileName = fileNames[0]
          let fileExtension = fileNames[1]
          finalKey = `ask/${fileName}.${fileExtension}`
        }
        
        return `( 'IMAGE', '${finalKey}' )`
      })
      let valuesString = values.join(', ')

      query = `
        insert into oi_file_info (
          media_type_cd, org_file_nm
        ) values 
        ${valuesString};
      `

      result = await mysql.execute(query)
      let fileId = result.insertId
      
      values = []

      for (let i = 0; i < images.length; i++) {
        values.push(` ( ${askId}, ${i+1}, ${fileId+i}, ${userId})`)
      }
      valuesString = values.join(', ')

      query = `
        insert into oi_user_ask_media_info (
          ask_id, media_seq, file_id, reg_user_id
        ) values
        ${valuesString};
      `
      result = await mysql.execute(query)
    }

    mysql.end()

    return response.result(200, {}, '문의가 접수되었습니다.')
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
