'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const mapper = require('mybatis-mapper')
const response = require('@ohing/ohingresponse')

/*
  * 리뷰 팝업 사용 여부 저장
  * @headers {String} token required
  * @param {String} reviewYn optional
  * @returns {statusCode}
*/

module.exports.handler = async event => {
    console.log(event)
    try {
        const loginAuthNo = event.headers.token
        const userInfo = await authorizer.userInfo(loginAuthNo)
        const userId = userInfo.userId

        const parameters = JSON.parse(event.body || '{}')
        let reviewYn = parameters.reviewYn

        mapper.createMapper(['./mapper.xml'])

        let query = mapper.getStatement('Setting', 'getReviewCnt', {
            userId : userId
        })
        const result = await mysql.execute(query)

        let reviewCnt = result[0]
        if(reviewCnt.app_review_cnt == 3) {
            reviewYn = "Y"
        }
        query = mapper.getStatement('Setting', 'updateReviewStatus', {
            reviewYn : reviewYn,
            userId : userId
        })
        await mysql.execute(query)

        mysql.end()

        return response.result(200, null, '리뷰 여부가 정상적으로 저장되었습니다.')
        
    } catch (e) {
    
        console.error(e)
        mysql.end()

        if(e.statusCode != null) {
            return e
        } else {
            return response.result(403, e, '리뷰 여부 저장이 실패 했습니다.')
        }
    }
}