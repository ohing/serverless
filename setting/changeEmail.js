'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 로그인 상태에서의 이메일 변경
 * @method PUT changeEmail
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let email = parameters.email

    if (email == null) {
      return response.result(400)
    }

    const isValid = /\S+@\S+\.\S+/.test(email)
    if (isValid == false) {
      return response.result(403, null, '유효하지 않은 이메일입니다.')
    }

    let query = `
      update
        oi_user_info 
      set
        user_email = '${email}'
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, null, '이메일 변경이 완료되었습니다.')
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
