'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 알림 유저 그룹 목록 
 * @method GET getNotificationUserGroup
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let notificationId = event.pathParameters.notificationId

    let query
    let result

    query = `
      select 
        notification_type_cd type, body, group_target_ids target_ids, reg_dt
      from
        oi_notification_info
      where
        notification_id = ${notificationId};
    `
    result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(404, null, '알림 정보가 없습니다.')
    }

    let notification = result[0]

    let targetIds = notification.target_ids

    if (targetIds == null) {
      mysql.end()
      return response.result(200, [])
    }

    let keyword

    if (notification.type == 'BDGROUP') {
      let date = new Date(notification.reg_dt)
      let month = date.getMonth() + 1
      let day = date.getDate()
      keyword = `${month}월 ${day}일`
    } else {
      let body = notification.body
      keyword = body.match(/\(.*\)/gi)[0];
      keyword = keyword.substring(1, keyword.length-2)
    }

    let parameters = event.queryStringParameters || {}
    let lastAccountId = (parameters.lastAccountId || '').trim()
    let rowCount = parseInt(parameters.rowCount || '0') || 30

    query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        case 
          when ur.accpt_dt is not null then 1
          else null
        end following,
        case 
          when ur.user_id is not null and ur.accpt_dt is null then 1
          else null
        end requested,
        ur4.user_id jjim
      from
        oi_user_info ui        
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_user_relation ur on ur.user_id = ui.user_id and ur.relation_user_id = ${userId} and ur.relation_type_cd = '01'
        left join oi_user_relation ur4 on ur4.user_id = ui.user_id and ur4.relation_user_id = ${userId} and ur4.relation_type_cd = '05'
      where
        ui.user_id in (${targetIds.join(',')})
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and ui.user_login_id > '${lastAccountId}'
      order by
        ui.user_login_id asc
      limit ${rowCount};
    `
    result = await mysql.execute(query)

    let userIdMap = {}
    let userIds = []
    let users = result.map(row => {
      row.following = row.following != null
      row.requested = row.requested != null
      row.jjim = row.jjim != null
      row.tags = []
      userIds.push(row.user_id)
      userIdMap[row.user_id] = row
      return row
    })

    query = `
      select 
        ut.user_id, ti.tag_val tag
      from 
        oi_user_tag ut
        join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ut.user_id in (${userIds.join(',')})
      order by 
        ut.tag_seq asc;
    `
    result = await mysql.execute(query)

    for (let row of result) {
      userIdMap[row.user_id].tags.push(row.tag)
    }

    mysql.end()

    let readable = await redis.readable(3)
    let onlineKeys = userIds.map(userId => {
      return `alive:${userId}`
    })
    let onlines = await readable.mget(onlineKeys)
    redis.end()
    console.log(onlines)

    for (let i=0; i < userIds.length; i++) {
      let userId = userIds[i]
      userIdMap[userId].is_online = onlines[i] != null
    }

    users = Object.values(users).sort( (lhs, rhs) => {
      return lhs.account_id < rhs.account_id ? -1 : lhs.account_id > rhs.account_id ? 1 : 0
    })
    
    return response.result(200, {
      keyword: keyword,
      users: users
    })
  
  } catch(e) {

    console.error(e)
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
