'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 로그인 상태에서의 휴대전화번호 변경
 * @method POST changePhoneNumber
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let phoneNumber = parameters.phoneNumber
    const authNumber = parameters.authNumber

    if (phoneNumber == null || authNumber == null) {
      return response.result(400)
    }

    phoneNumber = phoneNumber.trim()

    const isValid = (phoneNumber.length == 10 || phoneNumber.length == 11) && phoneNumber.startsWith('01') && /^[0-9]+$/.test(phoneNumber)
    if (isValid == false) {
      return response.result(403, null, '유효하지 않은 휴대전화번호입니다.')
    }

    let query = `
      select
        auth_no
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    let result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404)
    }

    if (result[0].auth_no != authNumber) {
      mysql.end()
      return response.result(403, null, '인증번호가 유효하지 않습니다.')
    }

    query = `
      update
        oi_user_info
      set
        user_hp_no = '${phoneNumber}'
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, null, '휴대전화번호 변경이 완료되었습니다.')
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
