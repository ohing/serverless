'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 알림설정
 * @method PUT getNotificationSettings
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query
    let result

    let parameters = JSON.parse(event.body || '{}') || {}
    const snsYn = parameters.snsYn
    const sns1Yn = parameters.sns1Yn
    const sns2Yn = parameters.sns2Yn
    const sns3Yn = parameters.sns3Yn
    const sns4Yn = parameters.sns4Yn
    const sns5Yn = parameters.sns5Yn
    const sns6Yn = parameters.sns6Yn
    const sns7Yn = parameters.sns7Yn
    const msgYn = parameters.msgYn
    const msg1Yn = parameters.msg1Yn
    const msg2Yn = parameters.msg2Yn
    const msg3Yn = parameters.msg3Yn
    const eventYn = parameters.eventYn
    const sound = parameters.sound
    const soundYn = parameters.soundYn
    const vibrateYn = parameters.vibrateYn

    let sets = []

    if (snsYn == "Y" || snsYn == "N") {
      sets.push(`sns_yn = '${snsYn}'`)
    }
    if (sns1Yn == "Y" || sns1Yn == "N") {
      sets.push(`sns1_yn = '${sns1Yn}'`)
    }
    if (sns2Yn == "Y" || sns2Yn == "N") {
      sets.push(`sns2_yn = '${sns2Yn}'`)
    }
    if (sns3Yn == "Y" || sns3Yn == "N") {
      sets.push(`sns3_yn = '${sns3Yn}'`)
    }
    if (sns4Yn == "Y" || sns4Yn == "N") {
      sets.push(`sns4_yn = '${sns4Yn}'`)
    }
    if (sns5Yn == "Y" || sns5Yn == "N") {
      sets.push(`sns5_yn = '${sns5Yn}'`)
    }
    if (sns6Yn == "Y" || sns6Yn == "N") {
      sets.push(`sns6_yn = '${sns6Yn}'`)
    }
    if (sns7Yn == "Y" || sns7Yn == "N") {
      sets.push(`sns7_yn = '${sns7Yn}'`)
    }
    if (msgYn == "Y" || msgYn == "N") {
      sets.push(`msg_yn = '${msgYn}'`)
    }
    if (msg1Yn == "Y" || msg1Yn == "N") {
      sets.push(`msg1_yn = '${msg1Yn}'`)
    }
    if (msg2Yn == "Y" || msg2Yn == "N") {
      sets.push(`msg2_yn = '${msg2Yn}'`)
    }
    if (msg3Yn == "Y" || msg3Yn == "N") {
      sets.push(`msg3_yn = '${msg3Yn}'`)
    }
    if (eventYn == "Y" || eventYn == "N") {
      sets.push(`event_yn = '${eventYn}'`)

      query = `
        insert into oi_terms_agree_log (
          user_id, terms_id, terms_ver, terms_check_dt, agree_yn
        ) values (
          ${userId}, 
          (select terms_id from oi_terms where terms_type_cd = 'MARKETING' order by terms_ver desc limit 1),
          (select terms_ver from oi_terms where terms_type_cd = 'MARKETING' order by terms_ver desc limit 1),
          now(), '${eventYn}'
        ) on duplicate key update
          terms_check_dt = now(), agree_yn = '${eventYn}';
      `
      await mysql.execute(query)
    }
    if (sound != null && sound.trim().length > 0) {
      sets.push(`sound = '${sound.trim()}'`)
    }
    if (soundYn == "Y" || soundYn == "N") {
      sets.push(`sound_yn = '${soundYn}'`)
    }
    if (vibrateYn == "Y" || vibrateYn == "N") {
      sets.push(`vibrate_yn = '${vibrateYn}'`)
    }

    if (sets.length == 0) {
      return response.result(200, null, '알림 설정이 변경되었습니다.')
    }

    let set = sets.join(', ')

    query = `
      update
        oi_notification_setting
      set
        ${set}
      where
        user_id = ${userId};
    `

    await mysql.execute(query)

    mysql.end()

    return response.result(200, null, '알림 설정이 변경되었습니다.')
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
