'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const fs = require('fs')

/**
 * @author Karl <karl@ohing.net> 
 * @description 공지사항 목록
 * @method GET getNotices
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }
    
    // const parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }
    
    const lastId = parameters.lastId == null ? 2147483647 : parseInt(parameters.lastId)
    const rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)

    let baseUrl = process.env.stage == 'dev' ? 'https://d2ro0vwgwjzuou.cloudfront.net/icon/notice/' : 'https://d257x8iyw0ju12.cloudfront.net/icon/notice/'

    let query = `
      select
        notice_id,
        category,
        case 
          when category = '이벤트' then '${baseUrl}event.png'
          when category = '업데이트' then '${baseUrl}update.png'
          else '${baseUrl}notice.png'
        end icon_image_url,
        fix_yn top_fixed,
        title, content_url, reg_dt updated_at
      from
        oi_notice_info
      where 
        onoff_yn = 'Y'
        and notice_id < ${lastId}
      order by 
        fix_yn desc, notice_id desc
      limit 
        ${rowCount};
    `
    let result = await mysql.execute(query)
    mysql.end()

    let notices = result.map(row => {
      row.top_fixed = row.top_fixed == 'Y'
    })

    return response.result(200, result)
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
