'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const sms = require(`@ohing/ohingsms-${process.env.stage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 로그인 상태에서의 휴대전화번호 변경을 위한 인증번호 발송
 * @method POST sendPhoneAuthCode
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let phoneNumber = parameters.phoneNumber

    if (phoneNumber == null) {
      return response.result(400)
    }

    phoneNumber = phoneNumber.trim()

    const isValid = (phoneNumber.length == 10 || phoneNumber.length == 11) && phoneNumber.startsWith('01') && /^[0-9]+$/.test(phoneNumber)
    if (isValid == false) {
      return response.result(403, null, '유효하지 않은 휴대전화번호입니다.')
    }

    const authNumber = Math.floor(Math.random() * (1000000 - 100000)) + 100000;

    let query = `
      update
        oi_user_info 
      set
        auth_no = '${authNumber}'
      where
        user_id = ${userId};
    `
    await mysql.execute(query)
    mysql.end()

    let message = `[오잉]인증번호 [${authNumber}]를 입력하세요.`
    await sms.send(phoneNumber, message)

    return response.result(200, null, '인증번호가 전송되었습니다.')
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
