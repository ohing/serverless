'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 프로필 공개범위 설정 변경
 * @method POST changeProfileScope
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let scope = parameters.scope

    if (!(scope == 'OPEN' || scope == 'FRIEND' || scope == 'CLOSE')) {
      return response.result(400)
    }

    let query = `
      update
        oi_user_info
      set
        profile_open_type_cd = '${scope}'
      where
        user_id = ${userId};
    `
    let result = await mysql.execute(query)
    mysql.end()

    if (result.length == 0) {
      return response.result(404)
    }

    return response.result(200, result[0])
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
