'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 로그인 상태에서의 비밀번호 변경
 * @method PUT changePassword
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let password = parameters.password
    let rePassword = parameters.rePassword

    if (password == null || rePassword == null) {
      return response.result(400)
    }

    password = password.trim()
    rePassword = password.trim()

    if (password.length < 4 || rePassword.length < 4) {
      return response.result(400)
    }

    if (password != rePassword) {
      return response.result(403)
    }

    let passwordHash = authorizer.createOldPasswordHash(password)

    let query = `
      update
        oi_user_info 
      set
        user_pwd = '${passwordHash}'
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, null, '비밀번호 변경이 완료되었습니다.')
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
