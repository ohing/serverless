module.tagTest = async () => {
  const request = {

  }
  return await require('./tagTest.js').handler(request)
}

module.changePassword = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "password": "",
      "rePassword": ""
    })
  }
  return await require('./changePassword.js').handler(request)
}

module.changeEmail = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "email": "ch082323"
    })
  }
  return await require('./changeEmail.js').handler(request)
}

module.sendPhoneAuthCode = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "phoneNumber": "01029107431"
    })
  }
  return await require('./sendPhoneAuthCode.js').handler(request)
}

module.changePhoneNumber = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "phoneNumber": "01029107431",
      "authNumber": "122733"
    })
  }
  return await require('./changePhoneNumber.js').handler(request)
}

module.changeProfileScope = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "scope": "OPEN"
    })
  }
  return await require('./changeProfileScope.js').handler(request)
}

module.getNotificationSettings = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'f7e692ea-8405-4439-b984-8a9f6c85c906'
    }
  }
  return await require('./getNotificationSettings.js').handler(request)
}

module.notificationSettings = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'f7e692ea-8405-4439-b984-8a9f6c85c906'
    },
    body: JSON.stringify({
      eventYn: 'N'
    })
  }
  return await require('./notificationSettings.js').handler(request)
}

module.getNotifications = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "rowCount": "30",
      "lastId": 2147483647
    })
  }
  return await require('./getNotifications.js').handler(request)
}

module.getNotificationUserGroup = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'a91f50b9-1cba-47ee-9f96-990ef38af5a3'
    },
    pathParameters: {
      notificationId: 674
    },
    queryStringParameters: {
      
    }
  }
  return await require('./getNotificationUserGroup.js').handler(request)
}



module.getNotices = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "rowCount": "30",
      "lastId": 2147483647
    })
  }
  return await require('./getNotices.js').handler(request)
}

module.getFAQs = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "code": "1"
    })
  }
  return await require('./getFAQs.js').handler(request)
}

module.sendAsk = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '10dc608b-15b5-4277-9cad-ac2200303cb3'
    },
    body: JSON.stringify({
      "email": "karl@ohing.net",
      "content": "TTTTTZZZ", 
      // "images": [
      //   'img/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836.jpg',
      //   'img/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836.jpg',
      //   'img/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836.jpg'
      // ]
    })
  }
  return await require('./sendAsk.js').handler(request)
}

module.sendReport = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      "targetType": "USER",
      "targetId": 14,
      "code": "1",
      "content": "TTTTT", 
      "images": [
        'img/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836.jpg',
        'img/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836.jpg',
        'img/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836/04085BA1-8BA5-4EFA-B766-F48A784EC5FF_1585885858836.jpg'
      ]
    })
  }
  return await require('./sendReport.js').handler(request)
}



test = async () => {
  
  const environment = require('../environment.js')
  environment.setEnvironment()

  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()