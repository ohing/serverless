'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 알림설정 가져오기
 * @method GET getNotificationSettings
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query = `
      select
        sns_yn, sns1_yn, sns2_yn, sns3_yn, sns4_yn, sns5_yn, sns6_yn, sns7_yn,
        msg_yn, msg1_yn, msg2_yn, msg3_yn, event_yn, sound, sound_yn, vibrate_yn
      from
        oi_notification_setting
      where
        user_id = ${userId};
    `
    let result = await mysql.execute(query)
    if (result.length == 0) {
      query = `
        insert into oi_notification_setting (
          user_id
        ) values (
          ${userId}
        );
      `
      await mysql.execute(query)
      
      query = `
        select
          sns_yn, sns1_yn, sns2_yn, sns3_yn, sns4_yn, sns5_yn, sns6_yn, sns7_yn,
          msg_yn, msg1_yn, msg2_yn, msg3_yn, event_yn, sound, sound_yn, vibrate_yn
        from
          oi_notification_setting
        where
          user_id = ${userId};
      `
      result = await mysql.execute(query)
    }

    let settings = result[0]

    query = `
      select 
        tal.terms_check_dt
      from 
        oi_terms_agree_log tal
        join oi_terms t on tal.terms_id = t.terms_id
      where
        tal.user_id = ${userId}
        and t.terms_type_cd = 'MARKETING'
      order by 
        t.terms_ver desc
      limit 1;
    `

    result = await mysql.execute(query)

    if (result.length > 0) {
      settings.event_agree_at = result[0].terms_check_dt
    }

    mysql.end()

    return response.result(200, settings)
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
