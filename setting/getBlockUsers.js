'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const mapper = require('mybatis-mapper')
const response = require('@ohing/ohingresponse')

/*
  * 차단 유저 리스트
  * @headers {String} token required
  * @param {Number} rowCount optional
  * @param {Number} startRecord optional
  * @returns {[getBlockUsers]}
*/

module.exports.handler = async event => {
  console.log(event)
    try {
      const loginAuthNo = event.headers.token
      const userInfo = await authorizer.userInfo(loginAuthNo)
      const userId = userInfo.userId

      const parameters = event.queryStringParameters || {}

      let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }
    
      const rowCount = parameters.rowCount || 30
      const startRecord = parameters.startRecord || 0

      mapper.createMapper(['./mapper.xml'])

      const query = mapper.getStatement('Setting', 'getBlockUsers', {
        userId : userId,
        rowCount : rowCount,
        startRecord : startRecord
      })

      const result = await mysql.execute(query)
      mysql.end()

      return response.result(200, result, '차단 상대 목록 조회가 성공했습니다.')
      
    } catch (e) {

      mysql.end()
      console.log(e)

      if(e.statusCode != null) {
        return e
      } else {
        return response.result(403, e, '차단 상대 목록 조회가 실패했습니다.')
      }
    }
};
