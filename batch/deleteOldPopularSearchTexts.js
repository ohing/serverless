'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 인기검색어 삭제(1시간 주기)
 * @method DELETE deleteOldPopularSearchTexts
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let query = `
      delete from 
        oi_search_history
      where
        search_dt < date_add(now(), interval -24 hour);
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
