'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅방 초대
 * @method POST sendChatRoomInvitePush
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    let parameters = JSON.parse(event.body || '{}') || {};
    let roomId = parseInt(parameters.roomId);
    let targetUserIds = parameters.targetUserIds.map(targetUserId => {
      return parseInt(targetUserId);
    });

    if (roomId == null || targetUserIds == null || targetUserIds.length == 0) {
      return response.result(404);
    }

    let query;
    let result;

    query = `
      select 
        cri.title,
        fi.org_file_nm cover_image_url, substring_index(fi.org_file_nm, '/', -1) cover_file_name
      from 
        oi_chat_room_info cri
        left join oi_file_info fi on cri.room_image_id = fi.file_id
      where 
        cri.room_id = ${roomId}
    `;
    result = await mysql.execute(query);

    if (result.length == 0) {
      mysql.end();
      return response.result(404);
    }

    let roomTitle = result[0].title;
    let coverImageUrl = result[0].cover_image_url;
    let coverFileName = result[0].cover_file_name;

    query = `
      select 
        udi.user_id, udi.push_auth_key, ns.msg_yn, ns.msg1_yn, ns.sound 
      from 
        oi_user_info ui 
        join oi_user_device_info udi on ui.user_id = udi.user_id and login_yn = 'Y' and udi.push_auth_key is not null and udi.push_auth_key  <> ''
        join oi_notification_setting ns on ui.user_id = ns.user_id
      where 
        ui.user_id in (${targetUserIds.join(',')})
        and ui.user_status_cd = 'ACTIVE'
    `;

    result = await mysql.execute(query);

    if (result.length == 0) {
      mysql.end();
      return response.result(404);
    }

    let values = [];

    for (let targetUser of result) {

      let title = '오잉챗';
      let body = `💌 '{roomTitle}' 오방에서 같이 놀아요.`;
      let linkInfo = [
        {
          key: 'roomTitle',
          value: roomTitle,
          color: '#FF4800',
          targetType: 'CHATROOM',
          targetId: roomId
        }
      ];

      let notificationType = 'INVITECHAT';
      let targetType = 'CHATROOM';
      let fileName = 'notification-chat';
      let storedFileName = coverFileName == null ? `'notification-default.png'` : `'${coverFileName}'`;
      let storedUrl = coverImageUrl == null ? `'img/notification-default/notification-default.png'` : `'${coverImageUrl}'`;

      values.push(`
        ( ${targetUser.user_id}, '${notificationType}', '${targetType}', ${roomId}, ${storedFileName}, '${title}', '${body}', '${JSON.stringify(linkInfo)}' )
      `);

      if (targetUser.msg_yn == 'Y' && targetUser.msg1_yn == 'Y' && (targetUser.push_auth_key || '').trim().length > 0) {

        let token = (targetUser.push_auth_key || '').trim();
        let sound = targetUser.sound;

        let notification = {
          title: title,
          body: `💌 '${roomTitle}' 오방에서 같이 놀아요.`,
          sound: `${sound}.wav`,
        };
        let data = {
          type: 'Push',
          payload: JSON.stringify({
            type: 'INVITECHAT',
            title: title,
            body: `💌 '${roomTitle}' 오방에서 같이 놀아요.`,
            targetType: 'CHATROOM',
            targetId: roomId
          })
        };

        console.log('will send push to', `${targetUser.user_id}(${token}), ${JSON.stringify(data)}`);

        await fcm.sendToTokens([token], notification, data);
      }
    }

    query = `
      insert into oi_notification_info (
        user_id, notification_type_cd, target_type_cd, target_id, profile_file_name, title, body, link_info
      ) values 
        ${values.join(',')};
    `;
    await mysql.execute(query);

    mysql.end();

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
