'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const iot = require(`@ohing/ohingiot`)
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`)

const camelcase = require('camelcase-keys')

/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅 부하 테스트
 * @method POST chatStressTest
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  let parameters = JSON.parse(event.body || '{}') || {}
  
  try {

    let roomId = parameters.roomId
    let userId = parameters.userId

    if (roomId == null || userId == null) {
      return module.result(200, {})
    }
    let interval = parameters.interval || 1000

    await iot.connect()

    let i = 1

    let timerId = setInterval(async () => {
    // let timerId = setTimeout(async () => {

      let timestamp = new Date().getTime()
      
      let payload = {
        roomId: roomId,
        type: 'CHAT',    
        text: `펑💥`+i,
        timestamp: timestamp,
        userId: userId,
      }

      i++

      let topic = `${process.env.stage}/message`

      console.log(topic, payload)

      await iot.publish(topic, JSON.stringify(payload))

    }, interval)

    return module.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()
    await iot.disconnect()

    if (e.statusCode != null) {
      return e
    } else {
      return module.result(403, e)
    }
  }
}

module.result = (statusCode, body = null, message = null) => {

  var response = {
      statusCode: statusCode,
      headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'os-type, access-token, imei',
          'Access-Control-Allow-Credentials': true,
          'Content-Type': 'application/json; charset=UTF-8'
      }
  }

  if (body != null) {
      response.body = JSON.stringify(camelcase(body, {deep:true}))
  }
  
  if (statusCode == 400 && message == null) {
      message = '필수 파라미터 정보가 없습니다.'
  } else if (statusCode == 401 && message == null) {
      message = '로그인 정보를 찾을 수 없습니다.'
  } else if (statusCode == 404 && message == null) {
      message = '정보를 찾을 수 없습니다.'
  }

  if (message != null) {
      response.headers.message = Buffer.from(message).toString('base64')
  }

  return response;
}