'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 알림 linkInfo 수정
 * @method POST notificationLinkInfoFix
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let query
    let result

    let lastId = 0

    for (;;) {

      query = `
        select 
          notification_id, link_info
        from
          oi_notification_info
        where
          link_info is not null
          and notification_id > ${lastId}
        order by 
          notification_id asc
        limit
          1000;
      `
      result = await mysql.execute(query)

      if (result.length == 0) {
        break
      }

      for (let row of result) {

        var changed = false

        for (let link of row.link_info) {
          if (link.targetId != null && !Number.isInteger(link.targetId)) {
            link.targetId = parseInt(link.targetId)
            changed = true
          }
        }

        if (changed == false) {
          continue
        }

        let linkInfo = JSON.stringify(row.link_info)
        let notificationId = row.notification_id

        query = `
          update
            oi_notification_info
          set
            link_info = ?
          where
            notification_id = ${notificationId}
        `

        await mysql.execute(query, [linkInfo])
      }

      lastId = result[result.length-1].notification_id
    }

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}