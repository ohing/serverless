'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

/**
 * @author Karl <karl@ohing.net> 
 * @description 생일자 푸시 전송
 * @method POST sendBirthdayPush
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    let query;
    let result;

    query = `
      select
        ur.user_id, ur.relation_user_id, ui.user_login_id, udi.push_auth_key, ns.sns_yn, ns.sns4_yn,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name
      from
        oi_user_relation ur
        join oi_user_info ui on ur.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE'
        join oi_user_device_info udi on ur.relation_user_id = udi.user_id
        join oi_notification_setting ns on udi.user_id = ns.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
      where 
        ur.user_id in (
          select 
            user_id
          from 
            oi_user_info
          where
            user_status_cd = 'ACTIVE'
            and profile_open_type_cd != 'CLOSE'
            and birth_md = date_format(date_add(now(), interval 1 day), '%m%d')
        )
        and ur.relation_type_cd = '01'
        and ur.accpt_dt is not null
        and udi.login_yn = 'Y'
        and udi.push_auth_key is not null 
        and udi.push_auth_key <> ''
      order by
        ur.user_id asc;
    `;
    console.log(query);
    result = await mysql.execute(query);
    console.log(result);
    mysql.end();

    if (result.length == 0) {
      return response.result(200, {});
    }

    let map = {};

    result.forEach(row => {
      if (map[row.relation_user_id] == null) {
        map[row.relation_user_id] = [
          row
        ];
      } else {
        map[row.relation_user_id].push(row);
      }
    });

    let userIds = Object.keys(map);

    for (let userId of userIds) {

      let rows = map[userId];

      if (rows.length == 0) {
        continue;
      }

      let targetUserId = rows[0].user_id;
      let loginId = rows[0].user_login_id;
      let profileImageUrl = rows[0].profile_image_url;
      let profileFileName = rows[0].profile_file_name;

      let tokens = [rows[0].push_auth_key];

      let title = '생일';
      let body = rows.length > 1 ? `🎂내일은 {accountId} 님 외 ${rows.length - 1}명이 생일입니다. 축하해주세요🎉` : `🎂내일은 {accountId} 님의 생일입니다. 축하해주세요🎉`;
      let linkInfo = [
        {
          key: 'accountId',
          value: loginId,
          color: '#FF4800',
          targetType: 'USER',
          targetId: targetUserId
        }
      ];

      let notificationType = rows.length > 1 ? 'BDGROUP' : 'BIRTHDAY';
      let targetType = rows.length > 1 ? 'BDGROUP' : 'USER';
      let fileName = 'notification-birthday';
      let storedFileName = rows.length > 1 ? `'${`${fileName}.png`}'` : profileFileName == null ? 'null' : `'${profileFileName}'`;
      let birthdayProfilePath = `img/${fileName}/${fileName}.png`;
      let storedUrl = rows.length > 1 ? `'${birthdayProfilePath}'` : profileImageUrl == null ? 'null' : `'${profileImageUrl}'`;

      let groupTargetIds = 'null';

      if (count > 1) {
        let targetUserIds = rows.map(row => {
          return row.user_id;
        });
        groupTargetIds = `'${JSON.stringify(targetUserIds)}'`;
      }

      query = `
        insert into oi_notification_info (
          user_id, notification_type_cd, target_type_cd, target_id, profile_file_name, title, body, link_info, group_target_ids
        ) values (
          ${userId}, '${notificationType}', '${targetType}', ${rows.length > 1 ? null : targetUserId}, ${storedFileName}, '${title}', '${body}', '${JSON.stringify(linkInfo)}', ${groupTargetIds}
        );
      `;
      result = await mysql.execute(query);
      let notificationId = result.insertId;

      mysql.end();

      if (rows[0].sns_yn == 'Y' && rows[0].sns4_yn == 'Y') {
        let notification = {
          title: title,
          body: rows.length > 1 ? `🎂내일은 ${loginId} 님 외 ${rows.length - 1}명이 생일입니다. 축하해주세요🎉` : `🎂내일은 ${loginId} 님의 생일입니다. 축하해주세요🎉`,
        };

        let payload = {
          notificationId: notificationId,
          notificationType: notificationType,
          title: title,
          body: body,
          targetType: targetType,
          targetId: userId,
          profileImageUrl: rows.length > 1 ? `img/${fileName}/${fileName}.png` : profileImageUrl == null ? null : profileImageUrl,
          profileFileName: rows.length > 1 ? `${fileName}.png` : profileFileName == null ? null : profileFileName,
          linkInfo: linkInfo
        };

        await fcm.sendToTokens(tokens, notification, {
          type: "Push",
          payload: JSON.stringify(payload)
        });
      }
    }

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
