'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const pushreserve = require(`@ohing/ohingpushreserve-${process.env.stage}`);
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅 외의 푸시 전송
 * @method POST sendPush
 * @returns  
*/

const pushSettingKeys = {
  'FOLLOW': 'sns2_yn', 'FOLLOWREQ': 'sns2_yn', 'FOLLOWACPT': 'sns2_yn', 'JJIM': 'sns7_yn',
  'FEEDLIKE': 'sns1_yn', 'IMAGELIKE': 'sns1_yn', 'MOVIELIKE': 'sns1_yn', 'COMTLIKE': 'sns1_yn', 'REPLYLIKE': 'sns1_yn',
  'COMMENT': 'sns1_yn', 'REPLY': 'sns1_yn', 'MENTION': 'sns3_yn',
  'PFIMAGE': 'sns5_yn', 'PROFILE': 'sns5_yn',
  'NOTICE': 'event_yn', 'EVENT': 'event_yn',
};

String.prototype.format = function () {
  var formatted = this;
  for (var arg in arguments) {
    formatted = formatted.replace("{" + arg + "}", arguments[arg]);
  }
  return formatted;
};


module.exports.handler = async event => {

  console.log(event);

  try {

    let parameters = JSON.parse(event.body || '{}') || {};
    let messages = parameters.messages || [];
    let targetUserIds = parameters.targetUserIds;
    let isAnonymous = parameters.isAnonymous;

    let query;
    let result;

    for (let message of messages) {

      let notificationType = message.notificationType;

      let userId = parseInt(message.userId);
      if (Number.isNaN(userId)) {
        userId = null;
      }

      if (notificationType == null || (notificationType != 'Event' && notificationType != 'Notice' && userId == null)) {
        console.log('필수 파라미터 없음', message);
        continue;
      }

      message.feedId = parseInt(message.feedId);
      if (Number.isNaN(message.feedId)) {
        message.feedId = null;
      }

      let pushSettingKey = pushSettingKeys[notificationType];
      if (pushSettingKey == null) {
        console.log('Type 이상', notificationType);
        continue;
      }

      let title;
      let body;

      let bodyNeedsLoginId = false;
      let linkBodyNeedsLoginId = false;

      let targetType = 'null';
      let targetId = null;
      let targetUrl = null;

      let linkBody;
      let linkInfo = null;

      let topic;

      let profileFileName;

      switch (notificationType) {

        case 'FOLLOW': {

          title = '팔로우';
          body = '{0}님이 나의 친구가 되었습니다. 오잉에서 만나요.';

          targetType = `'USER'`;
          targetId = userId;

          linkBody = '{accountId}님이 나의 친구가 되었습니다. 오잉에서 만나요.';

          bodyNeedsLoginId = true;
          linkBodyNeedsLoginId = true;

          break;
        }

        case 'FOLLOWREQ': {

          title = '팔로우';
          body = '{0}님이 친구가 되고 싶어합니다. 오잉에서 만나 볼까요?';

          targetType = `'FOLLOWREQS'`;
          targetId = userId;

          linkBody = '{accountId}님이 친구가 되고 싶어합니다. 오잉에서 만나 볼까요?';

          bodyNeedsLoginId = true;
          linkBodyNeedsLoginId = true;

          break;
        }

        case 'FOLLOWACPT': {

          title = '팔로우 수락';
          body = '{0}님과 친구가 되었습니다. 오잉에서 만나요.';

          targetType = `'USER'`;
          targetId = userId;

          linkBody = '{accountId}님과 친구가 되었습니다. 오잉에서 만나요.';

          bodyNeedsLoginId = true;
          linkBodyNeedsLoginId = true;

          break;
        }

        case 'FOLLOWRJCT': {

          title = '팔로우 거절';
          body = '{0}님은 팔로우를 원하지 않는대요. 다른 친구를 찾아볼까요?';

          targetType = `'USER'`;
          targetId = userId;

          linkBody = '{accountId}님은 팔로우를 원하지 않는대요. 다른 친구를 찾아볼까요?';

          bodyNeedsLoginId = true;
          linkBodyNeedsLoginId = true;

          break;
        }

        case 'JJIM': {

          title = '찜';
          body = '누군가 회원님을 찜🤩 했어요.';
          linkBody = '누군가 회원님을 찜🤩 했어요.';
          profileFileName = 'notification-jjim.png';

          break;
        }

        case 'PFIMAGE': {

          title = '프로필 변경';
          body = '✨샤방~ {0}님이 프로필 사진을 변경했습니다. ※얼평노노';

          targetType = `'USER'`;
          targetId = userId;

          linkBody = '✨샤방~ {accountId}님이 프로필 사진을 변경했습니다. ※얼평노노';

          bodyNeedsLoginId = true;
          linkBodyNeedsLoginId = true;

          break;
        }

        case 'PROFILE': {

          title = '프로필 변경';
          body = '{0}님이 프로필 정보를 변경했습니다.';

          targetType = `'USER'`;
          targetId = userId;

          linkBody = '{accountId}님이 프로필 정보를 변경했습니다.';

          bodyNeedsLoginId = true;
          linkBodyNeedsLoginId = true;

          break;
        }

        case 'FEEDLIKE': {

          title = '좋아요';
          body = '내 게시물이 {0}{1} 취저 ❤';

          targetType = `'FEED'`;
          targetId = message.feedId;

          if (isAnonymous) {
            linkBody = '내 게시물이 누군가의 취저 ❤';
          } else {
            linkBody = '내 게시물이 {accountId}님 취저 ❤';
            linkBodyNeedsLoginId = true;
          }

          bodyNeedsLoginId = true;

          break;
        }

        case 'IMAGELIKE': {

          title = '좋아요';
          body = '내 사진이 {0}{1} 취저 ❤';

          targetType = `'FEED'`;
          targetId = message.feedId;

          if (isAnonymous) {
            linkBody = '내 사진이 누군가의 취저 ❤';
          } else {
            linkBody = '내 사진이 {accountId}님 취저 ❤';
            linkBodyNeedsLoginId = true;
          }

          bodyNeedsLoginId = true;

          break;
        }

        case 'MOVIELIKE': {

          title = '좋아요';
          body = '내 동영상이 {0}{1} 취저 ❤';

          targetType = `'FEED'`;
          targetId = message.feedId;

          if (isAnonymous) {
            linkBody = '내 동영상이 누군가의 취저 ❤';
          } else {
            linkBody = '내 동영상이 {accountId}님 취저 ❤';
            linkBodyNeedsLoginId = true;
          }

          bodyNeedsLoginId = true;

          break;
        }

        case 'COMTLIKE': {

          title = '좋아요';
          body = '내 댓글이 {0}{1} 취저 ❤';

          targetType = `'FEED'`;
          targetId = message.feedId;

          if (isAnonymous) {
            linkBody = '내 댓글이 누군가의 취저 ❤';
          } else {
            linkBody = '내 댓글이 {accountId}님 취저 ❤';
            linkBodyNeedsLoginId = true;
          }

          bodyNeedsLoginId = true;

          break;
        }

        case 'REPLYLIKE': {

          title = '좋아요';
          body = '내 답글이 {0}{1} 취저 ❤';

          targetType = `'FEED'`;
          targetId = message.feedId;

          if (isAnonymous) {
            linkBody = '내 답글이 누군가의 취저 ❤';
          } else {
            linkBody = '내 답글이 {accountId}님 취저 ❤';
            linkBodyNeedsLoginId = true;
          }

          bodyNeedsLoginId = true;

          break;
        }

        case 'COMMENT': {

          title = '댓글';
          body = '{0}{1} 내 게시물에 🗨댓글 투척';

          targetType = `'FEED'`;
          targetId = message.feedId;

          if (isAnonymous) {
            linkBody = '누군가 내 게시물에 🗨댓글 투척';
          } else {
            linkBody = '{accountId}님이 내 게시물에 🗨댓글 투척';
            linkBodyNeedsLoginId = true;
          }

          bodyNeedsLoginId = true;

          break;
        }

        case 'REPLY': {

          title = '답글';
          body = '{0}{1} 내 댓글에 🗨답글 투척';

          targetType = `'FEED'`;
          targetId = message.feedId;

          if (isAnonymous) {
            linkBody = '누군가 내 댓글에 🗨답글 투척';
          } else {
            linkBody = '{accountId}님이 내 댓글에 🗨답글 투척';
            linkBodyNeedsLoginId = true;
          }

          bodyNeedsLoginId = true;

          break;
        }

        case 'MENTION': {

          title = '내 얘기';
          body = '{0}{1} 나를 언급했어요. 머선129?';

          targetType = `'FEED'`;
          targetId = message.feedId;

          if (isAnonymous) {
            linkBody = '누군가 나를 언급했어요. 머선129?';
          } else {
            linkBody = '{accountId}님이 나를 언급했어요. 머선129?';
            linkBodyNeedsLoginId = true;
          }

          bodyNeedsLoginId = true;

          break;
        }

        case 'NOTICE': {

          topic = message.topic;
          title = message.title || '';
          body = message.body;

          targetType = `'NOTICE'`;

          break;
        }

        case 'EVENT': {

          topic = message.topic;
          title = message.title || '';
          body = message.body;

          targetType = message.targetType;
          targetId = message.targetId;
          targetUrl = parameters.targetUrl;

          break;
        }

        default: {
          console.log('type 이상');
          continue;
        }
      }

      if ((targetUserIds == null || targetUserIds.length == 0) && topic == null) {
        console.log('푸시 보낼 대상 없음');
        continue;
      }

      if (targetUserIds != null) {

        query = `
          select 
            ui.user_login_id, substring_index(fi.org_file_nm, '/', -1) profile_file_name
          from
            oi_user_info ui
            left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
            left join oi_file_info fi on upi.file_id = fi.file_id
          where
            ui.user_id = ${userId};
        `;
        result = await mysql.execute(query);

        let accountId = result[0].user_login_id;
        if (profileFileName == null) {
          profileFileName = result[0].profile_file_name;
        }
        let sound = result[0].sound;

        let subposition = '님이';

        if (isAnonymous) {
          accountId = '누군가';

          if (notificationType.includes('LIKE')) {
            subposition = '의';
          } else {
            subposition = '';
          }
        }

        let profileImageUrl;

        if (profileFileName == null) {
          profileImageUrl = 'img/notification-default/notification-default.png';
          profileFileName = 'notification-default.png';
        } else {
          if (isAnonymous) {
            profileImageUrl = 'img/notification-anonymous/notification-anonymous.png';
            profileFileName = 'notification-anonymous.png';
          } else {
            profileImageUrl = `img/${profileFileName.split('.')[0]}/${profileFileName}`;
          }
        }

        if (bodyNeedsLoginId) {
          body = body.format(accountId, subposition);
        }

        if (linkBodyNeedsLoginId) {
          linkInfo = [
            {
              key: 'accountId',
              value: accountId,
              color: '#FF4800',
              targetType: 'USER',
              targetId: parseInt(userId)
            }
          ];
        }

        let linkInfoValue = linkInfo == null ? 'null' : `'${JSON.stringify(linkInfo)}'`;

        let values = [];

        for (let targetUserId of targetUserIds) {
          values.push(`
            ( ${targetUserId}, '${notificationType}', ${targetType}, ${targetId}, '${profileFileName}', '${title}', '${linkBody}', ${linkInfoValue} )
          `);
        }

        query = `
          insert into oi_notification_info (
            user_id, notification_type_cd, target_type_cd, target_id, profile_file_name, title, body, link_info
          ) values
          ${values.join(',')}
        `;
        console.log(query);
        result = await mysql.execute(query);

        let notificationId = result.insertId;

        query = `
          select 
            ui.user_id target_user_id, udi.push_auth_key, udi.os_type_cd, udi.login_yn,
            ns.*
          from
            oi_user_device_info udi
            right join oi_user_info ui on udi.user_id = ui.user_id
            left join oi_notification_setting ns on ui.user_id = ns.user_id
          where
            udi.user_id in (${targetUserIds.join(',')})
            and udi.user_id != ${userId}
            and udi.push_auth_key is not null and udi.push_auth_key  <> ''
            ;
        `;
        result = await mysql.execute(query);

        console.log('devices', result);

        let ios = [];
        let android = [];

        let iosTokens = [];
        let androidTokens = [];

        for (let row of result) {
          if ((row.sns_yn == 'Y') && row[pushSettingKey] == 'Y' && row.login_yn == 'Y') {
            if (row.os_type_cd == 'ANDROID') {
              android.push(row);
              androidTokens.push(row.push_auth_key);
            } else if (row.os_type_cd == 'IOS') {
              ios.push(row);
              iosTokens.push(row.push_auth_key);
            }
          }
        }

        if (ios.length == 0 && android.length == 0) {
          console.log('푸시 보낼 대상 정보 없음', targetUserIds);
          continue;
        }

        let notification = {
          title: title,
          body: body,
          // sound: `${sound}.wav`
          // sound: `sound1.wav`
        };

        if (targetType != 'null') {
          targetType = targetType.substring(1, targetType.length - 1);
        } else {
          targetType = null;
        }

        let payload = {
          notificationId: notificationId,
          type: notificationType,
          title: title,
          body: body,
          targetType: targetType,
          targetId: targetId,
          profileImageUrl: profileImageUrl,
          profileFileName: profileFileName,
          linkInfo: linkInfo
        };

        console.log('notification', notification, payload, iosTokens, androidTokens);

        if (iosTokens.length > 0) {
          await fcm.sendToTokens(iosTokens, notification, {
            type: "Push",
            payload: JSON.stringify(payload)
          });
        }
        if (androidTokens.length > 0) {
          await fcm.sendToTokens(androidTokens, null, {
            type: "Push",
            payload: JSON.stringify(payload),
            title: notification.title,
            body: notification.body
          });
        }

      } else if (topic != null) {

        console.log(topic);

        let notification = {
          title: title,
          body: body,
          // sound: `sound1.wav`
        };

        let payload = {
          notificationType: notificationType,
          title: title,
          body: body,
          targetType: targetType,
          targetId: targetId,
          targetUrl: targetUrl
        };

        await fcm.sendToTopic(topic, notification, {
          type: "Push",
          payload: JSON.stringify(payload)
        });
      }
    }

    mysql.end();

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};