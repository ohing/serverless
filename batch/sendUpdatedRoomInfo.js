'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const iot = require(`@ohing/ohingiot`)
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`)

const camelcase = require('camelcase-keys')

/**
 * @author Karl <karl@ohing.net> 
 * @description 변경된 방 정보 전송
 * @method POST sendUpdatedRoomInfo
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  let parameters = JSON.parse(event.body || '{}') || {}
  
  try {

    let roomId = parseInt(parameters.roomId)

    let query
    let result

    query = `
      select 
        cui.user_id,
        cri.room_id, cri.room_type_cd room_type, cri.room_status_cd room_status,
        cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password,
        (select 
          count(*) count
        from
          oi_chat_user_info
        where
          room_id = cri.room_id
          and use_yn = 'Y') join_user_count,
        cri.max_user_cnt max_user_count,
        cui.top_fix_yn top_fixed, cui.master_yn is_master, cui.deport_yn deported, cui.alarm_yn notification,
        fi.org_file_nm cover_image_url, substring_index(fi.org_file_nm, '/', -1) cover_file_name,
        (select count(*) from oi_chat_msg_receive cmr where cmr.room_id = cri.room_id and cmr.receive_user_id = cui.user_id and cmr.receive_yn = 'N') unread_count,
        concat(
          '[',
          (select
            group_concat(concat('"', ti.tag_val, '"') separator ',')
          from 
            oi_chat_room_info cri2
            join oi_chat_room_tag crt on cri2.room_id = crt.room_id
            join oi_tag_info ti on crt.tag_id = ti.tag_id
          where 
            cri2.room_id = cri.room_id
          order by
            crt.tag_seq asc
          ),
          ']'
        ) tags,
        ifnull(mcpi.nick_nm, mui.user_login_id) master_nickname
      from 
        oi_chat_room_info cri 
        left join oi_chat_user_info mcui on cri.room_id = mcui.room_id and mcui.master_yn = 'Y'
        left join oi_chat_profile_info mcpi on mcui.chat_profile_id = mcpi.chat_profile_id
        left join oi_user_info mui on mcui.user_id = mui.user_id
        left join oi_file_info fi on cri.room_image_id = fi.file_id
        left join oi_chat_user_info cui on cri.room_id = cui.room_id
          
      where
        cri.room_id = ${roomId}
        and cui.use_yn = 'Y';
    `

    result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return module.result(200, {})
    }

    let roomType = result[0].room_type

    let userIdMap = {}
    let lastMessageQueries = []

    for (let row of result) {
      
      lastMessageQueries.push(`
        (select
          cui2.user_id,
          case 
            when cm.msg_type_cd = 'CHGMASTER' then cm.msg_content
            when cm.msg_type_cd = 'NOTICE' then cm.msg_content
            when cm.msg_type_cd = 'DEPORT' then cm.msg_content
            when cm.msg_type_cd = 'GROUPINVITE' then cm.msg_content
            when cm.msg_type_cd = 'ROOMINMSG' then cm.msg_content
            when cm.msg_type_cd = 'ROOMOUT' then cm.msg_content
            when cm.msg_type_cd = 'BLOWUP' then cm.msg_content
            when cm.msg_type_cd = 'CHGTITLE' then cm.msg_content
            when cm.msg_type_cd = 'CHGPW' then cm.msg_content
            else concat(
            case
              when cm.target_user_id is null then ''
              else '(귓속말)'
            end,
            cm.send_user_nick_nm, 
            case 
              when cm.msg_type_cd = 'OPENINVITE' then ': (오방 초대)'
              when cm.media_info is not null then ': (미디어)'
              else concat(': ', cm.msg_content)
            end
            )
          end last_message,
          cm.send_dt_int send_time
        from
          oi_chat_msg cm
          left join oi_chat_user_info cui2 on cui2.room_id = cm.room_id
          left join oi_chat_msg_delete cmd on cm.room_id = cmd.room_id and cm.msg_id = cmd.msg_id and cmd.delete_user_id = cui2.user_id
        where
          cm.room_id = ${row.room_id}
          and cui2.user_id = ${row.user_id}
          and cmd.room_id is null
          and (
            cm.target_user_id is null
            or cm.target_user_id = cui2.user_id
            or cm.send_user_id = cui2.user_id
          )
        order by 
          cm.send_dt_int desc
        limit 1)
      `)

      row.top_fixed = row.top_fixed == 'Y'
      row.is_master = row.is_master == 'Y'
      row.needs_password = row.needs_password == 1
      row.deported = row.deported == 'Y'
      row.notification = row.notification == 'Y'
      row.member_count = row.join_user_count
      row.tags = row.tags == null ? [] : JSON.parse(row.tags)
      row.target_user_profiles = []

      userIdMap[row.user_id] = row
    }

    let userIds = Object.keys(userIdMap)

    query = lastMessageQueries.join(`
      union all
    `)

    result = await mysql.execute(query)
    
    for (let row of result) {
      userIdMap[row.user_id].last_message = row.last_message
      userIdMap[row.user_id].send_time = row.send_time
    }

    let profiles = []

    if (roomType != 'OPEN') {
      query = `
        select 
          cri.room_id,
          cui.user_id,
          if (cui.chat_profile_id is null, ui.user_login_id, cpi.nick_nm) nickname,
          if (cui.chat_profile_id is null, fi.org_file_nm, fi2.org_file_nm) profile_image_url, 
          if (cui.chat_profile_id is null, 
            substring_index(fi.org_file_nm, '/', -1), substring_index(fi2.org_file_nm, '/', -1)
          ) profile_file_name
        from 
          oi_chat_room_info cri
          left join oi_chat_user_info cui on cri.room_id = cui.room_id
          left join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where
          cri.room_id = ${roomId}
        order by
          cui.join_dt asc
        limit 
          5;
      `
      profiles = await mysql.execute(query)
    }

    mysql.end()

    if (profiles.length > 0) {

      let readable = await redis.readable(3)
      let onlineKeys = profiles.map(row => {
        return `alive:${row.user_id}`
      })

      let onlines = await readable.mget(onlineKeys)

      redis.end()

      for (let i = 0; i < profiles.length; i++) {
        let isOnline = onlines[i]
        isOnline = isOnline == "true" || isOnline == true
        let userId = userIds[i]
        
        profiles[i].is_online = isOnline
      }

      for (let userId of userIds) {
        userIdMap[userId].target_user_profiles = profiles.filter(profile => profile.user_id != userId).splice(0, 4)
      }

      console.log(userIdMap)
    }

    await iot.connect()

    for (let userId of userIds) {

      let roomInfo = userIdMap[userId]
      
      let camelcased = camelcase(roomInfo, {deep: true})

      let topic = `${process.env.stage}/user/${userId}`

      await iot.publish(topic, JSON.stringify({
        type: 'updateMyRoomInfo',
        roomInfo: camelcased
      }))  
    }

    await iot.disconnect()

    return module.result(200, userIdMap)
  
  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()
    await iot.disconnect()

    if (e.statusCode != null) {
      return e
    } else {
      return module.result(403, e)
    }
  }
}

module.result = (statusCode, body = null, message = null) => {

  var response = {
      statusCode: statusCode,
      headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'os-type, access-token, imei',
          'Access-Control-Allow-Credentials': true,
          'Content-Type': 'application/json; charset=UTF-8'
      }
  }

  if (body != null) {
      response.body = JSON.stringify(camelcase(body, {deep:true}))
  }
  
  if (statusCode == 400 && message == null) {
      message = '필수 파라미터 정보가 없습니다.'
  } else if (statusCode == 401 && message == null) {
      message = '로그인 정보를 찾을 수 없습니다.'
  } else if (statusCode == 404 && message == null) {
      message = '정보를 찾을 수 없습니다.'
  }

  if (message != null) {
      response.headers.message = Buffer.from(message).toString('base64')
  }

  return response;
}