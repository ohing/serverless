'use strict';

const pushreserve = require(`@ohing/ohingpushreserve-${process.env.stage}`)
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 예약푸시 보내기
 * @method POST sendReservedPush
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    for (;;) {

      let nexts = await pushreserve.getNext()

      console.log('nexts', nexts)

      if (nexts.length == 0) {
        break
      }

      let payloads = nexts.map(next => {
        return next.payload
      })

      let tokens = nexts.map(next => {
        return next.fcm_token
      })

      await fcm.sendPayloadsToTokens(payloads, tokens)

      await pushreserve.deleteRows(nexts)
    }

    pushreserve.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
