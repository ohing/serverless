'use strict';

const mysql2 = require(`mysql2/promise`)
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});

/**
 * @author Karl <karl@ohing.net> 
 * @description 과거앱 배너 갱신 
 * @method POST refreshOldBanners
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  let parameters = JSON.parse(event.body || '{}') || {}
  
  try {

    var mysql = await mysql2.createConnection({
      host: 'ohing-aurora-cluster-1.cluster-cztt5idwty9x.ap-northeast-2.rds.amazonaws.com',
      user: 'ohing_master',
      password: 'ohing123',
      database: 'mvpdbopr'
    })

    let query
    let result

    query = `
      select
        m.*, @rownum:=@rownum+1 as rownum
      from (
        select
          seq_no, banner_title, landing_target, ifnull(banner_type_cd, '') as banner_type_cd,
          ifnull(image_url, '') as image_url, ifnull(ext_url, '') as ext_url, ifnull(target_id, '') as target_id,
          service_type_cd
        from
          oi_banner_info
        where
          open_yn = 'Y'
          and banner_start_ymd <= date_format(now(), '%Y%m%d')
          and banner_end_ymd >= date_format(now(), '%Y%m%d')
          and del_yn = 'N'
          and service_type_cd = 'HOME'
        order by 
          banner_sort_no asc, seq_no asc
      ) m,
        (select @rownum:=0) r
      order by 
        rownum
    `
    result = await mysql.execute(query)

    mysql.end()
    

    return response.result(200, result)
  
  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return module.result(403, e)
    }
  }
}