module.makeRanking = async () => {
  return await require('./makeRanking.js').handler()
}

module.makeStats = async () => {
  return await require('./makeStats.js').handler()
}

module.sendChatMessage = async () => {
  let request = {
    // body: JSON.stringify({
    //   "type":"SendMessage",
    //   "roomId":1907,
    //   "chatMessage": {
    //     "messageId": 77318,
    //     "type": "CHAT",
    //     "roomId": 1907,
    //     "userId": 300,
    //     "chatProfileId": null,
    //     "nickname": "신용",
    //     "profileFileName": "0BB09953-5F9C-417C-A593-CA2DBC94C65A_1599023381424.jpg",
    //     "timestamp": 1635814043000,
    //     "text": "Test",
    //     "mediaInfo": null,
    //     "targetUserId": null,
    //     "targetUserNickname": null,
    //     "inviteRoomInfo": null,
    //     "unreadCount": 1
    //   }
    // })
    body: JSON.stringify({
      type: 'SendMessage',
      roomId: 5050,
      chatMessage: {
        roomId: 5050,
        type: 'CHAT',    
        text: `펑💥`,
        timestamp: new Date().getTime(),
        userId: 300,
      }
    })
  }
  return await require('./sendChatMessage.js').handler(request)
}

module.sendChatMessage2 = async () => {
  let request = {
    body: JSON.stringify({
      "messages": [
        {
          "type":"SendMessage",
          "roomId":"1581",
          "chatMessage":{"messageId":73974,"msgTypeCd":"CHAT","roomId":1581,"userId":20,"nickname":"chichewp11","profileFileName":"IAA_20191004055325252.jpg","chatProfileId":1,"timestamp":1631768466314,"text":"uuuu","mediaInfo":null,"targetUserId":null,"targetUserNickname":null,"inviteRoomInfo":null,"unreadCount":2}
        }
      ]
    })
  }
  return await require('./sendChatMessage.js').handler(request)
}

module.sendPush = async () => {
  let request = {
    body: JSON.stringify({
      targetUserIds: [300],
      // isAnonymous: true,
      messages: [
        {
          userId: 303,
          notificationType: 'FOLLOW',
          // notificationType: 'FOLLOWREQ',
          // notificationType: 'FOLLOWACPT',
          // notificationType: 'JJIM',
          // notificationType: 'PFIMAGE',
          // notificationType: 'PROFILE',

          // notificationType: 'FEEDLIKE',
          // notificationType: 'IMAGELIKE',
          // notificationType: 'MOVIELIKE',
          // notificationType: 'COMTLIKE',
          // notificationType: 'REPLYLIKE',
          // notificationType: 'COMMENT',
          // notificationType: 'REPLY',
          // notificationType: 'MENTION',
          // feedId: 578,

          // notificationType: 'EVENT',
          // topic: 'OHINGNOTICEIOS',
          // title: '이것은 이벤트',
          // body: '이벤트가 시작되었습니다.',

          // type: 'Notice',
          // dataType: 'Notice',
          // topic: 'OHINGNOTICEIOS',
          // title: '이것은 공지사항',
          // body: '공지입니다.'
        }
      ]
    })
  }
  return await require('./sendPush.js').handler(request)
}

module.sendBirthdayPush = async () => {
  let request = {
    
  }
  return await require('./sendBirthdayPush.js').handler(request)
}

module.sendSignUpPush = async () => {
  let request = {
    
  }
  return await require('./sendSignUpPush.js').handler(request)
}

module.sendRankerPush = async () => {
  let request = {
    
  }
  return await require('./sendRankerPush.js').handler(request)
}

module.sendChatRoomPush = async () => {
  let request = {
    body: JSON.stringify({
      roomId: 1746
    })
  }
  return await require('./sendChatRoomPush.js').handler(request)
}

module.sendChatRoomInvitePush = async () => {
  let request = {
    body: JSON.stringify({
      roomId: 1746,
      targetUserIds: [300, 529, 530]
    })
  }
  return await require('./sendChatRoomInvitePush.js').handler(request)
}

module.deleteOldPopularSearchTexts = async () => {
  let request = {
    
  }
  return await require('./deleteOldPopularSearchTexts.js').handler(request)
}

module.processReservedPush = async () => {
  let request = {
    body: JSON.stringify({
      pushId: 3
    })
  }
  return await require('./processReservedPush.js').handler(request)
}

module.checkSearchStrategy = async () => {

  let request = {
    body: JSON.stringify({
      searchText: '경기도 여자 중딩'
    })
  }
  return await require('./checkSearchStrategy.js').handler(request)
}

module.sendUpdatedRoomInfo = async () => {

  let request = {
    body: JSON.stringify({
      roomId: 6202,
      userIds: [
        550, 556, 558
      ]
    })
  }
  return await require('./sendUpdatedRoomInfo.js').handler(request)
}

module.sendUpdatedRoomInfo2 = async () => {

  let request = {
    body: JSON.stringify({
      roomId: 5516,
    })
  }
  return await require('./sendUpdatedRoomInfo2.js').handler(request)
}

module.chatStressTest = async () => {

  let request = {
    body: JSON.stringify({
      roomId: 5050,
      userId: 303,
      interval: 250
    })
  }
  return await require('./chatStressTest.js').handler(request)
}

module.chatStressTest2 = async () => {

  let request = {
    body: JSON.stringify({
      roomId: 5050,
      userId: 300,
      interval: 250
    })
  }
  return await require('./chatStressTest.js').handler(request)
}

module.chatStressTest3 = async () => {

  let request = {
    body: JSON.stringify({
      roomId: 5050,
      userId: 183,
      interval: 250
    })
  }
  return await require('./chatStressTest.js').handler(request)
}

module.chatStressTest4 = async () => {

  let request = {
    body: JSON.stringify({
      roomId: 5050,
      userId: 549,
      interval: 250
    })
  }
  return await require('./chatStressTest.js').handler(request)
}

module.chatStressTest5 = async () => {

  let request = {
    body: JSON.stringify({
      roomId: 5050,
      userId: 240,
      interval: 250
    })
  }
  return await require('./chatStressTest.js').handler(request)
}

module.refreshOldBanners = async () => {

  let request = {}
  return await require('./refreshOldBanners.js').handler(request)
}

module.notificationLinkInfoFix = async () => {

  let request = {}
  return await require('./notificationLinkInfoFix.js').handler(request)
}

module.noticeFix = async () => {

  let request = {}
  return await require('./noticeFix.js').handler(request)
}


test = async () => {

  let a
  let b = parseInt(a)

  console.log(a)
  console.log(b)
  console.log(Number.isNaN(a))
  console.log(Number.isNaN(b))

  return

  const environment = require('../environment.js')
  environment.setEnvironment()
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}



test()