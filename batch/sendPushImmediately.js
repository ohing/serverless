'use strict';

const fcm = require(`@ohing/ohingfcm-${process.env.stage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 즉시 푸시 전송. 모두가 다른 메시지
 * @method POST sendPushImmediately
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}

    let messages = parameters.messages

    for (let i = 0; i < messages.length; i++) {

      let message = messages[i]
      let token = message.token
      let notification = message.notification
      let data = message.data
      
      await fcm.sendToToken(token, notification, data)
    }

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
