'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 활동로그 남기기
 * @method POST logActivity
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}

    let userId = parameters.userId || null
    let mileage = parameters.mileage || null
    let type = parameters.type || null
    let actionId = parameters.actionId || null
    let targetType = parameters.targetType || null
    let targetId = parameters.targetId || null
    let linkInfo = parameters.linkInfo || null
    let log = parameters.log || null

    let query
    let result

    switch (type) {
      case 'SIGNUP':
        query = `
          select 
            log_id
          from
            oi_user_action_log
          where
            user_id = ${userId}
            and (log_type_cd = 'SIGNUP' or log_type_cd = 'OLDBIE')
          limit
            1;
        `
        result = await mysql.execute(query)
        if (result.length > 0) {
          mileage = 0
        }
        break

      case 'PFIMAGE':
        query = `
          select 
            log_id
          from
            oi_user_action_log
          where
            user_id = ${userId}
            and log_type_cd = 'PFIMAGE'
          limit
            1;
        `
        result = await mysql.execute(query)
        if (result.length > 0) {
          mileage = 0
        }
        break 

      case 'BGIMAGE':
        query = `
          select 
            log_id
          from
            oi_user_action_log
          where
            user_id = ${userId}
            and log_type_cd = 'BGIMAGE'
          limit
            1;
        `
        result = await mysql.execute(query)
        if (result.length > 0) {
          mileage = 0
        }
        break
      
      case 'COMMENT':
        query = `
          select 
            sum(mileage) mileage
          from
            oi_user_action_log
          where
            user_id = ${userId}
            and log_type_cd = 'COMMENT'
            and target_id = ${targetId};
        `
        result = await mysql.execute(query)

        if (result[0].mileage >= 15) {
          mileage = 0
        }
        
        break

      case 'LIKE':
        query = `
          select
            reg_user_id
          from
            oi_feed_info
          where
            feed_id = ${targetId};
        `
        result = await mysql.execute(query)
        if (result[0].reg_user_id == userId) {

          query = `
            select 
              sum(mileage) mileage
            from
              oi_user_action_log
            where
              user_id = ${userId}
              and log_type_cd = 'LIKE'
              and target_id = ${targetId}
            limit
              1;
          `
          result = await mysql.execute(query)
          if (result[0].mileage > 0) {
            mileage = 0
          } 
        }
        break
        
      case 'COMTLIKE':

        query = `
          select
            reg_user_id
          from
            oi_feed_comment
          where
            comment_id = ${actionId};
        `
        result = await mysql.execute(query)
        query = `
          select 
            log_id
          from
            oi_user_action_log
          where
            user_id = ${userId}
            and log_type_cd = 'LIKE'
            and target_id = ${targetId}
          limit
            10;
        `
        result = await mysql.execute(query)
        if (result.length > 9) {
          mileage = 0
        }
        break 

      case 'BANNERTAP':
        query = `
          select 
            log_id
          from
            oi_user_action_log
          where
            user_id = ${userId}
            and log_type_cd = 'BANNERTAP'
            and action_id = ${actionId}
          limit
            5;
        `
        result = await mysql.execute(query)
        if (result.length > 4) {
          mileage = 0
        }
        break
      
      case 'WRITEFEED':
        break
      
      case 'WRITEHELP':
        break
        
      case 'CREATECHAT':
        break
      
      case 'OLDBIE':
        query = `
          select 
            log_id
          from
            oi_user_action_log
          where
            user_id = ${userId}
            and (log_type_cd = 'SIGNUP' or log_type_cd = 'OLDBIE')
          limit
            1;
        `
        result = await mysql.execute(query)
        if (result.length > 0) {
          mileage = 0
        }
        break
        
      default:
        break
    }

    let targetTypeValue = targetType == null ? 'null' : `'${targetType}'`
    let linkValue = linkInfo == null ? 'null' : `'${JSON.stringify(linkInfo)}'`

    query = `
      insert into oi_user_action_log (
        user_id, mileage, log_type_cd, action_id, target_type_cd, target_id, link_info, action_log, reg_ymd
      ) values (
        ${userId}, ${mileage}, '${type}', ${actionId}, ${targetTypeValue}, ${targetId}, ${linkValue}, '${log}', date_format(now(), '%Y%m%d')
      );
    `
    await mysql.execute(query)
    mysql.end()
    return response.result(200, {})
  
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
