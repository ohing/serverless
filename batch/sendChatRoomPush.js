'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅방 개설 알림
 * @method POST sendRankerPush
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    let parameters = JSON.parse(event.body || '{}') || {};
    let roomId = parseInt(parameters.roomId);

    if (roomId == null) {
      return response.result(404);
    }

    let query;
    let result;
    let values = [];
    let tokens = [];
    let notifications = [];
    let payloads = [];

    query = `
      select 
        crt.tag_id, cri.title, ti.tag_val,
        fi.org_file_nm cover_image_url, substring_index(fi.org_file_nm, '/', -1) cover_file_name
      from 
        oi_chat_room_tag crt
        join oi_chat_room_info cri on crt.room_id = cri.room_id
        join oi_tag_info ti on crt.tag_id = ti.tag_id
        left join oi_file_info fi on cri.room_image_id = fi.file_id
      where 
        crt.room_id = ${roomId}
    `;
    result = await mysql.execute(query);

    if (result.length > 0) {

      let roomTitle = result[0].title;
      let coverImageUrl = result[0].cover_image_url;
      let coverFileName = result[0].cover_file_name;

      let tagIdMap = {};
      let tagIds = new Set();

      for (let roomTag of result) {

        if (tagIdMap[roomTag.tag_id] == null) {
          tagIdMap[roomTag.tag_id] = [roomTag];
        } else {
          tagIdMap[roomTag.tag_id].push(roomTag);
        }
        tagIds.add(roomTag.tag_id);
      }

      console.log(roomTitle, tagIds);

      query = `
        select 
          ti.tag_id, ti.tag_val, ui.user_id, ui.user_login_id, udi.push_auth_key,
          ns.msg_yn, ns.msg3_yn
        from 
          oi_user_tag ut
          join oi_tag_info ti on ut.tag_id = ti.tag_id
          join oi_user_info ui on ut.user_id = ui.user_id
          join oi_user_device_info udi on ui.user_id = udi.user_id and udi.login_yn = 'Y' and udi.push_auth_key is not null and udi.push_auth_key  <> ''
          join oi_notification_setting ns on udi.user_id = ns.user_id
        where 
          ut.tag_id in (${Array.from(tagIds).join(',')}) 
          and ui.user_status_cd = 'ACTIVE'
        order by 
          ui.user_id asc
      `;

      result = await mysql.execute(query);

      let targetUserIdMap = {};

      for (let targetUser of result) {

        if (targetUserIdMap[targetUser.user_id] == null) {
          targetUserIdMap[targetUser.user_id] = [targetUser];
        } else {
          targetUserIdMap[targetUser.user_id].push(targetUser);
        }
      }

      // console.log(userIdMap)

      for (let userId of Object.keys(targetUserIdMap)) {

        let token;

        tagIds = targetUserIdMap[userId].map(targetUser => {

          if (targetUser.msg_yn == 'Y' && targetUser.msg3_yn == 'Y') {
            token = targetUser.push_auth_key;
          }
          return targetUser.tag_id;
        });

        let users = [];

        for (let tagId of tagIds) {
          users = users.concat(tagIdMap[tagId]);
        }

        let tagMap = {};
        for (let user of users) {
          if (tagMap[user.tag_val] == null) {
            tagMap[user.tag_val] = [user];
          } else {
            tagMap[user.tag_val].push(user);
          }
        }

        for (let tag of Object.keys(tagMap)) {

          users = tagMap[tag];

          let count = users.length;
          let targetUserId = users[0].user_id;
          let accountId = users[0].user_login_id;

          let title = '오잉챗';
          let body = `나와 같은 관심사({tag})를 가진 오픈채팅방 ‘{roomTitle}’ 이 생성되었습니다.`;
          let linkInfo = [
            {
              key: 'tag',
              value: tag,
              color: '#222222'
            },
            {
              key: 'roomTitle',
              value: roomTitle,
              color: '#FF4800',
              targetType: 'CHATROOM',
              targetId: roomId
            }
          ];

          let notificationType = 'RECOMCHAT';
          let targetType = 'CHATROOM';
          let fileName = 'notification-chat';
          let storedFileName = coverFileName == null ? `'notification-default.png'` : `'${coverFileName}'`;
          let storedUrl = coverImageUrl == null ? `'img/notification-default/notification-default.png'` : `'${coverImageUrl}'`;

          values.push(`
            ( ${userId}, '${notificationType}', '${targetType}', ${roomId}, ${storedFileName}, '${title}', '${body}', '${JSON.stringify(linkInfo)}')
          `);

          if (token != null, token.length > 0) {

            tokens.push(token);
            notifications.push({
              title: title,
              body: `나와 같은 관심사(${tag})를 가진 오픈채팅방 ‘${roomTitle}’ 이 생성되었습니다.`
            });

            payloads.push({
              notificationType: 'RECOMCHAT',
              title: title,
              body: `나와 같은 관심사(${tag})를 가진 오픈채팅방 ‘${roomTitle}’ 이 생성되었습니다.`,
              targetType: 'CHATROOM',
              targetId: roomId
            });

            console.log(payloads);
          }
        }
      }
    }

    if (values.length > 0) {
      query = `
        insert into oi_notification_info (
          user_id, notification_type_cd, target_type_cd, target_id, profile_file_name, title, body, link_info
        ) values 
          ${values.join(',')};
      `;
      console.log(query);
      result = await mysql.execute(query);
    }

    for (let i = 0; i < tokens.length; i++) {
      await fcm.sendToTokens([tokens[i]], notifications[i], {
        type: 'Push',
        payload: JSON.stringify(payloads[i])
      });
    }

    mysql.end();

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
