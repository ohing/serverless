'use strict';

const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3();

/**
 * @author Karl <karl@ohing.net> 
 * @description s3 객체 삭제
 * @method DELETE deleteS3Objet
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  return

  try {

    const parameters = JSON.parse(event.body || '{}') || {}
    let bucket = parameters.bucket
    let key = parameters.key

    if (bucket == null || key == null) {
      return response.result(400)
    }
    
    let params = {
      Bucket: bucket,
      Key: key
    }
    await s3.deleteObject(params).promise()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
