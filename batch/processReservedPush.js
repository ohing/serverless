'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const axios = require(`axios`);
const sms = require(`@ohing/ohingsms-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();
const eventBridge = new AWS.EventBridge();

/**
 * @author Karl <karl@ohing.net> 
 * @description 관리자를 통한 예약 푸시
 * @method POST processReservedPush
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    let parameters = JSON.parse(event.body || '{}') || {};
    let pushId = parameters.pushId;

    if (pushId == null) {
      return response.result(200, {});
    }

    let query;
    let result;

    query = `
      select 
        * 
      from 
        oi_admin_push
      where 
        push_id = ${pushId}
    `;
    result = await mysql.execute(query);

    let pushInfo = result[0];

    let targetType = pushInfo.target_type_cd;
    let targetId = pushInfo.target_id;

    let errorType = null;

    if (targetType == 'FEED') {

      query = `
        select
          feed_except_yn 
        from 
          oi_feed_info
        where
          feed_id = ${targetId};
      `;
      result = await mysql.execute(query);

      if (result == 0) {
        errorType = 'NoFeed';
      } else {
        let excepted = result[0].feed_except_yn == 'Y';
        if (excepted) {
          errorType = 'FeedIsBlocked';
        }
      }

    } else if (targetType == 'USER') {

      query = `
        select
          user_status_cd, profile_open_type_cd
        from
          oi_user_info
        where
          user_id = ${targetId};
      `;
      result = await mysql.execute(query);

      if (result == 0) {
        errorType = 'NoUser';
      } else {
        let notActive = result[0].user_status_cd != 'ACTIVE';
        if (notActive) {
          errorType = 'UserIsNotActivated';
        } else {
          let closed = result[0].profile_open_type_cd == 'CLOSE';
          if (closed) {
            errorType = 'UserIsClosed';
          }
        }
      }
    }

    if (errorType != null) {

      let adminPhoneNumbers = [
        '01029107431',
        '01091441201',
        '01073487548',
        '01054953609'
      ];

      let message = {
        'NoFeed': '피드가 존재하지 않아 예약 발송이 실패했습니다.',
        'FeedIsBlocked': '피드가 차단되어 예약 발송이 실패했습니다.',
        'NoUser': '유저가 존재하지 않아 예약 발송이 실패했습니다.',
        'UserIsNotActivated': '유저가 활성화 상태가 아니어서 예약 발송이 실패했습니다.',
        'UserIsClosed': '유저가 비공개 프로필 상태여서 예약 발송이 실패했습니다.'
      }[errorType];

      for (let phoneNumber of adminPhoneNumbers) {
        await sms.send(phoneNumber, message);
      }

      await removeReserve(pushId);
      mysql.end();

      await deleteRule(pushId);

      return response.result(200, {});
    }

    let notification = {
      title: pushInfo.title,
      body: pushInfo.body
    };

    let data = {
      notificationType: 'EVENT',
      title: pushInfo.title,
      body: pushInfo.body,
      targetType: targetType,
      targetId: targetId,
      targetUrl: pushInfo.target_url
    };

    switch (pushInfo.fire_type) {
      case 'TEST': {
        query = `
          select 
            push_auth_key
          from 
            oi_admin_push_tester pt
            join oi_user_device_info udi on pt.user_id = udi.user_id
          where
            udi.login_yn = 'Y'
            and udi.push_auth_key is not null and udi.push_auth_key  <> '';
        `;
        result = await mysql.execute(query);

        let tokens = result.map(row => {
          return row.push_auth_key;
        });

        await sendFCM(fcmTokensPayload(tokens, notification, data));
        break;
      }

      case 'WHOLE': {
        let topic = 'notification';
        await sendFCM(fcmTopicPayload(topic, notification, data));
        break;
      }

      default: {
        break;
      }
    }

    await removeReserve(pushId);
    mysql.end();

    await deleteRule(pushId);

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};

const fcmServerKey = process.env.stage == 'dev'
  ? 'AAAAl4zZDWQ:APA91bFM-ImE3Er_pov80-XmmeVnX9oXe6YS4WjUsd7npagM6LTMqL7Z6uRCud3kGsBcQVuVxQtLMV9R4tLAlm9D511gCtwHTQkeYN878_zVkqYKzG7so4XkXCbIKAQUa_Nd9zw4AUE_'
  : 'AAAASDIjXcY:APA91bHXSJRQvPEL8d-crG9ALJs-33vXWUTm5vZkrs9VP7vNwq07AJT7Z5WnJWl_X3E0JllvlyP1YCGHqW4LDKhLDmQW9MhDZdHTPtlxYmoaGINAKKpnw83QD9Y_0NILSGpkZBvZ6hQU';
const sendMessageEndpoint = 'https://fcm.googleapis.com/fcm/send';

const fcmAxiosOptions = {
  headers: {
    'Authorization': `key=${fcmServerKey}`
  }
};

let androidOptions = {
  priority: 'high'
};

let iosOptions = {
  headers: {
    'apns-priority': 10,
    'content-available': true,
    'mutable-content': true,
  }
};

let fcmTokensPayload = (tokens, notification, data = null) => {

  console.log(tokens, notification, data);
  return {
    registration_ids: tokens,
    data: data,
    notification: notification,
    android: androidOptions,
    apns: iosOptions
  };
};

let fcmTopicPayload = (topic, notification, data = null) => {

  console.log(topic, notification, data);
  return {
    to: `/topics/${topic}`,
    data: data,
    notification: notification,
    android: androidOptions,
    apns: iosOptions
  };
};

let sendFCM = async (payload) => {

  try {
    let url = `${sendMessageEndpoint}`;
    let result = await axios.post(url, payload, fcmAxiosOptions);
    console.log(result.data);
  } catch (e) {
    console.error(e);
  }
};

let removeReserve = async (pushId) => {
  let query = `
    update
      oi_admin_push
    set
      fire_dt = null, fire_type = null
    where
      push_id = ${pushId};
  `;
  await mysql.execute(query);
};

let deleteRule = async (pushId) => {

  try {
    const ruleName = `Push-Process-${pushId}`;

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-processReservedPush`,
      StatementId: ruleName
    };

    await lambda.removePermission(params).promise();

    params = {
      Rule: ruleName,
      Ids: [
        ruleName
      ]
    };

    await eventBridge.removeTargets(params).promise();

    params = {
      Name: ruleName
    };

    console.log(params);

    await eventBridge.deleteRule(params).promise();

  } catch (e) {
    console.error(e);
  }
};