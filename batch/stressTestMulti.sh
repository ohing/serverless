#!/bin/sh

fullscreen_size=$( osascript -e 'tell application "Finder" to get bounds of window of desktop' )
echo $fullscreen_size
sizes=$(echo $fullscreen_size | tr "," "\n")
i=0

topMargin=24

for size in $sizes
do
  if [ ${i} -eq 2 ]; then 
  width=$(($size / 4))
  elif [ ${i} -eq 3 ]; then
  fullHeight=$(( $size - $topMargin ))
  height=$(( $fullHeight / 4 ))
  fi
  i=$(($i + 1))
done

osascript -e 'tell app "Terminal"
  activate
  do script "cd /Users/karl/Documents/Serverless/batch && node test chatStressTest && exit"
  do script "cd /Users/karl/Documents/Serverless/batch && node test chatStressTest2 && exit"
  do script "cd /Users/karl/Documents/Serverless/batch && node test chatStressTest3 && exit"
  
end tell'

osascript -e "tell app \"Terminal\"
  set the bounds of the window 3 to {0, $topMargin, $width, $(($topMargin + $height))}
  set the bounds of the window 2 to {$width, $topMargin, $(($width * 2)), $(($topMargin + $height))}
  set the bounds of the window 1 to {$(($width * 2)), $topMargin, $(($width * 3)), $(($topMargin + $height))}
end tell"