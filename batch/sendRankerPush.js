'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

/**
 * @author Karl <karl@ohing.net> 
 * @description 랭커 푸시 전송
 * @method POST sendRankerPush
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    let query;
    let result;
    let values = [];

    query = `
      select
        ur.rank,
        ur.user_id,
        udi.push_auth_key,
        ns.event_yn
      from
        oi_user_rank ur 
        join oi_user_info ui on ur.user_id = ui.user_id
        left join oi_user_device_info udi on ui.user_id = udi.user_id and udi.login_yn = 'Y' and udi.push_auth_key is not null and udi.push_auth_key  <> ''
        left join oi_notification_setting ns on udi.user_id = ns.user_id
      where
        ur.rank_type_cd = 'Top100'
        and ur.rank_ymd = date_format(now(), '%Y%m%d') 
        and ur.rank >= 1 and ur.rank <= 100
        and ui.user_status_cd = 'ACTIVE';
    `;
    result = await mysql.execute(query);

    let tokens = [];
    let notifications = [];
    let payloads = [];

    if (result.length > 0) {

      for (let ranker of result) {

        let rank = ranker.rank;
        let userId = ranker.user_id;
        let token = ranker.push_auth_key;

        let eventPossible = ranker.event_yn == 'Y';

        let title = '랭킹';
        let body = `🏆나님! 랭킹 ${rank}위 등극!! 기특한 나를 만나러 레고~`;

        values.push(`
          ( ${userId}, 'RANKER', 'RANK', 'notification-ranker', '${title}', '${body}' )
        `);

        if (eventPossible && (token || '').length > 0) {
          tokens.push(token);

          notifications.push({
            title: title,
            body: body
          });
          payloads.push({
            type: 'Push',
            payload: JSON.stringify({
              type: 'RANKER',
              title: title,
              body: body,
              targetType: 'RANK'
            })
          });
        }
      }

      query = `
        insert into oi_notification_info (
          user_id, notification_type_cd, target_type_cd, profile_file_name, title, body
        ) values 
          ${values.join(',')};
      `;
      console.log(query);
      await mysql.execute(query);
      mysql.end();

      for (let i = 0; i < tokens.length; i++) {
        await fcm.sendToTokens([tokens[i]], notifications[i], payloads[i]);
      }
    }

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
