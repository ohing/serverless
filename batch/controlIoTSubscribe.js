'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const iot = require(`@ohing/ohingiot`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description IoT 구독 정보 확인
 * @method POST controlIoTSubscribe
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    // if (readable != null) {
    //   redis.end()
    // }
    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    // if (readable != null) {
    //   redis.end()
    // }

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
