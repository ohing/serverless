'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const fs = require('fs')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3();

/**
 * @author Karl <karl@ohing.net> 
 * @description 공지사항 수정
 * @method POST notificationLinkInfoFix
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let query
    let result

    const notices = JSON.parse(fs.readFileSync('./old_notice.json', 'utf8'))

    for (let notice of notices) {

      let escapedTitle = (await mysql.escape(notice.title)).trim()
      notice.content = `<p>${notice.content}`.replace(/\r\n\r\n/gi, '</p><br/></p>').replace(/\r\n/gi, '</p><p>')

      let html = `
        <!DOCTYPE html>
        <html lang="ko">

        <head>
          <meta charset="utf-8">
          <title>ohing</title>
          <link rel="stylesheet" href="./css/style.css">
          <meta name="viewport" content="width=device-width,initial-scale=1">
          <link rel="shortcut icon" href="./img/favicon.ico">
          <!-- Stylesheets -->
          <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;400;500&display=swap" rel="stylesheet">
        </head>

        <body>

          <div class="container">

            <hgcgroup>
              {{content}}
            </hgcgroup>

            <footer>
              <p></p>
            </footer>
          </div> <!-- end container -->

        </body>

        </html>
      `.replace('{{content}}', notice.content)

      console.log(html)

      query = `
        select
          notice_id
        from
          oi_notice_info
        where
          tmp_notice_id = '${notice.notice_id}';
      `
      result = await mysql.execute(query)
      let noticeId = result[0].notice_id

      let s3Key = `notice/${noticeId}.html`
      let bucket = 'ohing-public-prod'
      let buffer = Buffer.from(html)

      await s3.putObject({
        Bucket: bucket,
        ContentType: 'text/html',
        ContentLanguage: 'utf-8',
        Body: buffer,
        Key: s3Key
      }).promise()
      
      query = `
        update
          oi_notice_info
        set
          title = ${escapedTitle}, category = '${notice.category}'
        where
          tmp_notice_id = '${notice.notice_id}';
      `
      await mysql.execute(query)
      
    }

    

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}