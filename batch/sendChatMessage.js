'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const iot = require(`@ohing/ohingiot`);
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅 메시지 전송
 * @method POST sendChatMessage
 * @returns  
*/

module.send = (message) => {

  return new Promise(async (resolve, reject) => {

    let type = message.type;
    let targetRoomId = message.roomId;
    let targetUserId = message.targetUserId;
    let userId = message.userId;

    if (message.isMaster == null) {
      message.isMaster = false;
    }
    if (message.unreadCount == null) {
      message.unreadCount = 0;
    }

    let payload;

    let notification;
    let data;

    let query;
    let result;

    query = `
      select 
        room_type_cd, title
      from 
        oi_chat_room_info 
      where 
        room_id = ${targetRoomId};
    `;
    console.log(query);
    result = await mysql.execute(query);
    if (result.length == 0) {
      console.log('no room!');
      resolve(404);
      return;
    }

    let roomType = result[0].room_type_cd;
    let roomTitle = result[0].title || '오잉챗';

    let isDeportMessage = false;
    let isExplodeMessage = false;

    switch (type) {
      case 'SendMessage': {

        payload = message.chatMessage;

        let body = '';

        switch (payload.type) {
          case 'CHGMASTER':
          case 'CHGPW':
          case 'GROUPINVITE':
          case 'ROOMINMSG':
          case 'ROOMOUT':
          case 'GETMASTER':
          case 'NOTICE':
            body = payload.text;
            break;
          case 'OPENINVITE':
            body = '오픈채팅방에 초대합니다.';
            break;
          case 'DEPORT':
            body = payload.text;
            isDeportMessage = true;
            break;
          case 'BLOWUP':
            body = payload.text;
            isExplodeMessage = true;
            break;
          default:
            if (payload.mediaInfo != null) {
              body = `${payload.nickname}: ${targetUserId == null ? '' : '(귓속말)'} (미디어)`;
            } else {
              body = `${payload.nickname}: ${targetUserId == null ? '' : '(귓속말)'}${payload.text}`;
            }
            break;
        }

        notification = {
          title: roomTitle || '오잉챗',
          body: body
        };

        data = {
          type: 'ChatMessage',
          chatMessage: payload
        };

        break;
      }

      case 'ReadCount': {

        payload = message.chatRead;

        notification = {};
        data = {
          type: 'ChatRead',
          chatRead: payload
        };

        break;
      }

      default: {
        break;
      }
    }

    let userIds;

    if (targetUserId != null) {
      userIds = [userId, targetUserId];
    } else {
      query = `
        select
          user_id
        from 
          oi_chat_user_info
        where
          room_id = ${targetRoomId}
          and use_yn = 'Y';
      `;
      result = await mysql.execute(query);

      userIds = result.map(row => {
        return row.user_id;
      });
    }

    if (isDeportMessage || isExplodeMessage) {

      let targetUserCondition = targetUserId != null ? `
        or cui.user_id = ${targetUserId}
      ` : '';

      let iotTopic = `${process.env.stage}/message/${targetRoomId}/All`;
      await iot.publish(iotTopic, JSON.stringify(data));

      query = `
        select 
          udi.push_auth_key, udi.os_type_cd, udi.user_id, ns.msg2_yn
        from 
          oi_chat_user_info cui
          join oi_user_device_info udi on cui.user_id = udi.user_id and udi.login_yn = 'Y' and udi.push_auth_key is not null and udi.push_auth_key <> ''
          join oi_notification_setting ns on cui.user_id = ns.user_id
        where 
          cui.room_id = ${targetRoomId}
          and (
            udi.os_type_cd = 'ANDROID'
            or (
              ns.msg_yn = 'Y'
              and ns.msg1_yn = 'Y'
              and (
                (cui.use_yn = 'Y' and cui.alarm_yn = 'Y')
                ${targetUserCondition}
              )
            )
          );
      `;

      result = await mysql.execute(query);

      console.log('devices', result);

      let targetUserDevice = null;

      let iosTokens = [];
      let androidTokens = [];

      for (let row of result) {
        if (row.user_id == targetUserId) {
          targetUserDevice = row;
        } else {
          if (row.os_type_cd == 'ANDROID') {
            androidTokens.push(row.push_auth_key);
          } else if (row.os_type_cd == 'IOS') {
            iosTokens.push(row.push_auth_key);
          }
        }
      }

      if (isDeportMessage) {

        if (iosTokens.length > 0 || androidTokens.length > 0) {
          if (iosTokens.length > 0) {
            let pushData = JSON.parse(JSON.stringify(data));
            pushData.chatMessage = JSON.stringify(pushData.chatMessage);
            await fcm.sendToTokens(iosTokens, notification, pushData);
          }

          if (androidTokens.length > 0) {

            console.log('android tokens', androidTokens);
            let pushData = JSON.parse(JSON.stringify(data));

            if (data.chatMessage != undefined) {
              pushData.chatMessage.title = notification.title;
              pushData.chatMessage.body = notification.body;
            }

            console.log('data', data);

            pushData.chatMessage = JSON.stringify(pushData.chatMessage);
            await fcm.sendToTokens(androidTokens, null, pushData);
          }
        }

        data = {
          type: 'deportedFromChatRoom',
          roomId: parseInt(targetRoomId)
        };

        iotTopic = `${process.env.stage}/user/${targetUserId}`;
        await iot.publish(iotTopic, JSON.stringify(data));

        if (targetUserDevice != null) {

          let text = `🚫 허거덕! "${roomTitle}" 에서 강퇴 되었다네`;

          notification = {
            title: '오잉챗',
            body: text
          };

          data = {
            type: 'Push',
            payload: {
              type: 'Deported',
              title: notification.title,
              body: notification.body,
              userId: userId,
              accountId: '',
              nickname: '',
              roomId: targetRoomId,
            }
          };

          console.log('deport target', targetUserDevice);
          console.log('data', data);

          let pushData = JSON.parse(JSON.stringify(data));
          pushData.payload = JSON.stringify(pushData.payload);

          if (targetUserDevice.os_type_cd == 'ANDROID') {
            await fcm.sendToTokens([targetUserDevice.push_auth_key], null, pushData);
          } else {
            await fcm.sendToTokens([targetUserDevice.push_auth_key], notification, pushData);
          }
        }

      } else {

        data = {
          type: 'chatRoomIsExploded',
          roomId: parseInt(targetRoomId)
        };

        query = `
          select
            user_id
          from
            oi_chat_user_info
          where
            room_id = ${targetRoomId}
            and use_yn = 'Y'
            and alarm_yn = 'Y'
        `;
        result = await mysql.execute(query);

        for (let row of result) {
          iotTopic = `${process.env.stage}/user/${row.user_id}`;
          await iot.publish(iotTopic, JSON.stringify(data));
        }

        data = {
          type: 'Push',
          payload: {
            type: 'Exploded',
            title: notification.title,
            body: notification.body,
            userId: userId,
            accountId: '',
            nickname: '',
            roomId: parseInt(targetRoomId),
          }
        };

        if (iosTokens.length > 0 || androidTokens.length > 0) {
          if (iosTokens.length > 0) {
            let pushData = JSON.parse(JSON.stringify(data));
            pushData.payload = JSON.stringify(pushData.payload);
            await fcm.sendToTokens(iosTokens, notification, pushData);
          }

          if (androidTokens.length > 0) {
            let pushData = JSON.parse(JSON.stringify(data));
            if (data.chatMessage != undefined) {
              pushData.chatMessage.title = notification.title;
              pushData.chatMessage.body = notification.body;
            }
            pushData.payload = JSON.stringify(pushData.payload);
            await fcm.sendToTokens(androidTokens, null, pushData);
          }
        }
      }

      if (userIds.length > 0) {

        let params = {
          FunctionName: `api-new-batch-${process.env.stage}-sendUpdatedRoomInfo`,
          InvocationType: "Event",
          Payload: JSON.stringify({
            body: JSON.stringify({
              roomId: targetRoomId,
              userIds: userIds,
            })
          })
        };

        await lambda.invoke(params).promise();
      }

      resolve(200);
      return;
    }

    let iotTopic = `${process.env.stage}/message/${targetRoomId}/${targetUserId == null ? 'All' : targetUserId}`;
    console.log(iotTopic);
    await iot.publish(iotTopic, JSON.stringify(data));

    if (targetUserId != null) {
      iotTopic = `${process.env.stage}/message/${targetRoomId}/${userId}`;
      await iot.publish(iotTopic, JSON.stringify(data));
    }

    if (userIds.length > 0) {

      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-sendUpdatedRoomInfo`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            roomId: targetRoomId,
            userIds: userIds,
          })
        })
      };

      await lambda.invoke(params).promise();
    }

    if ((data.chatMessage || {}).type == 'GETMASTER' || (data.chatMessage || {}).type == 'DEPORT') {

      let masterUserId = data.chatMessage.userId;

      query = `
        select 
          udi.push_auth_key
        from 
          oi_user_device_info udi
        where 
          udi.user_id = ${masterUserId}
          and udi.login_yn = 'Y'
          and udi.push_auth_key is not null
          and udi.push_auth_key <> ''
          ;
      `;
      console.log(query);
      result = await mysql.execute(query);
      let token = result[0].push_auth_key;

      let pushData = JSON.parse(JSON.stringify(data));
      pushData.payload = JSON.stringify(pushData.payload);

      await fcm.sendToTokens([token], notification, pushData);

    } /* else if (roomType == 'OTHER') {
    
      let fcmTopic = `openchat-${targetRoomId}`

      await fcm.sendToTopic(fcmTopic, notification, data)

    } */ else {

      let userIdCondition = targetUserId != null ? `
        and cui.user_id = ${targetUserId}
      ` : '';

      query = `
        select 
          udi.push_auth_key, udi.os_type_cd, cui.alarm_yn
        from 
          oi_chat_user_info cui
          join oi_user_device_info udi on cui.user_id = udi.user_id and udi.login_yn = 'Y' and push_auth_key is not null
        where 
          cui.room_id = ${targetRoomId}
          ${userIdCondition}
          and cui.use_yn = 'Y';
      `;
      console.log(query);
      result = await mysql.execute(query);

      console.log('targets', result);

      let iosTokens = [];
      let androidTokens = [];

      for (let row of result) {
        if (row.os_type_cd == 'ANDROID') {
          androidTokens.push(row.push_auth_key);
        } else if (row.os_type_cd == 'IOS' && row.alarm_yn == 'Y') {
          iosTokens.push(row.push_auth_key);
        }
      }

      console.log('data', data);

      if (iosTokens.length > 0 || androidTokens.length > 0) {
        if (iosTokens.length > 0) {
          let pushData = JSON.parse(JSON.stringify(data));
          pushData.chatMessage = JSON.stringify(pushData.chatMessage);
          await fcm.sendToTokens(iosTokens, notification, pushData);
        }

        if (androidTokens.length > 0) {
          let pushData = JSON.parse(JSON.stringify(data));
          if (data.chatMessage != undefined) {
            pushData.chatMessage.title = notification.title;
            pushData.chatMessage.body = notification.body;
          }
          pushData.chatMessage = JSON.stringify(pushData.chatMessage);
          await fcm.sendToTokens(androidTokens, null, pushData);
        }
      }
    }

    resolve(200);
  });
};

module.exports.handler = async event => {

  console.log(event);

  let parameters = JSON.parse(event.body || '{}') || {};

  try {

    if (parameters.chatMessage != null) {
      parameters.chatMessage.roomId = parseInt(parameters.chatMessage.roomId);
      parameters.chatMessage.userId = parseInt(parameters.chatMessage.userId);
      parameters.chatMessage.targetUserId = parseInt(parameters.chatMessage.targetUserId);

      if (Number.isNaN(parameters.chatMessage.roomId)) {
        parameters.chatMessage.roomId = null;
      }
      if (Number.isNaN(parameters.chatMessage.userId)) {
        parameters.chatMessage.userId = null;
      }
      if (Number.isNaN(parameters.chatMessage.targetUserId)) {
        parameters.chatMessage.targetUserId = null;
      }
    }

    if (Number.isNaN(parameters.roomId)) {
      parameters.roomId = null;
    }
    if (Number.isNaN(parameters.targetUserId)) {
      parameters.targetUserId = null;
    }

    let result;

    let targetRoomId = parameters.roomId;
    let targetUserId = parameters.targetUserId;

    console.log('parameters', parameters);

    await iot.connect();

    if (targetRoomId != null) {
      await module.send(parameters);
    }

    let messages = parameters.messages || [];
    console.log('messages', messages);

    if (messages.length > 0) {
      for (let message of messages) {
        await module.send(message);
      }
    }

    await iot.disconnect();

    mysql.end();

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
