'use strict';

const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 테스트
 * @method POST testLambdaCall
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          targetUserIds: [8],
          messages: [
            {
              userId: 20756,
              notificationType: 'FOLLOW', 
            }
          ]
        })
      })
    }
    console.log(params)
    
    // console.log("I'm here!!")

    const resultVal = await lambda.invoke(params).promise()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}