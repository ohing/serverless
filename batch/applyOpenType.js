'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 프로필 공개범위 변경에 따른 데이터 변경
 * @method POST applyOpenType
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}
    let userId = parameters.userId

    if (userId == null) {
      return response.result(400)
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)

    let openType = result[0].open_type

    await mysql.beginTransaction()

    if (openType == 'CLOSE') {

      // 나를 찜하는 목록 제거 
      query = `
        delete from
          oi_user_relation         
        where
          relation_type_cd = '05'
          and (
            user_id = ${userId}
            or relation_user_id = ${userId}
          );
      `
      await mysql.execute(query)
      
      // 내가 방장인 오픈채팅방의 방장 물려받은 사람들 가져옴
      query = `
        select 
          room_id, user_id, min(join_dt)
        from 
          oi_chat_user_info 
        where 
          room_id in (select room_id from oi_chat_user_info where user_id = ${userId} and master_yn = 'Y')
          and user_id != ${userId}
          and use_yn = 'Y'
          and deport_yn = 'N'
        group by 
          room_id;   
      `
      result = await mysql.execute(query)

      if (result.length > 0) {

        let values = []

        result.forEach(row => {
          values.push(`
            (${row.room_id}, ${row.user_id}, 'Y')
          `)
        })

        // 방장 위임 처리
        query = `
          insert into oi_chat_user_info (
            room_id, user_id, master_yn
          ) values 
            ${values.join(',')}
          on duplicate key update
            master_yn = 'Y'
        `
        await mysql.execute(query)
      }

      // 내가 속한 모든 채팅방 아이디
      query = `
        select
          room_id
        from 
          oi_chat_user_info
        where
          user_id = ${userId};
      `
      result = await mysql.execute(query)

      // 내가 속한 모든 채팅방 나가기
      query = `
        delete from
          oi_chat_user_info
        where
          user_id = ${userId};
      `
      await mysql.execute(query)

      // 나간 채팅방들 카운트 재조정
      for (let row of result) {
        
        query  = `
          update 
            oi_chat_room_info
          set
            join_user_cnt = (
              select 
                count(*) count
              from 
                oi_chat_user_info
              where
                room_id = ${row.room_id}
                and use_yn = 'Y'
            )
          where
            room_id = ${row.room_id};
        `
        await mysql.execute(query)
      }

      // es에서 유저 제거 필요
      
    } else {

      if (openType == 'OPEN') {
        
        query = `
          select
            relation_user_id
          from
            oi_user_relation
          where
            user_id = ${userId}
            and relation_type_cd = '01'
            and accpt_dt is null;
        `
        result = await mysql.execute(query)

        if (result.length > 0) {

          let targetUserIds = result.map(row => {
            return row.relation_user_id
          })

          query = `
            update
              oi_user_relation
            set
              accpt_dt = now()
            where
              user_id = ${userId}
              and relation_type_cd = '01'
              and accpt_dt is null;
          `
          await mysql.execute(query)

          let params = {
            FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
            InvocationType: "Event",
            Payload: JSON.stringify({
              body: JSON.stringify({
                targetUserIds: targetUserIds,
                messages: [
                  {
                    userId: userId,
                    notificationType: 'FOLLOWACPT',
                  }
                ]
              })
            })
          }
          console.log(params)

          await lambda.invoke(params).promise()
        }
      }

      // es에서 유저 추가 필요
    }    

    query = `
      select 
        target_id feed_id
      from 
        oi_like_log 
      where 
        like_target_type_cd = 'FEED' 
        and user_id = ${userId};
    `
    result = await mysql.execute(query)

    let feedIds = result.map(row => {
      return row.feed_id
    })

    query = `
      select 
        feed_id
      from 
        oi_feed_comment
      where
        reg_user_id = ${userId}
      group by 
        feed_id;
    `
    result = await mysql.execute(query)

    feedIds = feedIds.concat(result.map(row => {
      return row.feed_id
    }))

    feedIds = feedIds.filter((item, pos) => {
      return feedIds.indexOf(item) === pos
    })

    query = `
      update 
        oi_feed_popul_aggr fpa
      set
        fpa.like_cnt = (
          (select count(*) from oi_like_log ll join oi_user_info ui on ll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where ll.like_target_type_cd = 'FEED' and ll.target_id = fpa.feed_id)+
          (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fmll.feed_id = fpa.feed_id)
        ),
        fpa.view_cnt = (select count(*) from oi_feed_view_log where feed_id = fpa.feed_id),
        fpa.comment_cnt = (select count(*) from oi_feed_comment fc join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fc.feed_id = fpa.feed_id and fc.del_yn = 'N')
      where
        fpa.feed_id in (${feedIds.join(',')});
    `
    await mysql.execute(query)

    await mysql.commit()
    mysql.end()

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-makeRanking`,
      InvocationType: "Event"
    }

    await lambda.invoke(params).promise()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.rollback()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}