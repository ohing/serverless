'use strict';

const fcm = require(`@ohing/ohingfcm-${process.env.stage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 즉시 푸시 전송. 모두에게 같은 메시지.
 * @method POST sendPushImmediately
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}

    let tokens = parameters.tokens
    let notification = parameters.notification
    let data = parameters.data

    if (tokens == null) {
      return response.result(404, {})
    }

    await fcm.sendToTokens(tokens, notification, data)

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
