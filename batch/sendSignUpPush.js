'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

/**
 * @author Karl <karl@ohing.net> 
 * @description 회원가입 추천 푸시 전송
 * @method POST sendSignUpPush
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    let query;
    let result;
    let map = {};

    let sendUserTokens = new Set();

    // 전날 가입자
    query = `
      select 
        user_id
      from
        oi_user_info
      where
        user_status_cd = 'ACTIVE'
              and profile_open_type_cd != 'CLOSE'
        and reg_dt > curdate() - interval 1 day 
              and reg_dt < curdate();
    `;
    let users = await mysql.execute(query);
    let userIds = users.map(user => {
      return user.user_id;
    });

    if (userIds.length == 0) {
      mysql.end();
      return response.result(200);
    }

    let values = [];

    // 학교
    query = `
      select 
        sh.user_id user_id, sh.school_short_nm,
        ui2.user_id target_user_id, ui2.user_login_id, 
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        udi.push_auth_key, ns.sns_yn, ns.sns6_yn
      from 
        oi_user_school_history sh
        join oi_user_info ui on sh.user_id = ui.user_id
        join oi_user_device_info udi on sh.user_id = udi.user_id and udi.login_yn = 'Y' and udi.push_auth_key is not null and udi.push_auth_key  <> ''
        join oi_notification_setting ns on udi.user_id = ns.user_id
        right join oi_user_school_history sh2 on sh.school_short_nm = sh2.school_short_nm and sh2.school_hist_cd = 'CURR' and sh.user_id != sh2.user_id
        join oi_user_info ui2 on sh2.user_id = ui2.user_id
        left join oi_user_profile_image upi on ui2.user_id = upi.user_id and image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
      where
        sh2.user_id in (${userIds.join(',')})
        and ui.reg_dt < curdate() - interval 1 day 
        and ui.user_status_cd = 'ACTIVE'
        and sh.school_hist_cd = 'CURR';
    `;
    result = await mysql.execute(query);
    console.log(result);

    if (result.length > 0) {

      result.forEach(row => {
        if (map[row.user_id] == null) {
          map[row.user_id] = [
            row
          ];
        } else {
          map[row.user_id].push(row);
        }
      });

      let userIds = Object.keys(map);

      for (let userId of userIds) {

        let rows = map[userId];
        console.log(rows);
        let count = rows.length;

        if (rows[0].sns_yn == 'Y' && rows[0].sns6_yn == 'Y') {
          sendUserTokens.add(rows[0].push_auth_key);
        }

        let targetUserId = rows[0].target_user_id;
        let accountId = rows[0].user_login_id;
        let profileImageUrl = rows[0].profile_image_url;
        let profileFileName = rows[0].profile_file_name;
        let schoolName = rows[0].schoolName;

        let title = '친구추천';
        let body = count > 1 ? `🏫회원님과 같은 학교({schoolName})인 {accountId} 님 외 ${count - 1}명이 가입했습니다.` : `🏫회원님과 같은 학교({schoolName})인 {accountId} 님이 가입했습니다.`;
        let linkInfo = [
          {
            key: 'schoolName',
            value: schoolName,
            color: '#222222'
          },
          {
            key: 'accountId',
            value: accountId,
            color: '#FF4800',
            targetType: 'USER',
            targetId: targetUserId
          }
        ];

        let notificationType = count > 1 ? 'SUSCGROUP' : 'SUSCHOOL';
        let targetType = count > 1 ? 'SUSCGROUP' : 'USER';
        let fileName = 'notification-school';
        let storedFileName = count > 1 ? `'${`${fileName}.png`}'` : profileFileName == null ? `'notification-default.png'` : `'${profileFileName}'`;
        let schoolProfilePath = `img/${fileName}/${fileName}.png`;
        let storedUrl = count > 1 ? `'${schoolProfilePath}'` : profileImageUrl == null ? `'img/notification-default/notification-default.png'` : `'${profileImageUrl}'`;

        let groupTargetIds = 'null';

        if (count > 1) {
          let targetUserIds = rows.map(row => {
            return row.target_user_id;
          });
          groupTargetIds = `'${JSON.stringify(targetUserIds)}'`;
        }

        values.push(`
          ( ${userId}, '${notificationType}', '${targetType}', ${count > 1 ? null : targetUserId}, ${storedFileName}, '${title}', '${body}', '${JSON.stringify(linkInfo)}', ${groupTargetIds})
        `);
      }
    }

    // 태그

    query = `
      select 
        ut.tag_id, ti.tag_val, ui.user_id, ui.user_login_id,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name
      from 
        oi_user_tag ut
        join oi_tag_info ti on ut.tag_id = ti.tag_id
        join oi_user_info ui on ut.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
      where 
        ut.user_id in (${userIds.join(',')}); 
    `;
    result = await mysql.execute(query);

    if (result.length > 0) {

      let tagIdMap = {};
      let tagIds = new Set();

      for (let userTag of result) {

        if (tagIdMap[userTag.tag_id] == null) {
          tagIdMap[userTag.tag_id] = [userTag];
        } else {
          tagIdMap[userTag.tag_id].push(userTag);
        }
        tagIds.add(userTag.tag_id);
      }

      console.log(tagIds);

      query = `
        select 
          ti.tag_id, ti.tag_val, ui.user_id, ui.user_login_id, udi.push_auth_key,
          ns.sns_yn, ns.sns6_yn
        from 
          oi_user_tag ut
          join oi_tag_info ti on ut.tag_id = ti.tag_id
          join oi_user_info ui on ut.user_id = ui.user_id
          join oi_user_device_info udi on ui.user_id = udi.user_id and udi.login_yn = 'Y'
          join oi_notification_setting ns on udi.user_id = ns.user_id
        where 
          ut.tag_id in (${Array.from(tagIds).join(',')}) 
          and ui.user_status_cd = 'ACTIVE'
        order by 
          ui.user_id asc
      `;

      result = await mysql.execute(query);

      let userIdMap = {};

      for (let targetUser of result) {

        if (userIdMap[targetUser.user_id] == null) {
          userIdMap[targetUser.user_id] = [targetUser];
        } else {
          userIdMap[targetUser.user_id].push(targetUser);
        }
      }

      console.log(userIdMap);

      for (let userId of Object.keys(userIdMap)) {

        let token;

        tagIds = userIdMap[userId].map(targetUser => {

          if (targetUser.sns_yn == 'Y' && targetUser.sns6_yn == 'Y') {
            token = targetUser.push_auth_key;
          }
          return targetUser.tag_id;
        });

        if (token != null) {
          sendUserTokens.add(token);
        }

        let users = [];

        for (let tagId of tagIds) {
          users = users.concat(tagIdMap[tagId]);
        }

        let tagMap = {};
        for (let user of users) {
          if (tagMap[user.tag_val] == null) {
            tagMap[user.tag_val] = [user];
          } else {
            tagMap[user.tag_val].push(user);
          }
        }
        console.log(tagMap);

        for (let tag of Object.keys(tagMap)) {

          users = tagMap[tag];

          let count = users.length;
          let targetUserId = users[0].user_id;
          let accountId = users[0].user_login_id;

          let title = '친구추천';
          let body = count > 1 ? `🏫회원님과 같은 관심사({tag})를 가진 {accountId} 님 외 ${count - 1}명이 가입했습니다.` : `🏫회원님과 같은 관심사({tag})를 가진 {accountId} 님이 가입했습니다.`;
          let linkInfo = [
            {
              key: 'tag',
              value: tag,
              color: '#222222'
            },
            {
              key: 'accountId',
              value: accountId,
              color: '#FF4800',
              targetType: 'USER',
              targetId: targetUserId
            }
          ];

          let profileImageUrl = users[0].profile_image_url;
          let profileFileName = users[0].profile_file_name;
          let notificationType = count > 1 ? 'SUTAGGROUP' : 'SUTAG';
          let targetType = count > 1 ? 'SUTAGGROUP' : 'USER';
          let fileName = 'notification-tag';
          let storedFileName = count > 1 ? `'${`${fileName}.png`}'` : profileFileName == null ? `'notification-default.png'` : `'${profileFileName}'`;
          let tagProfilePath = `img/${fileName}/${fileName}.png`;
          let storedUrl = count > 1 ? `'${tagProfilePath}'` : profileImageUrl == null ? `'img/notification-default/notification-default.png'` : `'${profileImageUrl}'`;

          let groupTargetIds = 'null';

          if (count > 1) {
            let targetUserIds = users.map(row => {
              return row.user_id;
            });
            groupTargetIds = `'${JSON.stringify(targetUserIds)}'`;
          }

          values.push(`
            ( ${userId}, '${notificationType}', '${targetType}', ${count > 1 ? null : targetUserId}, ${storedFileName}, '${title}', '${body}', '${JSON.stringify(linkInfo)}', ${groupTargetIds} )
          `);
        }
      }
    }

    // 지역

    query = `
      select 
        region_info 
      from 
        oi_system_config 
      order by 
        config_id desc 
      limit 
        1;
    `;
    result = await mysql.execute(query);
    let regionInfo = result[0].region_info;
    let regions = regionInfo.doList.concat(regionInfo.cityList);
    let regionCodes = {};
    for (let region of regions) {
      regionCodes[region.code] = region.codeNm;
    }

    query = `
      select 
        ui2.user_id, ui.region_cd, ui.user_id target_user_id, ui.user_login_id,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        udi.push_auth_key, ns.sns_yn, ns.sns6_yn
      from 
        oi_user_info ui
        join oi_user_info ui2 on ui.region_cd = ui2.region_cd
        join oi_user_device_info udi on ui2.user_id = udi.user_id and udi.login_yn = 'Y' and udi.push_auth_key is not null and udi.push_auth_key  <> ''
        join oi_notification_setting ns on udi.user_id = ns.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
      where
        ui.user_id in (${userIds.join(',')})
        and ui2.reg_dt < curdate() - interval 1 day 
        and ui2.user_status_cd = 'ACTIVE'
      order by 
        ui2.user_id asc;
    `;
    result = await mysql.execute(query);

    if (result.length > 0) {

      result.forEach(row => {
        if (map[row.user_id] == null) {
          map[row.user_id] = [
            row
          ];
        } else {
          map[row.user_id].push(row);
        }
      });

      let userIds = Object.keys(map);

      for (let userId of userIds) {

        let rows = map[userId];
        console.log(rows);
        let count = rows.length;

        if (rows[0].sns_yn == 'Y' && rows[0].sns6_yn == 'Y') {
          sendUserTokens.add(rows[0].push_auth_key);
        }

        let regionCode = rows[0].region_cd;

        let targetUserId = rows[0].target_user_id;
        let accountId = rows[0].user_login_id;
        let profileImageUrl = rows[0].profile_image_url;
        let profileFileName = rows[0].profile_file_name;
        let schoolName = rows[0].schoolName;

        let regionName = regionCodes[regionCode];

        let title = '친구추천';
        let body = count > 1 ? `🗺회원님과 같은 지역({regionName})에 사는 {accountId} 님 외 ${count - 1}명이 가입했습니다.` : `🗺회원님과 같은 지역({regionName})에 사는 {accountId} 님이 가입했습니다.`;
        let linkInfo = [
          {
            key: 'regionName',
            value: regionName,
            color: '#222222'
          },
          {
            key: 'accountId',
            value: accountId,
            color: '#FF4800',
            targetType: 'USER',
            targetId: targetUserId
          }
        ];

        let notificationType = count > 1 ? 'SUREGGROUP' : 'SUREGION';
        let targetType = count > 1 ? 'SUREGGROUP' : 'USER';
        let fileName = 'notification-school';
        let storedFileName = count > 1 ? `'${`${fileName}.png`}'` : profileFileName == null ? `'notification-default.png'` : `'${profileFileName}'`;
        let schoolProfilePath = `img/${fileName}/${fileName}.png`;
        let storedUrl = count > 1 ? `'${schoolProfilePath}'` : profileImageUrl == null ? `'img/notification-default/notification-default.png'` : `'${profileImageUrl}'`;

        let groupTargetIds = 'null';

        if (count > 1) {
          let targetUserIds = rows.map(row => {
            return row.target_user_id;
          });
          groupTargetIds = `'${JSON.stringify(targetUserIds)}'`;
        }

        values.push(`
          ( ${userId}, '${notificationType}', '${targetType}', ${count > 1 ? null : targetUserId}, ${storedFileName}, '${title}', '${body}', '${JSON.stringify(linkInfo)}', ${groupTargetIds})
        `);
      }
    }

    if (values.length > 0) {
      query = `
        insert into oi_notification_info (
          user_id, notification_type_cd, target_type_cd, target_id, profile_file_name, title, body, link_info, group_target_ids
        ) values 
          ${values.join(',')};
      `;
      result = await mysql.execute(query);
    }

    mysql.end();

    let notification = {
      title: '새 친구 추천',
      body: '회원님과 같은 학교, 지역, 관심사를 가진 친구들이 가입하였습니다.'
    };

    let data = {
      type: 'Push',
      payload: JSON.stringify({
        type: 'RECOMUSER',
        title: '새 친구 추천',
        body: '회원님과 같은 학교, 지역, 관심사를 가진 친구들이 가입하였습니다.',
        targetType: 'NOTI'
      })
    };

    if (sendUserTokens.size > 0) {
      await fcm.sendToTokens(Array.from(sendUserTokens), notification, data);
    }

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
