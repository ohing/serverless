'use strict';

const search = require(`@ohing/ohingsearch`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 검색전략 체크
 * @method POST checkSearchStrategy
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}
    let searchText = parameters.searchText
    let searchInfo = search.parse(searchText)

    console.log(searchInfo)

    return response.result(200, searchInfo)
  
  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}