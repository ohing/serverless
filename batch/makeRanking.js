'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()


/**
 * @author Karl <karl@ohing.net> 
 * @description 랭킹 산정
 * @method POST makeRanking
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    // let query = `
    //   select 
    //     user_id
    //   from
    //     (select 
    //       user_id, sum(feed_cnt) feed_count
    //     from
    //       oi_user_daily_stats
    //     where
    //       ymd > date_format(date_add(now(), interval -8 day), '%Y%m%d')
    //       and feed_cnt > 0
    //     group by 
    //       user_id) a
    //   where 
    //     a.feed_count > 2
    //   limit
    //     100;
    // `
    // let result = await mysql.execute(query)
    // if (result.length == 0) {
    //   mysql.end()
    //   return response.result(404)
    // }

    await mysql.beginTransaction()

    let query = `
      delete from 
        oi_user_rank
      where
        rank_ymd = date_format(now(), '%Y%m%d');
    `
    await mysql.execute(query)

    // query = `
    //   select 
    //     a.user_id
    //   from
    //     (select 
    //       uds.user_id, 
    //       sum(uds.feed_cnt) feed_cnt, sum(uds.profile_view_cnt) profile_view_cnt, 
    //       sum(uds.follower_cnt) follower_cnt
    //     from
    //       oi_user_daily_stats uds
    //       join oi_user_info ui on uds.user_id = ui.user_id
    //     where
    //       ui.user_status_cd = 'ACTIVE'
    //       and ui.user_type_cd = 'USER'
    //       and ui.profile_open_type_cd != 'CLOSE'
    //       and date(uds.ymd) > date(curdate() - interval 30 day)
    //     group by 
    //       uds.user_id) a
    //   order by 
    //     (a.follower_cnt * 20 + a.profile_view_cnt * 4 + a.feed_cnt * 3) desc
    //   limit 100;
    // `

    query = `
      select 
        x.user_id
      from 
      (
      -- 사용자 활동 내역
      select 
            uds.user_id, 
              sum(uds.feed_cnt) feed_cnt, sum(uds.profile_view_cnt) profile_view_cnt, 
              sum(uds.follower_cnt) follower_cnt
              from
                oi_user_daily_stats uds
                join oi_user_info ui on uds.user_id = ui.user_id
              where
                ui.user_status_cd = 'ACTIVE'
                and ui.user_type_cd = 'USER'
                and ui.profile_open_type_cd != 'CLOSE'
                and date(uds.ymd) > date(curdate() - interval 30 day)
              group by 
                uds.user_id
      ) x
      join (
        -- 미디어가 포함된 게시물 3건 이상 작성한 유저
        select m3.reg_user_id, count(*) m3_cnt
        from (
          select fi.reg_user_id, fi.feed_id, count(fmi.feed_id)
          from oi_feed_info fi
          join oi_feed_media_info fmi on fi.feed_id = fmi.feed_id
          group by fi.feed_id
          having count(fmi.feed_id) >= 1
        ) m3
        group by m3.reg_user_id
        having count(*) >= 3
      ) y on x.user_id = y.reg_user_id 
      join (
        -- 최근 일주일 게시물
        select fi2.reg_user_id, count(*) w1_cnt
        from oi_feed_info fi2
        where date(fi2.reg_dt) > date(curdate() - interval 7 day)
        group by fi2.reg_user_id
      ) z on x.user_id = z.reg_user_id 
      order by 
              (x.follower_cnt * 20 + x.profile_view_cnt * 4 + x.feed_cnt * 3) desc
            limit 100;
      ;
    `
    let newRankers = await mysql.execute(query)

    query = `
      select
        ur.rank, ur.user_id
      from
        oi_user_rank ur
      where
        ur.rank_ymd = date_format(date_add(now(), interval -1 day), '%Y%m%d') 
      order by 
        ur.rank asc;      
    `
    let oldRankers = await mysql.execute(query)
    console.log(newRankers, oldRankers)

    let values = []

    for (let i = 0; i < newRankers.length; i++) {
      let newRanker = newRankers[i]
      let oldRanker = oldRankers.find(row => {
        return row.user_id == newRanker.user_id
      })

      let newRank = i + 1
      console.log(newRank, (oldRanker || {}).rank)
      let change = oldRanker == null ? 0 : oldRanker.rank - newRank
      let isNew = oldRanker == null ? 'Y' : 'N'

      values.push(
        `(date_format(now(), '%Y%m%d'), 'Top100', ${newRank}, ${change}, '${isNew}', ${newRanker.user_id})`
      )
    }

    query = `
      insert into oi_user_rank (
        rank_ymd, rank_type_cd, rank, \`change\`, new_yn, user_id
      ) values 
        ${values.join(',')};
    `

    console.log(query)

    await mysql.execute(query)
    await mysql.commit()

    mysql.end()

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendRankerPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({})
      })
    }
    console.log(params)

    await lambda.invoke(params).promise()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    await mysql.rollback()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
