'use strict';

const fcm = require(`@ohing/ohingfcm-${process.env.stage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description FCM 구독 관련 처리에 걸리는 시간을 단축시키기 위하여 백그라운드 로직으로 돌림
 * @method POST processFCMSubscribe
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}

    let steps = parameters.steps

    for (let step of steps) {

      let tokens = step.tokens
      let topics = step.topics

      if (step.type == 'subscribe') {

        if (topics.length > 0) {
          for (let token of tokens) {
            await fcm.subscribeAll(token, topics)
          }
        }

      } else {

        if (topics.length == 0) {
          await fcm.unsubscribeAllFrom(tokens)
        } else {
          for (let token of tokens) {
            await fcm.unsubscribeAll(token, topics)
          }
        }
      }
    }

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
