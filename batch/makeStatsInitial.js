'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description stats 마이그레이션
 * @method POST makeStatsInitial
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {
    
    let dateBegin = '2021-01-01'
    let dateLimit = '2021-01-01'

    let query = `
      select 
        fi.reg_user_id user_id, date_format(fi.reg_dt, '%Y%m%d') date, count(fi.reg_user_id) count
      from 
        oi_feed_info fi
        join oi_user_info ui on fi.reg_user_id = ui.user_id
      where
        ui.user_status_cd = 'ACTIVE'
        and fi.reg_dt >= '${dateBegin}'
        and fi.reg_dt < '${dateLimit}'
      group by
        fi.reg_user_id, date_format(fi.reg_dt, '%Y%m%d')
      order by 
        fi.reg_dt asc
      limit 1000;
    `
    let result = await mysql.execute(query)

    let users = {}

    result.forEach(row => {
      let dateKey = row.date
      if (users[row.user_id] == null) {
        users[row.user_id] = {}
      }

      if (users[row.user_id][dateKey] == null) {
        users[row.user_id][dateKey] = {}
      }
      users[row.user_id][dateKey].feed_cnt = row.count
    })
    
    query = `
      select 
        pvl.user_id, date_format(pvl.request_dt, '%Y%m%d') date, count(pvl.user_id) count
      from
        oi_user_profile_view_log pvl
          join oi_user_info ui on pvl.user_id = ui.user_id
      where
        ui.user_status_cd = 'ACTIVE'
        and pvl.request_dt >= '${dateBegin}'
        and pvl.request_dt < '${dateLimit}'
      group by 
        pvl.user_id, date_format(pvl.request_dt, '%Y%m%d');
    `
    result = await mysql.execute(query)
    
    result.forEach(row => {
      let dateKey = row.date
      if (users[row.user_id] == null) {  
        users[row.user_id] = {}
      }

      if (users[row.user_id][dateKey] == null) {
        users[row.user_id][dateKey] = {}
      }

      users[row.user_id][dateKey].profile_view_cnt = row.count
    })

    query = `
      select
        reg_user_id user_id, date, sum(count) count
      from 
        (select 
          fi.reg_user_id, ll.target_id, date_format(ll.like_dt, '%Y%m%d') date, count(ll.target_id) count
        from
          oi_feed_info fi
          left join oi_like_log ll on fi.feed_id = ll.target_id
        where 
          ll.like_target_type_cd = 'FEED'
          and ll.like_dt >= '${dateBegin}'
          and ll.like_dt < '${dateLimit}'
        group by 
          ll.target_id, date_format(ll.like_dt, '%Y%m%d')) g
      group by
        g.reg_user_id, g.date;
    `
    result = await mysql.execute(query)

    result.forEach(row => {
      let dateKey = row.date
      if (users[row.user_id] == null) {  
        users[row.user_id] = {}
      }

      if (users[row.user_id][dateKey] == null) {
        users[row.user_id][dateKey] = {}
      }

      users[row.user_id][dateKey].feed_like_cnt = parseInt(row.count)
    })

    query = `
      select 
        ur.user_id, date_format(ur.accpt_dt, '%Y%m%d') date, count(ur.user_id) count
      from
        oi_user_relation ur
      where
        ur.relation_type_cd = '01'
          and ur.accpt_dt is not null
          and ur.accpt_dt >= '${dateBegin}'
          and ur.accpt_dt < '${dateLimit}'
      group by
        ur.user_id, date_format(ur.accpt_dt, '%Y%m%d')
    `
    result = await mysql.execute(query)

    result.forEach(row => {
      let dateKey = row.date
      if (users[row.user_id] == null) {  
        users[row.user_id] = {}
      }

      if (users[row.user_id][dateKey] == null) {
        users[row.user_id][dateKey] = {}
      }

      users[row.user_id][dateKey].follower_cnt = parseInt(row.count)
    })

    let values = []

    Object.keys(users).forEach(userIdKey => {
      let user = users[userIdKey]

      Object.keys(user).forEach(dateKey => {
        let object = user[dateKey]

        values.push(
          `( ${userIdKey}, '${dateKey}', ${object.feed_cnt || 0}, ${object.profile_view_cnt || 0}, ${object.follower_cnt || 0}, ${object.feed_like_cnt || 0} )`
        )
      })
    })

    query = `
      insert ignore into oi_user_daily_stats (
        user_id, ymd, feed_cnt, profile_view_cnt, follower_cnt, feed_like_cnt
      ) values 
        ${values.join(',')};
    `
    await mysql.execute(query)
    
    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
