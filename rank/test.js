module.getRanks = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      minimum: 1,
      maximum: 50,
      ymd: '20211005'
    })
  }
  return await require('./getRanks.js').handler(request)
}

// module. = async () => {
//   return await require('./.js').handler()
// }


test = async () => {

  const environment = require('../environment.js')
  environment.setEnvironment()
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()