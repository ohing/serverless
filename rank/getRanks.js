'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 랭킹 조회
 * @method GET getRanks
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    // const parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    let minimum = parameters.minimum == null ? 1 : parseInt(parameters.minimum)
    let maximum = parameters.maximum == null ? 30 : parseInt(parameters.maximum)

    if (minimum >= maximum) {
      return response.result(400)
    }

    minimum = parseInt(minimum)
    maximum = parseInt(maximum)

    let ymd = (parameters.ymd || '').trim()

    let ymdCondition = ymd.length == 0 ? `
      and ur.rank_ymd = date_format(now(), '%Y%m%d') 
    ` : `
      and ur.rank_ymd = '${ymd}'
    `


    let query = `
      select
        ur.rank, ur.change, ur.new_yn is_new, ur.user_id, ui.user_login_id account_id,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) file_name,
        sh.school_short_nm, sh.grade, sh.class classe,
        (select count(*) from oi_feed_info where reg_user_id = ur.user_id) feed_count,
        (select count(*) from oi_user_relation where user_id = ur.user_id and relation_type_cd = '01' and accpt_dt is not null) follower_count,
        (select count(*) from oi_user_relation where relation_user_id = ur.user_id and relation_type_cd = '01' and accpt_dt is not null) following_count
      from
        oi_user_rank ur 
        join oi_user_info ui on ur.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on fi.file_id = upi.file_id
        left join oi_user_school_history sh on ui.user_id = sh.user_id and sh.school_hist_cd = 'CURR'
      where
        ur.rank_type_cd = 'Top100'
        ${ymdCondition}
        and ur.rank >= ${minimum} and ur.rank <= ${maximum}
      order by 
        ur.rank asc;
    `
    console.log(query)
    let result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(200, [])
    }

    let rankers = result.map(row => {
      row.is_new = row.is_new == 'Y'
      row.medias = []
      // row.feed_count = 19999
      // row.follower_count = 525252
      // row.following_count = 5252
      return row
    })

    let userIds = rankers.map(ranker => {
      return ranker.user_id
    })

    query = `
      SELECT 
        fmi.reg_user_id user_id, 
        fmi.media_seq, 
        fmi.feed_id, 
        fi.org_file_nm media_url, 
        fi.media_type_cd media_type, 
        substring_index(fi.org_file_nm, '/', -1) media_file_name
      FROM (
        SELECT 		
          a.*,
          @rn := case when a.reg_user_id != @reg_user_id then 1 else @rn + 1 end rn, 
          @reg_user_id := a.reg_user_id x
        FROM (
          SELECT f.*, c.anony_yn
          FROM oi_feed_media_info f
          JOIN oi_feed_info c ON f.feed_id = c.feed_id AND c.anony_yn != 'Y'
          AND f.reg_user_id in (${userIds.join(',')})
        ) a, 
        (select @reg_user_id := '', @rn := 0) b
        WHERE a.media_seq = 1
        ORDER BY a.reg_user_id, a.reg_dt DESC, a.feed_id DESC
      ) fmi
      LEFT JOIN oi_file_info fi ON fmi.file_id = fi.file_id
      WHERE rn <= 3 
      ;
    `
    console.log(query)

    result = await mysql.execute(query)
    
    for (let i = 0; i < rankers.length; i++) {
      for (let j = 0; j < result.length; j++) {
        let media = result[j]
        if (media.user_id == rankers[i].user_id) {
          rankers[i].medias.push(media) 
        }
      }
    }

    mysql.end()

    console.log('mysql ended')

    let readable = await redis.readable(3)

    let onlineKeys = rankers.map(user => {
      return `alive:${user.user_id}`
    })

    let onlines = await readable.mget(onlineKeys)

    redis.end()
    console.log('redis ended')

    for (let i = 0; i < onlines.length; i++) {
      rankers[i].is_online = onlines[i] != null
    }
    
    return response.result(200, rankers)
  
  } catch(e) {

    console.error(e)
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}