const fs = require('fs')
const yaml = require('yaml')

exports.setEnvironment = (isSecondDepth = false) => {

  const file = fs.readFileSync(isSecondDepth ? '../../properties.yml' : '../properties.yml', 'utf8')
  const properties = yaml.parse(file)

  process.env.stage = properties.stage
  process.env.databaseStage = 'local'
  process.env.mediaCloudFront = properties.mediaCloudFront
  process.env.mediaBucket = properties.mediaBucket
  process.env.shareSecret = properties.shareSecret
};
