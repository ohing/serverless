'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 피드 작성 태그 검색
 * @method POST searchFeedTag
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body || '{}')
    const content = parameters.content || ''
    if (content.length == 0) {
      return response.result(200, [])
    }

    const query = `
      select
        t.tag_val
      from
        oi_tag_info t 
        join oi_feed_tag ft on t.tag_id = ft.tag_id
        left join oi_banned_search_text b on t.tag_val = b.search_text
      where 
        t.tag_val like '%${content}%'
        and ft.tag_id is not null
        and b.search_text is null
      group by tag_val
      order by 
        t.tag_val asc
      limit 30;
    `
    let result = await mysql.execute(query)
    result = result.map(element => { return element.tag_val })

    mysql.end()

    return response.result(200, result)

  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}