'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const response = require(`@ohing/ohingresponse`)

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3();

/**
 * @author Karl <karl@ohing.net> 
 * @description 미디어 체크
 * @method POST checkMediaExists
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}
    let keys = parameters.keys

    if (!Array.isArray(keys) || keys.length == 0) {
      return response.result(400)
    }

    let results = []

    for (let key of keys) {

      let params = {
        Bucket: process.env.mediaBucket,
        Key: key
      }
      console.log(params) 

      let exists = false

      try {
        let result = await s3.headObject(params).promise()
        exists = result.ETag != null
      } catch (e) {
        console.log(e)
      }

      results.push({
        key: key,
        exists: exists
      })
    }

    return response.result(200, results)

  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
