'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 내가 사용 가능한 이모티콘 목록
 * @method GET getEmoticons

*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query = `
      select
        egi.name, egi.s3_key, egi.version
      from
        oi_emoticon_group_info egi
        left join oi_emoticon_group_purchase egp on egi.emoticon_group_id = egp.emoticon_group_id
      where
        egi.use_yn = 'Y'
        and egi.price = 0
        or (egi.price > 0 and egp.user_id = ${userId})
      order by 
        field(egi.price, 0) asc,
        egi.name asc,
        egp.seq asc;
    `
    let result = await mysql.execute(query)

    mysql.end()

    return response.result(200, result)

  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
