'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 초기데이터 생성 
*/
module.exports.handler = async event => {

  console.log(event)

  try {    

    let tagList = ["블랙핑크", "노래", "아이돌", "크리넥스안심물티슈", "치킨", "퇴근", "점심", "맛집", "집에갈래요", "불금", "롤린", "브레이브걸스", "운전만해"]
    let regionInfo = {
      "doList": [], 
      "cityList": 
      [
        {
          "code": "100276", 
          "codeNm": "경기"
        }, 
        {
          "code": "100260", 
          "codeNm": "서울"
        }, 
        {
          "code": "100291", 
          "codeNm": "경남"
        }, 
        {
          "code": "100267", 
          "codeNm": "부산"
        }, 
        {
          "code": "100277", 
          "codeNm": "인천"
        }, 
        {
          "code": "100285", 
          "codeNm": "경북"
        }, 
        {
          "code": "100272", 
          "codeNm": "대구"
        },
        {
          "code": "100281", 
          "codeNm": "충남"
        }, 
        {
          "code": "100282", 
          "codeNm": "전북"
        }, 
        {
          "code": "100283", 
          "codeNm": "전남"
        }, 
        {
          "code": "100280", 
          "codeNm": "충북"
        }, 
        {
          "code": "100278", 
          "codeNm": "강원"
        }, 
        {
          "code": "100271", 
          "codeNm": "대전"
        }, 
        {
          "code": "100275", 
          "codeNm": "광주"
        }, 
        {
          "code": "100266", 
          "codeNm": "울산"
        }, 
        {
          "code": "100292", 
          "codeNm": "제주"
        }, 
        {
          "code": "100200", 
          "codeNm": "세종"
        }
      ]
    }

    let helpCategory = [
      {
        "code": "",
        "codeNm": "전체",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_all.png"
      },
      {
        "code": "HC004",
        "codeNm": "연애/사랑",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_love.png"
      },
      {
        "code": "HC001",
        "codeNm": "친구/가족",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_friends.png"
      },
      {
        "code": "HC003",
        "codeNm": "학교/일상",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_school.png"
      },
      {
        "code": "HC002",
        "codeNm": "공부/진로",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_study.png"
      },
      {
        "code": "HC006",
        "codeNm": "게임/여가",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_game.png"
      },
      {
        "code": "HC008",
        "codeNm": "운동/취미",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_hobby.png"
      },
      {
        "code": "HC007",
        "codeNm": "패션/뷰티",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_fashion.png"
      },
      {
        "code": "HC009",
        "codeNm": "음악/미술",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_music.png"
      },
      {
        "code": "HC005",
        "codeNm": "감정/기분",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_mood.png"
      },
      {
        "code": "HC010",
        "codeNm": "팬클럽/연예인",
        "iconUrl": "https://d2ro0vwgwjzuou.cloudfront.net/help/category/Ic_category_star.png"
      }
    ]

    let askCategory = [
      {
        "code": "ASK001",
        "codeNm": "가입/탈퇴"
      }, 
      {
        "code": "ASK005",
        "codeNm": "프로필"
      }, 
      {
        "code": "ASK006", 
        "codeNm": "피드"
      }, 
      {
        "code": "ASK004", 
        "codeNm": "도와줘"
      }, 
      {
        "code": "ASK007", 
        "codeNm": 
        "친구찾기/검색"
      }, 
      {
        "code": "ASK008", 
        "codeNm": "채팅"
      }, 
      {
        "code": "ASK009", 
        "codeNm": "오픈채팅"
      }, 
      {
        "code": "ASK999", 
        "codeNm": "기타"
      }
    ]
    
    let faqCategory = [
      {
        "code": "1", 
        "codeNm": "가입/탈퇴"
      }, 
      {
        "code": "2", 
        "codeNm": "프로필"
      }, 
      {
        "code": "3", 
        "codeNm": "친구찾기/검색"
      }, 
      {
        "code": "4", 
        "codeNm": "도와줘"
      }, 
      {
        "code": "5", 
        "codeNm": "랭커"
      }, 
      {
        "code": "6", 
        "codeNm": "오잉챗"
      }, 
      {
        "code": "7", 
        "codeNm": "마이피드"
      }
    ]

    let reportType = [
      {
        "code": "RT011",
        "codeNm": "타인 사칭, 10대 아님"
      },
      {
        "code": "RT012",
        "codeNm": "음란물, 폭력물 게재"
      },
      {
        "code": "RT013",
        "codeNm": "괴롭힘, 혐오발언"
      },
      {
        "code": "RT014",
        "codeNm": "지적 재산권 침해"
      },
      {
        "code": "RT015",
        "codeNm": "스팸, 허가되지 않은 판매"
      },
      {
        "code": "RT999",
        "codeNm": "기타"
      }
    ]

    let query = `
      insert into oi_system_config(
        config_ver, movie_upload_size_limit, movie_upload_time_limit, invite_msg_text, school_ban_limit, recomm_tag_list,
        os_type_cd, app_ver, forced_update_yn, region_info, help_category_list, ask_category_list, faq_category_list, report_type_list, use_yn
      ) values (
        1000, 1000, 60, '오직 10대만을 위한 SNS, 오잉에 초대합니다.\n앱 다운로드▶ bit.ly/ohing_invite', 20, '${JSON.stringify(tagList)}',
        'iOS', '3.0.0', 'Y', '${JSON.stringify(regionInfo)}', '${JSON.stringify(helpCategory)}', '${JSON.stringify(askCategory)}', '${JSON.stringify(faqCategory)}', '${JSON.stringify(reportType)}', 'Y' 
      ), (
        1000, 1000, 60, '오직 10대만을 위한 SNS, 오잉에 초대합니다.\n앱 다운로드▶ bit.ly/ohing_invite', 20, '${JSON.stringify(tagList)}',
        'Android', '3.0.0', 'Y', '${JSON.stringify(regionInfo)}', '${JSON.stringify(helpCategory)}', '${JSON.stringify(askCategory)}', '${JSON.stringify(faqCategory)}', '${JSON.stringify(reportType)}', 'Y' 
      );
    `
    console.log(query)
    // await mysql.execute(query)
    mysql.end()

    return response.result(200, {})

  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
};
