'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 금칙어 검사
 * @method POST checkBannedWord
 * @param content String
 * @returns null 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body)
    var content = parameters.content
    content = content.toLowerCase().trim().replace(/\n/gi, ' ').replace(/\s+/gi, '|')
    const contentArray = content.split('|')

    if (contentArray.length == 0 || (contentArray.length == 1 && contentArray[0] == '')) {
      return response.result(200, {isValid: true})
    }

    let inQueries = []

    for (var i = 0; i < contentArray.length; i++) {
      inQueries.push('?')
    }

    console.log(contentArray)

    content = contentArray.join(',')
    
    const query = `
      select 
        count(*) as count 
      from 
        oi_banned_search_text 
      where 
        search_text in (${inQueries.join(',')})
    `
    const result = await mysql.execute(query, contentArray)
    
    mysql.end()

    if (result[0].count > 0) {
      return response.result(200, {isValid: false}, '입력할 수 없는 단어가 포함되어 있습니다. 확인 후 다시 입력해주세요.')
    } else {
      return response.result(200, {isValid: true})
    }

  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
};
