module.makeConfig = async () => {
  const request = {
    headers: {
      'access-token': '0d3bc0d4-31c2-4799-9f4c-9bfbc29d2249',
      'os-type': 'IOS'
    }
  }
  return await require('./makeConfig.js').handler(request)
}

module.getInit = async () => {
  const request = {
    headers: {
      'access-token': '0d3bc0d4-31c2-4799-9f4c-9bfbc29d2249',
      'os-type': 'IOS'
    }
  }
  return await require('./getInit.js').handler(request)
}

module.getRegion = async () => {
  return await require('./getRegion.js').handler()
}

module.getReportType = async () => {
  return await require('./getReportType.js').handler()
}

module.getRecommendTag = async () => {
  return await require('./getRecommendTag.js').handler()
}

module.getHelpCategory = async () => {
  return await require('./getHelpCategory.js').handler()
}

module.getInquirementCategory = async () => {
  return await require('./getInquirementCategory.js').handler()
}

module.getPhotoEditorData = async () => {
  return await require('./getPhotoEditorData.js').handler()
}

module.getEmoticons = async () => {
  return await require('./getEmoticons.js').handler()
}

module.getAgreements = async () => {
  return await require('./getAgreements.js').handler({headers: {}})
}

module.getEvents = async () => {
  return await require('./getEvents.js').handler({headers: {}})
}

module.getBanners = async () => {
  return await require('./getBanners.js').handler({headers: {}})
}

module.checkBannedWord = async () => {
  const request = {
    body: JSON.stringify({
      content: '오잉마스터1 \''
    })
  }
  return await require('./checkBannedWord.js').handler(request)
}

module.checkBannedWordTest = async () => {
  const request = {
    body: JSON.stringify({
      content: '섹스\ndddd      bbb\nccccc   dldldl'
    })
  }
  return await require('./checkBannedWordTest.js').handler(request)
}

module.searchSchool = async () => {
  const request = {
    body: JSON.stringify({
      content: '인헌'
    })
  }
  return await require('./searchSchool.js').handler(request)
}

module.searchInterestTag = async () => {
  const request = {
    body: JSON.stringify({
      content: '메'
    })
  }
  return await require('./searchInterestTag.js').handler(request)
}

module.searchFeedTag = async () => {
  const request = {
    body: JSON.stringify({
      content: '게임'
    })
  }
  return await require('./searchFeedTag.js').handler(request)
}

module.checkTimeDiffers = async () => {
  const request = {
    body: JSON.stringify({
      timestamp: new Date().getTime()
    })
  }
  return await require('./checkTimeDiffers.js').handler(request)
}

module.getNotificationBadges = async () => {
  const request = {
    headers: {
    }
  }
  return await require('./getNotificationBadges.js').handler(request)
}

module.getEmoticons = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    }
  }
  return await require('./getEmoticons.js').handler(request)
}

module.getSearchTexts = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6f4f8592-7ed5-4aa7-9747-e369fb2142c8'
    }
  }
  return await require('./getSearchTexts.js').handler(request)
}

module.sendBannerTap = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '8b141253-52d9-4cb0-bbad-d2eaff29156e'
    },
    body: JSON.stringify({
      bannerId: 77
    })
  }
  return await require('./sendBannerTap.js').handler(request)
}

module.checkMediaExists = async () => {
  let request = {
    body: JSON.stringify({
      keys: [
        'mov/006FFA2E-4111-4933-AE4B-C0C66AA80041_1617093339415/mp4/006FFA2E-4111-4933-AE4B-C0C66AA80041_1617093339415.mp4'
      ]
    })
  }
  return await require('./checkMediaExists.js').handler(request)
}


test = async () => {
  
  const environment = require('../environment.js')
  environment.setEnvironment()
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()