'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 초기데이터 가져오기.
 * @method GET getInit
 * @param access-token **Header** String, Optional
 * @param os-type ** header ** String(IOS, ANDROID), Required
 * @returns {InitialInfo} InitialInfo
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const osType = event.headers['os-type']

    if (osType == null) {
      return response.result(400)
    }

    let query = `
      select
        config_ver, movie_upload_size_limit, movie_upload_time_limit, invite_msg_text, school_ban_limit, 
        recomm_tag_list, os_type_cd, app_ver, forced_update_yn, region_info, help_category_list, ask_category_list, report_type_list, faq_category_list
      from 
        oi_system_config
      where
        use_yn = 'Y' and os_type_cd = '${osType}'
      order by
        config_id desc;
    `
    let result = await mysql.execute(query)
    result = result[0]
    
    query = `
      select
        banner_id, banner_title, image_url, ext_url target_url, banner_start_ymd, banner_end_ymd, banner_begin_dt banner_begin_at, banner_end_dt banner_end_at, banner_sort_no, 
        landing_target, popup_hidden_title, popup_hidden_day,
        case 
          when landing_target = 1 then 'FEED'
          when landing_target = 2 then 'PROFILE'
          when landing_target = 3 then 'INWEB'
          when landing_target = 4 then 'EXWEB'
          when landing_target = 5 then 'FEEDWRITE'
          when landing_target = 6 then 'HELPWRITE'
          when landing_target = 7 then 'NOTICE'
          when landing_target = 8 then 'TAB'
          else null
        end target_type, 
        target_id,
        service_type_cd
      from
        oi_banner_info
      where
        open_yn = 'Y' and del_yn = 'N'
        and banner_begin_dt<=now()
        and banner_end_dt>=now()
      order by
        banner_sort_no asc, banner_title asc;
    `
    let bannerResult = await mysql.execute(query)

    result.eventList = bannerResult.filter(banner => {
      return banner.service_type_cd == 'POPUP'
    })

    for (let event of result.eventList) {
      event.event_id = event.banner_id
      event.event_title = event.banner_title
      event.event_url = event.image_url,
      event.event_start_ymd = event.banner_start_ymd
      event.event_end_ymd = event.banner_end_ymd
      event.event_begin_at = event.banner_begin_at
      event.event_end_at = event.banner_end_at

      delete event.banner_id
      delete event.banner_title
      delete event.image_url,
      delete event.banner_start_ymd
      delete event.banner_end_ymd
      delete event.banner_sort_no
      delete event.service_type_cd
      delete event.banner_begin_at
      delete event.banner_end_at
    }

    result.bannerInfo = {}

    result.bannerInfo.home = bannerResult.filter(banner => {
      return banner.service_type_cd == 'HOME'
    })
    result.bannerInfo.home.map(home => {
      delete home.service_type_cd
      delete home.popup_hidden_title
      delete home.popup_hidden_day
      return home
    })
    result.bannerInfo.homeEvent = bannerResult.filter(banner => {
      return banner.service_type_cd == 'HOME_EVENT'
    })
    result.bannerInfo.homeEvent.map(homeEvent => {
      delete homeEvent.service_type_cd
      delete homeEvent.popup_hidden_title
      delete homeEvent.popup_hidden_day
      return homeEvent
    })
    result.bannerInfo.chat = bannerResult.filter(banner => {
      return banner.service_type_cd == 'CHAT'
    })
    result.bannerInfo.chat.map(chat => {
      delete chat.service_type_cd
      delete chat.popup_hidden_title
      delete chat.popup_hidden_day
      return chat
    })
    result.bannerInfo.feed = bannerResult.filter(banner => {
      return banner.service_type_cd == 'FEED'
    })
    result.bannerInfo.feed.map(feed => {
      delete feed.service_type_cd
      delete feed.popup_hidden_title
      delete feed.popup_hidden_day
      return feed
    })

    result.bannerInfo.rank = bannerResult.filter(banner => {
      return banner.service_type_cd == 'RANK'
    })
    result.bannerInfo.rank.map(rank => {
      delete rank.service_type_cd
      delete rank.popup_hidden_title
      delete rank.popup_hidden_day
      return rank
    })

    result.bannerInfo.content = bannerResult.filter(banner => {
      return banner.service_type_cd == 'CONTENT'
    })
    result.bannerInfo.content.map(content => {
      delete content.service_type_cd
      delete content.popup_hidden_title
      delete content.popup_hidden_day
      return content
    })
    

    query = `
      select
        terms_id, terms_type_cd, terms_title, terms_url, must_yn
      from
        oi_terms
      where
        use_yn = 'Y' and del_yn = 'N'
      order by 
        sort_no asc;
    `
    result.agreements = await mysql.execute(query)

    mysql.end()

    return response.result(200, result)

  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
};
