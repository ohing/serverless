'use strict';

const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 거주지역 데이터 가져오기 
 * @method POST 서버 시간과의 차이 체크
 * @returns 
*/
module.exports.handler = async event => {

  console.log(event)

  const parameters = JSON.parse(event.body || '{}')
  const timestamp = parameters.timestamp

  if (timestamp == null) {
    return response.result(400)
  }

  const difference = new Date().getTime() - timestamp

  return response.result(200, {difference: difference})
};
