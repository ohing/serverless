'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const response = require(`@ohing/ohingresponse`)

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 배너 누른 정보
 * @method POST sendBannerTap
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let parameters = JSON.parse(event.body || '{}') || {}
    let bannerId = parameters.bannerId
    
    if (bannerId == null) {
      return response.result(400)
    }

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: 2,
          type: 'BANNERTAP',
          actionId: bannerId,
          log: '광고 배너 감상'
        })
      })
    }
    console.log(logParams)

    await lambda.invoke(logParams).promise()

    return response.result(200, {})

  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
