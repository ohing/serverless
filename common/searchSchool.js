'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 학교정보 검색 GET
 * @method POST searchSchool
 * @returns [Tag] 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body || '{}')
    const content = parameters.content || ''
    var where = ''
    if (content.length > 0) {
      where = `where school_nm like '%${content}%'`
    }

    const query = `
      select
        school_id, school_nm, school_short_nm, school_gubun_cd, ifnull(adres, '') as adres
      from 
        oi_school_info
      ${where}
      order by
        field(school_gubun_cd, 'ELEM', 'MIDD', 'HIGH'), 
        school_nm
      limit 
        30
    `

    const result = await mysql.execute(query)

    mysql.end()

    return response.result(200, result)

  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
