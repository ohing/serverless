'use strict';

const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 이벤트 확인용
 * @method GET echo
*/
module.exports.handler = async event => {

  console.log(event)
  return response.result(200, event)
}
