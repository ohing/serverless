'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 저장된 검색어 
 * @method getSearchTexts
 * @returns [Tag] 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query = `
      select
        search_text
      from 
        oi_search_history 
      where 
        except_yn = 'N'
      group by 
        search_text_id 
      order by 
        count(search_text_id) desc 
      limit 
        15;
    `
    let result = await mysql.execute(query)
    console.log(result)
    let populars = result.map(row => {
      return row.search_text
    })

    let readable = await redis.readable(5)
    result = await readable.zrevrange(`search-recent-${userId}`, 0, 9, 'withscores')

    let count = result.length / 2

    let recents = []

    for (let i = 0; i < count; i++) {
      let recent = JSON.parse(result[i * 2])
      recent.score = parseInt(result[i * 2 + 1])
      let searchAt = recent.searchAt
      recent.searchAt = searchAt.replace(/(\d{4})(\d{2})(\d{2})(\d{2})/, "$1-$2-$3T$4:00:00.000Z")
      recents.push(recent)
    }

    mysql.end()
    redis.end()

    return response.result(200, {popular: populars, recent: recents})

  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
