'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 최근검색어 삭제
 * @method deleteRecentSearchText
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let score = parseInt(event.pathParameters.searchText)

    console.log(score)

    let key = `search-recent-${userId}`

    let writable = await redis.writable(5)
    await writable.zremrangebyscore(key, score, score)
    
    redis.end()

    return response.result(200, {})

  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
