'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 관심사 태그 검색 GET
 * @method POST searchInterestTag
 * @returns [Tag] 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body || '{}')
    const content = parameters.content || ''
    var tagWhere = ''
    if (content.length > 0) {
      tagWhere = `and t.tag_val like '%${content}%'`
    }

    const query = `
      select
        t.tag_val
      from
        oi_tag_info t left join oi_banned_search_text b on t.tag_id = b.banned_id
      where 
        b.search_text is null
        ${tagWhere}
      order by 
        t.tag_val asc
      limit 30;
    `
    let result = await mysql.execute(query)
    result = result.map(element => { return element.tag_val })

    mysql.end()

    return response.result(200, result)

  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
