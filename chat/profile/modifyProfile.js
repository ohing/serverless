'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅프로필 수정
 * @method PATCH modifyProfile
 * @returns  
*/

module.fileCheck = async (key) => {
  try {
    let params = {
      Bucket: process.env.mediaBucket,
      Key: profileKey
    }
    console.log(params) 

    let result = await s3.headObject(params).promise()
    return result.ETag != null

  } catch (e) {
    return false
  }
}

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let profileId = event.pathParameters.profileId

    let parameters = JSON.parse(event.body || '{}') || {}

    let imageKey = parameters.imageKey
    let nickname = (parameters.nickname || '').trim()
    let selfIntroduction = (parameters.selfIntroduction || '').trim()

    let nicknameLength = Array.from(nickname.split(/[\ufe00-\ufe0f]/).join("")).length;
    let selfIntroductionLength = selfIntroduction.length == 0 ? 0 : Array.from(selfIntroduction.split(/[\ufe00-\ufe0f]/).join("")).length;

    if ((parameters.nickname != null && nicknameLength < 2 || nicknameLength > 20) || selfIntroductionLength.length > 20) {
      return response.result(400)
    }

    let sets = []

    if (imageKey != null) {

      if (!imageKey.includes('/') && imageKey.includes('.')) {
        let fileNames = imageKey.split('.')
        let fileName = fileNames[0]
        let fileExtension = fileNames[1]
        imageKey = `img/${fileName}/${fileName}.${fileExtension}`
      }

      // let checkKey1
      // let checkKey2

      // if (!imageKey.includes('/') && imageKey.includes('.')) {
      //   let fileNames = imageKey.split('.')
      //   let fileName = fileNames[0]
      //   let fileExtension = fileNames[1]
      //   checkKey1 = `img/${fileName}/${fileName}.${fileExtension}`
      //   checkKey2 = `img/source/${fileName}.${fileExtension}`
      // }

      // for (;;) {
      //   if (module.fileCheck(imageKey)) {
      //     break
      //   }
      //   if (checkKey1 == null) {
      //     response.result(404, null, '이미지가 존재하지 않습니다.')
      //   }

      //   if (imageKey == checkKey1) {
      //     imageKey = checkKey2
      //   } else if (imageKey == checkKey2) {
      //     response.result(404, null, '이미지가 존재하지 않습니다.')
      //   } else {
      //     imageKey = checkKey1
      //   }
      // }

      let query = `
        insert into oi_file_info (
          media_type_cd, org_file_nm, encoded_yn
        ) value (
          'IMAGE', '${imageKey}', 'Y'
        );
      `
      let result = await mysql.execute(query)

      let fileId = result.insertId
      sets.push(`image_id = ${fileId}`)
    }

    if (parameters.nickname != null) {
      nickname = await mysql.escape(nickname)
      sets.push(`nick_nm = ${nickname}`)
    }

    if (parameters.selfIntroduction != null) {
      selfIntroduction = selfIntroduction.length == 0 ? 'null' : await mysql.escape(selfIntroduction)
      sets.push(`self_intro_content = ${selfIntroduction}`)
    }

    if (sets.length > 0) {
      let query = `
        update 
          oi_chat_profile_info
        set
          ${sets.join(',')}
        where
          chat_profile_id = ${profileId}
      `
      await mysql.execute(query)
    }
    
    let query = `
      select
        pi.chat_profile_id, pi.nick_nm nickname, pi.self_intro_content self_introduction, fi.org_file_nm image_url, substring_index(fi.org_file_nm, '/', -1) file_name
      from
        oi_chat_profile_info pi
        left join oi_file_info fi on pi.image_id = fi.file_id
      where
        pi.chat_profile_id = ${profileId};
    `
    let result = await mysql.execute(query)

    mysql.end()

    return response.result(200, result[0])
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
