'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 상대방 채팅프로필 조회
 * @method GET getProfile
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let chatProfileId =  event.pathParameters.chatProfileId

    let query = `
      select
        pi.user_id, pi.chat_profile_id, pi.nick_nm nickname, pi.self_intro_content self_introduction, fi.org_file_nm image_url, substring_index(fi.org_file_nm, '/', -1) file_name
      from
        oi_chat_profile_info pi
        left join oi_file_info fi on pi.image_id = fi.file_id
      where
        pi.chat_profile_id = ${chatProfileId};  
    `
    let result = await mysql.execute(query)

    mysql.end()

    if (result.length == 0) {
      return response.result(404)
    }

    return response.result(200, result[0])
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
