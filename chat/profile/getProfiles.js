'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅프로필 조회
 * @method GET getProfiles
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query = `
      select 
        null chat_profile_id, ui.user_login_id nickname, ui.self_intro_content self_introduction, fi.org_file_nm image_url, substring_index(fi.org_file_nm, '/', -1) file_name
      from
        oi_user_info ui
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where
        ui.user_id = ${userId};
    `
    let result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(404)
    }
    
    let profiles = result

    query = `
      select
        pi.chat_profile_id, pi.nick_nm nickname, pi.self_intro_content self_introduction, fi.org_file_nm image_url, substring_index(fi.org_file_nm, '/', -1) file_name
      from
        oi_chat_profile_info pi
          left join oi_file_info fi on pi.image_id = fi.file_id
      where
        pi.user_id = ${userId};  
    `
    result = await mysql.execute(query)

    mysql.end()

    return response.result(200, profiles.concat(result))
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
