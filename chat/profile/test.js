module.getProfiles = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    }
  }
  return await require('./getProfiles.js').handler(request)
}

module.createProfile = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    body: JSON.stringify({
      nickname: '이것은닉네임',
      selfIntroduction: '이거슨자기소개ㅐㅐㅐㅐㅐ'
    })
  }
  return await require('./createProfile.js').handler(request)
}

module.modifyProfile = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'cdc1847c-918b-4617-bb14-d98aff47285e'
    },
    pathParameters: {
      profileId: 27
    },
    body: JSON.stringify({
      nickname: '😂😂😂😂😂😂😂☺️☺️☺️☺️☺️☺️☺️☺️☺️☺️☺️☺️1'
    })
  }
  return await require('./modifyProfile.js').handler(request)
}

module.deleteProfile = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      profileId: 60
    }
  }
  return await require('./deleteProfile.js').handler(request)
}

module.getProfile = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      chatProfileId: 44
    }
  }
  return await require('./getProfile.js').handler(request)
}


test = async () => {

  const environment = require('../../environment.js')
  environment.setEnvironment(true)
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()