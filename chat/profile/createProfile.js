'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅프로필 생성
 * @method PUT createProfile
 * @returns  
*/

module.fileCheck = async (key) => {
  return true
  // try {
  //   let params = {
  //     Bucket: process.env.mediaBucket,
  //     Key: profileKey
  //   }
  //   console.log(params) 

  //   let result = await s3.headObject(params).promise()
  //   return result.ETag != null

  // } catch (e) {
  //   return false
  // }
}

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let parameters = JSON.parse(event.body || '{}') || {}
    let imageKey = parameters.imageKey
    let nickname = (parameters.nickname || '').trim()
    let selfIntroduction = (parameters.selfIntroduction || '').trim()

    let nicknameLength = Array.from(nickname.split(/[\ufe00-\ufe0f]/).join("")).length;
    let selfIntroductionLength = selfIntroduction.length == 0 ? 0 : Array.from(selfIntroduction.split(/[\ufe00-\ufe0f]/).join("")).length;

    if (nicknameLength < 2 || nicknameLength > 20 || selfIntroductionLength > 20) {
      return response.result(400)
    }

    let query
    let result
    let valueParameters

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅프로필을 변경하실 수 없습니다.')
    }

    query = `
      select
        count(*) count
      from 
        oi_chat_profile_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)

    if (result[0].count >= 2) {
      mysql.end()
      return response.result(409, null, '채팅프로필은 2개까지만 생성 가능합니다.')
    }

    let fileId = 'null'

    if (imageKey != null) {

      if (!imageKey.includes('/') && imageKey.includes('.')) {
        let fileNames = imageKey.split('.')
        let fileName = fileNames[0]
        let fileExtension = fileNames[1]
        imageKey = `img/${fileName}/${fileName}.${fileExtension}`
      }

      // let checkKey1
      // let checkKey2

      // if (!imageKey.includes('/') && imageKey.includes('.')) {
      //   let fileNames = imageKey.split('.')
      //   let fileName = fileNames[0]
      //   let fileExtension = fileNames[1]
      //   checkKey1 = `img/${fileName}/${fileName}.${fileExtension}`
      //   checkKey2 = `img/source/${fileName}.${fileExtension}`
      // }

      // for (;;) {
      //   if (module.fileCheck(imageKey)) {
      //     break
      //   }
      //   if (checkKey1 == null) {
      //     response.result(404, null, '이미지가 존재하지 않습니다.')
      //   }

      //   if (imageKey == checkKey1) {
      //     imageKey = checkKey2
      //   } else if (imageKey == checkKey2) {
      //     response.result(404, null, '이미지가 존재하지 않습니다.')
      //   } else {
      //     imageKey = checkKey1
      //   }
      // }

      let query = `
        insert into oi_file_info (
          media_type_cd, org_file_nm, encoded_yn
        ) value (
          'IMAGE', '${imageKey}', 'Y'
        );
      `
      let result = await mysql.execute(query)

      fileId = result.insertId
    }

    selfIntroduction = (selfIntroduction.trim().length == 0) ? null : selfIntroduction.trim()

    // query = `
    //   select
    //     count(*)
    //   from
    //     oi_chat_profile_info
    //   where
    //     nick_nm = ${nickname};
    // `
    // result = await mysql.execute(query)
    
    query = `
      insert into oi_chat_profile_info (
        user_id, nick_nm, self_intro_content, image_id
      ) values (
        ?, ?, ?, ?
      );
    `
    valueParameters = [userId, nickname, selfIntroduction, fileId]
    result = await mysql.execute(query, valueParameters)

    query = `
      select
        pi.chat_profile_id, pi.nick_nm nickname, pi.self_intro_content self_introduction, fi.org_file_nm image_url, substring_index(fi.org_file_nm, '/', -1) file_name
      from
        oi_chat_profile_info pi
        left join oi_file_info fi on pi.image_id = fi.file_id
      where
        pi.chat_profile_id = ${result.insertId};  
    `
    result = await mysql.execute(query)

    mysql.end()

    return response.result(200, result[0])
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
