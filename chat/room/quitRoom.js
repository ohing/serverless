'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 나가기
 * @method POST quitRoom
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let roomId = event.pathParameters.roomId;

    let query;
    let result;
    let valueParameters;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    if (openType == 'CLOSE') {
      mysql.end();
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.');
    }

    query = `
      select
        room_type_cd, room_status_cd
      from
        oi_chat_room_info
      where
        room_id = ${roomId};
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(404);
    }

    let type = result[0].room_type_cd;
    let status = result[0].room_status_cd;

    if (status != 'ACTIVE') {

      query = `
        delete from 
          oi_chat_user_info
        where
          room_id = ${roomId}
          and user_id = ${userId};
      `;
      await mysql.execute(query);

      mysql.end();
      return response.result(200, {});
    }

    query = `
      select
        master_yn, deport_yn
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId};
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(403, null, '권한이 없습니다.');
    }

    let isMaster = result[0].master_yn == 'Y';
    let deported = result[0].deport_yn == 'Y';

    let nickname;
    let profileFileName;

    let roomTitle;

    let nextMasterId;
    let nextMasterNickname;


    if (type == 'OPEN') {

      query = `
        select 
          case when cpi.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
          case when cpi.chat_profile_id is null then substring_index(fi.org_file_nm, '/', -1) else substring_index(fi2.org_file_nm, '/', -1) end profile_file_name
        from
          oi_chat_user_info cui
          join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where
          cui.room_id = ${roomId}
          and cui.user_id = ${userId};
      `;
      result = await mysql.execute(query);

      nickname = result[0].nickname;
      profileFileName = result[0].profile_file_name;

    } else {

      query = `
        select 
          ui.user_login_id, substring_index(fi.org_file_nm, '/', -1) profile_file_name
        from
          oi_user_info ui
          left join oi_user_profile_image upi on ui.user_id = upi.user_id
          left join oi_file_info fi on upi.file_id = fi.file_id
        where
          ui.user_id = ${userId};
      `;
      result = await mysql.execute(query);
      nickname = result[0].user_login_id;
      profileFileName = result[0].profile_file_name;
    }

    if (type == 'ONE') {

      query = `
        update
          oi_chat_user_info
        set
          use_yn = 'N'
        where
          room_id = ${roomId}
          and user_id = ${userId};
      `;
      await mysql.execute(query);

    } else {

      if (deported) {

        query = `
          update
            oi_chat_user_info
          set
            use_yn = 'N', conn_yn = 'N'
          where
            room_id = ${roomId}
            and user_id = ${userId};
        `;
        await mysql.execute(query);

      } else {
        query = `
          delete from 
            oi_chat_user_info
          where
            room_id = ${roomId}
            and user_id = ${userId};
        `;
        await mysql.execute(query);
      }

      if (type == 'OPEN') {

        if (isMaster) {

          query = `
            select 
              title
            from
              oi_chat_room_info
            where
              room_id = ${roomId};
          `;
          result = await mysql.execute(query);
          roomTitle = result[0].title;

          query = `
            select
              cui.user_id,
              case when cpi.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname
            from
              oi_chat_user_info cui
              join oi_user_info ui on cui.user_id = ui.user_id
              left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
            where
              cui.room_id = ${roomId}
              and cui.use_yn = 'Y'
              and cui.deport_yn = 'N'
            order by 
              cui.join_dt asc
            limit 1
          `;
          result = await mysql.execute(query);
          if (result.length > 0) {
            nextMasterId = result[0].user_id;
            nextMasterNickname = result[0].nickname;
          }
        }

        query = `
          select
            push_auth_key
          from
            oi_user_device_info
          where
            user_id = ${userId}
            and login_yn = 'Y'
            and push_auth_key is not null and push_auth_key  <> '';
        `;
        result = await mysql.execute(query);

        if (result.length > 0) {
          let fcmToken = result[0].push_auth_key;
          if (fcmToken != null && fcmToken.length > 0) {
            await lambda.invoke({
              FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
              InvocationType: "Event",
              Payload: JSON.stringify({
                body: JSON.stringify({
                  steps: [
                    {
                      type: 'unsubscribe',
                      tokens: [fcmToken],
                      topics: [`openchat-${roomId}`]
                    }
                  ]
                })
              })
            }).promise();
          }
        }
      }
    }
    if (!deported && type != 'ONE') {

      let quitRoomMessage = {
        roomId: roomId,
        type: 'ROOMOUT',
        text: `안녕히계세요. 여러분. ${nickname}님 퇴장`,
        timestamp: new Date().getTime(),
        userId: userId,
        profileFileName: profileFileName,
        nickname: nickname,
        isMaster: false,
        unreadCount: 0,
      };

      query = `
        insert into oi_chat_msg (
          room_id, msg_type_cd, msg_content, send_dt_int, send_user_id, 
          send_profile_image_url, send_user_nick_nm
        ) values (
          ?, ?, ?, ?, ?,
          ?, ?
        );
      `;
      valueParameters = [
        quitRoomMessage.roomId, quitRoomMessage.type, quitRoomMessage.text, quitRoomMessage.timestamp, quitRoomMessage.userId,
        profileFileName, nickname
      ];
      result = await mysql.execute(query, valueParameters);
      quitRoomMessage.messageId = result.insertId;

      let pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: roomId,
            chatMessage: quitRoomMessage
          })
        })
      };
      console.log(pushParams);

      await lambda.invoke(pushParams).promise();
    }

    if (roomTitle != null && nextMasterId != null) {

      query = `
        update
          oi_chat_user_info
        set
          master_yn = 'Y'
        where
          room_id = ${roomId}
          and user_id = ${nextMasterId};
      `;
      console.log(query);
      await mysql.execute(query);

      let changeMasterMessage = {
        roomId: roomId,
        type: 'CHGMASTER',
        text: `"${roomTitle}"의 방장이 변경되었어요.`,
        timestamp: new Date().getTime(),
        userId: userId,
        isMaster: false,
        unreadCount: 0,
      };

      query = `
        insert into oi_chat_msg (
          room_id, msg_type_cd, msg_content, send_dt_int, send_user_id
        ) values (
          ${changeMasterMessage.roomId}, '${changeMasterMessage.type}', '${`${nextMasterNickname}님이 새로운 방장이 되었습니다.`}', ${changeMasterMessage.timestamp}, ${changeMasterMessage.userId}
        );
      `;
      result = await mysql.execute(query);
      changeMasterMessage.messageId = result.insertId;

      mysql.end();

      let pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: roomId,
            chatMessage: changeMasterMessage
          })
        })
      };
      console.log(pushParams);

      await lambda.invoke(pushParams).promise();

      let getMasterMessage = {
        roomId: roomId,
        type: 'GETMASTER',
        text: `오~ 나님 "${roomTitle}" 👑방장 등극`,
        timestamp: new Date().getTime(),
        userId: nextMasterId,
        isMaster: false,
        unreadCount: 0,
      };

      pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: roomId,
            chatMessage: getMasterMessage
          })
        })
      };
      console.log(pushParams);

      await lambda.invoke(pushParams).promise();
    }

    query = `
      update 
        oi_chat_room_info
      set
        join_user_cnt = (
          select 
            count(*) count
          from 
            oi_chat_user_info
          where
            room_id = ${roomId}
            and use_yn = 'Y'
        )
      where
        room_id = ${roomId};
    `;
    await mysql.execute(query);

    query = `
      select 
        cnc.comment_id
      from 
        oi_chat_notice_comment cnc
        join oi_chat_notice_info cni on cnc.chat_notice_id = cni.chat_notice_id
      where
        cni.room_id = ${roomId}
        and cnc.reg_user_id = ${userId}
    `;
    result = await mysql.execute(query);
    let commentIds = result.map(row => {
      return row.comment_id;
    });

    if (commentIds.length > 0) {
      query = `
        update
          oi_chat_notice_comment
        set
          reg_user_id = null, reg_user_chat_profile_id = null
        where
          comment_id in (
            ${commentIds.join(',')}
          );
      `;
      await mysql.execute(query);
    }

    query = `
      update
        oi_chat_notice_info
      set
        reg_user_id = null
      where 
        room_id = ${roomId}
        and reg_user_id = ${userId};
    `;
    await mysql.execute(query);

    mysql.end();

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};