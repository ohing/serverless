'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const search = require(`@ohing/ohingsearch`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require('@ohing/ohingresponse');


/**
 * @author Karl <karl@ohing.net> 
 * @description 오픈채팅 목록
 * @method get getOpenRooms
 * @returns  
*/

module.exports.handler = async event => {

    console.log(event);

    try {

        const imei = event.headers.imei;
        const osType = event.headers['os-type'];
        const appVersion = event.headers['app-ver'];
        const accessToken = event.headers['access-token'];

        if (imei == null || osType == null || appVersion == null || accessToken == null) {
            return response.result(400);
        }

        const userId = await authorizer.userId(accessToken);
        if (userId == null) {
            return response.result(401);
        }

        // let parameters = JSON.parse(event.body || '{}') || {}

        let parameters = event.queryStringParameters || {};

        let keys = Object.keys(parameters);
        for (let key of keys) {
            if (parameters[key].length == 0) {
                delete parameters[key];
            }
        }

        let targetUserId = parameters.targetUserId;

        let searchText = (parameters.searchText || '').trim();

        let tag = (parameters.tag || '').trim();

        let query;
        let result;

        if (searchText.length > 0) {

            let searchInfo = search.parse(searchText);
            let searchCondition = search.searchCondition(searchInfo, 'CHATROOM');

            query = `
        select 
          cri.room_id, cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password, 
          cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, cri.reg_dt created_at,
          fir.org_file_nm cover_image_url, substring_index(fir.org_file_nm, '/', -1) cover_file_name,
          cui.user_id, cui.chat_profile_id,
          case 
            when cui2.user_id is not null then 'Y'
            else 'N'
          end already_join, cui2.master_yn is_master, cui2.deport_yn deported,
          ui.user_login_id nickname, fi.org_file_nm profile_image_url, 
          cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url
        from 
          oi_chat_room_info cri
          left join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
          left join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_file_info fir on cri.room_image_id = fir.file_id
          left join oi_user_profile_image upi on cui.user_id = upi.user_id
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
          left join oi_chat_room_tag crt on cri.room_id = crt.room_id
          left join oi_tag_info ti on crt.tag_id = ti.tag_id
          left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id and cui2.user_id = ${userId}
        where 
          cri.room_type_cd = 'OPEN'
          and cri.room_status_cd = 'ACTIVE'
          and cri.except_yn = 'N'
          ${searchCondition}
        group by 
          cri.room_id
        order by 
          case 
            when cri.title = '${searchText}' then 2
            when ui.user_login_id = '${searchText}' then 3
            when cpi.nick_nm = '${searchText}' then 4
          end asc
        limit 30;
      `;
            console.log(query);

            result = await mysql.execute(query);

            result = await this.applyTags(result);

            mysql.end();

            return response.result(200, result);

        } else {

            let finalResult = {};
            let hasTag = tag.length > 0;

            let roomIds = [];
            let roomIdCondition = '';

            if (hasTag) {

                query = `
          select 
            cri.room_id
          from
            oi_chat_room_info cri
            join oi_chat_room_tag crt on cri.room_id = crt.room_id
          where
            crt.tag_id = (select tag_id from oi_tag_info where tag_val = '${tag}')
            and cri.room_type_cd = 'OPEN'
            and cri.room_status_cd = 'ACTIVE'
            and cri.except_yn = 'N'
            ;
        `;
                console.log(query);
                result = await mysql.execute(query);

                if (result.length == 0) {
                    finalResult.popularInTag = [];
                } else {
                    roomIds = result.map(row => {
                        return row.room_id;
                    });

                    if (roomIds.length > 0) {
                        roomIdCondition = `
              and cm.room_id in (${roomIds.join(',')})
            `;
                    }

                    query = `
            select 
              cri.room_id, cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password,
              cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, cri.reg_dt created_at,
              cui.user_id, cui.chat_profile_id,
              case 
                when cui2.user_id is not null then 'Y'
                else 'N'
              end already_join, cui2.master_yn is_master, cui2.deport_yn deported,
              fir.org_file_nm cover_image_url, substring_index(fir.org_file_nm, '/', -1) cover_file_name,
              ui.user_login_id nickname, fi.org_file_nm profile_image_url, 
              substring_index(fi.org_file_nm, '/', -1) profile_file_name,
              cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url,
              substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name
            from 
              oi_chat_msg cm
              join oi_chat_room_info cri on cm.room_id = cri.room_id
              join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
              join oi_user_info ui on cui.user_id = ui.user_id
              left join oi_file_info fir on cri.room_image_id = fir.file_id
              left join oi_user_profile_image upi on cui.user_id = upi.user_id
              left join oi_file_info fi on upi.file_id = fi.file_id
              left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
              left join oi_file_info fi2 on cpi.image_id = fi2.file_id
              left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id and cui2.user_id = ${userId}
            where 
              cri.room_type_cd = 'OPEN'
              and cri.except_yn = 'N'
              ${roomIdCondition}          
              and send_dt_int > ROUND((UNIX_TIMESTAMP(now(3)) - 86400) * 1000)
              and cui2.user_id is null
            group by 
              cri.room_id
            order by 
              count(cm.send_user_id) desc
            limit 
              4;
          `;
                    console.log(query);
                    let popularInTag = await mysql.execute(query);
                    finalResult.popularInTag = await this.applyTags(popularInTag);

                    roomIdCondition = '';

                    if (popularInTag.length > 0) {
                        roomIds = roomIds.concat(
                            popularInTag.map(popular => {
                                return popular.room_id;
                            })
                        );
                    }

                    if (roomIds.length > 0) {
                        roomIdCondition = `
              and cri.room_id not in (${roomIds.join(',')})
            `;
                    }
                }

            } else {

                finalResult.popularInTag = [];
            }

            query = `
        select 
          cri.room_id, cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password,
          cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, cri.reg_dt created_at,
          cui.user_id, cui.chat_profile_id,
          case 
            when cui2.user_id is not null then 'Y'
            else 'N'
          end already_join, cui2.master_yn is_master, cui2.deport_yn deported,
          fir.org_file_nm cover_image_url, substring_index(fir.org_file_nm, '/', -1) cover_file_name,
          ui.user_login_id nickname, fi.org_file_nm profile_image_url, 
          cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url
        from 
          oi_chat_room_info cri
          join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
          join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_file_info fir on cri.room_image_id = fir.file_id
          left join oi_user_profile_image upi on cui.user_id = upi.user_id
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
          left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id and cui2.user_id = ${userId}
        where 
          1=1
          ${roomIdCondition}
          and cri.room_type_cd = 'OPEN'
          and cri.room_status_cd = 'ACTIVE'
          and cri.except_yn = 'N'
        group by
          cri.room_id
        order by 
          cri.reg_dt desc
        limit 
          10;
      `;
            let recents = await mysql.execute(query);
            finalResult.recents = await this.applyTags(recents);

            if (recents.length > 0) {
                roomIds = roomIds.concat(
                    recents.map(recent => {
                        return recent.room_id;
                    })
                );
            }

            roomIdCondition = '';

            if (roomIds.length > 0) {
                roomIdCondition = `
          and cm.room_id not in (${roomIds.join(',')})
        `;
            }

            query = `
        select 
          cri.room_id, cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password,
          cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, cri.reg_dt created_at,
          cui.user_id, cui.chat_profile_id,
          case 
            when cui2.user_id is not null then 'Y'
            else 'N'
          end already_join, cui2.master_yn is_master, cui2.deport_yn deported,
          fir.org_file_nm cover_image_url, substring_index(fir.org_file_nm, '/', -1) cover_file_name,
          ui.user_login_id nickname, fi.org_file_nm profile_image_url, 
          cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url
        from 
          oi_chat_msg cm
          join oi_chat_room_info cri on cm.room_id = cri.room_id
          join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
          join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_file_info fir on cri.room_image_id = fir.file_id
          left join oi_user_profile_image upi on cui.user_id = upi.user_id
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
          left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id and cui2.user_id = ${userId}
        where 
          cri.room_type_cd = 'OPEN'
          and cri.room_status_cd = 'ACTIVE'
          and cri.except_yn = 'N'
          ${roomIdCondition}
          and cm.room_id in (
            select 
              cri.room_id
            from
              oi_chat_room_info cri
              left join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.user_id = ${userId}
            where 
              cui.user_id is null
          )
          and send_dt_int > ROUND((UNIX_TIMESTAMP(now(3)) - 86400) * 1000)    
          and cui2.user_id is null
        group by 
          cri.room_id
        order by 
          count(cm.send_user_id) desc
        limit 
          30;
      `;
            console.log(query);
            let popular = await mysql.execute(query);
            finalResult.popular = await this.applyTags(popular);

            console.log(finalResult);

            mysql.end();

            return response.result(200, finalResult);
        }

    } catch (e) {

        console.error(e);

        mysql.end();

        if (e.statusCode != null) {
            return e;
        } else {
            return response.result(403, e);
        }
    }
};

exports.applyTags = async (rooms) => {

    if (rooms.length > 0) {

        let roomIds = rooms.map(row => {
            return row.room_id;
        });

        let roomIdMap = {};
        for (let i = 0; i < rooms.length; i++) {
            rooms[i].tags = [];
            rooms[i].needs_password = rooms[i].needs_password != 0;
            if (rooms[i].chat_profile_image_url != null) {
                rooms[i].profile_image_url = rooms[i].chat_profile_image_url;
            }
            if (rooms[i].chat_nickname != null) {
                rooms[i].nickname = rooms[i].chat_nickname;
            }
            delete rooms[i].chat_nickname;
            delete rooms[i].chat_profile_image_url;

            rooms[i].already_join = rooms[i].already_join == 'Y';
            rooms[i].is_master = rooms[i].is_master == 'Y';
            rooms[i].deported = rooms[i].deported == 'Y';

            roomIdMap[rooms[i].room_id] = i;
        }

        let query = `
      select 
        ti.tag_val, crt.room_id
      from 
        oi_chat_room_tag crt
        left join oi_tag_info ti on crt.tag_id = ti.tag_id
      where 
        crt.room_id in (${roomIds.join(',')}) 
      order by 
        crt.room_id asc, crt.tag_seq asc;
    `;
        let result = await mysql.execute(query);

        result.forEach(row => {
            let index = roomIdMap[row.room_id];
            rooms[index].tags.push(row.tag_val);
        });
    }

    return rooms;
};