'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 공지사항 댓글 삭제
 * @method DELETE deleteNoticeComment
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        room_id, master_yn
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y'
        and deport_yn = 'N';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let isMaster = result[0].master_yn == 'Y'

    let commentId = event.pathParameters.commentId

    query = `
      select
        comment_id, parent_comment_id
      from
        oi_chat_notice_comment
      where
        comment_id = ${commentId}
        and reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result.length == 0 && isMaster == false) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let remainComment = null
    let deletedParentId = null

    if (result[0].parent_comment_id == null) {

      let query = `
        select
          count(*) count
        from
          oi_chat_notice_comment
        where
          parent_comment_id = ${commentId};
      `
      let result = await mysql.execute(query)
      if (result[0].count > 0) {

        let query = `
          update 
            oi_chat_notice_comment 
          set 
            content = '삭제된 댓글입니다.', del_yn = 'Y', upd_dt = now() 
          where 
            comment_id = ${commentId};
        `
        await mysql.execute(query)

        query = `
          select
            (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id 
              where like_target_type_cd = 'CNCOMT' and target_id = cnc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
            if ((select count(*) from oi_like_log where like_target_type_cd = 'CNCOMT' and target_id = cnc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
            cnc.comment_id, cnc.content, cnc.anony_yn is_anonymous, cnc.reg_dt,
            ui.user_id,
            case when cnc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end nickname,
            case when cnc.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
            case when cnc.anony_yn = 'Y' then null else cpi.chat_profile_id end chat_profile_id,
            case when cnc.anony_yn = 'Y' then '익명사용자' else cpi.nick_nm end chat_nickname,
            case when cnc.anony_yn = 'Y' then null else fi2.org_file_nm end chat_profile_image_url, substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name
          from 
            oi_chat_notice_comment cnc
            left join oi_user_info ui on cnc.reg_user_id = ui.user_id
            left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
            left join oi_file_info fi on pi.file_id = fi.file_id
            left join oi_chat_profile_info cpi on cnc.reg_user_chat_profile_id = cpi.chat_profile_id
            left join oi_file_info fi2 on cpi.image_id = fi2.file_id
          where
            cnc.comment_id = ${commentId};
        `
        result = await mysql.execute(query)

        remainComment = result[0]
        remainComment.replies = []
        remainComment.previousReplyCount = 0
        remainComment.liked = remainComment.liked == 'Y'
        remainComment.is_anonymous = remainComment.is_anonymous == 'Y'
        remainComment.is_deleted = true

      } else {

        query = `
          delete from 
            oi_chat_notice_comment
          where
            comment_id = ${commentId};
        `
        await mysql.execute(query)
      }

    } else {

      let parentId = result[0].parent_comment_id

      query = `
        delete from 
          oi_chat_notice_comment
        where
          comment_id = ${commentId};
      `
      await mysql.execute(query)

      query = `
        select
          count(*) count
        from
          oi_chat_notice_comment
        where
          parent_comment_id = ${parentId};
      `
      result = await mysql.execute(query)
      if (result[0].count == 0) {
        query = `
          delete from
            oi_chat_notice_comment 
          where
            comment_id = ${parentId} 
            and del_yn = 'Y';
        `
        await mysql.execute(query)

        deletedParentId = parentId
      }
    }

    mysql.end()

    return response.result(200, {remainComment: remainComment, deletedParentId: deletedParentId})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
