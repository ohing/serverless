'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅방 멤버 목록
 * @method PUT getMembers
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        room_type_cd
      from
        oi_chat_room_info
      where
        room_id = ${roomId};
    `
    result = await mysql.execute(query)
    let roomType = result[0].room_type_cd

    let masterCondition = roomType == 'OPEN' ? `
      , field(is_master, 'Y') desc
    ` : ''
    
    query = `
      select 
        a.* 
      from 
        (select 
          cui.user_id, cui.chat_profile_id, cui.master_yn is_master,
          case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
          case when cui.chat_profile_id is null then ui.self_intro_content else cpi.self_intro_content end self_introduction,
          case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end profile_image_url,
          substring_index(case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end, '/', -1) profile_file_name
        from 
          oi_chat_user_info cui 
          join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where 
          cui.room_id = ${roomId}
          and cui.use_yn = 'Y') a
      order by
        field(user_id, ${userId}) desc${masterCondition}, nickname asc;
    `

    result = await mysql.execute(query)

    let members = result.map(row => {
      row.is_master = row.is_master == 'Y'
      return row
    })

    return response.result(200, result)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
