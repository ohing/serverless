'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 
 * @method POST enterRoom
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let roomId = event.pathParameters.roomId;

    let query;
    let result;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    if (openType == 'CLOSE') {
      mysql.end();
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.');
    }

    query = `
      select
        room_type_cd
      from
        oi_chat_room_info
      where
        room_id = ${roomId};
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(404);
    }

    let type = result[0].room_type_cd;

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y';
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(403, null, '권한이 없습니다.');
    }

    let parameters = JSON.parse(event.body || '{}') || {};

    query = `
      update
        oi_chat_user_info
      set
        alarm_yn = '${parameters.isOn ? 'Y' : 'N'}'
      where
        room_id = ${roomId} 
        and user_id = ${userId};
    `;

    await mysql.execute(query);

    if (type == 'OPEN') {
      query = `
        select
          push_auth_key
        from
          oi_user_device_info
        where
          user_id = ${userId}
          and login_yn = 'Y'
          and push_auth_key is not null and push_auth_key  <> ''
          ;
      `;
      result = await mysql.execute(query);

      if (result.length > 0) {
        let fcmToken = result[0].push_auth_key;
        if (fcmToken != null && fcmToken.length > 0) {
          let fcmStep;
          if (parameters.isOn) {
            fcmStep = {
              type: 'subscribe',
              tokens: [fcmToken],
              topics: [`openchat-${roomId}`]
            };
          } else {
            fcmStep = {
              type: 'unsubscribe',
              tokens: [fcmToken],
              topics: [`openchat-${roomId}`]
            };
          }
          await lambda.invoke({
            FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
            InvocationType: "Event",
            Payload: JSON.stringify({
              body: JSON.stringify({
                steps: [fcmStep]
              })
            })
          }).promise();
        }
      }
    }

    mysql.end();

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};