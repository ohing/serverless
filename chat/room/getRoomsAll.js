'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse');


/**
 * @author Karl <karl@ohing.net> 
 * @description 내 채팅방 목록 전체 불러오는 버전
 * @method get getRoomsAll
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query
    let result
    
    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result[0].open_type == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅 기능을 사용할 수 없습니다.')
    }

    // let parameters = JSON.parse(event.body || '{}') || {}
    let parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    let targetRoomId = parseInt(parameters.roomId || '0') || 0

    let rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    let lastRoomId = parseInt((parameters.lastRoomId || '0')) || 0
    let searchText = (parameters.searchText || '').trim()
    let sortOrder = (parameters.sortOrder || '').trim()

    let openRoomOnly = parameters.openRoomOnly == 'Y'

    let lastIsTopFixed = false

    if (lastRoomId > 0) {
      query = `
        select 
          cui.top_fix_yn
        from
          oi_chat_user_info cui
        where
          cui.room_id = ${lastRoomId}
          and cui.user_id = ${userId}
      `
      result = await mysql.execute(query)

      if (result.length > 0 && result[0].top_fix_yn == 'Y') {
        lastIsTopFixed = true
      }
    }

    let rooms = await this.rooms(userId, targetRoomId, rowCount, lastRoomId, lastIsTopFixed, openRoomOnly, searchText, sortOrder)
    mysql.end()

    return response.result(200, rooms)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}

exports.rooms = async (userId, targetRoomId, rowCount, lastRoomId, lastIsTopFixed, openRoomOnly, searchText, sortOrder) => {

  console.log('Conditions', `targetRoomId: ${targetRoomId}, rowCount: ${rowCount}, lastRoomId: ${lastRoomId}, lastIsTopFixed: ${lastIsTopFixed}, openRoomOnly: ${openRoomOnly}, searchText: ${searchText}, sortOrder: ${sortOrder}`)

  let query
  let result

  let searchJoin = searchText.length > 0 ? `
    JOIN (
      SELECT 
        f.room_id, group_concat(f.user_id) user_id, group_concat(g.user_login_id) user_login_id, group_concat(h.nick_nm) nick_nm
      from 
        oi_chat_user_info i
        JOIN oi_chat_user_info f ON f.room_id = i.room_id
        JOIN oi_user_info g on g.user_id = f.user_id
        LEFT JOIN oi_chat_profile_info h on h.user_id = f.user_id 
      WHERE 
        i.user_id = ${userId}
      GROUP BY 
        f.room_id
    ) j ON j.room_id = a.room_id AND (j.user_login_id like ? or j.nick_nm like ?)
  ` : ''
  let searchTitleCondition = searchText.length > 0 ? `
    AND b.title like ?
  ` : ''

  let executeParams = searchText.length > 0 ? [`%${searchText}%`, `%${searchText}%`, `%${searchText}%`] : []

  let openRoomOnlyCondition = openRoomOnly ? `
    AND b.room_type_cd = 'OPEN'
  ` : ''

  let orderBy = sortOrder == 'Unread' ? `
    ORDER BY z.top_fix_yn DESC, z.receive_cnt DESC, z.msg_id DESC
  ` : `
    ORDER BY z.top_fix_yn DESC, z.msg_id DESC
  `

  query = `
    select
      cri.room_id, cri.room_type_cd room_type, cri.room_status_cd room_status,
      cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password,
      cui.top_fix_yn top_fixed, cui.master_yn is_master, cui.deport_yn deported,
      cui.alarm_yn notification,
      fi.org_file_nm cover_image_url, substring_index(fi.org_file_nm, '/', -1) cover_file_name,
      case 
        when cui.deport_yn = 'Y' then ''
        else (case 
          when cm.msg_type_cd = 'CHGMASTER' then cm.msg_content
          when cm.msg_type_cd = 'NOTICE' then cm.msg_content
          when cm.msg_type_cd = 'DEPORT' then cm.msg_content
          when cm.msg_type_cd = 'GROUPINVITE' then cm.msg_content
          when cm.msg_type_cd = 'ROOMINMSG' then cm.msg_content
          when cm.msg_type_cd = 'ROOMOUT' then cm.msg_content
          when cm.msg_type_cd = 'BLOWUP' then cm.msg_content
          when cm.msg_type_cd = 'CHGTITLE' then cm.msg_content
          when cm.msg_type_cd = 'CHGPW' then cm.msg_content
          else concat(
            case
              when cm.target_user_id is null then ''
              else '(귓속말)'
            end,
            ifnull(cmcpi.nick_nm, cmui.user_login_id), 
            case 
              when cm.msg_type_cd = 'OPENINVITE' then ': (오방 초대)'
              when cm.media_info is not null then ': (미디어)'
              else concat(': ', cm.msg_content)
            end
          )
        end)
      end last_message,
      cm.send_dt_int send_time,
      (select count(*) from oi_chat_msg_receive cmr where cmr.room_id = cri.room_id and cmr.receive_user_id = ${userId} and cmr.receive_yn = 'N') unread_count,
      case 
        when mcui.chat_profile_id is null then mui.user_login_id
        else mcpi.nick_nm
      end master_nickname,

      (select 
        count(*) count
      from
        oi_chat_user_info
      where
        room_id = cri.room_id
        and use_yn = 'Y') join_user_count,

      cri.max_user_cnt max_user_count,
      
      concat(
        '[',
        (
          select
            group_concat(concat('"', ti.tag_val, '"') separator ',')
          from 
            oi_chat_room_info cri2
            join oi_chat_room_tag crt on cri2.room_id = crt.room_id
            join oi_tag_info ti on crt.tag_id = ti.tag_id
          where 
            cri2.room_id = cri.room_id
          order by
            crt.tag_seq asc
        ),
        ']'
      ) tags

    from (
      SELECT *
      FROM (
        SELECT 
        a.user_id, a.room_id, max(c.msg_id) msg_id, a.top_fix_yn, max(e.receive_cnt) receive_cnt
        FROM oi_chat_user_info a 
        JOIN oi_chat_room_info b ON b.room_id = a.room_id 
        JOIN oi_chat_msg c ON a.room_id = c.room_id
          and c.del_yn = 'N'
          and c.target_user_id IS NULL
          AND (
              c.target_user_id IS NULL
            OR c.target_user_id = ${userId}
              OR c.send_user_id = ${userId}
          )
        -- 안읽은 메시지 가져오기
        LEFT JOIN (
          SELECT d.receive_user_id, d.room_id, count(*) receive_cnt
          from oi_chat_msg_receive d
          WHERE d.receive_user_id = ${userId} 
          AND d.receive_yn = 'N'
          GROUP BY d.room_id
        ) e ON e.room_id = a.room_id and e.receive_user_id = a.user_id 
        -- 참여자 정보 가져오기
     		${searchJoin}
        WHERE a.user_id = ${userId}
    -- 오픈 채팅만 검색하는 경우
        ${openRoomOnlyCondition}
    -- 타이틀로 검색
        ${searchTitleCondition}
        AND (
              a.use_yn = 'Y'
              OR (
                  a.deport_yn = 'Y'
                  AND a.conn_yn = 'Y'
              )
          )
        GROUP BY a.room_id

        ) y
    -- 		LIMIT ${rowCount}
    -- 		OFFSET 60
    ) z

    JOIN oi_chat_room_info cri on cri.room_id = z.room_id
    JOIN oi_chat_user_info cui on cui.room_id = cri.room_id and z.user_id = cui.user_id
    LEFT JOIN oi_file_info fi ON cri.room_image_id = fi.file_id
    LEFT JOIN oi_chat_msg cm ON cm.room_id = z.room_id AND cm.msg_id = z.msg_id

    LEFT JOIN oi_user_info cmui ON cm.send_user_id = cmui.user_id
    LEFT JOIN oi_chat_user_info cmcui ON cmui.user_id = cmcui.user_id AND cmcui.room_id = z.room_id
    LEFT JOIN oi_chat_profile_info cmcpi ON cmcui.chat_profile_id = cmcpi.chat_profile_id

    LEFT JOIN oi_chat_user_info mcui ON cri.room_id = mcui.room_id AND mcui.master_yn = 'Y'
    LEFT JOIN oi_user_info mui ON mcui.user_id = mui.user_id
    LEFT JOIN oi_chat_profile_info mcpi ON mcui.chat_profile_id = mcpi.chat_profile_id
        
    LEFT JOIN oi_chat_user_info cui2 ON z.room_id = cui2.room_id and cui2.user_id = z.user_id

    ${orderBy}
  `
  console.log(query)
  result = await mysql.execute(query, executeParams)

  let rooms = result.map(row => {
    
    row.top_fixed = row.top_fixed == 'Y'
    row.is_master = row.is_master == 'Y'
    row.needs_password = row.needs_password == 1
    row.deported = row.deported == 'Y'
    row.notification = row.notification == 'Y'
    row.member_count = row.join_user_count
    row.target_user_profiles = []
    row.tags = row.tags == null ? [] : JSON.parse(row.tags)
    return row
  })
  let roomIdMap = {}
  for (let i = 0; i < rooms.length; i++) {
    roomIdMap[rooms[i].room_id] = i
  }
  let roomIds = Object.keys(roomIdMap)

  if (roomIds.length > 0) {

    if (!openRoomOnly) {

      for (let i = 0; i < rooms.length; i++) {
        if (rooms[i].room_type != 'OPEN') {
          roomIdMap[rooms[i].room_id] = i
        }
      }

      roomIds = Object.keys(roomIdMap)

      query = `
        select 
        cri.room_id,
        cui.user_id,
        if (cui.chat_profile_id is null, ui.user_login_id, cpi.nick_nm) nickname,
        if (cui.chat_profile_id is null, fi.org_file_nm, fi2.org_file_nm) profile_image_url, 
        if (cui.chat_profile_id is null, 
          substring_index(fi.org_file_nm, '/', -1), substring_index(fi2.org_file_nm, '/', -1)
        ) profile_file_name
        from 
          oi_chat_room_info cri
          left join oi_chat_user_info cui on cri.room_id = cui.room_id
          left join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where
          cri.room_type_cd != 'OPEN'
          and cri.room_id in (${roomIds})
          and cui.user_id != ${userId}
        order by
          cri.room_id asc, 
          cui.join_dt asc;
      `
      console.log(query)
      result = await mysql.execute(query)

      let userIdSet = new Set(result.map(row => {
        return row.user_id
      }))

      let userIds = Array.from(userIdSet)
      let userIsOnlines = {}

      if (userIds.length > 0) {

        let onlineKeys = userIds.map(userId => {
          return `alive:${userId}`
        })

        let readable = await redis.readable(3)

        try {

          let onlines = await readable.mget(onlineKeys) 
          console.log('onlines', onlines)

          for (let i = 0; i < onlines.length; i++) {
            let isOnline = onlines[i]
            isOnline = isOnline == 'true' || isOnline == true
            let userId = userIds[i]

            userIsOnlines[userId] = isOnline
          }

          redis.end()

        } catch (e) {
          console.error(e)
          redis.end()
        }
      }

      result.forEach(row => {
        let index = roomIdMap[row.room_id]
        delete row.room_id
        if (rooms[index].target_user_profiles.length < 4) {
          row.is_online = userIsOnlines[row.user_id] || false
          rooms[index].target_user_profiles.push(row)
        }
      })
    }
    
  }

  return rooms
}