'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();


/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅방 생성
 * @method PUT createRoom
 * @returns  
*/

module.fileCheck = async (key) => {
  try {
    let params = {
      Bucket: process.env.mediaBucket,
      Key: profileKey
    };
    console.log(params);

    let result = await s3.headObject(params).promise();
    return result.ETag != null;

  } catch (e) {
    return false;
  }
};

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let parameters = JSON.parse(event.body || '{}') || {};

    let query;
    let result;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    if (openType == 'CLOSE') {
      mysql.end();
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.');
    }

    let type = parameters.type;
    let room;

    switch (type) {
      case "ONE": {

        let targetId = parameters.targetId;

        if (targetId == null) {
          return response.result(400);
        }

        let unavailableMessage = await this.unavailable(userId, [targetId]);
        if (unavailableMessage != null) {
          mysql.end();
          return response.result(403, null, unavailableMessage);
        }

        query = `
          select 
            cri.room_id
          from
            oi_chat_room_info cri
          where
            cri.room_id in (
              select 
                cui.room_id
              from 
                oi_chat_user_info cui
                join oi_chat_user_info cui2 on cui.room_id = cui2.room_id
              where
                cui.user_id = ${userId}
                and cui2.user_id = ${targetId}
            )
          and cri.room_type_cd = 'ONE';
        `;
        result = await mysql.execute(query);

        if (result.length > 0) {

          let roomId = result[0].room_id;
          room = await this.room(roomId, userId);

          query = `
            update 
              oi_chat_user_info
            set
              use_yn = 'Y'
            where
              room_id = ${roomId};
          `;
          await mysql.execute(query);

          console.log(room);

        } else {

          query = `
            insert into oi_chat_room_info (
              room_type_cd, max_user_cnt, reg_user_id
            ) values (
              'ONE', 0, ${userId}
            );
          `;
          result = await mysql.execute(query);
          let roomId = result.insertId;

          query = `
            insert into oi_chat_user_info (
              room_id, user_id, join_dt, master_yn
            ) values (
              ${roomId}, ${userId}, now(), 'Y'
            ), (
              ${roomId}, ${targetId}, now(), 'N'
            )
          `;
          await mysql.execute(query);

          room = await this.room(roomId, userId);
        }

        break;
      }

      case "MESG": {

        let targetIds = parameters.targetIds;

        if (targetIds == null || !Array.isArray(targetIds)) {
          return response.result(400);
        }

        let availableMessage = await this.unavailable(userId, targetIds);
        if (availableMessage != null) {
          mysql.end();
          return response.result(403, null, availableMessage);
        }

        query = `
          insert into oi_chat_room_info (
            room_type_cd, max_user_cnt, reg_user_id
          ) values (
            'MESG', 0, ${userId}
          );
        `;
        result = await mysql.execute(query);
        let roomId = result.insertId;

        let values = [
          `( ${roomId}, ${userId}, now(), 'Y' )`
        ];

        targetIds.forEach(targetId => {
          values.push(
            `( ${roomId}, ${targetId}, now(), 'N' )`
          );
        });

        query = `
          insert into oi_chat_user_info (
            room_id, user_id, join_dt, master_yn
          ) values 
            ${values.join(',')};
        `;
        await mysql.execute(query);
        room = await this.room(roomId, userId);

        break;
      }
      case "OPEN": {

        let title = (parameters.title || '').trim();
        title = await mysql.escape(title);
        let tags = parameters.tags;
        let onlyOhingProfile = parameters.onlyOhingProfile;
        let password = parameters.password;
        let usePassword = (password || '').length > 0;
        if (usePassword) {
          password = authorizer.createOldPasswordHash(password);
        }
        let maxUserCount = Math.max(1, Math.min(100, parameters.maxUserCount));
        let chatProfileId = parameters.chatProfileId;
        let imageKey = (parameters.imageKey || '').trim();

        if (title.length == 0 || onlyOhingProfile == null || maxUserCount == null) {
          return response.result(400);
        }

        let imageId = 'null';

        if (imageKey.length > 0) {

          if (!imageKey.includes('/') && imageKey.includes('.')) {
            let fileNames = imageKey.split('.');
            let fileName = fileNames[0];
            let fileExtension = fileNames[1];
            imageKey = `img/${fileName}/${fileName}.${fileExtension}`;
          }

          // let checkKey1
          // let checkKey2

          // if (!imageKey.includes('/') && imageKey.includes('.')) {
          //   let fileNames = imageKey.split('.')
          //   let fileName = fileNames[0]
          //   let fileExtension = fileNames[1]
          //   checkKey1 = `img/${fileName}/${fileName}.${fileExtension}`
          //   checkKey2 = `img/source/${fileName}.${fileExtension}`
          // }

          // for (;;) {
          //   if (module.fileCheck(imageKey)) {
          //     break
          //   }
          //   if (checkKey1 == null) {
          //     response.result(404, null, '이미지가 존재하지 않습니다.')
          //   }

          //   if (imageKey == checkKey1) {
          //     imageKey = checkKey2
          //   } else if (imageKey == checkKey2) {
          //     response.result(404, null, '이미지가 존재하지 않습니다.')
          //   } else {
          //     imageKey = checkKey1
          //   }
          // }

          query = `
            insert into oi_file_info (
              media_type_cd, org_file_nm, width, height, play_time, encoded_yn
            ) values (
              'IMAGE', '${imageKey}', 0, 0, '00:00', 'Y'
            );
          `;
          let result = await mysql.execute(query);
          imageId = result.insertId;
        }

        let passwordString = usePassword ? `'${password}'` : 'null';

        query = `
          insert into oi_chat_room_info (
            room_type_cd, room_image_id, title, room_pwd_use_yn, room_pwd, max_user_cnt, join_user_cnt, ohing_profile_use_yn
          ) values (
            'OPEN', ${imageId}, ${title}, '${usePassword ? 'Y' : 'N'}', ${passwordString}, ${maxUserCount}, 1, '${onlyOhingProfile ? 'Y' : 'N'}'
          );
        `;
        let result = await mysql.execute(query);
        let roomId = result.insertId;

        if (Array.isArray(tags) && tags.length > 0) {

          let values = [];

          for (let tag of tags) {
            let escaped = await mysql.escape(tag);
            values.push(`
              ( ${escaped}, 1 )
            `);
          }

          query = `
            insert into oi_tag_info (
              tag_val, tag_cnt
            ) values 
              ${values.join(',')}
            on duplicate key update
              tag_cnt = values(tag_cnt) + 1;
          `;
          await mysql.execute(query);

          values = tags.map(tag => {
            return `'${tag}'`;
          });

          query = `
            select 
              tag_id, tag_val
            from
              oi_tag_info
            where
              tag_val in (${values.join(',')});
          `;
          let result = await mysql.execute(query);

          values = [];

          for (let i = 0; i < tags.length; i++) {

            let tag = tags[i];
            let tagInfo = result.find(tagInfo => {
              return tagInfo.tag_val == tag;
            });
            values.push(
              `( ${roomId}, ${tagInfo.tag_id}, ${i + 1} )`
            );
          }

          query = `
            insert into oi_chat_room_tag (
              room_id, tag_id, tag_seq
            ) values
              ${values.join(',')};
          `;
          await mysql.execute(query);
        }

        query = `
          insert into oi_chat_user_info (
            room_id, user_id, chat_profile_id, join_dt, master_yn
          ) values (
            ${roomId}, ${userId}, ${chatProfileId == null ? 'null' : chatProfileId}, now(), 'Y'
          );
        `;
        await mysql.execute(query);

        room = await this.room(roomId, userId);

        query = `
          select
            push_auth_key
          from
            oi_user_device_info
          where
            user_id = ${userId}
            and login_yn = 'Y'
            and push_auth_key is not null and push_auth_key  <> ''
            ;
        `;
        result = await mysql.execute(query);

        if (result.length > 0) {
          let fcmToken = result[0].push_auth_key;
          if (fcmToken != null && fcmToken.length > 0) {

            await lambda.invoke({
              FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
              InvocationType: "Event",
              Payload: JSON.stringify({
                body: JSON.stringify({
                  steps: [
                    {
                      type: 'subscribe',
                      tokens: [fcmToken],
                      topics: [`openchat-${roomId}`]
                    }
                  ]
                })
              })
            }).promise();
          }
        }

        break;
      }
      default:
        return response.result(400);
    }

    mysql.end();

    if (room == null) {
      return response.result(404);
    }

    if (type == 'OPEN') {
      let logParams = {
        FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            userId: userId,
            mileage: 2,
            type: 'CREATECHAT',
            targetType: 'CHATROOM',
            targetId: room.room_id,
            actionId: room.room_id,
            log: '오픈채팅방 대개방'
          })
        })
      };
      console.log(logParams);

      await lambda.invoke(logParams).promise();

      let pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatRoomPush`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            roomId: room.room_id
          })
        })
      };
      console.log(pushParams);

      await lambda.invoke(pushParams).promise();
    }

    return response.result(200, room);

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};

exports.unavailable = async (userId, targetIds) => {

  let query = `
    select 
      user_id 
    from 
      oi_user_info 
    where
      user_id in (${targetIds.concat([userId]).join(',')})
      and profile_open_type_cd = 'CLOSE';
  `;
  let result = await mysql.execute(query);
  if (result.length > 0) {
    let isMine = (result.map(row => {
      return row.user_id;
    })).includes(userId);
    return isMine ? '프로필 비공개 상태에서는 채팅을 시작할 수 없습니다.' : '프로필 비공개 상대와 채팅을 시작할 수 없습니다.';
  }

  query = `
    select 
      count(*) count
    from 
      oi_user_relation ur
    where
      (
        (ur.user_id = ${userId} and ur.relation_user_id in (${targetIds.join(',')})) 
        or (ur.user_id in (${targetIds.join(',')}) and ur.relation_user_id = ${userId})
      )
      and ur.relation_type_cd = '02';
  `;
  result = await mysql.execute(query);
  if (result.count > 0) {
    return '채팅을 시작할 수 없는 상대입니다.';
  }

  return null;
};

exports.room = async (roomId, userId) => {

  let query = `
    select 
      cri.room_id, cri.room_type_cd room_type, cri.room_status_cd, cri.title, if (length(cri.room_pwd) > 0, 1, 0) use_password,
      cri.max_user_cnt max_user_count,
      (select 
        count(*) count
      from
        oi_chat_user_info
      where
        room_id = cri.room_id
        and use_yn = 'Y') join_user_count,
      cri.ohing_profile_use_yn only_ohing_profile,
      fi.org_file_nm image_url, substring_index(fi.org_file_nm, '/', -1) file_name, 
      cui.top_fix_yn top_fixed, cui.alarm_yn use_notification, cui.master_yn is_master,
      case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
      substring_index(case when cui.chat_profile_id is null then fi2.org_file_nm else fi3.org_file_nm end, '/', -1) profile_file_name
    from 
      oi_chat_room_info cri
      left join oi_file_info fi on cri.room_image_id = fi.file_id
      left join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.user_id = ${userId}
      left join oi_user_info ui on cui.user_id = ui.user_id
      left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
      left join oi_file_info fi2 on upi.file_id = fi2.file_id
      left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
      left join oi_file_info fi3 on cpi.image_id = fi3.file_id
    where
      cri.room_id = ${roomId};
  `;
  let result = await mysql.execute(query);

  if (result.length == 0) {
    return null;
  }

  let room = result[0];
  room.use_password = room.use_password == 'Y';
  room.only_ohing_profile = room.only_ohing_profile == 'Y';
  room.top_fixed = room.top_fixed == 'Y';
  room.use_notification = room.use_notification == 'Y';
  room.is_master = room.is_master == 'Y';
  room.is_member = true;

  let nickname = room.nickname;
  let profileFileName = room.profile_file_name;
  delete room.nickname;
  delete room.profile_file_name;

  if (room.room_type == 'OPEN') {

    let timestamp = new Date().getTime();
    let escapedNickname = await mysql.escape(nickname);

    let query = `
      insert into oi_chat_msg (
        room_id, msg_type_cd, msg_content, send_dt_int, send_user_id, 
        send_profile_image_url, send_user_nick_nm
      ) values (
        ${roomId}, 'ROOMINMSG', "뚜둥~ ${nickname}님 등장", ${timestamp}, ${userId},
        '${profileFileName}', ${escapedNickname}
      );
    `;
    console.log(query);
    await mysql.execute(query);

    query = `
      select 
        ti.tag_val
      from
        oi_chat_room_tag crt
          join oi_tag_info ti on crt.tag_id = ti.tag_id
      where
        crt.room_id = ${roomId}
      order by 
        crt.tag_seq asc;
    `;
    let result = await mysql.execute(query);
    room.tags = result.map(row => {
      return row.tag_val;
    });
  } else {
    room.tags = [];
  }

  let masterCondition = room.room_type == 'OPEN' ? `
    , field(is_master, 'Y') desc
  ` : '';

  query = `
    select 
        a.* 
      from 
        (select 
          cui.user_id, cui.chat_profile_id, cui.master_yn is_master,
          case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
          case when cui.chat_profile_id is null then ui.self_intro_content else cpi.self_intro_content end self_introduction,
          case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end profile_image_url,
          substring_index(case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end, '/', -1) profile_file_name
        from 
          oi_chat_user_info cui join
          oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where 
          room_id = ${roomId}) a
      order by
        field(user_id, ${userId}) desc${masterCondition}, nickname asc;
  `;

  result = await mysql.execute(query);



  // query = `
  //   select 
  //     count(*) count
  //   from 
  //     oi_chat_user_info 
  //   where
  //     room_id = ${roomId};
  // `
  // result = await mysql.execute(query)
  // let memberCount = result[0].count

  // query = `
  //   select 
  //     cpi.chat_profile_id, cpi.nick_nm chat_nickname, cpi.self_intro_content chat_self_introduction, fi.org_file_nm chat_profile_image_url, substring_index(fi.org_file_nm, '/', -1) chat_profile_file_name,
  //     ui.user_id, ui.user_nick_nm nickname, ui.self_intro_content self_introduction, fi2.org_file_nm profile_image_url, substring_index(fi2.org_file_nm, '/', -1) profile_file_name
  //   from 
  //     oi_chat_msg cm
  //     join oi_chat_user_info cui on cm.send_user_id = cui.user_id
  //     left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
  //     left join oi_file_info fi on cpi.image_id = fi.file_id
  //     left join oi_user_info ui on cm.send_user_id = ui.user_id
  //     left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
  //     left join oi_file_info fi2 on upi.file_id = fi2.file_id
  //   where
  //     cm.room_id = ${roomId}
  //   group by 
  //     cm.send_user_id
  //   order by 
  //     max(cm.send_dt_int) desc;
  // `
  // result = await mysql.execute(query)
  // let members = result.map(row => {
  //   let user = row
  //   if (user.chat_profile_id != null) {
  //     user.nickname = user.chat_nickname
  //     user.self_introduction = user.chat_self_introduction
  //     user.profile_image_url = user.chat_profile_image_url
  //     user.profile_file_name = user.chat_profile_file_name
  //   }

  //   delete user.chat_nickname
  //   delete user.chat_self_introduction
  //   delete user.chat_profile_image_url
  //   delete user.chat_profile_file_name

  //   return user
  // })

  // if (members.length < memberCount) {
  //   let existsUserIds = members.map(member => {
  //     return member.user_id
  //   })

  //   let query = `
  //     select 
  //       null chat_profile_id,
  //       ui.user_id, ui.user_nick_nm nickname, ui.self_intro_content self_introduction, fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name
  //     from 
  //       oi_chat_user_info cui
  //       join oi_user_info ui on cui.user_id = ui.user_id
  //       left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
  //       left join oi_file_info fi on upi.file_id = fi.file_id  
  //     where 
  //       cui.room_id = ${roomId}
  //       ${existsUserIds.length == 0 ? '' : `and cui.user_id not in (${existsUserIds.join(',')})`}
  //     order by 
  //       ui.user_nick_nm asc;
  //   `
  //   console.log(query)
  //   let result = await mysql.execute(query)
  //   members = members.concat(result)
  // }

  room.members = result;

  return room;
};