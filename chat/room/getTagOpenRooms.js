'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 태그 포함된 오픈채팅 목록
 * @method get getTagOpenRooms
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    // let parameters = JSON.parse(event.body || '{}') || {}

    let parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    let tag = (parameters.tag || '').trim()
    let rowCount = parseInt(parameters.rowCount || '30') || 30
    let lastCreatedAt = parameters.lastCreatedAt

    if (tag.length == 0) {
      return response.result(400)
    }

    let query
    let result

    let lastCreatedAtCondition = lastCreatedAt == null ? '' : `
      and cri.reg_dt < '${lastCreatedAt}'
    `

    query = `
      select
        cri.room_id, cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password,
        cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, cri.reg_dt created_at,
        cui.user_id, cui.chat_profile_id,
        case 
          when cui2.user_id is not null then 'Y'
          else 'N'
        end already_join, cui2.master_yn is_master, cui2.deport_yn deported,
        fir.org_file_nm cover_image_url, substring_index(fir.org_file_nm, '/', -1) cover_file_name,
        ui.user_login_id nickname, fi.org_file_nm profile_image_url, 
        cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url
      from 
        oi_chat_msg cm
        join oi_chat_room_info cri on cm.room_id = cri.room_id
        join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_file_info fir on cri.room_image_id = fir.file_id
        left join oi_user_profile_image upi on cui.user_id = upi.user_id
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id and cui2.user_id = ${userId}
      where 
        cm.room_id in (
          select 
            cri.room_id
          from
            oi_chat_room_info cri
            join oi_chat_room_tag crt on cri.room_id = crt.room_id
          where
            crt.tag_id = (select tag_id from oi_tag_info where tag_val = '${tag}')
            and cri.room_type_cd = 'OPEN'
            and cri.room_status_cd = 'ACTIVE'
        )
        ${lastCreatedAtCondition}
      group by 
        cri.room_id
      order by 
        cri.reg_dt desc
      limit 
        ${rowCount};
    `
    result = await mysql.execute(query)
    query = `
      select
        count(*) count
      from (
        select
          cri.room_id
        from 
          oi_chat_msg cm
          join oi_chat_room_info cri on cm.room_id = cri.room_id
          join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
          join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_file_info fir on cri.room_image_id = fir.file_id
          left join oi_user_profile_image upi on cui.user_id = upi.user_id
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where 
          cm.room_id in (
            select 
              cri.room_id
            from
              oi_chat_room_info cri
              join oi_chat_room_tag crt on cri.room_id = crt.room_id
            where
              crt.tag_id = (select tag_id from oi_tag_info where tag_val = '${tag}')
              and cri.room_type_cd = 'OPEN'
              and cri.room_status_cd = 'ACTIVE'
          )
        group by 
          cri.room_id
      ) a;
    `
    let countResult = await mysql.execute(query)
    let count = countResult[0].count
    
    let rooms = await this.applyTags(result)
    
    mysql.end()

    return response.result(200, {total_count: count, rooms: rooms})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}

exports.applyTags = async (rooms) => {

  if (rooms.length > 0) {
        
    let roomIds = rooms.map(row => {
      return row.room_id
    })

    let roomIdMap = {}
    for (let i = 0; i < rooms.length; i++) {
      rooms[i].tags = []
      rooms[i].needs_password = rooms[i].needs_password != 0
      if (rooms[i].chat_profile_image_url != null) {
        rooms[i].profile_image_url = rooms[i].chat_profile_image_url
      }
      if (rooms[i].chat_nickname != null) {
        rooms[i].nickname = rooms[i].chat_nickname
      }
      delete rooms[i].chat_nickname
      delete rooms[i].chat_profile_image_url

      rooms[i].already_join = rooms[i].already_join == 'Y'
      rooms[i].is_master = rooms[i].is_master == 'Y'
      rooms[i].deported = rooms[i].deported == 'Y'

      roomIdMap[rooms[i].room_id] = i
    }

    let query = `
      select 
        ti.tag_val, crt.room_id
      from 
        oi_chat_room_tag crt
        left join oi_tag_info ti on crt.tag_id = ti.tag_id
      where 
        crt.room_id in (${roomIds.join(',')}) 
      order by 
        crt.room_id asc, crt.tag_seq asc;
    `
    let result = await mysql.execute(query)

    result.forEach(row => {
      let index = roomIdMap[row.room_id]
      rooms[index].tags.push(row.tag_val)
    }) 
  }

  return rooms
}