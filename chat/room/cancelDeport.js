'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 강퇴해제
 * @method DELETE cancelDeport
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId
    let targetUserId = event.pathParameters.targetUserId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        room_id
      from
        oi_chat_room_info
      where
        room_id = ${roomId}
        and room_type_cd = 'OPEN';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404)
    }

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and master_yn = 'Y'
        and use_yn = 'Y';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    query = `
      delete from
        oi_chat_user_info
      where
        room_id = ${roomId}
        and user_id = ${targetUserId};
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}