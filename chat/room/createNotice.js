'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 공지사항작성
 * @method PUT createNotice
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }
    
    let roomId = event.pathParameters.roomId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        count(*) count 
      from
        oi_chat_notice_info
      where
        room_id = ${roomId};
    `
    result = await mysql.execute(query)
    let count = result[0].count
    if (count >= 10) {
      mysql.end()
      return response.result(409, null, '공지사항은 10개까지만 작성 가능합니다.')
    }

    query = `
      select
        cri.room_id, cui.master_yn is_master
      from
        oi_chat_room_info cri
        join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.user_id = ${userId}
      where
        cri.room_id = ${roomId}
        and cri.room_type_cd = 'OPEN';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404)
    }

    let isMaster = result[0].is_master == 'Y'

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y'
        and deport_yn = 'N';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let parameters = JSON.parse(event.body || '{}') || {}
    let content = (parameters.content || '').trim()

    if (content.length == 0) {
      return response.result(400)
    }

    content = await mysql.escape(content)

    query = `
      insert into oi_chat_notice_info (
        room_id, content, reg_user_id
      ) values (
        ${roomId}, ${content}, ${userId}
      );
    `
    result = await mysql.execute(query)
    let chatNoticeId = result.insertId

    query = `
      select
        cni.chat_notice_id, cni.top_fix_yn top_fixed, cni.content, cni.reg_dt, cui.chat_profile_id,
        ui.user_id, ui.user_login_id nickname, fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url, substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name,
        cpi.self_intro_content self_introduction
      from 
        oi_chat_notice_info cni
        join oi_chat_user_info cui on cni.room_id = cui.room_id and cni.reg_user_id = cui.user_id
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
      where
        cni.chat_notice_id = ${chatNoticeId};
    `
    result = await mysql.execute(query)

    let notice = result[0]
    if (notice.chat_nickname != null) {
      notice.nickname = notice.chat_nickname
    }
    if (notice.chat_profile_image_url != null) {
      notice.profile_image_url = notice.chat_profile_image_url
      notice.profile_file_name = notice.chat_profile_file_name
    } 

    delete notice.chat_nickname
    delete notice.chat_profile_image_url
    delete notice.chat_profile_file_name

    notice.isMaster = isMaster

    let noticeMessage = {
      roomId: roomId,
      type: 'NOTICE',    
      // text: `📣 공지 : ${content}`,
      text: '새로운 공지가 등록되었습니다.',
      timestamp: new Date().getTime(),
      userId: userId,
      isMaster: false,
      unreadCount: 0,
    }

    query = `
      insert into oi_chat_msg (
        room_id, msg_type_cd, msg_content, send_dt_int, send_user_id
      ) values (
        ${noticeMessage.roomId}, '${noticeMessage.type}', '새로운 공지가 등록되었습니다.', ${noticeMessage.timestamp}, ${noticeMessage.userId}
      );
    `
    result = await mysql.execute(query)
    noticeMessage.messageId = result.insertId

    mysql.end()

    let pushParams = {
      FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          type: 'SendMessage',
          roomId: roomId,
          chatMessage: noticeMessage
        })
      })
    }
    console.log(pushParams)

    await lambda.invoke(pushParams).promise()

    return response.result(200, notice, '등록되었습니다.')
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}