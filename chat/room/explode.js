'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 폭파
 * @method POST explode
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let roomId = event.pathParameters.roomId;
    let targetUserId = event.pathParameters.targetUserId;

    let query;
    let result;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    if (openType == 'CLOSE') {
      mysql.end();
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.');
    }

    query = `
      select
        room_id, title
      from
        oi_chat_room_info
      where
        room_id = ${roomId}
        and room_type_cd = 'OPEN';
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(404);
    }

    let roomTitle = result[0].title;

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and master_yn = 'Y'
        and use_yn = 'Y';
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(403, null, '권한이 없습니다.');
    }

    query = `
      update
        oi_chat_room_info
      set
        room_status_cd = 'BLOWUP'
      where
        room_id = ${roomId};
    `;
    await mysql.execute(query);

    query = `
      delete from
        oi_feed_chat_room_invite
      where
        room_id = ${roomId};
    `;
    await mysql.execute(query);

    query = `
      select 
        udi.push_auth_key fcm_token
      from 
        oi_chat_user_info cui 
        join oi_user_device_info udi on cui.user_id = udi.user_id and udi.push_auth_key is not null and udi.push_auth_key  <> ''
      where
        cui.room_id = ${roomId}
        and cui.alarm_yn = 'Y'
        and udi.login_yn = 'Y'
    `;
    result = await mysql.execute(query);

    for (let i = 0; i < result.length; i++) {
      let row = result[i];
      let fcmToken = row.fcm_token;
      await lambda.invoke({
        FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            steps: [
              {
                type: 'unsubscribe',
                tokens: [fcmToken],
                topics: [`openchat-${roomId}`]
              }
            ]
          })
        })
      }).promise();
    }

    let explodeMessage = {
      roomId: roomId,
      type: 'BLOWUP',
      text: `펑💥 "${roomTitle}" 이 폭파 되었습니다.`,
      timestamp: new Date().getTime(),
      userId: userId,
      isMaster: false,
      unreadCount: 0,
    };

    query = `
      insert into oi_chat_msg (
        room_id, msg_type_cd, msg_content, send_dt_int, send_user_id
      ) values (
        ${explodeMessage.roomId}, '${explodeMessage.type}', '폭파되었습니다.', ${explodeMessage.timestamp}, ${explodeMessage.userId}
      );
    `;
    result = await mysql.execute(query);
    explodeMessage.messageId = result.insertId;

    mysql.end();

    let pushParams = {
      FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          type: 'SendMessage',
          roomId: roomId,
          chatMessage: explodeMessage
        })
      })
    };
    console.log(pushParams);

    await lambda.invoke(pushParams).promise();

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: -2,
          type: 'EXCHAT',
          actionId: roomId,
          log: '오픈채팅방 폭파'
        })
      })
    };
    console.log(logParams);

    await lambda.invoke(logParams).promise();

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};