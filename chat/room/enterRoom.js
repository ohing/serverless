'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 오픈채팅방 입장
 * @method POST enterRoom
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let roomId = event.pathParameters.roomId;

    let parameters = JSON.parse(event.body || '{}') || {};

    let chatProfileId = parameters.chatProfileId;
    let password = parameters.password || '';

    let query;
    let result;
    let valueParameters;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    if (openType == 'CLOSE') {
      mysql.end();
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.');
    }

    query = `
      select 
        cri.room_id, cui.use_yn, cui.deport_yn,
        cri.room_type_cd room_type, cri.room_status_cd, cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, cri.ohing_profile_use_yn,
        cri.room_pwd password
      from
        oi_chat_room_info cri
        left join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.user_id = ${userId}
      where
        cri.room_id = ${roomId}
      limit 
        1;
    `;
    result = await mysql.execute(query);
    let room;

    let alreadyJoin = false;

    if (result.length > 0) {

      if (result[0].use_yn != null) {

        let isDeported = result[0].deport_yn == 'Y';

        if (isDeported) {
          mysql.end();
          return response.result(403, null, '방장이 회원님의 참여를 허가하지 않습니다.');
        }

        let roomStatus = result[0].room_status_cd;

        if (roomStatus != 'ACTIVE') {
          mysql.end();
          return response.result(403, null, '입장할 수 없는 오픈채팅방입니다.');
        }

        alreadyJoin = result[0].use_yn == 'Y';

        room = await this.room(roomId, userId);

      } else {

        let isOpenChat = result[0].room_type == 'OPEN';
        if (isOpenChat) {

          let roomPassword = result[0].password || '';
          let passwordHash = authorizer.createOldPasswordHash(password);
          if (password.length > 0 && roomPassword != passwordHash) {
            mysql.end();
            return response.result(403, null, '비밀번호가 다릅니다.');
          }

          let onlyOhingProfile = result[0].ohing_profile_use_yn == 'Y';
          if (onlyOhingProfile && chatProfileId != null) {
            mysql.end();
            return response.result(403, null, '채팅프로필을 사용할 수 없는 채팅방입니다.');
          }

          let max = result[0].max_user_count;
          let join = result[0].join_user_count;

          if ((max - join) > 0) {

            let query = `
              update 
                oi_chat_room_info
              set
                join_user_cnt = join_user_cnt + 1
              where
                room_id = ${roomId};
            `;
            await mysql.execute(query);

            query = `
              insert into oi_chat_user_info (
                room_id, user_id, chat_profile_id, join_dt, master_yn
              ) values (
                ${roomId}, ${userId}, ${chatProfileId == null ? 'null' : chatProfileId}, now(), 'N'
              );
            `;
            await mysql.execute(query);

            room = await this.room(roomId, userId);

          } else {
            mysql.end();
            return response.result(403, null, '인원 제한으로 인해 더 이상 참여할 수 없습니다.');
          }
        }
      }
    }

    if (room == null) {
      mysql.end();
      return response.result(404, null, '존재하지 않는 오픈채팅방입니다.');
    }

    query = `
      select
        cni.chat_notice_id, cni.top_fix_yn top_fixed, cni.content, ui.user_login_id nickname, 
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url, substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name
      from 
        oi_chat_notice_info cni
        join oi_chat_user_info cui on cni.room_id = cui.room_id and cni.reg_user_id = cui.user_id
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
      where
        cni.room_id = ${roomId}
        and cni.top_fix_yn = 'Y'
      limit 
        1;
    `;
    result = await mysql.execute(query);

    if (result.length > 0) {

      let notice = result[0];

      notice.top_fixed = notice.top_fixed == 'Y';
      notice.isMaster = false;

      if (notice.chat_nickname != null) {
        notice.nickname = notice.chat_nickname;
      }
      if (notice.chat_profile_image_url != null) {
        notice.nickname = notice.chat_nickname;
        notice.profile_image_url = notice.chat_profile_image_url;
        notice.profile_file_name = notice.chat_profile_file_name;
      }

      delete notice.chat_nickname;
      delete notice.chat_profile_image_url;
      delete notice.chat_profile_file_name;

      room.fixedNotice = notice;

    } else {

      room.fixedNotice = null;
    }

    query = `
      select
        push_auth_key
      from
        oi_user_device_info
      where
        user_id = ${userId}
        and login_yn = 'Y'
        and push_auth_key is not null and push_auth_key  <> '';
    `;
    result = await mysql.execute(query);

    if (result.length > 0) {
      let fcmToken = result[0].push_auth_key;
      if (fcmToken != null && fcmToken.length > 0) {

        await lambda.invoke({
          FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
          InvocationType: "Event",
          Payload: JSON.stringify({
            body: JSON.stringify({
              steps: [
                {
                  type: 'subscribe',
                  tokens: [fcmToken],
                  topics: [`openchat-${roomId}`]
                }
              ]
            })
          })
        }).promise();
      }
    }

    let nickname;
    let profileFileName;

    if (!alreadyJoin) {

      if (chatProfileId == null) {

        query = `
          select 
            ui.user_login_id, substring_index(fi.org_file_nm, '/', -1) profile_file_name
          from
            oi_user_info ui
            left join oi_user_profile_image upi on ui.user_id = upi.user_id
            left join oi_file_info fi on upi.file_id = fi.file_id
          where
            ui.user_id = ${userId};
        `;
        result = await mysql.execute(query);
        nickname = result[0].user_login_id;
        profileFileName = result[0].profile_file_name;

      } else {

        query = `
          select 
            cpi.nick_nm, substring_index(fi.org_file_nm, '/', -1) profile_file_name
          from
            oi_chat_profile_info cpi 
            join oi_chat_user_info cui on cpi.chat_profile_id = cui.chat_profile_id
            left join oi_file_info fi on cpi.image_id = fi.file_id
          where
            cui.room_id = ${roomId}
            and cui.user_id = ${userId};
        `;
        result = await mysql.execute(query);
        nickname = result[0].nick_nm;
        profileFileName = result[0].profile_file_name;
      }

      let roomInMessage = {
        roomId: roomId,
        type: 'ROOMINMSG',
        text: `뚜둥~ ${nickname}님 등장`,
        timestamp: new Date().getTime(),
        userId: userId,
        profileFileName: profileFileName,
        nickname: nickname,
        isMaster: false,
        unreadCount: 0
      };

      query = `
        insert into oi_chat_msg (
          room_id, msg_type_cd, msg_content, send_dt_int, send_user_id, 
          send_profile_image_url, send_user_nick_nm
        ) values (
          ?, ?, ?, ?, ?, 
          ?, ?
        );
      `;
      valueParameters = [
        roomInMessage.roomId, roomInMessage.type, roomInMessage.text, roomInMessage.timestamp, roomInMessage.userId,
        roomInMessage.profileFileName, roomInMessage.nickname
      ];
      result = await mysql.execute(query, valueParameters);
      roomInMessage.messageId = result.insertId;

      mysql.end();

      let pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: roomId,
            chatMessage: roomInMessage
          })
        })
      };
      console.log(pushParams);

      await lambda.invoke(pushParams).promise();

    } else {

      mysql.end();
    }

    return response.result(200, room);

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};

exports.room = async (roomId, userId) => {

  let query = `
    select 
      cri.room_id, cri.room_type_cd room_type, cri.room_status_cd, cri.title, if (length(cri.room_pwd) > 0, 1, 0) use_password,
      cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, cri.ohing_profile_use_yn only_ohing_profile,
      fi.org_file_nm cover_image_url, substring_index(fi.org_file_nm, '/', -1) cover_file_name, cui.top_fix_yn top_fixed, cui.alarm_yn use_notification, cui.master_yn is_master
    from 
      oi_chat_room_info cri
      left join oi_file_info fi on cri.room_image_id = fi.file_id
      left join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.user_id = ${userId}
    where
      cri.room_id = ${roomId};
  `;
  let result = await mysql.execute(query);

  if (result.length == 0) {
    return null;
  }

  let room = result[0];
  room.use_password = room.use_password == 'Y';
  room.only_ohing_profile = room.only_ohing_profile == 'Y';
  room.top_fixed = room.top_fixed == 'Y';
  room.use_notification = room.use_notification == 'Y';
  room.is_master = room.is_master == 'Y';
  room.is_member = true;

  if (room.room_type == 'OPEN') {
    let query = `
      select 
        ti.tag_val
      from
        oi_chat_room_tag crt
          join oi_tag_info ti on crt.tag_id = ti.tag_id
      where
        crt.room_id = ${roomId}
      order by 
        crt.tag_seq asc;
    `;
    let result = await mysql.execute(query);
    room.tags = result.map(row => {
      return row.tag_val;
    });
  } else {
    room.tags = [];
  }

  let masterCondition = room.room_type == 'OPEN' ? `
    , field(is_master, 'Y') desc
  ` : '';

  query = `
    select 
        a.* 
      from 
        (select 
          cui.user_id, cui.chat_profile_id, cui.master_yn is_master,
          case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
          case when cui.chat_profile_id is null then ui.self_intro_content else cpi.self_intro_content end self_introduction,
          case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end profile_image_url,
          substring_index(case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end, '/', -1) profile_file_name
        from 
          oi_chat_user_info cui join
          oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where 
          room_id = ${roomId}) a
      order by
        field(user_id, ${userId}) desc${masterCondition}, nickname asc;
  `;

  result = await mysql.execute(query);

  room.members = result;

  return room;
};