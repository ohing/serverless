'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 공지사항 댓글 좋아요
 * @method post likeNoticeComment
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y'
        and deport_yn = 'N';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let commentId = event.pathParameters.commentId

    let parameters = JSON.parse(event.body || '{}') || {}
    let like = parameters.like

    if (like == null || typeof like != 'boolean') {
      mysql.end()
      return response.result(400)
    } 

    if (like) {
      query = `
        insert ignore into oi_like_log (
          target_id, like_target_type_cd, user_id, like_dt
        ) values (
          ${commentId}, 'CNCOMT', ${userId}, now()
        );
      `
      await mysql.execute(query)

    } else {

      query = `
        delete from
          oi_like_log
        where
          target_id = ${commentId} and like_target_type_cd = 'CNCOMT' and user_id = ${userId};
      `
      await mysql.execute(query)
    }

    query = `
      select 
        count(*) count
      from
        oi_like_log 
      where
        target_id = ${commentId}
        and like_target_type_cd = 'CNCOMT';
    `
    result = await mysql.execute(query)
    let likeCount = result[0].count
    
    mysql.end()

    return response.result(200, {liked: like, likeCount: likeCount})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
