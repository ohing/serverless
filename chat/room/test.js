module.createRoom = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    body: JSON.stringify({
      "type": "OPEN",
      "title": "테스트오픈채팅",
      "tags": [
        "여기는", "대한민국", "체험", "삶의", "현장"
      ],
      "onlyOhingProfile": false,
      "password": "testtest",
      "maxUserCount": 99,
      "chatProfileId": 39,
      "imageKey": "img/IAA_20200330071405317/IAA_20200330071405317.png"
    })
  }
  return await require('./createRoom.js').handler(request)
}

module.topFixRoom = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 139
    },
    body: JSON.stringify({
      fix: false
    })
  }
  return await require('./topFixRoom.js').handler(request)
}

module.enterRoom = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'e40ff0ff-8dea-4e54-8851-180932ff23f7'
    },
    pathParameters: {
      roomId: 1746
    }
  }
  return await require('./enterRoom.js').handler(request)
}

module.modifyOpenRoom = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 7
    },
    body: JSON.stringify({
      "title": "이곳은!",
      "tags": [ // Optional
        "수원남부지방법원",
        "왜",
        "목동에있냐"
      ],
      "password": 'test!!!', // Optional. null이거나 키가 없거나, 빈 스트링인 경우 지정하지 않습니다.
      "maxUserCount": 50, // 1~100
      "imageKey": '' // Optional. S3 Key
    })
  }
  return await require('./modifyOpenRoom.js').handler(request)
}

module.deport = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '35949f99-fde6-43cb-a91d-0c3299b1e822'
    },
    pathParameters: {
      roomId: 5050,
    },
    body: JSON.stringify({
      targetUserId: 549
    })
  }
  return await require('./deport.js').handler(request)
}

module.getDeports = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 1746,
    }
  }
  return await require('./getDeports.js').handler(request)
}

module.cancelDeport = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 1746,
      targetUserId: 52
    }
  }
  return await require('./cancelDeport.js').handler(request)
}

module.createNotice = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 191
    },
    body: JSON.stringify({
      content: '테스트'
    })
  }
  return await require('./createNotice.js').handler(request)
}

module.getNotices = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 191
    }
  }
  return await require('./getNotices.js').handler(request)
}

module.createNoticeComment = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 191,
      chatNoticeId: 162
    },
    body: JSON.stringify({
      content: '테스트',
      isAnonymous: false,
      parentId: 3
    })
  }
  return await require('./createNoticeComment.js').handler(request)
}

module.changeMaster = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 1746,
      targetUserId: 52
    }
  }
  return await require('./changeMaster.js').handler(request)
}

module.getNotices = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 1746
    }
  }
  return await require('./getNotices.js').handler(request)
}

module.getNoticeComments = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 191,
      chatNoticeId: 162
    }
  }
  return await require('./getNoticeComments.js').handler(request)
}

module.getNoticeReplies = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 191,
      chatNoticeId: 162,
      commentId: 3
    },
    body: JSON.stringify({
      firstId: 7
    })
  }
  return await require('./getNoticeReplies.js').handler(request)
}

module.modifyNoticeComment = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 191,
      chatNoticeId: 162,
      commentId: 7
    },
    body: JSON.stringify({
      content: "asfdjlsaf",
      isAnonymous: true
    })
  }
  return await require('./modifyNoticeComment.js').handler(request)
}

module.deleteNoticeComment = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '3364a226-f765-4050-9b1e-471850132e98'
    },
    pathParameters: {
      roomId: 1746,
      chatNoticeId: 166,
      commentId: 11
    }
  }
  return await require('./deleteNoticeComment.js').handler(request)
}


module.getOpenRoom = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 498
    }
  }
  return await require('./getOpenRoom.js').handler(request)
}

module.getOpenRooms = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'bf4431c3-98b1-4f9b-b3f0-778c458a1145'
    },
    queryStringParameters: {
      // tag: "테스트"
      // searchText: "테스트 치킨"
    }
  }
  return await require('./getOpenRooms.js').handler(request)
}

module.getTagOpenRooms = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    queryStringParameters: {
      tag: "테스트",
      rowCount: 1,
      lastCreatedAt: '2020-08-06T10:40:21.000Z'
    }
  }
  return await require('./getTagOpenRooms.js').handler(request)
}

module.getRooms = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'd56f1c83-2a08-4373-8c82-7c3caf90ab22'
    },
    queryStringParameters: {
      // openRoomOnly: 'Y',
      // roomId: 1907,
      // searchText: '-',
      // sortOrder: 'Unread',
    },
  }
  return await require('./getRooms.js').handler(request)
}

module.inviteRoom = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '15d77b4e-0a3d-4ac7-b76a-d82213652725'
    },
    pathParameters: {
      roomId: 5719,
    },
    body: JSON.stringify({
      targetUserIds: [
        46, 48
      ]
    })
  }
  return await require('./inviteRoom.js').handler(request)
}

module.fixInviteRoom = async () => {
  let request = {
  }
  return await require('./fixInviteRoom.js').handler(request)
}

module.quitRoom = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '060dd56e-1440-48dc-a12e-4ddc398a211e'
    },
    pathParameters: {
      roomId: 3489
    }
  }
  return await require('./quitRoom.js').handler(request)
}

test = async () => {

  // let a = '\'한글\''
  // console.log(a)
  // a = a.replace(/\'/g, "\\'")
  // console.log(a)

  // return

  const environment = require('../../environment.js')
  environment.setEnvironment(true)
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()