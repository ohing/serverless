'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const iot = require(`@ohing/ohingiot`)
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()


/**
 * @author Karl <karl@ohing.net> 
 * @description 채팅방 초대정보 수정
 * @method 
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let query
    let result

    query = `
      select msg_id, invite_room_info from oi_chat_msg where invite_room_info is not null and json_extract(invite_room_info, '$[0].tags') like '"%';
    `
    result = await mysql.execute(query)

    for (let row of result) {
      
      let messageId = row.msg_id

      let inviteRoomInfo = row.invite_room_info
      console.log(inviteRoomInfo.tags)
      inviteRoomInfo.tags = JSON.parse(inviteRoomInfo.tags)

      // console.log(inviteRoomInfo)
      
      // let tagsString = JSON.parse(tagsString)

      // query = `
      //   update
      //     oi_chat_msg
      //   set
      //     invite_room_info = ${JSON.stringify(inviteRoomInfo)}
      //   where
      //     msg_id = ${messageId};
      // `
      // await mysql.execute(query)
    }

    mysql.end()
    
  } catch (e) {
    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
