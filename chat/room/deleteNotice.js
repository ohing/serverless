'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 공지사항 삭제
 * @method DELETE deleteNOtice
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId
    let chatNoticeId = event.pathParameters.chatNoticeId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }
    
    query = `
      select
        chat_notice_id
      from
        oi_chat_notice_info
      where
        chat_notice_id = ${chatNoticeId}
        and reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      
      let query = `
        select
          user_id
        from
          oi_chat_user_info
        where
          room_id = ${roomId}
          and user_id = ${userId}
          and master_yn = 'Y'
      `
      let result = await mysql.execute(query)

      if (result.length == 0) {
        mysql.end()
        return response.result(403, null, '권한이 없습니다.')
      }
    }

    query = `
      delete from
        oi_chat_notice_info
      where
        chat_notice_id = ${chatNoticeId}
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, {}, '삭제되었습니다.')
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}