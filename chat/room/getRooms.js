'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse');


/**
 * @author Karl <karl@ohing.net> 
 * @description 내 채팅방 목록
 * @method get getRooms
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query
    let result
    
    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result[0].open_type == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅 기능을 사용할 수 없습니다.')
    }

    // let parameters = JSON.parse(event.body || '{}') || {}
    let parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    let targetRoomId = parseInt(parameters.roomId || '0') || 0

    let rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    let lastRoomId = parseInt((parameters.lastRoomId || '0')) || 0
    let searchText = (parameters.searchText || '').trim()
    let sortOrder = (parameters.sortOrder || '').trim()

    let openRoomOnly = parameters.openRoomOnly == 'Y'

    let lastIsTopFixed = false

    if (lastRoomId > 0) {
      query = `
        select 
          cui.top_fix_yn
        from
          oi_chat_user_info cui
        where
          cui.room_id = ${lastRoomId}
          and cui.user_id = ${userId}
      `
      result = await mysql.execute(query)

      if (result.length > 0 && result[0].top_fix_yn == 'Y') {
        lastIsTopFixed = true
      }
    }

    let rooms = await this.rooms(userId, targetRoomId, rowCount, lastRoomId, lastIsTopFixed, openRoomOnly, searchText, sortOrder)
    mysql.end()

    return response.result(200, rooms)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}

exports.rooms = async (userId, targetRoomId, rowCount, lastRoomId, lastIsTopFixed, openRoomOnly, searchText, sortOrder) => {

  console.log('Conditions', `targetRoomId: ${targetRoomId}, rowCount: ${rowCount}, lastRoomId: ${lastRoomId}, lastIsTopFixed: ${lastIsTopFixed}, openRoomOnly: ${openRoomOnly}, searchText: ${searchText}, sortOrder: ${sortOrder}`)

  let query
  let result

  let where = ''
  let having = ''
  let orderBy = ''
  let limit = ''

  if (targetRoomId > 0) {

    where = `
      where
        cui.user_id = ${userId}
        and cui.use_yn = 'Y'
        and cri.room_id = ${targetRoomId}
    `

  } else {

    let openRoomOnlyCondition = openRoomOnly ? `
      and cri.room_type_cd = 'OPEN'
    ` : ''

    let lastCondition = ''

    if (lastRoomId > 0) {

      if (sortOrder === 'Unread') {
        query = `
          select 
            count(*) count
          from 
            oi_chat_msg_receive cmr
          where 
            cmr.room_id = ${lastRoomId};
        `
        result = await mysql.execute(query)
        let lastUnreadCount = result[0].count

        query = `
          select
            z.room_id
          from
            (select 
              cui.room_id,
              count(cmr.room_id) unread_count
            from
              oi_chat_user_info cui
              left join oi_chat_msg_receive cmr on cui.room_id = cmr.room_id
            where
              cui.user_id = ${userId}
              and cui.use_yn = 'Y'
              ${lastIsTopFixed ? '' : `and cui.top_fix_yn = 'N'`}
            group by
              cui.room_id
            order by 
              count(cmr.room_id) desc
            ) z
          where
            z.unread_count = ${lastUnreadCount};
        `
        console.log(query)
        result = await mysql.execute(query)

        let exceptRoomIds = []

        for (let row of result) {

          exceptRoomIds.push(row.room_id)
          if (row.room_id == lastRoomId) {
            break
          }
        }

        if (!lastIsTopFixed) {
          query = `
            select
              room_id
            from
              oi_chat_user_info
            where
              user_id = ${userId}
              and use_yn = 'Y'
              and top_fix_yn = 'Y';
          `
          result = await mysql.execute(query)

          for (let row of result) {
            exceptRoomIds.push(row.room_id)
          }
        }

        lastCondition = exceptRoomIds.length > 0 ? `
          and cri.room_id not in (${exceptRoomIds.join(',')})
        ` : ''

        having = `
          having
            count(cmr.room_id) <= ${lastUnreadCount}
        `

      } else {

        query = `
          select 
            cm.msg_id, cm.send_dt_int last_send_time
          from 
            oi_chat_msg cm
          where 
            cm.room_id = ${lastRoomId}
          order by 
            cm.msg_id desc
          limit 
            1;
        `
        result = await mysql.execute(query)
        let lastSendTime = result.length > 0 ? result[0].last_send_time : 0
        let lastMessageId = result.length > 0 ? result[0].msg_id : Number.MAX_SAFE_INTEGER

        lastCondition = `
          and cm.send_dt_int <= ${lastSendTime}
          and cm.msg_id < ${lastMessageId}
        `
      }
    }

    where = `
      where
        cui.user_id = ${userId}
        and (
          cui.use_yn = 'Y'
          or (cui.deport_yn = 'Y' and cui.conn_yn = 'Y')
        )
        ${openRoomOnlyCondition}
        ${lastCondition}
        ${lastIsTopFixed ? `and cui.top_fix_yn = 'N'` : ''}
        ${searchText.length > 0 ? `
          and (
	      	  cri.title like '%${searchText}%'
            or cri.room_id in (	
              select 
                cui2.room_id
              from 
                oi_chat_user_info cui2
                left join oi_chat_profile_info cpi on cui2.chat_profile_id = cpi.chat_profile_id
                left join oi_user_info ui on cui2.user_id = ui.user_id
              where 
                cui2.user_id != ${userId}
                and (
                  (cui2.chat_profile_id is null and ui.user_login_id like '%${searchText}%')
                  or (cui2.chat_profile_id is not null and cpi.nick_nm like '%${searchText}%')
                )
            )
	        )` : ''}
    `

    let sortCondition = sortOrder == 'Unread' ? `
      count(cmr.room_id) desc, max(cm.send_dt_int) desc
    ` : `
      max(cm.send_dt_int) desc,
      max(cm.msg_id) desc
    `

    orderBy = `
      order by
        cui.top_fix_yn desc,
        ${sortCondition}
    `

    limit = `
      limit 
        ${rowCount}
    `
  }

  query = `
    select
      cri.room_id, cri.room_type_cd room_type, cri.room_status_cd room_status,
      cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password,
      cui.top_fix_yn top_fixed, cui.master_yn is_master, cui.deport_yn deported,
      cui.alarm_yn notification,
      fi.org_file_nm cover_image_url, substring_index(fi.org_file_nm, '/', -1) cover_file_name,
      case 
        when cui.deport_yn = 'Y' then ''
        else (case 
          when cm.msg_type_cd = 'CHGMASTER' then cm.msg_content
          when cm.msg_type_cd = 'NOTICE' then cm.msg_content
          when cm.msg_type_cd = 'DEPORT' then cm.msg_content
          when cm.msg_type_cd = 'GROUPINVITE' then cm.msg_content
          when cm.msg_type_cd = 'ROOMINMSG' then cm.msg_content
          when cm.msg_type_cd = 'ROOMOUT' then cm.msg_content
          when cm.msg_type_cd = 'BLOWUP' then cm.msg_content
          when cm.msg_type_cd = 'CHGTITLE' then cm.msg_content
          when cm.msg_type_cd = 'CHGPW' then cm.msg_content
          else concat(
            case
              when cm.target_user_id is null then ''
              else '(귓속말)'
            end,
            ifnull(cmcpi.nick_nm, cmui.user_login_id), 
            case 
              when cm.msg_type_cd = 'OPENINVITE' then ': (오방 초대)'
              when cm.media_info is not null then ': (미디어)'
              else concat(': ', cm.msg_content)
            end
          )
        end)
      end last_message,
      cm.send_dt_int send_time,
      (select count(*) from oi_chat_msg_receive cmr where cmr.room_id = cri.room_id and cmr.receive_user_id = ${userId} and cmr.receive_yn = 'N') unread_count,
      case 
        when mcui.chat_profile_id is null then mui.user_login_id
        else mcpi.nick_nm
      end master_nickname,

      (select 
        count(*) count
      from
        oi_chat_user_info
      where
        room_id = cri.room_id
        and use_yn = 'Y') join_user_count,

      cri.max_user_cnt max_user_count,
      
      concat(
        '[',
        (
          select
            group_concat(concat('"', ti.tag_val, '"') separator ',')
          from 
            oi_chat_room_info cri2
            join oi_chat_room_tag crt on cri2.room_id = crt.room_id
            join oi_tag_info ti on crt.tag_id = ti.tag_id
          where 
            cri2.room_id = cri.room_id
          order by
            crt.tag_seq asc
        ),
        ']'
      ) tags

    from 
      oi_chat_room_info cri
      join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.user_id = ${userId}
      left join oi_user_info ui on cui.user_id = ui.user_id
      left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
      left join oi_file_info fi on cri.room_image_id = fi.file_id
      left join oi_chat_msg cm on cri.room_id = cm.room_id and cm.msg_id = (
        select 
          cm2.msg_id
        from 
          oi_chat_msg cm2 
          left join oi_chat_msg_delete cmd on cm2.room_id = cmd.room_id and cm2.msg_id = cmd.msg_id and cmd.delete_user_id = 22
        where (
          cm2.room_id = cri.room_id 
          and cm2.del_yn = 'N'
          and cmd.msg_id is null
        ) 
        and (
          cm2.target_user_id is null 
          or cm2.target_user_id = ${userId}
          or cm2.send_user_id = ${userId}
        )
        ORDER BY msg_id desc
        LIMIT 1
      )
      left join oi_user_info cmui on cm.send_user_id = cmui.user_id
      left join oi_chat_user_info cmcui on cmui.user_id = cmcui.user_id and cmcui.room_id = cri.room_id
      left join oi_chat_profile_info cmcpi on cmcui.chat_profile_id = cmcpi.chat_profile_id

      left join oi_chat_user_info mcui on cri.room_id = mcui.room_id and mcui.master_yn = 'Y'
      left join oi_user_info mui on mcui.user_id = mui.user_id
      left join oi_chat_profile_info mcpi on mcui.chat_profile_id = mcpi.chat_profile_id

      left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id

      left join oi_chat_msg_receive cmr on cri.room_id = cmr.room_id and cmr.receive_user_id = ${userId} and cmr.receive_yn = 'N'

      

    ${where}
    group by 
      cri.room_id
    ${having}
    ${orderBy}
    ${limit};
  `
  console.log(query)
  result = await mysql.execute(query)

  let rooms = result.map(row => {
    
    row.top_fixed = row.top_fixed == 'Y'
    row.is_master = row.is_master == 'Y'
    row.needs_password = row.needs_password == 1
    row.deported = row.deported == 'Y'
    row.notification = row.notification == 'Y'
    row.member_count = row.join_user_count
    row.target_user_profiles = []
    row.tags = row.tags == null ? [] : JSON.parse(row.tags)
    return row
  })
  let roomIdMap = {}
  for (let i = 0; i < rooms.length; i++) {
    roomIdMap[rooms[i].room_id] = i
  }
  let roomIds = Object.keys(roomIdMap)

  if (roomIds.length > 0) {
    // query = `
    //   select 
    //     cmr.room_id, count(cmr.room_id) count
    //   from
    //     oi_chat_msg_receive cmr
    //   where
    //     room_id in (${roomIds.join(',')})
    //     and receive_user_id = ${userId}
    //   group by
    //     cmr.room_id
    // `
    // console.log(query)
    // result = await mysql.execute(query)

    // result.forEach(row => {
    //   rooms[roomIdMap[row.room_id]].unread_count = row.count
    // })

    // query = `
    //   select 
    //     room_id, count(*) count
    //   from
    //     oi_chat_user_info
    //   where
    //     room_id in (${roomIds.join(',')})
    //     and use_yn = 'Y'
    //   group by
    //     room_id
    // `
    // result = await mysql.execute(query)

    // result.forEach(row => {
    //   rooms[roomIdMap[row.room_id]].member_count = row.count
    // })

    if (!openRoomOnly) {

      for (let i = 0; i < rooms.length; i++) {
        if (rooms[i].room_type != 'OPEN') {
          roomIdMap[rooms[i].room_id] = i
        }
      }

      roomIds = Object.keys(roomIdMap)

      query = `
        select 
        cri.room_id,
        cui.user_id,
        if (cui.chat_profile_id is null, ui.user_login_id, cpi.nick_nm) nickname,
        if (cui.chat_profile_id is null, fi.org_file_nm, fi2.org_file_nm) profile_image_url, 
        if (cui.chat_profile_id is null, 
          substring_index(fi.org_file_nm, '/', -1), substring_index(fi2.org_file_nm, '/', -1)
        ) profile_file_name
        from 
          oi_chat_room_info cri
          left join oi_chat_user_info cui on cri.room_id = cui.room_id
          left join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where
          cri.room_type_cd != 'OPEN'
          and cri.room_id in (${roomIds})
          and cui.user_id != ${userId}
        order by
          cri.room_id asc, 
          cui.join_dt asc;
      `
      console.log(query)
      result = await mysql.execute(query)

      let userIdSet = new Set(result.map(row => {
        return row.user_id
      }))

      let userIds = Array.from(userIdSet)
      let userIsOnlines = {}

      if (userIds.length > 0) {

        let onlineKeys = userIds.map(userId => {
          return `alive:${userId}`
        })

        let readable = await redis.readable(3)

        try {

          let onlines = await readable.mget(onlineKeys) 
          console.log('onlines', onlines)

          for (let i = 0; i < onlines.length; i++) {
            let isOnline = onlines[i]
            isOnline = isOnline == 'true' || isOnline == true
            let userId = userIds[i]

            userIsOnlines[userId] = isOnline
          }

          redis.end()

        } catch (e) {
          console.error(e)
          redis.end()
        }
      }

      result.forEach(row => {
        let index = roomIdMap[row.room_id]
        delete row.room_id
        if (rooms[index].target_user_profiles.length < 4) {
          row.is_online = userIsOnlines[row.user_id] || false
          rooms[index].target_user_profiles.push(row)
        }
      })
    }
    
  }

  return rooms
}