'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 방장위임
 * @method POST changeMaster
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId
    let targetUserId = event.pathParameters.targetUserId

    let query
    let result
    let valueParameters

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        room_id, title
      from
        oi_chat_room_info
      where
        room_id = ${roomId}
        and room_type_cd = 'OPEN';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404)
    }
    
    let roomTitle = result[0].title

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and master_yn = 'Y'
        and use_yn = 'Y';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    query = `
      select 
        cui.user_id,
        case when cpi.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname
      from
        oi_chat_user_info cui
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
      where
        cui.room_id = ${roomId}
        and cui.user_id = ${targetUserId}
        and cui.master_yn = 'N'
        and cui.use_yn = 'Y'
        and cui.deport_yn = 'N';
    `
    result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '방장으로 지정할 수 없는 회원입니다.')
    }

    let nextMasterNickname = result[0].nickname

    query = `
      update
        oi_chat_user_info
      set
        master_yn = 'N'
      where
        room_id = ${roomId}
        and user_id = ${userId};
    `
    await mysql.execute(query)

    query = `
      update
        oi_chat_user_info
      set
        master_yn = 'Y'
      where
        room_id = ${roomId}
        and user_id = ${targetUserId};
    `
    await mysql.execute(query)

    let changeMasterMessage = {
      roomId: roomId,
      type: 'CHGMASTER',    
      text: `"${roomTitle}"의 방장이 변경되었어요.`,
      timestamp: new Date().getTime(),
      userId: userId,
      isMaster: false,
      unreadCount: 0
    }

    query = `
      insert into oi_chat_msg (
        room_id, msg_type_cd, msg_content, send_dt_int, send_user_id
      ) values (
        ?, ?, ?, ?, ?
      );
    `
    valueParameters = [
      changeMasterMessage.roomId, changeMasterMessage.type, `${nextMasterNickname}님이 새로운 방장이 되었습니다.`, changeMasterMessage.timestamp, changeMasterMessage.userId
    ]
    result = await mysql.execute(query, valueParameters)
    changeMasterMessage.messageId = result.insertId

    mysql.end()

    let pushParams = {
      FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          type: 'SendMessage',
          roomId: roomId,
          chatMessage: changeMasterMessage
        })
      })
    }
    console.log(pushParams)

    await lambda.invoke(pushParams).promise()

    let getMasterMessage = {
        roomId: roomId,
        type: 'GETMASTER',    
        text: `오~ 나님 "${roomTitle}" 👑방장 등극`,
        timestamp: new Date().getTime(),
        userId: targetUserId,
        isMaster: false,
        unreadCount: 0
      }

      pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: roomId,
            chatMessage: getMasterMessage
          })
        })
      }
      console.log(pushParams)

      await lambda.invoke(pushParams).promise()
    
    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: -2,
          type: 'CHGMASTER',
          actionId: roomId,
          log: '오픈채팅방 방장 위임'
        })
      })
    }
    console.log(logParams)

    await lambda.invoke(logParams).promise()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}