'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 오픈채팅방 입장
 * @method POST enterRoom
 * @returns  
*/

module.fileCheck = async (key) => {
  try {
    let params = {
      Bucket: process.env.mediaBucket,
      Key: profileKey
    }
    console.log(params) 

    let result = await s3.headObject(params).promise()
    return result.ETag != null

  } catch (e) {
    return false
  }
}

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId
    
    let parameters = JSON.parse(event.body || '{}') || {}

    // let onlyOhingProfile = parameters.onlyOhingProfile
    // if (onlyOhingProfile != null && typeof onlyOhingProfile != 'boolean') {
    //   return response.result(400)
    // }

    let maxUserCount = parameters.maxUserCount
    if (maxUserCount < 1 || maxUserCount > 100) {
      return response.result(400)
    }

    let title = parameters.title

    if (title != null && title.trim().length == 0) {
      return response.result(400)
    }
    
    let tags = parameters.tags
    if (tags != null && Array.isArray(tags) == false) {
      return response.result(400)
    }

    let password = parameters.password
    let imageKey = parameters.imageKey

    let query
    let result
    let valueParameters

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = ` 
      select 
        cri.room_id, fi.org_file_nm
      from 
        oi_chat_room_info cri 
          left join oi_file_info fi on cri.room_image_id = fi.file_id
      where
        room_id = ${roomId};
    `

    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404)
    }

    let sets = []

    let roomInfo = result[0]

    if (imageKey != null) {

      if (!imageKey.includes('/') && imageKey.includes('.')) {
        let fileNames = imageKey.split('.')
        let fileName = fileNames[0]
        let fileExtension = fileNames[1]
        imageKey = `img/${fileName}/${fileName}.${fileExtension}`
      }

      // let checkKey1
      // let checkKey2

      // if (!imageKey.includes('/') && imageKey.includes('.')) {
      //   let fileNames = imageKey.split('.')
      //   let fileName = fileNames[0]
      //   let fileExtension = fileNames[1]
      //   checkKey1 = `img/${fileName}/${fileName}.${fileExtension}`
      //   checkKey2 = `img/source/${fileName}.${fileExtension}`
      // }

      // for (;;) {
      //   if (module.fileCheck(imageKey)) {
      //     break
      //   }
      //   if (checkKey1 == null) {
      //     response.result(404, null, '이미지가 존재하지 않습니다.')
      //   }

      //   if (imageKey == checkKey1) {
      //     imageKey = checkKey2
      //   } else if (imageKey == checkKey2) {
      //     response.result(404, null, '이미지가 존재하지 않습니다.')
      //   } else {
      //     imageKey = checkKey1
      //   }
      // }

      // let oldKey = roomInfo.org_file_nm
      // if (oldKey != null && oldKey.length > 0) {

      //   let params = {
      //     FunctionName: `api-new-batch-${process.env.stage}-deleteS3Object`,
      //     InvocationType: "Event",
      //     Payload: JSON.stringify({
      //       body: JSON.stringify({
      //         bucket: process.env.mediaBucket,
      //         key: oldKey
      //       })
      //     })
      //   }
      //   await lambda.invoke(params).promise()
      // }

      let query = `
        insert into oi_file_info (
          media_type_cd, org_file_nm, encoded_yn
        ) value (
          'IMAGE', '${imageKey}', 'Y'
        );
      `
      let result = await mysql.execute(query)
      let fileId = result.insertId

      sets.push(`room_image_id = ${fileId}`)
    }

    let titleChanged = false

    let trimTitle = (title || '').trim()
    let escapedTitle = null
    if (trimTitle.length > 0) {

      query = `
        select
          title
        from
          oi_chat_room_info
        where
          room_id = ${roomId};
      `
      result = await mysql.execute(query)
      let currentTitle = result[0].title
      if (currentTitle != trimTitle) {

        titleChanged = true
        escapedTitle = await mysql.escape(trimTitle)
        sets.push(`title = ${escapedTitle}`)
      }
    }

    let passwordChanged = false

    if (password != null) {
      let passwordString = password.trim()

      let passwordHashValue = passwordString.length > 0 ? `'${authorizer.createOldPasswordHash(passwordString)}'` : ''

      let query = `
        select
          ifnull(room_pwd, '') current_password
        from
          oi_chat_room_info
        where
          room_id = ${roomId};
      `
      result = await mysql.execute(query)
      let currentPassword = result[0].current_password
      if (currentPassword != passwordHashValue) {

        passwordChanged = true

        if (passwordString.length > 0) {
          sets.push(`room_pwd_use_yn = 'Y'`)
          sets.push(`room_pwd = ${passwordHashValue}`)
        } else {
          sets.push(`room_pwd_use_yn = 'N'`)
          sets.push(`room_pwd = null`)
        }
      }
    }

    if (maxUserCount != null) {
      sets.push(`max_user_cnt = ${maxUserCount}`)
    }

    if (sets.length > 0) {
      let query = `
        update 
          oi_chat_room_info
        set
          ${sets.join(',')}
        where
          room_id = ${roomId};
      `
      console.log(query)
      await mysql.execute(query)
    }

    if (tags != null) {
      let query = `
        delete from
          oi_chat_room_tag
        where
          room_id = ${roomId};
      `
      await mysql.execute(query)

      if (tags.length > 0) {
        let values = tags.map(tag => {
          return `( '${tag}', 1 )`
        })
        
        let query = `
          insert into oi_tag_info (
            tag_val, tag_cnt
          ) values 
            ${values.join(',')}
          on duplicate key update
            tag_cnt = values(tag_cnt) + 1;
        `
        await mysql.execute(query)

        values = tags.map(tag => {
          return `'${tag}'`
        })

        query = `
          select 
            tag_id, tag_val
          from
            oi_tag_info
          where
            tag_val in (${values.join(',')});
        `
        let result = await mysql.execute(query)

        values = []

        for (let i = 0; i < tags.length; i++) {

          let tag = tags[i]
          let tagInfo = result.find(tagInfo => {
            return tagInfo.tag_val == tag
          })
          values.push(
            `( ${roomId}, ${tagInfo.tag_id}, ${i+1} )`
          )
        }

        query = `
          insert into oi_chat_room_tag (
            room_id, tag_id, tag_seq
          ) values
            ${values.join(',')};
        `
        await mysql.execute(query)
      }
    }

    if (titleChanged) {

      let text = `채팅방 이름이 ${escapedTitle}(으)로 변경되었습니다.`

      let modifyTitleMessage = {
        roomId: roomId,
        type: 'CHGTITLE',    
        text: text,
        timestamp: new Date().getTime(),
        userId: userId,
        isMaster: false,
        unreadCount: 0,
      }

      query = `
        insert into oi_chat_msg (
          room_id, msg_type_cd, msg_content, send_dt_int, send_user_id
        ) values (
          ?, ?, ?, ?, ?
        );
      `

      valueParameters = [modifyTitleMessage.roomId, modifyTitleMessage.type, text, modifyTitleMessage.timestamp, modifyTitleMessage.userId]

      console.log(query)
      result = await mysql.execute(query, valueParameters)
      modifyTitleMessage.messageId = result.insertId

      let pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: roomId,
            chatMessage: modifyTitleMessage
          })
        })
      }
      console.log(pushParams)

      await lambda.invoke(pushParams).promise()
    }

    if (passwordChanged) {

      // query = `
      //   select 
      //     title
      //   from
      //     oi_chat_room_info
      //   where
      //     room_id = ${roomId};
      // `
      // result = await mysql.execute(query)
      // let roomTitle = result[0].title

      let modifyPasswordMessage = {
        roomId: roomId,
        type: 'CHGPW',    
        text: `방장이 비밀번호를 변경하였습니다.`,
        timestamp: new Date().getTime(),
        userId: userId,
        isMaster: false,
        unreadCount: 0,
      }

      query = `
        insert into oi_chat_msg (
          room_id, msg_type_cd, msg_content, send_dt_int, send_user_id
        ) values (
          ${modifyPasswordMessage.roomId}, '${modifyPasswordMessage.type}', '방장이 비밀번호를 변경하였습니다.', ${modifyPasswordMessage.timestamp}, ${modifyPasswordMessage.userId}
        );
      `
      console.log(query)
      result = await mysql.execute(query)
      modifyPasswordMessage.messageId = result.insertId

      let pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: roomId,
            chatMessage: modifyPasswordMessage
          })
        })
      }
      console.log(pushParams)

      await lambda.invoke(pushParams).promise()
    }

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}