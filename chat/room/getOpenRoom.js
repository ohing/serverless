'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 오픈채팅방 정보
 * @method get getOpenRoom
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query = `
      select 
        cri.room_id, cri.room_status_cd room_status, cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password, cri.ohing_profile_use_yn only_ohing_profile, 
        cri.join_user_cnt join_user_count, cri.max_user_cnt max_user_count, cri.reg_dt created_at,
        fir.org_file_nm cover_image_url, substring_index(fir.org_file_nm, '/', -1) cover_file_name,
        cui.user_id, ui.user_login_id nickname, fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url, substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name,
        cui2.use_yn already_join, cui2.deport_yn deported, cui2.master_yn is_master
      from
        oi_chat_room_info cri 
        join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
        join oi_user_info ui on cui.user_id = ui.user_id 
        left join oi_file_info fir on cri.room_image_id = fir.file_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id and cui2.user_id = ${userId}
      where
        cri.room_id = ${roomId}
        and cri.room_type_cd = 'OPEN';
    `
    let result = await mysql.execute(query)

    if (result.length == 0) {
      return response.result(404, null, '채팅방을 찾을 수 없습니다.')
    }

    let room = result[0]
    room.already_join = room.already_join == 'Y'
    room.deported = room.deported == 'Y'
    room.is_master = room.is_master == 'Y'

    room = (await this.applyTags([room]))[0]

    query = `
      select 
        a.* 
      from 
        (select 
          cui.user_id, cui.chat_profile_id, cui.master_yn is_master,
          case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
          case when cui.chat_profile_id is null then ui.self_intro_content else cpi.self_intro_content end self_introduction,
          case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end profile_image_url,
          substring_index(case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end, '/', -1) profile_file_name
        from 
          oi_chat_user_info cui join
          oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where 
          room_id = ${roomId}) a
      order by
        field(user_id, ${userId}) desc, field(is_master, 'Y') desc, nickname asc;
    `

    result = await mysql.execute(query)

    room.members = result.map(row => {
      row.is_master = row.is_master == 'Y'
      return row
    })

    mysql.end()

    return response.result(200, room)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}

exports.applyTags = async (rooms) => {

  if (rooms.length > 0) {
        
    let roomIds = rooms.map(row => {
      return row.room_id
    })

    let roomIdMap = {}
    for (let i = 0; i < rooms.length; i++) {
      rooms[i].tags = []
      rooms[i].needs_password = rooms[i].needs_password != 0
      if (rooms[i].chat_profile_image_url != null) {
        rooms[i].profile_image_url = rooms[i].chat_profile_image_url
      }
      if (rooms[i].chat_nickname != null) {
        rooms[i].nickname = rooms[i].chat_nickname
      }
      delete rooms[i].chat_nickname
      delete rooms[i].chat_profile_image_url

      roomIdMap[rooms[i].room_id] = i
    }

    let query = `
      select 
        ti.tag_val, crt.room_id
      from 
        oi_chat_room_tag crt
        left join oi_tag_info ti on crt.tag_id = ti.tag_id
      where 
        crt.room_id in (${roomIds.join(',')}) 
      order by 
        crt.room_id asc, crt.tag_seq asc;
    `
    let result = await mysql.execute(query)

    result.forEach(row => {
      let index = roomIdMap[row.room_id]
      rooms[index].tags.push(row.tag_val)
    }) 
  }

  return rooms
}