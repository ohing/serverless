'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 강퇴목록
 * @method GET getDeports
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        room_id
      from
        oi_chat_room_info
      where
        room_id = ${roomId}
        and room_type_cd = 'OPEN';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404)
    }

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and master_yn = 'Y'
        and use_yn = 'Y';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    query = `
      select
        ui.user_id,
        ui.user_login_id nickname, fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name, cui.chat_profile_id,
        cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url, substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name,
        cpi.self_intro_content self_introduction
      from
        oi_chat_user_info cui
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
      where
        room_id = ${roomId}
        and deport_yn = 'Y'
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
      order by
        cui.deport_dt asc;
    `
    result = await mysql.execute(query)

    mysql.end()

    if (result.length == 0) {
      return response.result(200, [])
    }
    
    let deports = []

    for (let i = 0; i < result.length; i++) {
      let row = result[i]

      if (row.chat_profile_id != null) {
        row.nickname = row.chat_nickname
        row.profile_image_url = row.chat_profile_image_url
        row.profile_file_name = row.chat_profile_file_name
      }

      delete row.chat_nickname
      delete row.chat_profile_image_url
      delete row.chat_profile_file_name

      deports.push(row)
    }

    let readable = await redis.readable(3)

    let onlineKeys = deports.map(deport => {
      return `alive:${deport.user_id}`
    })

    let onlines = await readable.mget(onlineKeys)
    redis.end()

    for (let i = 0; i < deports.length; i++) {
      deports[i].is_online = onlines[i] != null
    }
    
    return response.result(200, deports)
  
  } catch(e) {

    console.error(e)

    redis.end()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}