'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 강퇴
 * @method PUT deport
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let roomId = event.pathParameters.roomId;

    let parameters = JSON.parse(event.body || '{}') || {};

    let targetUserId = parameters.targetUserId;
    let targetMessageId = parameters.targetMessageId;

    if (targetUserId == null && targetMessageId == null) {
      return response.result(400);
    }

    let query;
    let result;
    let valueParameters;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    if (openType == 'CLOSE') {
      mysql.end();
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.');
    }

    query = `
      select
        room_id, title
      from
        oi_chat_room_info
      where
        room_id = ${roomId}
        and room_type_cd = 'OPEN';
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(404, null, '존재하지 않는 채팅방입니다.');
    }

    let roomTitle = result[0].title;

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and master_yn = 'Y'
        and use_yn = 'Y';
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(403, null, '권한이 없습니다.');
    }

    if (targetMessageId != null) {
      query = `
        select
          cui.user_id
        from
          oi_chat_msg cm
          join oi_chat_user_info cui on cm.room_id = cui.room_id and cm.send_user_id = cui.user_id and cui.deport_yn = 'N' and cui.join_dt < from_unixtime(cm.send_dt_int / 1000)
        where
          cm.msg_id = ${targetMessageId}
        limit 1;
      `;
      result = await mysql.execute(query);

      if (result.length == 0) {
        mysql.end();
        return response.result(404, null, '이미 나간 사용자입니다.');
      }

      targetUserId = result[0].user_id;
    }

    query = `
      select
        cui.room_id,
        case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname
      from
        oi_chat_user_info cui
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
      where
        cui.room_id = ${roomId} 
        and cui.user_id = ${targetUserId}
        and cui.master_yn = 'N'
        and cui.use_yn = 'Y';
    `;
    result = await mysql.execute(query);

    if (result.length == 0) {
      mysql.end();
      return response.result(404, null, '이미 나간 사용자입니다.');
    }

    let deportedUserNickname = result[0].nickname;

    query = `
      update
        oi_chat_user_info
      set
        alarm_yn = 'N', use_yn = 'N', deport_yn = 'Y', deport_dt = now()
      where
        room_id = ${roomId}
        and user_id = ${targetUserId}
    `;
    await mysql.execute(query);

    query = `
      update
        oi_chat_room_info
      set
        join_user_cnt = (
          select 
            count(*) 
          from 
            oi_chat_user_info 
          where 
            room_id = ${roomId}
            and use_yn = 'Y'
        )
      where
        room_id = ${roomId}
    `;
    await mysql.execute(query);

    query = `
      select 
        cnc.comment_id
      from 
        oi_chat_notice_comment cnc
        join oi_chat_notice_info cni on cnc.chat_notice_id = cni.chat_notice_id
      where
        cni.room_id = ${roomId}
        and cnc.reg_user_id = ${userId}
    `;
    result = await mysql.execute(query);
    let commentIds = result.map(row => {
      return row.comment_id;
    });

    if (commentIds.length > 0) {
      query = `
        update
          oi_chat_notice_comment
        set
          reg_user_id = null, reg_user_chat_profile_id = null
        where
          comment_id in (
            ${commentIds.join(',')}
          );
      `;
      await mysql.execute(query);
    }

    query = `
      update
        oi_chat_notice_info
      set
        reg_user_id = null
      where 
        room_id = ${roomId}
        and reg_user_id = ${targetUserId};
    `;
    await mysql.execute(query);

    query = `
      select
        push_auth_key
      from
        oi_user_device_info
      where
        user_id = ${targetUserId}
        and login_yn = 'Y'
        and push_auth_key is not null and push_auth_key  <> ''
        ;
    `;
    result = await mysql.execute(query);

    if (result.length > 0) {
      let fcmToken = result[0].push_auth_key;
      if (fcmToken != null && fcmToken.length > 0) {
        await lambda.invoke({
          FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
          InvocationType: "Event",
          Payload: JSON.stringify({
            body: JSON.stringify({
              steps: [
                {
                  type: 'unsubscribe',
                  tokens: [fcmToken],
                  topics: [`openchat-${roomId}`]
                }
              ]
            })
          })
        }).promise();
      }
    }

    let deportText = `방장의 이름으로 ${deportedUserNickname}님 응징`;

    let deportedMessage = {
      roomId: roomId,
      type: 'DEPORT',
      text: deportText,
      timestamp: new Date().getTime(),
      userId: userId,
      targetUserId: targetUserId,
      isMaster: false,
      unreadCount: 0,
    };

    query = `
      insert into oi_chat_msg (
        room_id, msg_type_cd, msg_content, send_dt_int, send_user_id
      ) values (
        ?, ?, ?, ?, ?
      );
    `;
    valueParameters = [
      deportedMessage.roomId, deportedMessage.type, deportText, deportedMessage.timestamp, deportedMessage.userId
    ];
    result = await mysql.execute(query, valueParameters);
    deportedMessage.messageId = result.insertId;

    mysql.end();

    let pushParams = {
      FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          type: 'SendMessage',
          roomId: roomId,
          targetUserId: targetUserId,
          chatMessage: deportedMessage
        })
      })
    };
    console.log(pushParams);


    await lambda.invoke(pushParams).promise();

    return response.result(200, {});

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};