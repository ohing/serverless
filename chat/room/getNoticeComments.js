'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`);
const response = require('@ohing/ohingresponse');


/**
 * @author Karl <karl@ohing.net> 
 * @description 공지사항 댓글 목록
 * @method GET getNoticeComment
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let roomId = event.pathParameters.roomId;

    let query;
    let result;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    if (openType == 'CLOSE') {
      mysql.end();
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.');
    }

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y'
        and deport_yn = 'N';
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(403, null, '권한이 없습니다.');
    }

    let chatNoticeId = event.pathParameters.chatNoticeId;

    // const parameters = JSON.parse(event.body || '{}') || {}

    let parameters = event.queryStringParameters || {};

    let keys = Object.keys(parameters);
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key];
      }
    }

    let rowCount = parameters.rowCount == null ? 15 : parseInt(parameters.rowCount);
    let firstId = parameters.firstId == null ? 2147483647 : parseInt(parameters.firstId);

    query = `
      select
        (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id 
          where like_target_type_cd = 'CNCOMT' and target_id = cnc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
        if ((select count(*) from oi_like_log where like_target_type_cd = 'CNCOMT' and target_id = cnc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
        cnc.comment_id, cnc.content, cnc.del_yn is_deleted, cnc.anony_yn is_anonymous, cnc.reg_dt,
        ui.user_id,
        case
          when ui.user_id is null then '알수없음'
          else case 
            when cnc.anony_yn = 'Y' then '익명사용자' 
            else case
              when cpi.chat_profile_id is null then ui.user_login_id 
              else cpi.nick_nm
            end
          end
        end nickname,
        case
          when ui.user_id is null then null
          else case 
            when cnc.anony_yn = 'Y' then null 
            else case
              when cpi.chat_profile_id is null then fi.org_file_nm 
              else fi2.org_file_nm
            end
          end
        end profile_image_url, 
        case
          when ui.user_id is null then null
          else case
            when cnc.anony_yn = 'Y' then null 
            else case
              when cpi.chat_profile_id is null then substring_index(fi.org_file_nm, '/', -1)
              else substring_index(fi2.org_file_nm, '/', -1)
            end
          end
        end profile_file_name,
        case when cnc.anony_yn = 'Y' then null else cpi.chat_profile_id end chat_profile_id,
        cpi.self_intro_content self_introduction
      from 
        oi_chat_notice_comment cnc
        left join oi_user_info ui on cnc.reg_user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cnc.reg_user_chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
      where
        cnc.chat_notice_id = ${chatNoticeId}
        and ui.profile_open_type_cd != 'CLOSE'
        and cnc.parent_comment_id is null
      order by
        cnc.comment_id desc
      ;
    `;
    console.log(query);
    let comments = await mysql.execute(query);
    let previousCount = 0;

    if (comments.length > 0) {

      comments = comments.map(comment => {
        comment.replies = [];
        comment.previous_reply_count = 0;
        comment.liked = comment.liked == 'Y';
        comment.is_anonymous = comment.is_anonymous == 'Y';
        comment.is_deleted = comment.is_deleted == 'Y';

        // if (comment.chat_profile_id != null) {
        //   comment.nickname = comment.chat_nickname
        //   comment.profile_image_url = comment.chat_profile_image_url
        //   comment.profile_file_name = comment.chat_profile_file_name
        // }

        // delete comment.chat_nickname
        // delete comment.chat_profile_image_url
        // delete comment.chat_profile_file_name

        return comment;
      });

      comments.reverse();

      console.log(comments);

      query = `
        select
          count(*) count
        from
          oi_chat_notice_comment
        where
          chat_notice_id = ${chatNoticeId}
          and parent_comment_id is null
          and comment_id < ${comments[0].comment_id};
      `;
      result = await mysql.execute(query);
      if (result.length > 0) {
        previousCount = result[0].count;

        let commentIds = comments.map(comment => {
          return comment.comment_id;
        });
        let commentIdString = commentIds.join(',');

        query = `
          select parent_comment_id, count(*) count from oi_chat_notice_comment where parent_comment_id in (${commentIdString}) group by parent_comment_id;
        `;
        let countResult = await mysql.execute(query);

        countResult.forEach(row => {
          for (let i = 0; i < comments.length; i++) {
            if (comments[i].comment_id == row.parent_comment_id) {
              comments[i].previous_reply_count = row.count;
              break;
            }
          }
        });

        console.log(comments);

        query = `
          select
            (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id
              where like_target_type_cd = 'CNCOMT' and target_id = cnc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
            if ((select count(*) from oi_like_log where like_target_type_cd = 'CNCOMT' and target_id = cnc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
            cnc.comment_id, cnc.parent_comment_id parent_id, cnc.content, cnc.anony_yn is_anonymous, cnc.reg_dt,
            ui.user_id,
            case
              when ui.user_id is null then '알수없음'
              else case 
                when cnc.anony_yn = 'Y' then '익명사용자' 
                else case
                  when cpi.chat_profile_id is null then ui.user_login_id 
                  else cpi.nick_nm
                end
              end
            end nickname,
            case
              when ui.user_id is null then null
              else case 
                when cnc.anony_yn = 'Y' then null 
                else case
                  when cpi.chat_profile_id is null then fi.org_file_nm 
                  else fi2.org_file_nm
                end
              end
            end profile_image_url, 
            case
              when ui.user_id is null then null
              else case
                when cnc.anony_yn = 'Y' then null 
                else case
                  when cpi.chat_profile_id is null then substring_index(fi.org_file_nm, '/', -1)
                  else substring_index(fi2.org_file_nm, '/', -1)
                end
              end
            end profile_file_name,
            case when cnc.anony_yn = 'Y' then null else cpi.chat_profile_id end chat_profile_id,
            cpi.self_intro_content self_introduction
          from
            oi_chat_notice_comment cnc
            left join oi_chat_notice_comment cnc2 on cnc.parent_comment_id = cnc2.parent_comment_id and cnc.comment_id < cnc2.comment_id
            join oi_user_info ui on cnc.reg_user_id = ui.user_id
            left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
            left join oi_file_info fi on pi.file_id = fi.file_id
            left join oi_chat_profile_info cpi on cnc.reg_user_chat_profile_id = cpi.chat_profile_id
            left join oi_file_info fi2 on cpi.image_id = fi2.file_id
          where 
            cnc.parent_comment_id in (${commentIdString})
            and ui.profile_open_type_cd != 'CLOSE'
          group by 
            cnc.parent_comment_id, cnc.comment_id
          order by
            cnc.parent_comment_id, cnc.comment_id desc;
        `;
        result = await mysql.execute(query);

        for (let row of result) {

          row.liked = row.liked == 'Y';
          row.is_anonymous = row.is_anonymous == 'Y';
          row.is_deleted = false;

          // if (row.chat_profile_id != null) {
          //   row.nickname = row.chat_nickname
          //   row.profile_image_url = row.chat_profile_image_url
          //   row.profile_file_name = row.chat_profile_file_name
          // }

          // delete row.chat_nickname
          // delete row.chat_profile_image_url
          // delete row.chat_profile_file_name

          for (let i = 0; i < comments.length; i++) {
            if (comments[i].comment_id == row.parent_id) {
              comments[i].replies.unshift(row);
              comments[i].previous_reply_count--;
              break;
            }
          }
        }
      }
    }

    mysql.end();

    let readable = await redis.readable(3);

    var onlines = {};

    comments.forEach(comment => {
      onlines[comment.user_id] = false;
      comment.replies.forEach(reply => {
        onlines[reply.user_id] = false;
      });
    });

    keys = Object.keys(onlines);
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];
      let onlineKey = `alive:${key}`;
      let isOnline = await readable.get(onlineKey);
      onlines[key] = isOnline == "true" || isOnline == true;
    }

    for (let i = 0; i < comments.length; i++) {
      let comment = comments[i];
      comments[i].is_online = onlines[comment.user_id];
      for (let j = 0; j < comment.replies.length; j++) {
        let reply = comment.replies[j];
        comments[i].replies[j].is_online = onlines[reply.user_id];
      }
    }

    redis.end();
    return response.result(200, { previous_count: previousCount, comments: comments });

  } catch (e) {

    console.error(e);

    redis.end();
    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
