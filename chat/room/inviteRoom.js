'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const iot = require(`@ohing/ohingiot`)
const response = require('@ohing/ohingresponse')

const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()


/**
 * @author Karl <karl@ohing.net> 
 * @description 오픈채팅방 초대
 * @method post inviteOpenRoom
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    let userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }
    userId = parseInt(userId)

    let roomId = event.pathParameters.roomId

    let parameters = JSON.parse(event.body || '{}') || {}
    let targetUserIds = parameters.targetUserIds

    if (targetUserIds == null || targetUserIds.length == 0) {
      return response.result(400)
    }

    let query
    let result
    let valueParameters

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    let sendTime = new Date().getTime()

    query = `
      select 
        room_type_cd 
      from 
        oi_chat_room_info
      where
        room_id = ${roomId}
        and room_status_cd = 'ACTIVE';
    `
    result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(404)
    }
    let roomType = result[0].room_type_cd

    let values = []
    valueParameters = []
    let newRoomId
    let inviteMessage = null

    let updateRoomInfos = []

    if (roomType == 'OPEN') {

      newRoomId = roomId

      query = `
        select 
          cri.room_id, cui2.user_id
        from 
          oi_chat_user_info cui
          join oi_chat_user_info cui2 on cui.room_id = cui2.room_id
          join oi_chat_room_info cri on cui.room_id = cri.room_id
        where
          cui.user_id = ${userId} 
          and cui2.user_id in (${targetUserIds.join(',')})
          and cri.room_type_cd = 'ONE'
        group by  
          cui2.user_id;
      `
      result = await mysql.execute(query)

      let chatUserInsertValues = []
      let includedUserIds = []      

      for (let row of result) {
        includedUserIds.push(row.user_id)
        updateRoomInfos.push({
          roomId: row.room_id,
          userId: row.user_id
        })
      }

      console.log('includedUserIds', includedUserIds)

      for (let targetUserId of targetUserIds) {
        if (!includedUserIds.includes(parseInt(targetUserId))) {

          query = `
            select
              count(*) count
            from
              oi_user_relation
            where
              relation_type_cd = '02'
              and (
                (user_id = ${userId} and relation_user_id = ${targetUserId})
                or (user_id = ${targetUserId} and relation_user_id = ${userId})
              );
          `
          result = await mysql.execute(query)
          if (result[0].count > 0) {
            continue
          }

          query = `
            insert into oi_chat_room_info (
              room_type_cd, max_user_cnt, join_user_cnt, reg_user_id
            ) values (
              'ONE', 0, 2, ${userId}
            );
          `
          console.log(query)
          let roomResult = await mysql.execute(query)
          let insertRoomId = roomResult.insertId

          chatUserInsertValues.push(`(
            ${insertRoomId}, ${userId}, now(), 'Y'
          )`)
          chatUserInsertValues.push(`(
            ${insertRoomId}, ${targetUserId}, now(), 'N'
          )`)

          updateRoomInfos.push({
            roomId: insertRoomId,
            userId: targetUserId
          })
        }
      }

      console.log(updateRoomInfos)

      if (chatUserInsertValues.length > 0) {
        query = `
          insert into oi_chat_user_info (
            room_id, user_id, join_dt, master_yn
          ) values 
            ${chatUserInsertValues.join(',')};
        `
        console.log(query)
        await mysql.execute(query)
      }

      query = `
        select 
          ui.user_login_id nick_name, fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
          cpi.nick_nm chat_nick_name, fi2.org_file_nm chat_profile_image_url, substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name, cui2.room_id, cui2.user_id
        from 
          oi_chat_room_info cri
          join oi_chat_user_info cui on cri.room_id = cui.room_id
          join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
          join oi_chat_user_info cui2 on cui.room_id = cui2.room_id
        where
          cri.room_type_cd = 'ONE'
          and cui.user_id = ${userId}
          and cui2.user_id in (${targetUserIds.join(',')})
          and ui.user_status_cd = 'ACTIVE'
          and ui.profile_open_type_cd != 'CLOSE' 
      `
      console.log(query)
      result = await mysql.execute(query)

      console.log(result)

      let completedUserIds = []

      for (let chatUser of result) {

        let oneRoomId = chatUser.room_id
        let chatUserId = chatUser.user_id

        let profileImageUrl = chatUser.chat_profile_file_name != null ? chatUser.chat_profile_file_name : chatUser.profile_file_name != null ? chatUser.profile_file_name : null
        let nickname = chatUser.chat_nick_name != null ? chatUser.chat_nick_name : chatUser.nick_name

        query = `
          update 
            oi_chat_user_info
          set 
            use_yn = 'Y', alarm_yn = 'Y'
          where
            room_id = ${oneRoomId}
        `
        console.log(query)
        await mysql.execute(query)

        query = `
          select 
            cri.room_id, cri.title, cri.join_user_cnt join_user_count, cri.max_user_cnt max_user_count,
            substring_index(fi.org_file_nm, '/', -1) cover_file_name,
            case when length(cri.room_pwd) > 0 then 'Y' else 'N' end needs_password,            
            case when cui.chat_profile_id is null then fi2.org_file_nm else fi3.org_file_nm end profile_image_url,
            case when cui.chat_profile_id is null then substring_index(fi2.org_file_nm, '/', -1) else substring_index(fi3.org_file_nm, '/', -1) end profile_file_name,
            case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
            concat(
              '[',
              (
                select
                  group_concat(concat('"', ti.tag_val, '"') separator ',')
                from 
                  oi_chat_room_info cri2
                  join oi_chat_room_tag crt on cri2.room_id = crt.room_id
                  join oi_tag_info ti on crt.tag_id = ti.tag_id
                where 
                  cri2.room_id = cri.room_id
                order by
                  crt.tag_seq asc
              ),
              ']'
            ) tags
            
          from
            oi_chat_room_info cri
            left join oi_file_info fi on cri.room_image_id = fi.file_id
            join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.use_yn = 'Y' and cui.master_yn = 'Y'
            join oi_user_info ui on cui.user_id = ui.user_id
            left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
            left join oi_file_info fi2 on upi.file_id = fi2.file_id
            left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
            left join oi_file_info fi3 on cpi.image_id = fi3.file_id

          where
            cri.room_id = ${roomId}
        `
        result = await mysql.execute(query)

        let roomInfo = result.length == 0 ? null : result[0]
        roomInfo.tags = JSON.parse(roomInfo.tags)
        roomInfo.needs_password = roomInfo.needs_password == 'Y'
        
        let inviteRoomInfo = JSON.stringify(roomInfo)

        query = `
          select 
            case
              when cpi.chat_profile_id is null then null
              else ifnull(cpi.self_intro_content, '')
            end self_introduction,
            cui.master_yn
          from
            oi_chat_user_info cui
            left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          where
            cui.room_id = ${oneRoomId}
            and cui.user_id = ${userId};
        `
        result = await mysql.execute(query)

        let selfIntroduction = result[0].self_introduction

        let masterYn = result[0].master_yn

        values.push('(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)')
        valueParameters.push(oneRoomId, 'OPENINVITE', null, sendTime, userId, profileImageUrl, nickname, selfIntroduction, masterYn, inviteRoomInfo)

        completedUserIds.push(chatUser.user_id)
      }

    } else {

      query = `
        select 
          ui.user_login_id, substring_index(fi.org_file_nm, '/', -1) profile_image_url
        from
          oi_user_info ui
          left join oi_user_profile_image upi on ui.user_id = upi.user_id
          left join oi_file_info fi on upi.file_id = fi.file_id
        where
          ui.user_id = ${userId};
      `
      result = await mysql.execute(query)
      let userLoginId = result[0].user_login_id
      let profileImageUrl = result[0].profile_image_url

      query = `
        select 
          user_login_id
        from
          oi_user_info
        where
          user_id in (${targetUserIds.join(',')});
      `
      result = await mysql.execute(query)
      let loginIds = result.map(row => {
        return row.user_login_id
      })

      let userIds = [userId]
      for (let targetUserId of targetUserIds) {
        userIds.push(parseInt(targetUserId))
      }

      query = `
        select 
          user_id
        from 
          oi_chat_user_info cui
        where
          cui.room_id = ${roomId}
          and use_yn = 'Y';
      `
      result = await mysql.execute(query)
      for (let row of result) {
        userIds.push(row.user_id)
      }

      let userIdSet = new Set(userIds)
      userIds = Array.from(userIdSet)

      query = `
        insert into oi_chat_room_info (
          room_type_cd, reg_user_id
        ) values (
          'MESG', ${userId}
        );
      `
      result = await mysql.execute(query)
      newRoomId = result.insertId

      let userValues = []

      for (let targetUserId of userIds) {

        query = `
          select
            count(*) count
          from
            oi_user_relation
          where
            relation_type_cd = '02'
            and (
              (user_id = ${userId} and relation_user_id = ${targetUserId})
              or (user_id = ${targetUserId} and relation_user_id = ${userId})
            );
        `
        result = await mysql.execute(query)
        if (result[0].count == 0) {
          userValues.push(`
            ( ${newRoomId}, ${targetUserId}, now(), now(), now() )
          `) 
        }
      }

      if (userValues.length == 0) {
        query = `
          delete from
            oi_chat_room_info
          where
            room_id = ${newRoomId};
        `
        result = await mysql.execute(query)

        mysql.end()
        return response.result(403, null, '초대 가능한 대상이 없습니다.') 
      }

      query = `
        insert into oi_chat_user_info (
          room_id, user_id, join_dt, last_online_dt, first_room_in_dt
        ) values
          ${userValues.join(',')};
      `
      await mysql.execute(query)

      let message = `${userLoginId}님이 ${loginIds.join(', ')}님을 초대했습니다.`

      // values.push(`
      //   ( ${newRoomId}, 'GROUPINVITE', '${message}', ${sendTime}, ${userId}, ${profileImageUrl}, ${nickname}, null )
      // `)

      inviteMessage = {
        roomId: newRoomId,
        type: 'GROUPINVITE',
        text: message,
        timestamp: sendTime,
        userId: userId,
        media_key: profileImageUrl,
        nickname: userLoginId,
        isMaster: false,
        unreadCount: 0,
      }
    }
    
    if (values.length > 0) {

      query = `
        insert into oi_chat_msg (
          room_id, msg_type_cd, msg_content, send_dt_int, send_user_id, send_profile_image_url, send_user_nick_nm, chat_profile_intro_content, master_yn, invite_room_info
        ) values 
          ${values.join(',')};
      `

      result = await mysql.execute(query, valueParameters)
      let firstId = result.insertId

      let messageIds = []
      for (let i = 0; i < values.length; i++) {
        messageIds.push(firstId+i)
      }
      
      query = `
        select 
          msg_id, room_id, msg_type_cd type, msg_content text, send_dt_int timestamp, send_user_id user_id, 
          send_profile_image_url profile_file_name, send_user_nick_nm nickname, chat_profile_intro_content chat_profile_self_introduction, invite_room_info
        from 
          oi_chat_msg
        where
          msg_id in (${messageIds.join(',')})
      `    
      result = await mysql.execute(query)

      

      iot.connect()
      
      for (let iotMessage of result) {
        iotMessage.is_master = false
        await iot.publish(`${process.env.stage}/message/${iotMessage.room_id}`, JSON.stringify(camelcase(iotMessage, {deep:true})))
      }

      iot.disconnect()
    }

    let room

    if (roomType == 'OPEN') {

      query = `
         select 
          cri.room_id, cri.room_type_cd room_type, cri.room_status_cd room_status, cri.title, if(length(cri.room_pwd) > 0, 1, 0) needs_password, cri.ohing_profile_use_yn only_ohing_profile, 
          cri.join_user_cnt join_user_count, cri.max_user_cnt max_user_count, cri.reg_dt created_at,
          fir.org_file_nm cover_image_url, substring_index(fir.org_file_nm, '/', -1) cover_file_name,
          cui.user_id, ui.user_login_id nickname, fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
          cpi.nick_nm chat_nickname, fi2.org_file_nm chat_profile_image_url, substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name,
          cui2.use_yn already_join, cui2.deport_yn deported, cui2.master_yn is_master, cui2.alarm_yn use_notification
        from
          oi_chat_room_info cri 
          join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
          join oi_user_info ui on cui.user_id = ui.user_id 
          left join oi_file_info fir on cri.room_image_id = fir.file_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
          left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id and cui2.user_id = ${userId}
        where
          cri.room_id = ${newRoomId};
      `
      result = await mysql.execute(query)

      room = result[0]
      room.already_join = room.already_join == 'Y'
      room.deported = room.deported == 'Y'
      room.is_master = room.is_master == 'Y'
      room = (await this.applyTags([room]))[0]

    } else {

      query = `
        select 
          cri.room_id, cri.room_type_cd room_type, cri.reg_dt created_at,
          cui2.alarm_yn use_notification
        from
          oi_chat_room_info cri
          left join oi_chat_user_info cui2 on cri.room_id = cui2.room_id and cui2.user_id = ${userId}
        where
          cri.room_id = ${newRoomId}
      `
      result = await mysql.execute(query)
      room = result[0]
    }
    
    room.use_notification = room.use_notification == 'Y'

    query = `
      select 
        a.* 
      from 
        (select 
          cui.user_id, cui.chat_profile_id, cui.master_yn is_master,
          case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
          case when cui.chat_profile_id is null then ui.self_intro_content else cpi.self_intro_content end self_introduction,
          case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end profile_image_url,
          substring_index(case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end, '/', -1) profile_file_name
        from 
          oi_chat_user_info cui join
          oi_user_info ui on cui.user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where 
          room_id = ${roomId}) a
      order by
        field(user_id, ${userId}) desc, field(is_master, 'Y') desc, nickname asc;
    `

    result = await mysql.execute(query)

    room.members = result.map(row => {
      row.is_master = row.is_master == 'Y'
      return row
    })

    mysql.end()

    if (roomType == 'OPEN') {

      let pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatRoomInvitePush`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            roomId: roomId,
            targetUserIds: targetUserIds
          })
        })
      }
      console.log('pushParams', pushParams)

      await lambda.invoke(pushParams).promise()

      for (let updateRoomInfo of updateRoomInfos) {
        let params = {
          FunctionName: `api-new-batch-${process.env.stage}-sendUpdatedRoomInfo`,
          InvocationType: "Event",
          Payload: JSON.stringify({
            body: JSON.stringify({
              roomId: updateRoomInfo.roomId,
              userIds: [updateRoomInfo.userId, userId],
            })
          })
        }

        await lambda.invoke(params).promise()
      }
      
    } else {

      let pushParams = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: newRoomId,
            chatMessage: inviteMessage
          })
        })
      }
      console.log(pushParams)

      await lambda.invoke(pushParams).promise()

      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-sendUpdatedRoomInfo`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            roomId: newRoomId,
            userIds: targetUserIds.concat([userId]),
          })
        })
      }

      await lambda.invoke(params).promise()
    }

    return response.result(200, room, '선택하신 대상(들)에게 초대 메시지가 전송되었습니다.')
    
  } catch (e) {
    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}

exports.applyTags = async (rooms) => {

  if (rooms.length > 0) {
        
    let roomIds = rooms.map(row => {
      return row.room_id
    })

    let roomIdMap = {}
    for (let i = 0; i < rooms.length; i++) {
      rooms[i].tags = []
      rooms[i].needs_password = rooms[i].needs_password != 0
      if (rooms[i].chat_profile_image_url != null) {
        rooms[i].profile_image_url = rooms[i].chat_profile_image_url
      }
      if (rooms[i].chat_nickname != null) {
        rooms[i].nickname = rooms[i].chat_nickname
      }
      delete rooms[i].chat_nickname
      delete rooms[i].chat_profile_image_url

      roomIdMap[rooms[i].room_id] = i
    }

    let query = `
      select 
        ti.tag_val, crt.room_id
      from 
        oi_chat_room_tag crt
        left join oi_tag_info ti on crt.tag_id = ti.tag_id
      where 
        crt.room_id in (${roomIds.join(',')}) 
      order by 
        crt.room_id asc, crt.tag_seq asc;
    `
    let result = await mysql.execute(query)

    result.forEach(row => {
      let index = roomIdMap[row.room_id]
      rooms[index].tags.push(row.tag_val)
    }) 
  }

  return rooms
}