'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 공지사항 댓글 수정
 * @method PUT modifyNoticeComment
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y'
        and deport_yn = 'N';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let commentId = event.pathParameters.commentId

    query = `
      select
        comment_id
      from
        oi_chat_notice_comment
      where
        comment_id = ${commentId}
        and reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let parameters = JSON.parse(event.body || '{}') || {}
    let content = (parameters.content || '').trim()
    let isAnonymous = parameters.isAnonymous

    if (content.length == 0 && isAnonymous == null) {
      mysql.end()
      return response.result(400)
    }

    let sets = []

    if (content.length > 0) {
      content = await mysql.escape(content)
      sets.push(`content = ${content}`)
    }
    sets.push(`anony_yn = '${isAnonymous ? 'Y' : 'N'}'`)

    query = `
      update
        oi_chat_notice_comment
      set
        ${sets.join(',')}
      where
        comment_id = ${commentId};
    `
    await mysql.execute(query)

    query = `
      select
        (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id
          where like_target_type_cd = 'CNCOMT' and target_id = cnc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
        if ((select count(*) from oi_like_log where like_target_type_cd = 'CNCOMT' and target_id = cnc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
        cnc.comment_id, cnc.parent_comment_id parent_id, cnc.content, cnc.anony_yn is_anonymous, cnc.reg_dt,
        ui.user_id,
        case 
          when cnc.anony_yn = 'Y' then '익명사용자' 
          else case 
            when cpi.chat_profile_id is null then ui.user_login_id 
            else cpi.nick_nm
          end
        end nickname,
        case 
          when cnc.anony_yn = 'Y' then null
          else case 
            when cpi.chat_profile_id is null then fi.org_file_nm
            else fi2.org_file_nm
          end
        end profile_image_url,
        case 
          when cnc.anony_yn = 'Y' then null
          else case 
            when cpi.chat_profile_id is null then substring_index(fi.org_file_nm, '/', -1)
            else substring_index(fi2.org_file_nm, '/', -1)
          end
        end profile_file_name,
        case when cnc.anony_yn = 'Y' then null else cpi.chat_profile_id end chat_profile_id,
        cpi.self_intro_content self_introduction
      from 
        oi_chat_notice_comment cnc
        join oi_user_info ui on cnc.reg_user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cnc.reg_user_chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
      where
        cnc.comment_id = ${commentId};
    `
    result = await mysql.execute(query)
    console.log(result)
    mysql.end()

    let comment = result[0]
    comment.is_online = true
    comment.is_anonymous = comment.is_anonymous == 'Y'

    if (comment.chat_nickname != null) {
      comment.nickname = comment.chat_nickname
    }

    if (comment.chat_profile_image_url != null) {
      comment.profile_image_url = comment.chat_profile_image_url
    }

    delete comment.chat_nickname
    delete comment.chat_profile_image_url
    
    mysql.end()

    return response.result(200, comment)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
