'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const search = require(`@ohing/ohingsearch`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 숨김 목록
 * @method GET getHiddens
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    // const parameters = JSON.parse(event.body || '{}') || {}
    let parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    let rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    const lastAccountId = parameters.lastAccountId || ''

    let searchText = (parameters.searchText || '').trim()
    let searchInfo = search.parse(searchText)
    let searchCondition = search.searchCondition(searchInfo, 'USER')

    let query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        case 
          when fi.org_file_nm is null then 'img/notification-default/notification-default.png'
          else fi.org_file_nm
        end profile_image_url, 
        case 
          when fi.org_file_nm is null then 'notification-default.png'
          else substring_index(fi.org_file_nm, '/', -1) 
        end profile_file_name
      from
        oi_user_relation ur
        join oi_user_info ui on ur.user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_user_tag ut on ui.user_id = ut.user_id
        left join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ur.relation_user_id = ${userId}
        and ur.relation_type_cd = '03'
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and ui.user_login_id > '${lastAccountId}'
        ${searchCondition}
      order by
        ui.user_login_id asc
      limit ${rowCount};

    `
    console.log(query)
    let result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(200, [])
    }

    let hiddens = result.map(row => {
      row.tags = []
      return row
    })
    let userIds = hiddens.map(hidden => {
      return hidden.user_id
    })
    let userIdsString = userIds.join(',')

    let hiddenMap = {}
    hiddens.forEach(hidden => {
      hiddenMap[hidden.user_id] = hidden
    })
    
    query = `
      select
        ut.user_id, ti.tag_val
      from
        oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ut.user_id in (${userIdsString})
      order by 
		    ut.user_id asc,
        ut.tag_seq asc;
    `
    result = await mysql.execute(query)
    mysql.end()

    result.forEach(row => {

      let tag = row.tag_val

      if (tag != null && tag.length > 0) {
        hiddenMap[row.user_id].tags.push(tag)
      }
    })

    hiddens = Object.values(hiddenMap)
    hiddens.sort((lhs, rhs) => {
      return lhs.account_id > rhs.account_id ? 1 : lhs.account_id < rhs.account_id ? -1 : 0
    })

    mysql.end()

    let readable = await redis.readable(3)

    let onlineKeys = hiddens.map(user => {
      return `alive:${user.user_id}`
    })

    let onlines = await readable.mget(onlineKeys)

    for (let i = 0; i < onlines.length; i++) {
      hiddens[i].is_online = onlines[i] != null
    }

    redis.end()

    return response.result(200, hiddens)
  
  } catch(e) {

    console.error(e)

    redis.end()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}