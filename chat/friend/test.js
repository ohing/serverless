module.getFriends = async () => {
  
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '10dc608b-15b5-4277-9cad-ac2200303cb3'
    },
    queryStringParameters: {
      searchText: 'baby'
    }
    // body: JSON.stringify({
    //   // "rowCount": 2,
	  //   // "lastAccountId": "iuforever"
      
    // })
  }
  return await require('./getFriends.js').handler(request)
}

module.setFavorite = async () => {

  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'f7e692ea-8405-4439-b984-8a9f6c85c906'
    },
    pathParameters: {
      targetUserId: 147,
    }
  }
  return await require('./setFavorite.js').handler(request)
}

module.removeFavorite = async () => {

  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      targetUserId: 33
    }
  }
  return await require('./removeFavorite.js').handler(request)
}

module.setHide = async () => {

  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      friendId: 33
    }
  }
  return await require('./setHide.js').handler(request)
}

module.removeHide = async () => {

  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      friendId: 33
    }
  }
  return await require('./removeHide.js').handler(request)
}

module.getHiddens = async () => {

  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'f7e692ea-8405-4439-b984-8a9f6c85c906'
    },
    queryStringParameters: {
      searchText: '이지금'
    }
  }
  return await require('./getHiddens.js').handler(request)
}

test = async () => {

  const environment = require('../../environment.js')
  environment.setEnvironment(true)
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()