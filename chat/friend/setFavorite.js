'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 즐겨찾기 설정
 * @method GET setFavorite
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let targetUserId = event.pathParameters.targetUserId

    if (userId == targetUserId) {
      return response.result(403, null, '스스로에게 요청할 수 없습니다.')
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 찜 하실 수 없습니다.')
    }

    query = `
      select 
        count(*) count
      from 
        oi_user_relation
      where
        user_id = ${targetUserId}
        and relation_user_id = ${userId}
        and relation_type_cd = '06';
    `
    result = await mysql.execute(query)
    let count = result[0].count

    if (count >= 30) {
      mysql.end()
      return response.result(403, null, '즐겨찾기는 30명까지만 가능합니다.')
    }

    query = `
      insert ignore into oi_user_relation (
        user_id, relation_user_id, relation_type_cd, req_dt
      ) values (
        ${targetUserId}, ${userId}, '06', now()
      );
    `
    await mysql.execute(query)

    query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        case 
          when fi.org_file_nm is null then 'img/notification-default/notification-default.png'
          else fi.org_file_nm
        end profile_image_url, 
        case 
          when fi.org_file_nm is null then 'notification-default.png'
          else substring_index(fi.org_file_nm, '/', -1) 
        end profile_file_name,
        ur.req_dt registered_at,
        concat(
          '[',
          (
            select
              group_concat(concat('"', ti.tag_val, '"') separator ',')
            from 
              oi_user_tag ut
              join oi_tag_info ti on ut.tag_id = ti.tag_id
            where 
              ut.user_id = ur.user_id
            order by
              ut.tag_seq asc
          ),
          ']'
        ) tags
      from
        oi_user_relation ur
        left join oi_user_relation ur2 on ur.user_id = ur2.user_id and ur.relation_user_id = ur2.relation_user_id and ur2.relation_type_cd = '02'
        left join oi_user_relation ur3 on ur.user_id = ur3.user_id and ur.relation_user_id = ur3.relation_user_id and ur3.relation_type_cd = '03'
        left join oi_user_relation ur4 on ur.user_id = ur4.relation_user_id and ur.relation_user_id = ur4.user_id and ur4.relation_type_cd = '02'
        left join oi_user_info ui on ur.user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where
        ur.relation_user_id = ${userId}
        and ur.user_id = ${targetUserId}
        and ur.relation_type_cd = '06';
    `
    result = await mysql.execute(query)

    let userInfo = result[0]
    userInfo.tags = JSON.parse(userInfo.tags)

    if (result.length > 0) {
      query = `
        select 
          accpt_dt
        from
          oi_user_relation
        where
          user_id = ${targetUserId}
          and relation_user_id = ${userId}
          and relation_type_cd = '01';
      `
      result = await mysql.execute(query)

      userInfo.following = result[0].accpt_dt != null
      userInfo.requested = result[0].accpt_dt == null
    } else {
      userInfo.following = false
      userInfo.requested = false
    }

    mysql.end()
    

    let readable = await redis.readable(3)
    let isOnline = await readable.get(`alive:${targetUserId}`)
    redis.end()

    userInfo.is_online = isOnline != null;    

    return response.result(200, userInfo)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}