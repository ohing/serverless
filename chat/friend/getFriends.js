'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 친구(팔로잉) 목록 조회
 * @method GET getFriends
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result[0].open_type == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅 기능을 사용할 수 없습니다.')
    }

    // let parameters = JSON.parse(event.body || '{}') || {}
    let parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    let rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    let lastAccountId = parameters.lastAccountId
    let searchText = parameters.searchText

    let needsFavorites = false

    if (lastAccountId != null && lastAccountId.length > 0) {
      query = `
        select
          ur.user_id
        from
          oi_user_relation ur
          join oi_user_info ui on ur.user_id = ui.user_id
        where 
          ui.user_login_id = '${lastAccountId}'
          and ur.relation_user_id = ${userId}
          and ur.relation_type_cd = '06'
        limit
          1;
      `
      console.log(query)
      result = await mysql.execute(query)
      console.log('aaaa', result)
      if (result.length > 0) {
        needsFavorites = true
      }

    } else {
      needsFavorites = true
      lastAccountId = ''
    }

    let moreOn = ''
    let moreCondition = ''

    if (searchText != null && searchText.trim().length > 0) {
      moreOn = `
        left join oi_user_tag ut on ur.user_id = ut.user_id
        left join oi_tag_info ti on ut.tag_id = ti.tag_id
      `
      moreCondition = `
        and (
          ui.user_login_id like '%${searchText}%'
          or ui.user_nick_nm like '%${searchText}%'
          or ti.tag_val = '${searchText}'
        )
      `
    }

    let favorites

    let needsCount = rowCount

    if (needsFavorites) {

      console.log('needsFavorites')
      let favoriteQuery = exports.favoritesQuery(userId, lastAccountId, moreOn, moreCondition, rowCount)
      let result = await mysql.execute(favoriteQuery) 
      let targets = result.map(row => {
        row.tags = []
        row.following = false
        row.requested = false
        return row
      })

      favorites = await exports.getRelationAndTags(userId, targets, true)
      needsCount -= favorites.length

      lastAccountId = ''
    } else {
      favorites = []
    }

    let followings

    if (needsCount > 0) {

      let query = exports.query(userId, lastAccountId, moreOn, moreCondition, needsCount) 
      let result = await mysql.execute(query) 
      let targets = result.map(row => {
        row.tags = []
        row.following = false
        row.requested = false
        return row
      })

      followings = await exports.getRelationAndTags(userId, targets)

    } else {
      
      followings = []
    }

    mysql.end()

    if (favorites.length > 0 || followings.length > 0 || (lastAccountId != null && lastAccountId.length > 0)) {

      let readable = await redis.readable(3)

      if (favorites.length > 0) {
        let onlineKeys = favorites.map(user => {
          return `alive:${user.user_id}`
        })

        let onlines = await readable.mget(onlineKeys)

        for (let i = 0; i < onlines.length; i++) {
          favorites[i].is_online = onlines[i] != null
        }
      }
      
      if (followings.length > 0) {
        let onlineKeys = followings.map(user => {
          return `alive:${user.user_id}`
        })

        let onlines = await readable.mget(onlineKeys)
    
        for (let i = 0; i < onlines.length; i++) {
          followings[i].is_online = onlines[i] != null
        }
      }    

      redis.end()

      return response.result(200, {favorites: favorites, followings: followings, suggested: []})

    } else {

      let params = {
        FunctionName: `api-new-friend-${process.env.stage}-suggestedFriends`,
        Payload: JSON.stringify(event)
      }
      let suggestedResult = await lambda.invoke(params).promise()

      let suggested = JSON.parse(JSON.parse(suggestedResult.Payload).body) 

      return response.result(200, {favorites: [], followings: [], suggested: suggested})
    }
  
  } catch(e) {

    console.error(e)

    redis.end()
    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}

exports.favoritesQuery = (userId, lastAccountId, moreOn, moreCondition) => {

  let query = `
    select
      ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
      case 
		    when fi.org_file_nm is null then 'img/notification-default/notification-default.png'
        else fi.org_file_nm
      end profile_image_url, 
	    case 
		    when fi.org_file_nm is null then 'notification-default.png'
        else substring_index(fi.org_file_nm, '/', -1) 
	    end profile_file_name,
      ur.req_dt registered_at
    from
      oi_user_relation ur
      left join oi_user_relation ur2 on ur.user_id = ur2.user_id and ur.relation_user_id = ur2.relation_user_id and ur2.relation_type_cd = '02'
      left join oi_user_relation ur3 on ur.user_id = ur3.user_id and ur.relation_user_id = ur3.relation_user_id and ur3.relation_type_cd = '03'
      left join oi_user_relation ur4 on ur.user_id = ur4.relation_user_id and ur.relation_user_id = ur4.user_id and ur4.relation_type_cd = '02'
      left join oi_user_info ui on ur.user_id = ui.user_id
      left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
      left join oi_file_info fi on pi.file_id = fi.file_id
      ${moreOn}
    where
      ur.relation_user_id = ${userId}
      and ur2.relation_type_cd is null
      and ur3.relation_type_cd is null
      and ur4.relation_type_cd is null
      and ur.relation_type_cd = '06'
      and ui.user_status_cd = 'ACTIVE'
      and ui.profile_open_type_cd != 'CLOSE'
      and ui.user_login_id > '${lastAccountId}'
      ${moreCondition}
    group by 
      ur.user_id
    order by
      ur.req_dt desc;
  `
  console.log(query)
  return query
}

exports.query = (userId, lastAccountId, moreOn, moreCondition, rowCount) => {
  
  let query = `
    select
      ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
      case 
		    when fi.org_file_nm is null then 'img/notification-default/notification-default.png'
        else fi.org_file_nm
      end profile_image_url, 
	    case 
		    when fi.org_file_nm is null then 'notification-default.png'
        else substring_index(fi.org_file_nm, '/', -1) 
	    end profile_file_name
    from
      oi_user_relation ur
      left join oi_user_relation ur2 on ur.user_id = ur2.user_id and ur.relation_user_id = ur2.relation_user_id and ur2.relation_type_cd = '02'
      left join oi_user_relation ur3 on ur.user_id = ur3.user_id and ur.relation_user_id = ur3.relation_user_id and ur3.relation_type_cd = '03'
      left join oi_user_relation ur4 on ur.user_id = ur4.relation_user_id and ur.relation_user_id = ur4.user_id and ur4.relation_type_cd = '02'
      left join oi_user_relation ur5 on ur.user_id = ur5.user_id and ur.relation_user_id = ur5.relation_user_id and ur5.relation_type_cd = '06'
      left join oi_user_info ui on ur.user_id = ui.user_id
      left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
      left join oi_file_info fi on pi.file_id = fi.file_id
      ${moreOn}
    where
      ur.relation_user_id = ${userId}
      and ur2.relation_type_cd is null
      and ur3.relation_type_cd is null
      and ur4.relation_type_cd is null
      and ur5.relation_type_cd is null
      and ur.relation_type_cd = '01'
      and ur.accpt_dt is not null
      and ui.user_status_cd = 'ACTIVE'
      and ui.profile_open_type_cd != 'CLOSE'
      and ui.user_login_id > '${lastAccountId}'
      ${moreCondition}
    group by 
      ur.user_id
    order by
      ui.user_login_id asc
    limit
      ${rowCount};
  `
  console.log(query)
  return query
}

exports.getRelationAndTags = async (userId, targets, isFavorites = false) => {

  if (targets.length == 0) {
    return []
  }

  let userIds = targets.map(target => {
    return target.user_id
  })
  let userIdsString = userIds.join(',')

  let targetMap = {}
  targets.forEach(target => {
    targetMap[target.user_id] = target
  })

  let query = `
    select
      relation_user_id, accpt_dt
    from
      oi_user_relation
    where 
      user_id = ${userId}
      and relation_type_cd = '01'
      and relation_user_id in (${userIdsString})
  `
  let result = await mysql.execute(query)

  result.forEach(row => {
    targetMap[row.relation_user_id].following = row.accpt_dt != null
    targetMap[row.relation_user_id].requested = row.accpt_dt == null
  })

  query = `
    select
      ut.user_id, ti.tag_val
    from
      oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
    where
      ut.user_id in (${userIdsString})
    order by 
		    ut.user_id asc,
        ut.tag_seq asc;
  `
  result = await mysql.execute(query)

  result.forEach(row => {

    let tag = row.tag_val

    if (tag != null && tag.length > 0) {
      targetMap[row.user_id].tags.push(tag)
    }
  })

  targets = Object.values(targetMap)

  if (isFavorites) {
    targets.sort((lhs, rhs) => {
      return lhs.registered_at > rhs.registered_at ? 1 : lhs.registered_at < rhs.registered_at ? -1 : 0
    })
  } else {
    targets.sort((lhs, rhs) => {
      return lhs.account_id > rhs.account_id ? 1 : lhs.account_id < rhs.account_id ? -1 : 0
    })
  }

  return targets
}