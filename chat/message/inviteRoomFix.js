'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 잘못 들어가있던 초대방정보 수정(테스트)
 * @method GET inviteRoomFix
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let query
    let result

    query = `
      select 
        msg_id,
        invite_room_info
      from 
        oi_chat_msg 
      where 
        invite_room_info is not null
        and msg_id > 122209
      order by 
        msg_id asc
      limit 
        100;
    `
    result = await mysql.execute(query)

    for (let row of result) {
      if (row.invite_room_info != null) {
        row.invite_room_info = camelcase(row.invite_room_info, {deep: true})

        // row.invite_room_info.needs_password = row.invite_room_info.needs_password != 0
        // if (row.invite_room_info.tags == null) {
        //   row.tags = []
        // } else {
        //   if (typeof row.invite_room_info.tags == 'string' || row.invite_room_info.tags instanceof String) {
        //     let tags = row.invite_room_info.tags
        //     tags = tags.substring(1, tags.length-1).split(',')
        //     row.invite_room_info.tags = tags
        //   }
        // }
        // row.needs_password = (row.needs_password || 0) != 0;
      }
    }
    
    console.log('last one', result[result.length-1].msg_id)

    let values = []
    let valueParameters = []

    for (let row of result) {
      values.push('(?, ?)')
      valueParameters.push(row.msg_id, JSON.stringify(row.invite_room_info))
    }

    query = `
      insert into oi_chat_msg (
        msg_id, invite_room_info
      ) values 
        ${values}
      on duplicate key update 
        invite_room_info = values(invite_room_info)
    `

    // console.log(query)
    
    result = await mysql.execute(query, valueParameters)

    mysql.end()

    return response.result(200, result)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
