module.sendMessage = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '3abfcd7b-6f71-434d-b201-2df8457b181f'
    },
    pathParameters: {
      roomId: 3287
    },
    body: JSON.stringify({
      "timestamp": 1640584773490,
      "userId": 537,
      "roomId": 3287,
      "text": "999",
    })
  }
  return await require('./sendMessage.js').handler(request)
}

module.getMessages = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '15d77b4e-0a3d-4ac7-b76a-d82213652725'
    },
    pathParameters: {
      roomId: 4292
    },
    queryStringParameters: {
      // firstTimestamp: '1635590028927',
      rowCount: 50
    }
  }
  return await require('./getMessages.js').handler(request)
}

module.getUnreadCount = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      roomId: 739
    },
    queryStringParameters: {
      messageIds: '73834,73833,33956'
    }
  }
  return await require('./getUnreadCount.js').handler(request)
}

module.inviteRoomFix = async () => {
  const request = {

  }
  return await require('./inviteRoomFix.js').handler(request)
}

module.mediaInfoFix = async () => {
  const request = {

  }
  return await require('./mediaInfoFix.js').handler(request)
}

module.shareMessage = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'd56f1c83-2a08-4373-8c82-7c3caf90ab22'
    },
    pathParameters: {
      roomId: 139,
      messageId: 11470
    },
    body: JSON.stringify({
      targetUserIds: [240, 548, 542]
    })
  }

  return await require('./shareMessage.js').handler(request)
}


test = async () => {

  const environment = require('../../environment.js')
  environment.setEnvironment({isSecondDepth: true})
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()