'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 안읽은 사람 수
 * @method GET getUnreadCount
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']
    
    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y'
        and deport_yn = 'N';
    `
    let result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    // let parameters = JSON.parse(event.body || '{}') || {}
    let parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }
    
    let messageIds = parameters.messageIds == null ? [] : parameters.messageIds.split(',')

    query = `
      select 
        cm.msg_id message_id, count(cmr.receive_user_id) unread_count
      from
        oi_chat_msg cm
        left join oi_chat_msg_receive cmr on cm.msg_id = cmr.msg_id and cmr.receive_yn = 'N'
      where 
        cm.room_id = ${roomId} 
        and cm.msg_id in (${messageIds.join(',')}) 
        and cm.del_yn = 'N' 
        and (cm.target_user_id is null or cm.target_user_id = ${userId})
      group by
        cm.msg_id;
    `
    console.log(query)
    result = await mysql.execute(query)
    mysql.end()
    
    return response.result(200, result)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
