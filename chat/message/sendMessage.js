'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require('@ohing/ohingresponse');

const camelcase = require('camelcase-keys');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 메시지 전송
 * @method PUT sendMessage
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let roomId = event.pathParameters.roomId;

    let query;
    let result;
    let valueParameters;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    if (openType == 'CLOSE') {
      mysql.end();
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.');
    }

    query = `
      select
        cui.room_id, cui.use_yn, cri.room_type_cd, cri.room_status_cd
      from
        oi_chat_user_info cui
        join oi_chat_room_info cri on cui.room_id = cri.room_id
      where
        cui.room_id = ${roomId} 
        and cui.user_id = ${userId}
        and cui.deport_yn = 'N';
    `;
    result = await mysql.execute(query);
    if (result.length == 0) {
      mysql.end();
      return response.result(403, null, '권한이 없습니다.');
    }

    let roomType = result[0].room_type_cd;
    let roomStatus = result[0].room_status_cd;
    let useYn = result[0].use_yn;

    if (roomStatus != 'ACTIVE') {
      mysql.end();
      return response.result(403, null, '폭파된 방에서는 메시지를 전송할 수 없습니다.');
    }

    if (useYn != 'Y') {
      if (roomType != 'ONE') {
        mysql.end();
        return response.result(403, null, '권한이 없습니다.');
      }
    }

    let parameters = JSON.parse(event.body || '{}') || {};
    let timestamp = parameters.timestamp;

    if (timestamp == null) {
      return response.result(400);
    }

    query = `
      select 
        msg_id
      from 
        oi_chat_msg
      where
        room_id = ${roomId}
        and send_user_id = ${userId}
        and send_dt_int = ${timestamp}
      limit 
        1;
    `;
    console.log(query);
    result = await mysql.execute(query);
    console.log(result);
    if (result.length > 0) {

      query = `
        select 
          cm.msg_id message_id, cm.msg_type_cd type, cm.room_id, cm.send_user_id user_id, cm.send_user_nick_nm nickname, 
          cm.send_profile_image_url profile_image_url, substring_index(cm.send_profile_image_url, '/', -1) profile_file_name,
          cui.chat_profile_id,
          cm.chat_profile_intro_content chat_profile_self_introduction,
          cm.send_dt_int timestamp, cm.msg_content text, cm.media_info,
          cm.target_user_id, cm.target_user_nick_nm target_user_nickname,
          cm.invite_room_info,
          count(cmr.receive_user_id) unread_count  
        from
          oi_chat_msg cm
          left join oi_user_info ui on cm.send_user_id = ui.user_id
          left join oi_chat_user_info cui on cm.room_id = cui.room_id and cm.send_user_id = cui.user_id
          left join oi_chat_msg_receive cmr on cm.msg_id = cmr.msg_id and cmr.receive_yn = 'N'
        where
          cm.room_id = ${roomId}
          and cm.send_dt_int = ${timestamp};
      `;
      result = await mysql.execute(query);

      console.log('exists!', result);

      mysql.end();

      return response.result(200, result.length == 0 ? null : result[0]);
    }

    if (roomType == 'ONE') {
      query = `
        update
          oi_chat_user_info
        set
          use_yn = 'Y',
          join_dt = ${timestamp - 1}
        where
          room_id = ${roomId}
          and use_yn = 'N';
      `;
      await mysql.execute(query);
    }

    let text = parameters.text;
    let mediaInfo = parameters.mediaInfo;

    if (mediaInfo != null && !Array.isArray(mediaInfo)) {
      mysql.end();
      return response.result(400);
    }

    if ((mediaInfo || []).length == 0) {
      mediaInfo = null;
    }

    let encodingMovies = [];
    let encoded = 'Y';

    if (mediaInfo != null) {

      if (mediaInfo.length > 10) {
        mediaInfo = mediaInfo.slice(0, 10);
      }

      for (let i = 0; i < mediaInfo.length; i++) {

        let media = mediaInfo[i];

        if (media.type == 'MOVIE') {
          encoded = 'N';
          encodingMovies.push({
            key: media.mediaKey,
            seq: i
          });
        }
        if (media.width == 0) {
          mediaInfo[i].width = 600;
        }
        if (media.height == 0) {
          mediaInfo[i].height = 600;
        }
      }
    }

    mediaInfo = mediaInfo == null ? null : JSON.stringify(mediaInfo);

    let targetUserId = parameters.targetUserId;
    let isWhisper = targetUserId != null;
    let inviteRoomId = parameters.inviteRoomId;
    inviteRoomId = inviteRoomId == null ? null : inviteRoomId;

    query = `
      select 
        case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
        substring_index(case when cui.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end, '/', -1) profile_file_name
      from
        oi_chat_user_info cui
        left join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
      where
        cui.room_id = ${roomId}
        and cui.user_id = ${userId};
    `;
    result = await mysql.execute(query);

    if (result.length == 0) {
      mysql.end();
      return response.result(404, null, '메시지를 전송할 수 없습니다.');
    }

    let profileFileName = result[0].profile_file_name;
    profileFileName = (profileFileName || '').length == 0 ? null : profileFileName;
    let nickname = result[0].nickname;

    let targetUserNickname;

    if (!isWhisper) {
      targetUserId = null;
      targetUserNickname = null;
    } else {
      query = `
        select 
          case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname
        from
          oi_chat_user_info cui
          left join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        where
          cui.room_id = ${roomId}
          and cui.user_id = ${targetUserId}
          and cui.use_yn = 'Y';
      `;
      result = await mysql.execute(query);
      if (result.length == 0) {
        mysql.end();
        return response.result(404, null, '귓속말을 보낼 수 없는 대상입니다.');
      }
      targetUserNickname = result[0].nickname;
    }

    query = `
      select 
        cri.room_id, cri.title, cri.join_user_cnt join_user_count, cri.max_user_cnt max_user_count,
        substring_index(fi.org_file_nm, '/', -1) cover_file_name, 
        case 
          when length(cri.room_pwd) > 0 then 1
          else 0
        end needs_password,
        case when cui.chat_profile_id is null then substring_index(fi2.org_file_nm, '/', -1) else substring_index(fi3.org_file_nm, '/', -1) end profile_file_name,
        case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
        concat(
          '[',
          (
            select
              group_concat(concat('"', ti.tag_val, '"') separator ',')
            from 
              oi_chat_room_info cri2
              join oi_chat_room_tag crt on cri2.room_id = crt.room_id
              join oi_tag_info ti on crt.tag_id = ti.tag_id
            where 
              cri2.room_id = cri.room_id
            order by
              crt.tag_seq asc
          ),
          ']'
        ) tags
        
      from
        oi_chat_room_info cri
        left join oi_file_info fi on cri.room_image_id = fi.file_id
        join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.use_yn = 'Y' and cui.master_yn = 'Y'
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi2 on upi.file_id = fi2.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi3 on cpi.image_id = fi3.file_id

      where
        cri.room_id = ${inviteRoomId}
    `;

    result = await mysql.execute(query);

    console.log(result[0]);
    let inviteRoomInfo = null;
    if (result.length > 0) {
      let roomInfo = result[0];
      roomInfo.tags = JSON.parse(roomInfo.tags);
      roomInfo.needs_password = roomInfo.needs_password == 1;
      let camelcased = camelcase(roomInfo, { deep: true });
      inviteRoomInfo = JSON.stringify(camelcased);
    }

    query = `
      select 
        case
          when cpi.chat_profile_id is null then null
          else ifnull(cpi.self_intro_content, '')
        end self_introduction,
        cui.master_yn
      from
        oi_chat_user_info cui
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
      where
        cui.room_id = ${roomId}
        and cui.user_id = ${userId};
    `;
    console.log(query);

    result = await mysql.execute(query);

    let selfIntroduction = result[0].self_introduction;

    let masterYn = result[0].master_yn;

    let messageType = (inviteRoomId == null || inviteRoomId == 'null') ? 'CHAT' : 'OPENINVITE';

    let chatHash = `${roomId}-${userId}-${timestamp}`;

    query = `
      insert ignore into oi_chat_msg (
        room_id, msg_type_cd, msg_content, send_dt_int, send_user_id, 
        send_user_nick_nm, send_profile_image_url, chat_profile_intro_content, master_yn, target_user_id, 
        target_user_nick_nm, invite_room_id, invite_room_info, media_info, encoded_yn, 
        chat_hash
      ) values (
        ?, ?, ?, ?, ?, 
        ?, ?, ?, ?, ?, 
        ?, ?, ?, ?, ?,
        ?
      );
    `;
    console.log(query);
    valueParameters = [
      roomId, messageType, text, timestamp, userId, nickname, profileFileName, selfIntroduction, masterYn,
      targetUserId, targetUserNickname, inviteRoomId, inviteRoomInfo, mediaInfo, encoded, chatHash
    ];
    result = await mysql.execute(query, valueParameters);

    let messageId = 0;

    // inseter ignore 되면 id는 0으로 올라옴
    if (result.insertId === 0) {
      const getChatMessageByChatHash = `
            select msg_id from oi_chat_msg where chat_hash = ? limit 1;
        `;
      console.log(query)
      const chatMsg = await mysql.execute(getChatMessageByChatHash, [chatHash]);
      console.log(chatMsg);
      messageId = chatMsg[0].msg_id;

    } else {
      messageId = result.insertId;
    }

    if (encodingMovies.length > 0) {

      let values = [];

      for (let movie of encodingMovies) {
        values.push(`
          ( ${messageId}, '${movie.key}' )
        `);
      }

      query = `
        insert into oi_chat_media_encoding_info (
          msg_id, media_key
        ) values
          ${values.join(',')};
      `;
      console.log(query)
      await mysql.execute(query);
    }

    let values = [];
    let unReadCount = 0;

    if (!isWhisper) {

      query = `
        select 
          user_id
        from 
          oi_chat_user_info 
        where 
          room_id = ${roomId}
          and use_yn = 'Y'
          and user_id != ${userId};
      `;
      result = await mysql.execute(query);
      unReadCount = result.length;

      result.forEach(row => {
        values.push(` ( ${roomId}, ${messageId}, ${row.user_id} ) `);
      });

    } else {
      unReadCount = 1;
      values.push(` ( ${roomId}, ${messageId}, ${targetUserId} ) `);
    }

    if (values.length > 0) {
      query = `
        insert ignore into oi_chat_msg_receive (
          room_id, msg_id, receive_user_id
        ) values
          ${values.join(',')};
      `;
      console.log(query)
      await mysql.execute(query);
    }

    query = `
    
      select 
        cm.msg_id message_id, 'CHAT' type, cm.room_id, cm.send_user_id user_id, cm.send_user_nick_nm nickname, 
        cm.send_profile_image_url profile_image_url, substring_index(cm.send_profile_image_url, '/', -1) profile_file_name, 
        cui.chat_profile_id,
        cm.chat_profile_intro_content chat_profile_self_introduction, cm.master_yn is_master,
        cm.send_dt_int timestamp, cm.msg_content text, cm.media_info,
        cm.target_user_id, cm.target_user_nick_nm target_user_nickname,
        cm.invite_room_info,
        ${unReadCount} unread_count
      from
        oi_chat_msg cm
        left join oi_user_info ui on cm.send_user_id = ui.user_id
        left join oi_chat_user_info cui on cm.room_id = cui.room_id and cm.send_user_id = cui.user_id
      where
        cm.msg_id = ${messageId}
      group by
        cm.msg_id;
    `;
    console.log(query)
    result = await mysql.execute(query);
    mysql.end();

    let message = result.length == 0 ? null : result[0];

    console.log('message', message);

    if (message != null) {

      message.is_master = message.is_master == 'Y';

      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            type: 'SendMessage',
            roomId: roomId,
            chatMessage: camelcase(message, { deep: true })
          })
        })
      };
      console.log(params);

      await lambda.invoke(params).promise();
    }

    return response.result(200, message);

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
