'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 메시지 삭제
 * @method DELETE deleteMessages
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']
    
    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y'
        and deport_yn = 'N';
    `
    let result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let parameters = JSON.parse(event.body || '{}') || {}
    let messageIds = parameters.messageIds

    if (parameters.messageIds == null) {
      parameters = event.queryStringParameters || {}
      messageIds = parameters.messageIds.split(',')
    }
    
    if (messageIds == null || Array.isArray(messageIds) == false || messageIds.length == 0) {
      return response.result(400)
    }
    
    let values = []

    for (let messageId of messageIds) {

      values.push(`(
        ${roomId}, ${messageId}, ${userId}
      )`)
    }
    
    query = `
      insert ignore into oi_chat_msg_delete (
        room_id, msg_id, delete_user_id
      ) values ${values.join(', ')};
    `
    await mysql.execute(query)
    
    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
