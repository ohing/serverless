'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 메시지 목록
 * @method GET getMessages
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']
    
    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        cui.room_id, cui.use_yn, cri.room_type_cd
      from
        oi_chat_user_info cui
        join oi_chat_room_info cri on cui.room_id = cri.room_id
      where
        cui.room_id = ${roomId} 
        and cui.user_id = ${userId}
        and cui.deport_yn = 'N';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let roomType = result[0].room_type_cd
    let isUsing = result[0].use_yn == 'Y'

    if (!isUsing) {

      if (roomType == 'ONE') {
        query = `
          update
            oi_chat_user_info
          set
            use_yn = 'Y'
          where
            room_id = ${roomId}
            and user_id = ${userId}
        `
        await mysql.execute(query)
      } else {
        mysql.end()
        return response.result(403, null, '권한이 없습니다.')
      }
    }

    // let parameters = JSON.parse(event.body || '{}') || {}
    let parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }
    
    let messageIds = parameters.messageIds == null ? [] : parameters.messageIds.split(',')
    let firstTimestamp = parameters.firstTimestamp
    let lastTimestamp = parameters.lastTimestamp
    let rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)

    let where
    let order
    let limit
    let needsReverse = false
    let needsRead = false

    if (messageIds.length > 0) {
      where = `
        where 
          cm.room_id = ${roomId} 
          and cm.msg_id in (${messageIds.join(',')}) 
          and cm.del_yn = 'N' 
          and (cm.target_user_id is null or cm.send_user_id = ${userId} or cm.target_user_id = ${userId})
          and cmd.msg_id is null
      `
      order = ''
      limit = ''
    } else if (firstTimestamp != null) {
      where = `
        where 
          cm.room_id = ${roomId} 
          and cm.send_dt_int < ${firstTimestamp} 
          and cm.del_yn = 'N'
          and (cm.target_user_id is null or cm.send_user_id = ${userId} or cm.target_user_id = ${userId})
          and cmd.msg_id is null
        `
      order = 'order by cm.msg_id desc'
      limit = `limit ${rowCount}`
      needsReverse = true
    } else if (lastTimestamp != null) {
      where = `
        where 
          cm.room_id = ${roomId} and cm.send_dt_int > ${lastTimestamp}
          and cm.del_yn = 'N' 
          and (cm.target_user_id is null or cm.send_user_id = ${userId} or cm.target_user_id = ${userId})
          and cmd.msg_id is null
        `
      order = 'order by cm.msg_id asc'
      limit = `limit ${rowCount}`
      needsRead = true
    } else {
      where = `
        where 
          cm.room_id = ${roomId} 
          and cm.del_yn = 'N' 
          and (cm.target_user_id is null or cm.send_user_id = ${userId} or cm.target_user_id = ${userId})
          and cmd.msg_id is null
        `
      order = 'order by cm.msg_id desc'
      limit = `limit ${rowCount}`
      needsReverse = true
      needsRead = true
    }

    where += ` and cm.send_dt_int > (select unix_timestamp(cui.join_dt) * 1000 from oi_chat_user_info cui where cui.room_id = ${roomId} and cui.user_id = ${userId})`

    if (needsRead) {
      query = `
        update 
          oi_chat_msg_receive
        set
          receive_yn = 'Y'
        where
          room_id = ${roomId}
          and receive_user_id = ${userId};
      `
      console.log(query)
      await mysql.execute(query)
      console.log('updated')
    }

    query = `
      select 
        cm.msg_id message_id, cm.msg_type_cd type, cm.room_id, cm.send_user_id user_id, cm.send_user_nick_nm nickname, cm.send_profile_image_url profile_file_name,
        cm.send_dt_int timestamp, cm.msg_content text, cm.media_info,
        cui.chat_profile_id,
        cm.chat_profile_intro_content chat_profile_self_introduction, cm.master_yn is_master,
        cm.target_user_id, cm.target_user_nick_nm target_user_nickname,
        cm.invite_room_info,
        count(cmr.receive_user_id) unread_count
      from
        oi_chat_msg cm
        left join oi_user_info ui on cm.send_user_id = ui.user_id
        left join oi_chat_user_info cui on cm.room_id = cui.room_id and cm.send_user_id = cui.user_id
        left join oi_chat_msg_receive cmr on cm.msg_id = cmr.msg_id and cmr.receive_yn = 'N'
        left join oi_chat_msg_delete cmd on cm.room_id = cmd.room_id and cm.msg_id = cmd.msg_id and cmd.delete_user_id = ${userId}
      ${where}
      group by
        cm.msg_id
      ${order}
      ${limit};
    `

    console.log(query)
    result = await mysql.execute(query)
    mysql.end()

    if (needsReverse) {
      result.reverse()
    }

    let messages = result.map(row => {
      row.is_master = row.is_master == 'Y'
      return row
    })

    // let messages = result.map(row => {
    //   return {
    //     message_id: row.message_id,
    //     unread_count: row.unread_count
    //   }
    // })
    // 
    // let params = {
    //   FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
    //   InvocationType: "Event",
    //   Payload: JSON.stringify({
    //     body: JSON.stringify({
    //       type: 'ReadCount',
    //       roomId: roomId,
    //       chatRead: camelcase(messages, {deep:true})
    //     })
    //   })
    // }
    // console.log(params)

    // await lambda.invoke(params).promise()

    return response.result(200, messages)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
