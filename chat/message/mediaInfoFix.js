'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 잘못 들어가있던 미디어 정보 수정(테스트)
 * @method GET mediaInfoFix
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let query
    let result

    query = `
      select 
        msg_id,
        media_info
      from 
        oi_chat_msg 
      where 
        media_info is not null
    `
    result = await mysql.execute(query)

    let values = []

    for (let row of result) {

      for (let i = 0; i < row.media_info.length; i++) {
        delete row.media_info[i].mediaKey
        if (row.media_info[i].mediaKey == null) {
          if (row.media_info[i].type == 'IMAGE') {
            row.media_info[i].mediaKey = 'img/0BB09953-5F9C-417C-A593-CA2DBC94C65A_1599012295914/0BB09953-5F9C-417C-A593-CA2DBC94C65A_1599012295914.jpg'
          } else if (row.media_info[i].type == 'MOVIE') {
            row.media_info[i].mediaKey = 'mov/0E2BDEDD-32FB-48DE-BFFD-CB80B7FA0AD2_1586233548979/mp4/0E2BDEDD-32FB-48DE-BFFD-CB80B7FA0AD2_1586233548979.mp4'
          } else if (row.media_info[i].type == 'ANI') {
            row.media_info[i].mediaKey = 'emoticon/default_01/1/E01070_ICON.png'
          }
        }
      }

      values.push(`( ${row.msg_id}, '${JSON.stringify(row.media_info)}' )`)
    }

    query = `
      insert into oi_chat_msg (
        msg_id, media_info
      ) values 
        ${values}
      on duplicate key update 
        media_info = values(media_info)
    `

    console.log(query)

    result = await mysql.execute(query)

    mysql.end()

    return response.result(200, result)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
