'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 메시지 공유
 * @method POST shareMessage
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let roomId = event.pathParameters.roomId
    let messageId = event.pathParameters.messageId

    let parameters = JSON.parse(event.body || '{}') || {}

    let targetUserIds = parameters.targetUserIds || []
    let roomIds = parameters.roomIds || []

    let query
    let result
    let valueParameters

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 채팅에 참여하실 수 없습니다.')
    }

    query = `
      select
        room_id
      from
        oi_chat_user_info
      where
        room_id = ${roomId} 
        and user_id = ${userId}
        and use_yn = 'Y'
        and deport_yn = 'N';
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    query = `
      select
        msg_content, media_info, invite_room_info, invite_room_id
      from
        oi_chat_msg
      where
        room_id = ${roomId}
        and msg_id = ${messageId};
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '권한이 없습니다.')
    }

    let message = result[0]

    if (targetUserIds.length > 0) {

      for (let targetUserId of targetUserIds) {

        query = `
          select
            cui.room_id, count(cui.room_id) count
          from
            oi_chat_room_info cri
            join oi_chat_user_info cui on cri.room_id = cui.room_id
          where
            cui.user_id in (${userId}, ${targetUserId})
            and cri.room_type_cd = 'ONE'
          group by 
            cui.room_id
          order by
            count(cui.room_id) desc
          limit 1;
        `
        result = await mysql.execute(query)

        if (result.length > 0 && result[0].count == 2) {
          roomIds.push(result[0].room_id)
        } else {

          query = `
            insert into oi_chat_room_info (
              room_type_cd, max_user_cnt, ohing_profile_use_yn, reg_user_id
            ) values (
              'ONE', 0, '', ${userId}
            );
          `
          result = await mysql.execute(query)
          let roomId = result.insertId

          query = `
            insert into oi_chat_user_info (
              room_id, user_id, join_dt, master_yn
            ) values (
              ${roomId}, ${userId}, now(), 'Y'
            ), (
              ${roomId}, ${targetUserId}, now(), 'N'
            );
          `
          await mysql.execute(query)

          roomIds.push(roomId)
        }
      }
    }

    if (roomIds.length == 0) {
      mysql.end()
      return response.result(404, null, '공유할 대상을 찾을 수 없습니다.')
    }

    let values = []
    let receiveValues = []
    let timestamp = new Date().getTime()

    valueParameters = []

    for (let roomId of roomIds) {

      let msgContent = message.msg_content
      let mediaInfo = message.media_info == null ? null : JSON.stringify(message.media_info)
      let inviteRoomInfo = JSON.stringify(message.invite_room_info)
      let inviteRoomId = message.invite_room_id

      query = `
        select 
          ui.user_login_id nickname, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
          cpi.nick_nm chat_nickname, substring_index(fi2.org_file_nm, '/', -1) chat_profile_file_name
        from 
          oi_user_info ui
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
          left join oi_file_info fi on upi.file_id = fi.file_id
          left join oi_chat_user_info cui on ui.user_id = cui.user_id and cui.room_id = ${roomId} and cui.use_yn = 'Y'
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
          left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        where 
          ui.user_id = ${userId};
      `
      result = await mysql.execute(query)
      if (result.length > 0) {

        let profile = result[0]

        let nickname = profile.chat_nickname != null ? profile.chat_nickname : profile.nickname
        let chatProfileFileName = (profile.chat_profile_file_name || '').trim()
        // let profileFileName = profile.profile_file_name == null ? 'null' : `'${profile.profile_file_name}'`
        let profileFileName = profile.chat_nickname == null ? profile.profile_file_name : chatProfileFileName.length > 0 ? chatProfileFileName : null
        
        values.push(`( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )`)
        valueParameters = valueParameters.concat([roomId, 'CHAT', msgContent, timestamp, userId, profileFileName, nickname, mediaInfo, inviteRoomInfo, inviteRoomId])
        receiveValues.push({
          roomId: roomId,
          receiveUserId: userId,
          received: true
        })
      }
    }
    
    query = `
      insert into oi_chat_msg (
        room_id, msg_type_cd, msg_content, send_dt_int, send_user_id, send_profile_image_url, send_user_nick_nm, media_info, invite_room_info, invite_room_id
      ) values 
        ${values.join(',')}
    `
    console.log(query, valueParameters)
    result = await mysql.execute(query, valueParameters)

    let messageIds = []

    for (let i = result.insertId; i < result.insertId + values.length; i++) {
      messageIds.push(i)
      let index = i - result.insertId
      receiveValues[index].messageId = i

      query = `
        select
          room_id, user_id 
        from
          oi_chat_user_info 
        where
          room_id = ${receiveValues[index].roomId}
          and user_id != ${userId}
          and use_yn = 'Y';
      `
      let otherResult = await mysql.execute(query)

      for (let row of otherResult) {
        let receiveValue = {
          roomId: row.room_id,
          receiveUserId: row.user_id,
          received: false,
          messageId: i
        }
        console.log(receiveValue)
        receiveValues.push(receiveValue)
      }
    }

    values = []

    for (let receiveValue of receiveValues) {
      values.push(`( ${receiveValue.roomId}, ${receiveValue.messageId}, ${receiveValue.receiveUserId}, '${receiveValue.received ? 'Y' : 'N'}' )`)
    }

    query = `
      insert into oi_chat_msg_receive (
        room_id, msg_id, receive_user_id, receive_yn
      ) values
        ${values.join(',')};
    `
    console.log(query)
    await mysql.execute(query)

    query = `
      select 
        cm.msg_id message_id, 'CHAT' type, cm.room_id, cm.send_user_id user_id, cm.send_user_nick_nm nickname, cm.send_profile_image_url profile_file_name,
        cui.chat_profile_id,
        cm.chat_profile_intro_content chat_profile_self_introduction, cm.master_yn is_master,
        cm.send_dt_int timestamp, cm.msg_content text, cm.media_info,
        cm.target_user_id, cm.target_user_nick_nm target_user_nickname,
        cm.invite_room_info,
        (select count(*) from oi_chat_user_info where room_id = cm.room_id and use_yn = 'Y')-1 unread_count
      from
        oi_chat_msg cm
        left join oi_user_info ui on cm.send_user_id = ui.user_id
        left join oi_chat_user_info cui on cm.room_id = cui.room_id and cm.send_user_id = cui.user_id
      where
        cm.msg_id in (${messageIds.join(',')})
      group by 
        cm.msg_id;
    `
    console.log(query)
    result = await mysql.execute(query)

    console.log(result)

    let messages = []

    for (let row of result) {

      row.is_master = row.is_master == 'Y'

      messages.push({
        type: 'SendMessage',
        roomId: row.room_id,
        chatMessage: camelcase(row, {deep:true})
      })
    }

    mysql.end()

    console.log(messages)

    if (messages.length > 0) {

      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            messages: messages
          })
        })
      }
      console.log(params)

      await lambda.invoke(params).promise()
    }

    return response.result(200, {}, '메시지 공유가 완료되었습니다.')
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
