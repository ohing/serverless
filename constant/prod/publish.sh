#!/bin/sh

aws s3 cp ./appData.json s3://ohing-public-prod/appData.json --acl public-read --content-encoding utf-8 --content-type application/json
aws cloudfront create-invalidation --distribution-id E16EONPOZJ0HT8 --paths "/appData.json"