#!/bin/sh

aws s3 cp ./appData.json s3://ohing-public-dev/appData.json --acl public-read --content-encoding utf-8 --content-type application/json
aws cloudfront create-invalidation --distribution-id E2MKHYC1WFIEFZ --paths "/appData.json"