module.settlement2020 = async () => {
    const request = {
        body: JSON.stringify({
          userId : 'USR_9000000000000310'
        })
    }
    return await require('./settlement2020.js').handler(request)
}

module.happyNewYearRandomTest = async () => {
  const request = {
    body: JSON.stringify({
      p1: 5000,
      p2: 5000,
      p3: 5000,
      p4: 5000,
      p5: 5000,
      p6: 5000,
      request: 'p1'
    })
  }
  return await require('./happyNewYear2021RandomTest').handler(request)
}

module.happyNewYearRandomTestMultiple = async () => {
  const request = {
    body: JSON.stringify({
      p1: 5000,
      p2: 5000,
      p3: 5000,
      p4: 5000,
      p5: 5000,
      p6: 5000,
      request: 'USR_9000000000000310'
    })
  }
  return await require('./happyNewYear2021RandomTestMultiple').handler(request)
}

module.happyNewYear2021Check = async () => {
  const request = {
    body: JSON.stringify({
      userId: 'USR_9000000000000191'
    })
  }
  return await require('./happyNewYear2021Check').handler(request)
}

module.happyNewYear2021Raffle = async () => {
  const request = {
    body: JSON.stringify({
      code: 'p1',
      userId: 'USR_9000000000000191'
    })
  }
  return await require('./happyNewYear2021Raffle').handler(request)
}

module.happyNewYear2021GetComments = async () => {
  const request = {
    body: JSON.stringify({
    })
  }
  return await require('./happyNewYear2021GetComments').handler(request)
}



test = async () => {

    const environment = require('../environment.js')
    environment.setEnvironment()
  
    if (process.argv.length > 2) {
  
      const begin = new Date()
    
      let fname = process.argv[2]
      console.log(fname)
      if (fname.endsWith('.js')) {
        fname = fname.substring(0, fname.length-3)
      }
      const result = await module[fname]()
  
      console.log(result)
    
      console.log('duration', new Date() - begin)
  
    } else {
  
      console.log('no input')
    }
  }
  
  test()