'use strict';

const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  try {

    const parameters = JSON.parse(event.body || '{}') || {}

    const request = parameters.request || 'all'

    let result = {}
    const min = 1
    const max = 10000

    const upperPrizes = {
      'p1': '10', 
      'p2': '3',
      'p3': '10',
      'p4': '5',
      'p5': '3',
      'p6': '1'
    }

    const lowerPrizes = {
      'p1': '5', 
      'p2': '1',
      'p3': '3',
      'p4': '1',
      'p5': '1',
      'p6': '1'
    }
    
    if (request == 'p1' || request == 'p2' || request == 'p3' || request == 'p4' || request == 'p5' || request == 'p6') {

      const p = parameters[request] || 5000
      result[request] = `${(Math.floor(Math.random() * (max - min)) + min <= p) ? upperPrizes[request] : lowerPrizes[request]}만원 당첨!`

    } else {

      const p1 = parameters.p1 || 5000
      const p2 = parameters.p2 || 5000
      const p3 = parameters.p3 || 5000
      const p4 = parameters.p4 || 5000
      const p5 = parameters.p5 || 5000
      const p6 = parameters.p6 || 5000

      result.p1 = `${(Math.floor(Math.random() * (max - min)) + min <= p1) ? upperPrizes.p1 : lowerPrizes.p1}만원 당첨!`
      result.p2 = `${(Math.floor(Math.random() * (max - min)) + min <= p2) ? upperPrizes.p2 : lowerPrizes.p2}만원 당첨!`
      result.p3 = `${(Math.floor(Math.random() * (max - min)) + min <= p3) ? upperPrizes.p3 : lowerPrizes.p3}만원 당첨!`
      result.p4 = `${(Math.floor(Math.random() * (max - min)) + min <= p4) ? upperPrizes.p4 : lowerPrizes.p4}만원 당첨!`
      result.p5 = `${(Math.floor(Math.random() * (max - min)) + min <= p5) ? upperPrizes.p5 : lowerPrizes.p5}만원 당첨!`
      result.p6 = `${(Math.floor(Math.random() * (max - min)) + min <= p6) ? upperPrizes.p6 : lowerPrizes.p6}만원 당첨!`
    }

    return response.result(200, result)

  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}