'use strict';

const aws = require('aws-sdk')
aws.config.update({region: 'ap-northeast-2'});
const ses = new aws.SES()
const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  console.log(event)

  console.log(ses)
  try {
    const params = {
        Destination: {
            ToAddresses: [
                
                'jeonganWkd@ohing.net'
            ]
        },
        Message: {
            Body: {
                Text: {
                    Charset: 'UTF-8',
                    Data: 'Test'
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: '이메일 전송 테스트'
            }
        },
        Source: 'infra@ohing.net'
    }
    const result = await ses.sendEmail(params).promise()
    console.log(result)

  } catch(e) {

    
    console.error(e)


    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}