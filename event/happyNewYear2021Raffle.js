'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body)
    const userId = parameters.userId
    const code = parameters.code

    if (code == null) {
      return response.result(403)
    }

    let query = `
      select * from oi_user_info where user_id='${userId}' limit 1;
    `
    let result = await mysql.execute(query) 

    if (result.count == 0) {
      return response.result(403)
    }

    query = `
      select 
        user_id, win, code, prize, prize_code
      from 
        happy_new_year_2021
      where
        user_id = '${userId}'
      limit 
        1
    `

    result = await mysql.execute(query)

    if (result.length > 0) {
      mysql.end()
      return response.result(200, result[0])
    }

    const min = 1
    const max = 10000

    const upperPrizes = {
      'p1': 10, 
      'p2': 3,
      'p3': 10,
      'p4': 5,
      'p5': 3,
      'p6': 1
    }

    const lowerPrizes = {
      'p1': 5, 
      'p2': 1,
      'p3': 3,
      'p4': 1,
      'p5': 1,
      'p6': 1
    }

    const winRates = {

    }

    const winRate = winRates[code] || 5000
    const win = Math.floor(Math.random() * (max - min)) + min <= winRate
    const prize = win ? upperPrizes[code] : lowerPrizes[code]
    // const message = `${prize}만원 당첨!`

    const upperPrizeCodes = {
      'p1': '95ffe47879b4cd94f036f27ed668cd0c',
      'p2': '0411838e773bf942f0844b7ab6d877a8',
      'p3': '64efb1df1cdeedcb5caa5ab6e25b641a',
      'p4': '51e6a404a0fab12b1c7cacf61494521a',
      'p5': '4a1108a13cd96c955b88bf366e9f7dd6',
      'p6': '00884df3b67fb0fae1502738d50872ed'
    }

    const lowerPrizeCodes = {
      'p1': 'e5726dfe78ef9b6b5acd58f986b753e3',
      'p2': '8e479ccca24c46988c4bee97459822f0',
      'p3': '2c9fa187e006fd22ab462a3b4de2683e',
      'p4': '60239a3dceb0fce93415462ec6fd6bc4',
      'p5': '9e4503f1221f207f8ffa0880d7caeac8',
      'p6': '9cf994d06b3c9e8370f9b6b777336f91'
    }

    const prizeCode = win ? upperPrizeCodes[code] : lowerPrizeCodes[code]
    const winValue = win ? 1 : 0
    
    query = `
      insert into happy_new_year_2021 (
        user_id, win, code, prize, prize_code
      )
      values (
        '${userId}', ${winValue}, '${code}', ${prize}, '${prizeCode}'
      )
    `

    await mysql.execute(query)

    mysql.end()

    return response.result(200, {
      user_id: userId,
      win: winValue,
      code: code,
      prize: prize,
      prize_code: prizeCode
    })
    
  } catch(e) {

    mysql.end()
    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}