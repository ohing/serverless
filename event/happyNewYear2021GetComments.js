'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body)
    const firstId = parameters.firstId || 2100000000
    const lastId = parameters.lastId

    let lastIdWhere

    if (lastId != null) {
      lastIdWhere = `and id > ${lastId}`
    } else {
      lastIdWhere = ''
    }

    let query = `
      select 
      id, concat(substring(user_login_id, 1, length(user_login_id)-3), '***') as user_login_id, comment, date_format(created_at, '%y.%m.%d %H:%i:%s') as created_at
      from
        happy_new_year_2021_comment
      where
        id < ${firstId} 
        ${lastIdWhere}
      order by
        id desc
      limit 300
    `

    let result = await mysql.execute(query) 
    
    mysql.end()

    return response.result(200, result)

  } catch(e) {

    mysql.end()
    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}