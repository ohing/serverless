'use strict';

const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  try {

    const parameters = JSON.parse(event.body || '{}') || {}

    const request = parameters.request

    const min = 1
    const max = 10000

    const upperPrizes = {
      'p1': '10', 
      'p2': '3',
      'p3': '10',
      'p4': '5',
      'p5': '3',
      'p6': '1'
    }

    const lowerPrizes = {
      'p1': '5', 
      'p2': '1',
      'p3': '3',
      'p4': '1',
      'p5': '1',
      'p6': '1'
    }
    
    if (request == 'p1' || request == 'p2' || request == 'p3' || request == 'p4' || request == 'p5' || request == 'p6') {

      const p = parameters[request] || 5000

      let result = {
        upperCount: 0,
        lowerCount: 0,
        results: []
      }

      for (var i = 0; i < 100; i++) {
        let prize
        if (Math.floor(Math.random() * (max - min)) + min <= p) {
          prize = upperPrizes[request]
          result.upperCount += 1
        } else {
          prize = lowerPrizes[request]
          result.lowerCount += 1
        }
        result.results.push(`${prize}만원 당첨!`)
      }

      return response.result(200, result)

    } else {
      
      return response.result(200, `result를 지정해 주세요.`)
    }

  } catch(e) {

    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}