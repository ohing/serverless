'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const fileManager = require(`@ohing/ohingfilemanager-${process.env.stage}`)
const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body)
    const userId = parameters.userId
    const userLoginId = parameters.userLoginId
    const imageFileName = parameters.imageFileName
    const imageWidth = parameters.imageWidth
    const imageHeight = parameters.imageHeight

    const splits = imageFileName.split('/')
    const key = splits[splits.length-1]
    let attachFileId = await fileManager.create(mysql, userId, 'FEED', [{key: key, width: imageWidth, height: imageHeight}])

    let query = `
      insert into
        oi_feed_info (
          feed_id, feed_type_cd, feed_content, tag_list, anony_yn, use_yn, user_login_id, reg_dt, reg_user_id, upd_dt, upd_user_id
        )
      values (
        (select concat('FEED_', lpad(substring(i.feed_id,7)+1, 15, 0)) from ((select feed_id from oi_feed_info order by feed_id desc limit 1) as i)),
        'FEED', '2020 숫자로보는나의오잉은?', '#2020연말결산', 'N', 'Y','${userLoginId}', now(), '${userId}', now(), '${userId}'
      )
    `
    await mysql.execute(query)

    query = `
      select feed_id from oi_feed_info where reg_user_id = '${userId}' order by reg_dt desc limit 1
    `
    const feeds = await mysql.execute(query)
    const feedId = feeds[0].feed_id
    
    query = `
      insert into
        oi_media_info (
          media_id, feed_id, media_type_cd, atch_file_id, file_sn, order_no, reg_dt, reg_user_id, upd_dt, upd_user_id
        )
      values (
        (select concat('MEDIA_', lpad(substring(i.media_id,8)+1, 14, 0)) from ((select media_id from oi_media_info order by media_id desc limit 1) as i)),
        '${feedId}', 'IMAGE', '${attachFileId}', 0, 1, now(), '${userId}', now(), '${userId}'
      )
    `
    await mysql.execute(query)

    const tag = '2020연말결산'

    query = `
      select tag_id, use_yn from oi_tag_info where tag = '${tag}'
    `

    const tagInfos = await mysql.execute(query)
    if (tagInfos.length > 0) {

      if (tagInfos[0].use_yn != 'Y') {
        query = `
          UPDATE
            oi_tag_info
          SET
            use_yn = 'Y'
          WHERE
            tag_id = '${tagInfos[0].tag_id}'
        `

        await mysql.execute(query)
      }

    } else {

      query = `
        insert into 
          oi_tag_info (
            tag_id, tag, use_yn
          )
        values (
          md5('${tag}'), '${tag}', 'Y'
        )
      `
      await mysql.execute(query)
    }

    return response.result(200, '저장되었습니다.')

  } catch(e) {

    mysql.end()
    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}