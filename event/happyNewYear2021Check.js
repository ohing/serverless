'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body)
    const userId = parameters.userId
    
    let query = `
      select 
        user_id, win, code, prize, prize_code
      from 
        happy_new_year_2021
      where
        user_id = '${userId}'
      limit 
        1
    `
    const result = await mysql.execute(query)

    mysql.end()
    
    if (result.length > 0) {
      return response.result(200, {duplicated: true, data: result[0]})
    } else {
      return response.result(200, {duplicated: false})
    }
    
  } catch(e) {

    mysql.end()
    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}