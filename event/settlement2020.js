'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const mapper = require('mybatis-mapper')
const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body)
    const userId = parameters.userId
    const rediss = await redis.writable()
    
    mapper.createMapper(['./mapper.xml'])

    // 유저 아이디, 가입연도
    let query = mapper.getStatement('Event', 'userInfo', {
      userId : userId
    })
    
    let userInfo = await mysql.execute(query)

    // 유저 프로필 이미지

    query = mapper.getStatement('Event', 'userProfile', {
      userId : userId
    })

    let userProfile = await mysql.execute(query)

    // await rediss.del(`settlement2020:USER0031`)
    // return
    let result = await rediss.get(`settlement2020 : ${userId}`)

    if(result == null){
        // 가입한 지
        query = mapper.getStatement('Event', 'fromSignupDate', {
          userId : userId
        })
        let fromSignupDate = await mysql.execute(query)

        // 내가 작성한 게시물 개수
        query = mapper.getStatement('Event', 'countFeed', {
          userId : userId
        })
        let countFeed = await mysql.execute(query)

        // 내 게시물에 달린 댓글 개수
        query = mapper.getStatement('Event', 'countFeedComment', {
          userId : userId
        })
        let countFeedComment = await mysql.execute(query)

        // 내가 작성한 댓글 개수
        query = mapper.getStatement('Event', 'countComment', {
          userId : userId
        })
        let countComment = await mysql.execute(query)

        // 내가 좋아요 한 게시물 개수
        query = mapper.getStatement('Event', 'countLike', {
          userId : userId
        })
        let countLike = await mysql.execute(query)

        // 내가 쓴 채팅 메시지 개수
        query = mapper.getStatement('Event', 'sendMsg', {
          userId : userId
        })
        let sendMsg = await mysql.execute(query)

        // 내 게시물에 좋아요를 받은 개수
        query = mapper.getStatement('Event', 'likeFeed', {
          userId : userId
        })
        let likeFeed = await mysql.execute(query)

        // 내 게시물에 있는 미디어에 좋아요를 받은 개수
        query = mapper.getStatement('Event', 'likeMedia', {
          userId : userId
        })
        let likeMedia = await mysql.execute(query)

        // 내 게시물(사진포함) 좋아요 받은 총 개수
        let likeFeedMedia = likeFeed[0].like_feed + likeMedia[0].like_media

        // 나의 팔로잉 수
        query = mapper.getStatement('Event', 'following', {
          userId : userId
        })
        let following = await mysql.execute(query)

        // 나의 팔로워
        query = mapper.getStatement('Event', 'follower', {
          userId : userId
        })
        let follower = await mysql.execute(query)

        // 나의 팔로잉 + 팔로워
        let friend = following[0].following + follower[0].follower
    
        // 나와 맞팔 한 수
        query = mapper.getStatement('Event', 'f4f', {
          userId : userId
        })
        let f4f = await mysql.execute(query)

        // 최종 연결된 친구 수
        let connectFriend = friend - f4f[0].f4f
        
        result  = Object.assign({likeFeedMedia, connectFriend},
                                                fromSignupDate[0],
                                                countFeed[0],
                                                countFeedComment[0],
                                                countComment[0],
                                                countLike[0],
                                                sendMsg[0],
                                                )

        await rediss.set(`settlement2020:${userId}`, JSON.stringify(result))

    }else{
      
      result = JSON.parse(result)
    }

    mysql.end()
    rediss.end(true)

    if (userInfo.length > 0) {
      result = Object.assign(result, userInfo[0])
    }

    if (userProfile.length > 0) {
      result = Object.assign(result, userProfile[0])
    }
    
    return response.result(200, result)

  } catch(e) {

    mysql.end()
    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}