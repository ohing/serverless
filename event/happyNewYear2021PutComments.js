'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)

/**
 * 
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body)
    const userId = parameters.userId
    const comment = parameters.comment
    const lastId = parameters.lastId || 0

    let query = `
      select
        user_login_id
      from
        oi_user_info
      where
        user_id = '${userId}'
      limit
        1;
    `

    let result = await mysql.execute(query)

    if (result.length == 0) {
      return response.result(403)
    }

    const userLoginId = result[0].user_login_id

    query = `
      insert into happy_new_year_2021_comment (
        user_id, user_login_id, comment
      )
      values (
        '${userId}', '${userLoginId}', '${comment}'
      );
    `

    await mysql.execute(query)

    let append
    
    if (lastId > 0) {
      append = `where id > '${lastId}' order by id desc`
    } else {
      append = `order by id desc limit 30`
    }
    

    query = `
      select
      id, concat(substring(user_login_id, 1, length(user_login_id)-3), '***') as user_login_id, comment, date_format(created_at, '%y.%m.%d %H:%i:%s') as created_at
      from
        happy_new_year_2021_comment
      ${append};
    `

    result = await mysql.execute(query)
    
    mysql.end()

    return response.result(200, result)

  } catch(e) {

    mysql.end()
    console.error(e)

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}