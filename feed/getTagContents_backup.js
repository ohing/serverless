'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 태그 컨텐츠 목록
 * @method GET getTagContents
 * @returns  
*/

module.exports = async event => {

  try {

    let userId = event.userId
    let parameters = event.parameters

    let rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)

    let lastId = parameters.lastId

    if (tag == null) {
      return response.result(400)
    }

    let medias = await this.medias(userId, tag, rowCount, lastId, lastMediaSeq)

    console.log(medias)
    
    let query = `
      select found_rows() total_count;
    `
    let result = await mysql.execute(query)

    mysql.end()

    return response.result(200, {tag: tag, total_count: result[0].total_count, data: medias})
  
  } catch(e) {  

    console.error(e)    
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}

exports.medias = async (userId, tag, rowCount, lastId = null, lastMediaSeq = null) => {

  let whereConditions = '' 

  if (lastId != null) {

    if (lastMediaSeq != null) {
      whereConditions = ` 
        fmi.feed_id = ${lastId}
        and fmi.media_seq < ${lastMediaSeq}
      `
    } else {
      whereConditions = `
        ti.tag_val = '${tag}'
        and ur2.user_id is null
        and (f.encoded_yn = 'Y' or f.reg_user_id = ${userId})
        and f.feed_except_yn = 'N'
        and ui.user_status_cd = 'ACTIVE'
        and (ui.profile_open_type_cd = 'OPEN' or (ui.profile_open_type_cd = 'FRIEND' and ur.user_id is not null))
        and f.feed_id < ${lastId}
      `
    }

  } else {
    whereConditions = `
      ti.tag_val = '${tag}'
      and ur2.user_id is null
      and (f.encoded_yn = 'Y' or f.reg_user_id = ${userId})
      and f.feed_except_yn = 'N'
      and ui.user_status_cd = 'ACTIVE'
      and (ui.profile_open_type_cd = 'OPEN' or (ui.profile_open_type_cd = 'FRIEND' and ur.user_id is not null))
    `
  }

  let limit = (lastId != null && lastMediaSeq != null) ? '' : `limit ${rowCount}`

  let query = `
    select sql_calc_found_rows
      f.feed_id, f.feed_content, f.reg_user_id user_id,
      fmi.media_seq, fi.org_file_nm media_url, substring_index(fi.org_file_nm, '/', -1) media_file_name, fi.media_type_cd type, fi.width, fi.height, fi.play_time
    from
      oi_feed_info f
      join oi_feed_tag ft ON f.feed_id = ft.feed_id
      left join oi_tag_info ti ON ft.tag_id = ti.tag_id
      join oi_user_info ui on f.reg_user_id = ui.user_id
      left join oi_user_relation ur on f.reg_user_id = ur.user_id and ur.relation_user_id = ${userId} and ur.relation_type_cd = '01' and ur.accpt_dt is not null
      left join oi_user_relation ur2 on f.reg_user_id = ur2.user_id and ur2.relation_user_id = ${userId} and (ur2.relation_type_cd = '02' or ur2.relation_type_cd = '03') 
      left join oi_feed_media_info fmi on f.feed_id = fmi.feed_id
      left join oi_file_info fi on fmi.file_id = fi.file_id
    where
      ${whereConditions}
    group by
      f.feed_id, fmi.media_seq
    order by
      f.feed_id desc, fmi.media_seq desc
    ${limit}
  `
  console.log(query)
  let result = await mysql.execute(query)

  let medias = []

  let lastFeedId = 0

  for (let i = 0; i < result.length; i++) {

    let row = result[i]
    let text = (row.feed_content || '').trim()
    if (lastMediaSeq == null && lastFeedId != row.feed_id && text.length > 0) {
      let media = {
        feed_id: row.feed_id,
        media_seq: 0,
        type: 'TEXT',
        text: text
      }
      medias.push(media)

      if (medias.length == rowCount) {
        break
      }

      lastFeedId = row.feed_id
    }

    if (row.media_seq != null) {
      let media = {
        feed_id: row.feed_id,
        media_seq: row.media_seq,
        type: row.type,
        media_url: row.media_url,
        media_file_name: row.file_name,
        width: row.width,
        height: row.height
      }
      medias.push(media)

      if (medias.length == rowCount) {
        break
      }
    }
  }

  return medias

}