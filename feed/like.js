'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 좋아요
 * @method POST like
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }
    
    const parameters = JSON.parse(event.body || '{}') || {}

    const targetType = parameters.targetType
    const targetId = parameters.targetId
    const seq = parameters.seq
    const like = parameters.like
    const isAnonymous = parameters.isAnonymous || false

    let likeCount = 0
    let feedLikeCount = 0

    let query 
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 좋아요할 수 없습니다.')
    }

    let notificationType = null
    let targetUserId = null
    let feedId = null
    
    let logType = null
    let mileage = 0
    let actionId = null
    let log = null
    let linkInfo = null
    let accountId = null

    switch (targetType) {
      case 'FEED':
      case 'COMT': {
        actionId = targetId

        if (like) {

          logType = 'LIKE'
          mileage = 1

          if (targetType == 'FEED') {

            feedId = targetId

            query = `
              select
                f.feed_type_cd, ui.user_id, ui.user_login_id
              from 
                oi_feed_info f
                join oi_user_info ui on f.reg_user_id = ui.user_id
              where
                f.feed_id = ${targetId}
            `

            result = await mysql.execute(query)
            targetUserId = result[0].user_id
            let feedType = result[0].feed_type_cd
            accountId = result[0].user_login_id

            log = feedType == 'HELP' ? `{accountId} 님의 도와줘에 좋아요를 꾸욱❤` : `{accountId} 님의 피드에 좋아요를 꾸욱❤`

            linkInfo = {
              key: 'accountId',
              value: accountId,
              color: '#FF4800',
              targetType: 'USER',
              targetId: targetUserId
            }

          } else {

            query = `
              select
                fc.feed_id, fc.parent_comment_id, ui.user_id, ui.user_login_id
              from 
                oi_feed_comment fc
                join oi_user_info ui on fc.reg_user_id = ui.user_id
              where
                fc.comment_id = ${targetId}
            `

            result = await mysql.execute(query)
            targetUserId = result[0].user_id
            feedId = result[0].feed_id

            let parentId = result[0].parent_comment_id
            accountId = result[0].user_login_id

            log = parentId == null ? `{accountId} 님의 댓글에 좋아요를 꾸욱❤` : `{accountId} 님의 답글에 좋아요를 꾸욱❤`

            linkInfo = {
              key: 'accountId',
              value: accountId,
              color: '#FF4800',
              targetType: 'USER',
              targetId: targetUserId
            }
          }

          query = `
            insert ignore into oi_like_log (
              target_id, like_target_type_cd, user_id, like_dt
            ) values (
              ${targetId}, '${targetType}', ${userId}, now()
            );
          `
          await mysql.execute(query)

          if (targetType == 'FEED') {
            notificationType = 'FEEDLIKE'
          } else {
            query = `
              select 
                parent_comment_id
              from
                oi_feed_comment
              where
                comment_id = ${targetId}
            `
            result = await mysql.execute(query)

            if (result.length > 0) {
              notificationType = result[0].parent_comment_id == null ? 'COMTLIKE' : 'REPLYLIKE'
            }
          }

        } else {

          feedId = targetId

          query = `
            delete from
              oi_like_log
            where
              target_id = ${targetId} and like_target_type_cd = '${targetType}' and user_id = ${userId};
          `
          await mysql.execute(query)

          logType = targetType == 'FEED' ? 'DELETELIKE' : 'DELCOMLIKE'
          mileage = -1
          actionId = targetId
          log = '좋아요 취소'
        }

        if (targetType == 'FEED') {

          query = `
            select              
              (select count(*) from oi_like_log ll join oi_user_info ui on ll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where ll.target_id = ${targetId} and ll.like_target_type_cd = 'FEED') +
              (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fmll.feed_id = ${targetId}) count
          `
          result = await mysql.execute(query)
          console.log(result)
          likeCount = result[0].count
          feedLikeCount = likeCount

        } else {

          query = `

            select              
              (select count(*) from oi_like_log ll join oi_user_info ui on ll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where ll.target_id = ${feedId} and ll.like_target_type_cd = 'FEED') +
              (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fmll.feed_id = ${targetId}) feed_like_count,
              (select count(*) from oi_like_log ll join oi_user_info ui on ll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where target_id = ${targetId} and like_target_type_cd = 'COMT') count;
          `
          result = await mysql.execute(query)
          console.log(result)
          likeCount = result[0].count
          feedLikeCount = result[0].feed_like_count
        }

        break 
      }

      case 'MEDIA': {
        feedId = targetId
        actionId = targetId

        if (seq == null) {
          return response.result(400)
        }
        if (like) {

          logType = 'LIKE'
          mileage = 1

          query = `
            select
              f.feed_type_cd, ui.user_id, ui.user_login_id
            from 
              oi_feed_info f
              join oi_user_info ui on f.reg_user_id = ui.user_id
            where
              f.feed_id = ${targetId}
          `

          result = await mysql.execute(query)
          targetUserId = result[0].user_id
          let feedType = result[0].feed_type_cd
          accountId = result[0].user_login_id

          log = feedType == 'HELP' ? `{accountId} 님의 도와줘에 좋아요를 꾸욱❤` : `{accountId} 님의 피드에 좋아요를 꾸욱❤`

          linkInfo = {
            key: 'accountId',
            value: accountId,
            color: '#FF4800',
            targetType: 'USER',
            targetId: targetUserId
          }

          query = `
            select 
              fi.media_type_cd 
            from 
              oi_file_info fi
              join oi_feed_media_info fmi on fi.file_id = fmi.file_id
            where
              fmi.feed_id = ${targetId}
              and fmi.media_seq = ${seq};
          `
          result = await mysql.execute(query)

          if (result.length == 0) {
            mysql.end()
            return response.result(404)
          }

          let mediaType = result[0].media_type_cd

          query = `
            insert ignore into oi_feed_media_like_log (
              feed_id, media_seq, user_id, like_dt
            ) values (
              ${targetId}, ${seq}, ${userId}, now()
            );
          `
          console.log(query)
          result = await mysql.execute(query)

          notificationType = mediaType == 'MOVIE' ? 'MOVIELIKE' : 'IMAGELIKE'

        } else {

          query = `
            delete from
              oi_feed_media_like_log
            where
              feed_id = ${targetId} and media_seq = ${seq} and user_id = ${userId}
          `
          await mysql.execute(query)

          logType ='DELETELIKE'
          log = '좋아요 취소'
          mileage = -1
        }

        query = `
          select              
            (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id and lui.user_status_cd = 'ACTIVE' and lui.profile_open_type_cd != 'CLOSE'
              where target_id = ${targetId} and like_target_type_cd = 'FEED') feed_like_count,
            (select count(*) from oi_feed_media_like_log mll join oi_user_info mlui on mll.user_id = mlui.user_id and mlui.user_status_cd = 'ACTIVE' and mlui.profile_open_type_cd != 'CLOSE'
              where feed_id = ${targetId}) media_like_count,
            (select count(*) from oi_feed_media_like_log mll2 join oi_user_info mlui2 on mll2.user_id = mlui2.user_id and mlui2.user_status_cd = 'ACTIVE' and mlui2.profile_open_type_cd != 'CLOSE'
              where feed_id = ${targetId} and media_seq = ${seq}) this_media_like_count
        `
        result = await mysql.execute(query)
        console.log(result)
        likeCount = result[0].this_media_like_count
        feedLikeCount = result[0].feed_like_count + result[0].media_like_count

        break
      }
      
      default: {
        return response.result(400)
      }
    }

    query = `
      update
        oi_feed_popul_aggr
      set
        like_cnt = ${feedLikeCount}
      where
        feed_id = ${feedId};
    `
    console.log(query)
    await mysql.execute(query)

    mysql.end()

    if (notificationType != null) {

      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            targetUserIds: [targetUserId],
            isAnonymous: isAnonymous,
            messages: [
              {
                userId: userId,
                notificationType: notificationType,
                feedId: feedId
              }
            ]
          })
        })
      }
      console.log(params)
  
      await lambda.invoke(params).promise()
    }

    if (logType != null) {
      let logParams = {
        FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            userId: userId,
            mileage: mileage,
            type: logType,
            targetType: 'FEED',
            targetId: feedId,
            actionId: actionId,
            log: log,
            linkInfo: linkInfo == null ? null : [linkInfo],
            accountId: accountId
          })
        })
      }
      console.log(logParams)

      await lambda.invoke(logParams).promise()
    }

    return response.result(200, {liked: like, likeCount: likeCount, feedLikeCount: feedLikeCount})
  
  } catch(e) {

    console.error(e)

    await mysql.rollback()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
