'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 댓글 목록
 * @method get getComments
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId
    
    const parameters = event.queryStringParameters || {}

    const rowCount = parameters.rowCount || 15
    let firstId = parameters.firstId || 2147483647

    let query = `
      select
        (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id and lui.user_status_cd = 'ACTIVE' and lui.profile_open_type_cd != 'CLOSE'
          where like_target_type_cd = 'COMT' and target_id = fc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
        if ((select count(*) from oi_like_log where like_target_type_cd = 'COMT' and target_id = fc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
        fc.comment_id, fc.del_yn is_deleted, fc.content, fc.anony_yn is_anonymous, fc.reg_dt,
        case when fc.del_yn = 'Y' or (fc.anony_yn ='Y' and fc.reg_user_id != ${userId}) then null else ui.user_id end user_id,
        case 
          when fc.del_yn = 'Y' then ''
          else case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end
        end account_id,
        case 
          when fc.del_yn = 'Y' then null
                else case when fc.anony_yn = 'Y' then null else fi.org_file_nm end
        end profile_image_url,
        case 
          when fc.del_yn = 'Y' then null
          else case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end
        end profile_file_name
      from 
        oi_feed_comment fc
        left join oi_user_info ui on fc.reg_user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where
        fc.feed_id = ${feedId}
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and fc.parent_comment_id is null
        and comment_id < ${firstId}
      order by
        fc.comment_id desc
      limit 
        ${rowCount};
    `
    let comments = await mysql.execute(query)
    let previousCount = 0

    if (comments.length > 0) {

      comments = comments.map(comment => {
        comment.replies = []
        comment.previous_reply_count = 0
        comment.liked = comment.liked == 'Y'
        comment.is_anonymous = comment.is_anonymous == 'Y'
        comment.is_deleted = comment.is_deleted == 'Y'
        return comment
      })
  
      comments.reverse()
  
      console.log(comments)
  
      query = `
        select
          count(*) count
        from
          oi_feed_comment fc
          join oi_user_info ui on fc.reg_user_id = ui.user_id
        where
          fc.feed_id = ${feedId}
          and fc.parent_comment_id is null
          and fc.comment_id < ${comments[0].comment_id}
          and ui.user_status_cd = 'ACTIVE' 
          and ui.profile_open_type_cd != 'CLOSE';
      `
      let result = await mysql.execute(query)
      if (result.length > 0) {
        previousCount = result[0].count
  
        let commentIds = comments.map(comment => {
          return comment.comment_id
        })
        let commentIdString = commentIds.join(',')
        
        query = `
          select 
            fc.parent_comment_id, count(*) count 
          from 
            oi_feed_comment fc
            join oi_user_info ui on fc.reg_user_id = ui.user_id
          where 
            fc.parent_comment_id in (${commentIdString}) 
            and ui.user_status_cd = 'ACTIVE' 
            and ui.profile_open_type_cd != 'CLOSE'
          group by 
            fc.parent_comment_id;
        `
        let countResult = await mysql.execute(query)

        countResult.forEach(row => {
          for (let i = 0; i < comments.length; i++) {
            if (comments[i].comment_id == row.parent_comment_id) {
              comments[i].previous_reply_count = row.count
              break
            }
          }
        })

        console.log(comments)
  
        query = `
          select
            (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id
              where like_target_type_cd = 'COMT' and target_id = fc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
            if ((select count(*) from oi_like_log where like_target_type_cd = 'COMT' and target_id = fc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
            fc.comment_id, 'N' is_deleted, fc.parent_comment_id parent_id, fc.content, fc.anony_yn is_anonymous, fc.reg_dt,
            case when fc.anony_yn = 'Y' and fc.reg_user_id != ${userId} then null else ui.user_id end user_id,
            case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id,
            case when fc.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url,
            case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end profile_file_name
          from
              oi_feed_comment fc
              join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.profile_open_type_cd != 'CLOSE'
              left join (
          select fc.comment_id, fc.parent_comment_id, fc.reg_user_id,
          ui.profile_open_type_cd
          from oi_feed_comment fc
          join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.profile_open_type_cd != 'CLOSE'
        ) fc2 on fc.parent_comment_id = fc2.parent_comment_id and fc.comment_id < fc2.comment_id
              left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
              left join oi_file_info fi on pi.file_id = fi.file_id
            where 
              fc.parent_comment_id in (${commentIdString})
            group by 
              fc.parent_comment_id, fc.comment_id
            having 
              count(fc.comment_id) < 3
            order by
              fc.parent_comment_id, fc.comment_id desc
           ;
        `
        result = await mysql.execute(query)
  
        result.forEach(row => {
          row.liked = row.liked == 'Y'
          row.is_anonymous = row.is_anonymous == 'Y'
          row.is_deleted = row.is_deleted == 'Y'
          for (let i = 0; i < comments.length; i++) {
            if (comments[i].comment_id == row.parent_id && comments[i].replies.length < 3) {
              comments[i].replies.unshift(row)
              comments[i].previous_reply_count--
              break
            }
          }
        })
      }
    }
    
    mysql.end()

    let readable = await redis.readable(3)
    
    var onlines = {}

    comments.forEach(comment => {
      onlines[comment.user_id] = false
      comment.replies.forEach(reply => {
        onlines[reply.user_id] = false
      })
    })

    let keys = Object.keys(onlines)
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i]
      let onlineKey = `alive:${key}`
      let isOnline = await readable.get(onlineKey)
      onlines[key] = isOnline == "true" || isOnline == true
    }

    for (let i = 0; i < comments.length; i++) {
      let comment = comments[i]
      comments[i].is_online = onlines[comment.user_id]
      for (let j = 0; j < comment.replies.length; j++) {
        let reply = comment.replies[j]
        comments[i].replies[j].is_online = onlines[reply.user_id]
      }
    }

    redis.end()

    return response.result(200, {previous_count: previousCount, comments: comments})
  
  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
