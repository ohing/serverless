'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 피드 목록 게이트웨이
 * @method GET getFeedList
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    let nextEvent = {userId: userId, parameters: parameters}

    console.log(nextEvent)

    switch (parameters.type) {
      case "Help":
        return await require('./getHelps.js')(nextEvent) 

      case "HelpTest":
        return await require('./getHelpsTest.js')(nextEvent) 

      case "Feed":
        return await require('./getFeeds.js')(nextEvent) 

      case "Content":
        return await require('./getContents.js')(nextEvent) 

      case "Tag":
        return await require('./getTagContents.js')(nextEvent) 

      case "TagFeed":
        return await require('./getTagFeeds.js')(nextEvent) 

      case "TagHelp":
        return await require('./getTagHelps.js')(nextEvent) 

      case "TargetHelp":
        return await require('./getTargetHelps.js')(nextEvent) 

      case "TargetFeed":
        return await require('./getTargetFeeds.js')(nextEvent) 

      case "TargetAllFeed":
        return await require('./getTargetAllFeeds.js')(nextEvent) 

      default:
        return response.result(400)
    }
  
  } catch(e) {

    console.error(e)    

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
