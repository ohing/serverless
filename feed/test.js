
module.linkTest = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    body: JSON.stringify({
      feedType: 'FEED',
      feedContent: 'aslfjasl flaks flkajs lfj aslkdf jalksdj flka sjfl https://vt.tiktok.com/ZSJv9Mf5q/ asldjflkas dfkl jasldfj aslkdf',
      medias: [
        {
          "type": "IMAGE",
          "key": "img/IAA_20190518050654993/IAA_20190518050654993.jpg",
          "width": 600,
          "height": 600
        }
      ],
      tags: [
        "가나다", "라마바", "게임"        
      ]
    })
  }
  return await require('./linkTest.js').handler(request)
}


module.createFeed = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '7e5d30b7-4ea5-490c-b91b-b12bafb3b09a'
    },
    body: JSON.stringify({
      feedType: 'HELP',
      isAnonymous: false,
      helpCategoryCode: 'HC010',
      helpTitle: '\'제목\'',
      feedContent: '\'아아아\' https://m.bboom.naver.com/best/get?boardNo=9&postNo=3476110&entrance=2',
      // medias: [
      //   {
      //     "type": "IMAGE",
      //     "key": "img/IAA_20190518050654993/IAA_20190518050654993.jpg",
      //     "width": 600,
      //     "height": 600
      //   }
      // ],
      // tags: [
      //   "가나다", "라마바", "게임"        
      // ]
    })
  }
  return await require('./createFeed.js').handler(request)
}

module.createFeedForHelp = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      feedType: 'HELP',
      isAnonymous: true,
      helpTitle: '도와줘어어어',
      helpCategoryCode: 'HC004',
      medias: [
        {
          key: 'img/test1/test1.png',
          type: 'IMAGE',
          width: 1792,
          height: 828,
        },
        {
          key: 'img/test2/test2.png',
          type: 'IMAGE',
          width: 828,
          height: 1792,
        }
      ],
      tags: [
        "가나다", "라마바", "게임"
      ]
    })
  }
  return await require('./createFeed.js').handler(request)
}

module.viewFeed = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      feedId: 355
    },
    queryStringParameters: {
      withComments: 1
    }
  }
  return await require('./viewFeed.js').handler(request)
}

module.modifyFeed = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    pathParameters: {
      feedId: 597
    },
    body: JSON.stringify({
      "feedContent": "동해물과 백두산이 마르고 닳도록 집에가고싶다.",
      "tags": [
        "태그1", "태그2", "태그4"
      ],
      "medias": [
        {
          mediaSeq: 1
        }
      ]
    })
  }
  return await require('./modifyFeed.js').handler(request)
}

module.createComment = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      feedId: 585
    },
    body: JSON.stringify({
      content: '@karl01 6666',
      isAnonymous: false
    })
  }
  return await require('./createComment.js').handler(request)
}

module.getComments = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      feedId: 550
    },
    body: JSON.stringify({
      
    })
  }
  return await require('./getComments.js').handler(request)
}

module.getReplies = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      feedId: 550,
      parentId: 1032
    },
    body: JSON.stringify({
      
    })
  }
  return await require('./getReplies.js').handler(request)
}

module.like = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6f4f8592-7ed5-4aa7-9747-e369fb2142c8'
    },
    body: JSON.stringify({
      // targetType: 'MEDIA',
      targetType: 'FEED',
      targetId: 586,
      // seq: 1,
      like: true,
    })
  }
  return await require('./like.js').handler(request)
}

module.getLikeList = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    queryStringParameters: {
      targetType: 'FEED',
      targetId: 4
    }
  }
  return await require('./getLikeList.js').handler(request)
}

module.getFeedList = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'd3ea76f1-fdf8-448c-9ee1-577081cca445'
    },
    queryStringParameters: {
      // type: "Help",
      type: "HelpTest",
      // type: "Feed",
      // type: "Tag",
      // type: "TagFeed",
      // type: "TagHelp",
      // type: "Content",
      // type: "TargetFeed",
      // type: "TargetAllFeed",
      // targetUserId: 240,
      // tag: "게임",
      // order: "View",
      // order: "Popular",
      // lastId: "548",
      // lastMediaSeq: "4",
      // lastViewCount: "11",
      // lastPopular: 12,
      // searchText: '게임'
    }
  }
  return await require('./getFeedList.js').handler(request)
}

module.getHelpsTest = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '2352d8cd-6d09-4526-a773-c7137647c554'
    },
    queryStringParameters: {

      type: "Help",
      order: "View",
      helpCategoryCode: "HC004",
      // order: "Popular",
      // lastId: "187",
      // searchText: '게임'
    }
  }
  return await require('./getFeedList.js').handler(request)
}

module.getShareUrl = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6f4f8592-7ed5-4aa7-9747-e369fb2142c8'
    },
    pathParameters: {
      feedId: '23396'
    }
  }
  return await require('./getShareUrl.js').handler(request)
}

module.sharedFeed = async () => {

  let request = {    
    pathParameters: {
      encryptedId: 'bd918eeedf816183e9654f8409af1d16-95eb2930e53f1400e6b9ee83d8ae75b4'
    }
  }
  return await require('./sharedFeed.js').handler(request)
}

test = async () => {

  // let text = '@dahee.star fkfk'
  // let regex = /(?<=[\s>]|^)@(\w*[A-Za-z_0-9.]+\w*)/g
  // let matchedIds = text.match(regex)
  // console.log(matchedIds)

  // return
  
  const environment = require('../environment.js')
  environment.setEnvironment()
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()