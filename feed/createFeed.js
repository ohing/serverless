'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const urlRegex = require('url-regex');
const meta = require('html-metadata-parser');
const axios = require('axios')
// const TikTokScraper = require('tiktok-scraper');
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

const { URL } = require('url');


/**
 * @author Karl <karl@ohing.net> 
 * @description 피드 작성
 * @method PUT createFeed
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }
    
    const parameters = JSON.parse(event.body || '{}') || {}
    
    const feedType = parameters.feedType
    const medias = parameters.medias || []
    let feedContent = (parameters.feedContent || '').trim()

    if (feedType == null || (medias.length == 0 && feedContent.length == 0)) {
      return response.result(400)
    }

    let query
    let result
    let valueParameters

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 글을 작성하실 수 없습니다.')
    }

    if (feedContent.length > 0) {
      let words = feedContent.split(' ')
      let keys = []
      for (let word of words) {
        let trimmed = word.trim()
        if (trimmed.length > 0) {
          let escaped = await mysql.escape(trimmed)
          console.log(escaped)
          keys.push(`md5(${escaped})`) 
        }
      }

      if (keys.length > 0) {
        query = `
          select 
            count(*) count
          from
            oi_banned_search_text
          where
            banned_id in (${keys.join(',')});
        `
        console.log(query)
        result = await mysql.execute(query)
        if (result[0].count > 0) {
          return response.result(403, null, '금지된 단어가 포함되어 있어 게시물 등록에 실패하였습니다.')
        }
      }

      feedContent = await mysql.escape(feedContent)
    }

    let isAnonymous = false
    let helpCategoryCode = 'null'
    let helpTitle = 'null'
    
    if (feedType == 'HELP') {

      isAnonymous = parameters.isAnonymous
      helpCategoryCode = `'${parameters.helpCategoryCode}'`
      helpTitle = await mysql.escape(parameters.helpTitle)

      console.log(helpCategoryCode, (helpCategoryCode == null || helpCategoryCode.length == 0))
      console.log(helpTitle, (helpTitle == null || helpTitle.length == 0))
      
      if (isAnonymous == null || typeof isAnonymous != 'boolean' || (helpCategoryCode.length == 0) || (helpTitle.length == 0)) {
        
        return response.result(400)
      }
    }

    let needsEncoding = medias.filter(media => {
      return media.type == 'MOVIE'
    }).length > 0

    await mysql.beginTransaction()

    query = `
      insert into oi_feed_info (
        feed_title, feed_type_cd, feed_content, category_cd, anony_yn, encoded_yn, feed_except_yn, reg_user_id
      ) values (
        ${helpTitle}, '${feedType}', ${feedContent}, ${helpCategoryCode}, '${isAnonymous ? 'Y' : 'N'}', '${needsEncoding ? 'N' : 'Y'}', 'N', ${userId}
      );
    `
    console.log(query)
    result = await mysql.execute(query)
    let feedId = result.insertId

    query = `
      select
        0 like_count, 'N' liked,
        0 comment_count,
        0 view_count,
        0 share_count,
        fi.feed_id, fi.feed_type_cd feed_type, fi.feed_content, fi.reg_user_id user_id,
        fi.feed_title help_title, fi.category_cd help_category,
        fi.anony_yn is_anonymous, fi.encoded_yn encoded, fi.reg_dt,
        fii.org_file_nm profile_image_url, substring_index(fii.org_file_nm , '/', -1) profile_file_name
      from
        oi_feed_info fi 
        join oi_user_info ui on fi.reg_user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fii on pi.file_id = fii.file_id
      where
        feed_id = ${feedId};
    `
    result = await mysql.execute(query)
    let feed = result[0]
    feed.liked = feed.liked == 'Y'
    feed.is_anonymous = feed.is_anonymous == 'Y'
    feed.encoded = feed.encoded == 'Y'

    console.log(medias)

    if (medias.length > 0) {

      let fileInsertValues = []

      for (let i = 0; i< medias.length; i++) {

        let media = medias[i]

        let mediaKey = media.key
        
        if (media.type == null || !(media.type == 'IMAGE' || media.type == 'AGIF' || media.type == 'MOVIE') || media.key == null) {
          await mysql.rollback()
          mysql.end()
          return response.result(400)
        }
        let width = media.width || 600
        let height = media.height || 600
        let playTime = media.playTime || '00:00'

        let finalKey = mediaKey

        console.log(media.type)

        if (media.type == 'MOVIE') {
          if (!mediaKey.includes('/') && mediaKey.includes('.')) {
            let fileNames = mediaKey.split('.')
            let fileName = fileNames[0]
            let fileExtension = fileNames[1]
            finalKey = `mov/${fileName}/mp4/${fileName}.${fileExtension}`
          } else if (mediaKey.includes('source')) {
            let paths = mediaKey.split('/')
            let fileNames = paths[paths.length-1].split('.')
            let fileName = fileNames[0]
            let fileExtension = fileNames[1]
            finalKey = `mov/${fileName}/mp4/${fileName}.${fileExtension}`
          }
        } else {
          if (!mediaKey.includes('/') && mediaKey.includes('.')) {
            let fileNames = mediaKey.split('.')
            let fileName = fileNames[0]
            let fileExtension = fileNames[1]
            finalKey = `img/${fileName}/${fileName}.${fileExtension}`
          }
        }

        fileInsertValues.push(`( '${media.type}', '${finalKey}', ${width}, ${height}, '${playTime}', '${media.type == 'MOVIE' ? 'N': 'Y'}' )`)
      }

      let valuesString = fileInsertValues.join(',')

      query = `
        insert into oi_file_info (
          media_type_cd, org_file_nm, width, height, play_time, encoded_yn
        ) values
          ${valuesString};
      `
      console.log(query)
      result = await mysql.execute(query)
      let insertId = result.insertId

      let mediaInsertValues = []
      for (let i = 0; i < medias.length; i++) {
        let fileId = i + insertId
        mediaInsertValues.push(`( ${feedId}, ${i+1}, ${fileId}, ${userId} )`)
      }

      valuesString = mediaInsertValues.join(',')

      query = `
        insert into oi_feed_media_info (
          feed_id, media_seq, file_id, reg_user_id
        ) values
          ${valuesString};
      `
      await mysql.execute(query)

      // (case when fi.media_type_cd = 'MOVIE' then concat(substring_index(substring_index(org_file_nm, '/', -1), '.', 1), '0000000.jpg') else null end) thumbnail_file_name 
      

      query = `
        select 
          0 like_count, 'N' liked,
          fmi.feed_id, fmi.media_seq,
          fi.media_type_cd type, org_file_nm media_url, substring_index(org_file_nm, '/', -1) media_file_name, width, height, play_time
        from
          oi_feed_media_info fmi
          join oi_file_info fi on fmi.file_id = fi.file_id
        where
          fmi.feed_id = ${feedId};
      `
      result = await mysql.execute(query)
      let mediasInfos = result.map(row => {
        row.liked = row.liked == 'Y'
        return row
      })
      feed.medias = mediasInfos

    } else {
      
      feed.medias = []
    }

    const tags = parameters.tags || []

    if (tags.length > 0) {

      let tagValues = []
      let trimmedValues = []

      for (let tag of tags) {
        let trimmed = tag.trim()
        if (trimmed.length > 0) {
          let escaped = await mysql.escape(trimmed)
          tagValues.push(`( ${escaped}, 1 )`)
          trimmedValues.push(`${escaped}`)
        }
      }

      let valuesString = tagValues.join(',')

      query = `
        insert into oi_tag_info (
          tag_val, tag_cnt
        ) values
          ${valuesString}
        on duplicate key update
          tag_cnt = values(tag_cnt) + 1;
      `

      await mysql.execute(query)

      query = `
        select
          tag_id, tag_val
        from 
          oi_tag_info
        where
          tag_val in (${trimmedValues.join(',')});
      `
      result = await mysql.execute(query)

      let tagInfos = tags.map(tag => {
        return {tag_val: tag}
      })
      for (let i = 0; i < tagInfos.length; i++) {
        let filtered = result.filter(row => {
          return row.tag_val == tagInfos[i].tag_val
        })
        tagInfos[i].tag_id = filtered[0].tag_id
      }
      
      let tagIds = tagInfos.map(tagInfo => {
        return tagInfo.tag_id
      })
      
      let seq = 0
      let values = tagIds.map(tagId => { 
        seq++
        return `( ${feedId}, ${tagId}, ${seq} )`
      })
      valuesString = values.join(',')
      
      query = `
        insert into oi_feed_tag (
          feed_id, tag_id, tag_seq
        ) values 
        ${valuesString};
      `
      await mysql.execute(query)

      query = `
        select 
          ti.tag_val
        from
          oi_feed_tag ft
          join oi_tag_info ti on ft.tag_id = ti.tag_id
        where
          ft.feed_id = ${feedId}
        order by
          ft.tag_seq asc; 
      `
      result = await mysql.execute(query)
      feed.tags = result.map(row => {
        return row.tag_val
      })

    } else {

      feed.tags = []
    }

    let pureContent = (parameters.feedContent || '').trim()
    
    if (pureContent.length > 0) {

      let linkAllowed = false
      let inviteAllowed = false

      let urls = pureContent.match(urlRegex()) || []

      console.log('urls', urls)

      for (let i = 0; i < urls.length; i++) {
        let url = urls[i]

        if (inviteAllowed && linkAllowed) {
          break
        }

        if (!url.startsWith('http') && !url.includes('://')) {
          url = `http://${url}`
        }

        console.log('url', url)

        let parsedUrl

        try {
          parsedUrl = new URL(url)
        } catch (e) {
          console.log('parse url error', e)
        }

        if (parsedUrl == null) {
          continue
        }

        console.log('protocol', parsedUrl.protocol)

        if (parsedUrl.protocol == 'ohing:') {

          if (inviteAllowed) {
            continue
          }

          let paths = parsedUrl.pathname.split('/')
          console.log(paths)
          if (paths.length == 4 && paths[3].startsWith('EOL')) {

            let type = paths[1]
            let id = parseInt(paths[2])

            if (type == 'room') {

              query = `
                select
                  cri.room_id, cri.title,
                  case
                    when length(cri.room_pwd) is null or length(cri.room_pwd) = 0 then 'N'
                    else 'Y'
                  end needs_password,
                  max_user_cnt max_user_count, join_user_cnt join_user_count,
                  cui.user_id, 
                  case
                    when cui.chat_profile_id is null then ui.user_login_id 
                    else cpi.nick_nm
                  end account_id,
                  case
                    when cui.chat_profile_id is null then fi.org_file_nm 
                    else fi2.org_file_nm
                  end profile_image_url, 
                  case
                    when cui.chat_profile_id is null then substring_index(fi.org_file_nm, '/', -1) 
                    else substring_index(fi2.org_file_nm, '/', -1) 
                  end profile_file_name,
                  fi3.org_file_nm room_image_url, substring_index(fi3.org_file_nm, '/', -1) room_file_name
                from
                  oi_chat_room_info cri
                  join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'                  
                  join oi_user_info ui on cui.user_id = ui.user_id
                  left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
                  left join oi_file_info fi on upi.file_id = fi.file_id
                  left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
                  left join oi_file_info fi2 on cpi.image_id = fi.file_id
                  left join oi_file_info fi3 on cri.room_image_id = fi3.file_id
                where
                  cri.room_id = ${id} and cri.room_type_cd = 'OPEN';
              `
              result = await mysql.execute(query)
              if (result.length > 0) {

                let roomInfo = result[0]

                query = `
                  insert into oi_feed_chat_room_invite (
                    feed_id, room_id, invite_seq, reg_dt, reg_user_id
                  ) values (
                    ${feedId}, ${id}, 1, now(), ${userId}
                  );
                ` 

                await mysql.execute(query)
                query = `
                  select 
                    ti.tag_val
                  from 
                    oi_chat_room_tag crt
                        join oi_tag_info ti on crt.tag_id = ti.tag_id
                  where
                    crt.room_id = ${id}
                  order by 
                    crt.tag_seq asc;              
                `
                result = await mysql.execute(query)
                roomInfo.tags = result.map(row => {
                  return row.tag_val
                })
                feed.invite = roomInfo 

                inviteAllowed = true
              }
            }
          }

          continue
        }

        if (linkAllowed) {
          continue
        }

        query = `
          select link_id from oi_link_url_info where link_url = ?;
        `
        console.log(query)
        result = await mysql.execute(query, [url])
        let linkId
        if (result.length == 0) {

          let isUnavailable = false

          let linkType
          switch (parsedUrl.hostname) {
            case 'youtu.be':
            case 'youtube.com':
            case 'www.youtube.com':
              linkType = 'YTUBE'
              break;

            case 'instagram.com':
            case 'www.instagram.com':
              linkType = 'INSTAGRAM'
              break

            case 'facebook.com':
            case 'www.facebook.com':
              linkType = 'FACEBOOK'
              break

            case 'vt.tiktok.com':
            case 'www.tiktok.com':
            case 'tiktok.com':
              // linkType = 'TIKTOK'
              isUnavailable = true
              break
          
            default:

              console.log('hostname', parsedUrl.hostname)

              if (parsedUrl.hostname.includes('boom.naver')) {
                isUnavailable = true
              }
              linkType = 'PAGE'
              break;
          }

          if (isUnavailable) {
            // result = await module.tiktokMeta(url)
            continue
          } else {
            try {
              result = await meta.parser(url)
            } catch (e) {
              console.log(e)
            }
          }

          if (result) {
            let imageUrl = null
            let siteName = null
            let title = null
            let content = null

            if (result.og != null) {

              let og = result.og

              if (og.image != null && og.image.length > 0) {
                imageUrl = og.image
              } else if (og.images.length > 0) {
                let image = og.images[0]
                imageUrl = image.url
              }

              if (og.site_name != null) {
                siteName = og.site_name
              }

              if (og.title != null) {
                title = og.title
              }

              if (og.description != null) {
                content = og.description
              }
            }

            if ((imageUrl || '').length == 0) {
              continue
            }

            if (siteName == null) {
              siteName = parsedUrl.hostname
            }

            query = `
              insert into oi_link_url_info (
                link_url, link_type_cd, thumbnail_url, site_nm, title, content
              ) values (
                ?, ?, ?, ?, ?, ?
              );
            `

            valueParameters = [url, linkType, imageUrl, siteName, title, content]

            result = await mysql.execute(query, valueParameters)
            linkId = result.insertId
          }
          
        } else {
          linkId = result[0].link_id
        }

        if (linkId != null) {
          query = `
            insert into oi_feed_link_url_mapping (
              feed_id, link_seq, link_id
            ) values (
              ${feedId}, 1, ${linkId}
            );
          `
          await mysql.execute(query)

          linkAllowed = true
        }
      }
    }
    
    query = `
      select
        lui.link_id, lui.link_url, lui.link_type_cd link_type, lui.thumbnail_url, lui.site_nm site_name, lui.title, lui.content
      from
        oi_link_url_info lui
        join oi_feed_link_url_mapping lum on lui.link_id = lum.link_id
      where
        lum.feed_id = ${feedId}
      order by 
        lum.link_seq asc
      limit 1;
    `
    result = await mysql.execute(query)

    if (result.length > 0) {
      feed.link = result[0]
    }

    query = `
      insert into oi_feed_popul_aggr (
        feed_id, like_cnt, view_cnt, comment_cnt, share_cnt, reg_dt
      ) values (
        ${feed.feed_id}, 0, 0, 0, 0, now()
      )
    `
    await mysql.execute(query)
    
    await mysql.commit()
    mysql.end()

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: 2,
          type: feed.feed_type == 'HELP' ? 'WRITEHELP' : 'WRITEFEED',
          targetType: 'FEED',
          targetId: feed.feed_id,
          actionId: feed.feed_id,
          log: feed.feed_type == 'HELP' ? '도와줘 작성 완료' : '마이피드 작성 완료'
        })
      })
    }
    console.log(logParams)

    await lambda.invoke(logParams).promise()

    return response.result(200, feed)
  
  } catch(e) {

    console.error(e)

    await mysql.rollback()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
