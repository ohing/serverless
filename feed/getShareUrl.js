'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

const crypto = require('crypto')

/**
 * @author Karl <karl@ohing.net> 
 * @description 공유용 피드 조회
 * @method GET sharedFeed
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId

    const iv = crypto.randomBytes(16)
    console.log(process.env.shareSecret)
    const cipher = crypto.createCipheriv('aes-256-cbc', process.env.shareSecret, iv)
    const encrypted = cipher.update(feedId)

    let encryptedString = iv.toString('hex') + '-' + Buffer.concat([encrypted, cipher.final()]).toString('hex')
    console.log(encryptedString)

    let url = `https://feed.ohing.net/${process.env.stage == 'dev' ? 'dev/' : ''}${encryptedString}`
    
    return response.result(200, {url: url})
  
  } catch(e) {

    console.error(e)    
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
