'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()


/**
 * @author Karl <karl@ohing.net> 
 * @description 댓글 작성
 * @method PUT createComment
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId
    
    const parameters = JSON.parse(event.body || '{}') || {}

    let parentId = parameters.parentId
    let content = (parameters.content || '').trim()
    const isAnonymous = parameters.isAnonymous

    if (content.length == 0 || isAnonymous == null || typeof isAnonymous != 'boolean') {
      return response.result(400)
    }

    content = await mysql.escape(content)

    let parentCommentId = parentId != null ? `${parentId}` : 'null'

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 댓글을 작성하실 수 없습니다.')
    }

    let log = null
    let linkInfo = null
    let accountId = null

    query = `
      insert into oi_feed_comment (
        feed_id, parent_comment_id, content, anony_yn, reg_user_id
      ) values (
        ${feedId}, ${parentCommentId}, ${content}, '${isAnonymous ? 'Y' : 'N'}', ${userId}
      );
    `
    console.log(query)
    result = await mysql.execute(query)
    let commentId = result.insertId

    console.log(commentId)

    query = `
      select
        fc.comment_id, fc.parent_comment_id parent_id, fc.content, fc.anony_yn is_anonymous, fc.reg_dt,
        case when fc.anony_yn = 'Y' and fc.reg_user_id != ${userId} then null else ui.user_id end user_id,
        case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id,
        case when fc.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url,
        case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end profile_file_name
      from 
        oi_feed_comment fc
        join oi_user_info ui on fc.reg_user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where
        fc.comment_id = ${commentId};
    `
    result = await mysql.execute(query)
    console.log(result)

    let comment = result[0]
    comment.like_count = 0
    comment.liked = false
    comment.is_online = true
    comment.is_anonymous = comment.is_anonymous == 'Y'
    comment.is_deleted = false

    query = `
      update
        oi_feed_popul_aggr
      set
        comment_cnt = (select count(*) from oi_feed_comment fc join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fc.feed_id = ${feedId} and fc.del_yn = 'N')
      where
        feed_id = ${feedId};
    `
    await mysql.execute(query)

    query = parentId == null ? `
      select 
        f.reg_user_id
      from
        oi_feed_info f
      where 
        f.feed_id = ${feedId}
        and f.reg_user_id != ${userId};
    ` : `
      select 
        fc.reg_user_id
      from
        oi_feed_comment fc
      where 
        fc.comment_id = ${parentId}
        and fc.reg_user_id != ${userId};
    `
    result = await mysql.execute(query)

    if (result.length > 0) {
      let targetUserId = result[0].reg_user_id

      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            targetUserIds: [targetUserId],
            isAnonymous: isAnonymous,
            messages: [
              {
                userId: userId,
                notificationType: parentId == null ? 'COMMENT' : 'REPLY',
                feedId: feedId
              }
            ]
          })
        })
      }
      console.log(params)
  
      await lambda.invoke(params).promise()
    }

    // let regex = /(?<=[\s>]|^)@(\w*[A-Za-z_0-9.]+\w*)/g
    // let matchedIds = content.match(regex)

    // console.log(matchedIds)

    // if (matchedIds != null) {

    //   matchedIds = matchedIds.filter(atId => {
    //     return atId.length > 3
    //   }).map(atId => {
    //     return `'${atId.substring(1)}'`
    //   })

    //   if (matchedIds.length > 0) {
  
    //     query = `
    //       select	
    //         ui.user_id
    //       from
    //         oi_user_info ui
    //       where
    //         ui.user_login_id in (${matchedIds.join(',')});
    //     `
    //     result = await mysql.execute(query)

    //     mysql.end()

    //     let targetUserIds = result.map(row => {
    //       return row.user_id
    //     })

    //     let params = {
    //       FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
    //       InvocationType: "Event",
    //       Payload: JSON.stringify({
    //         body: JSON.stringify({
    //           targetUserIds: targetUserIds,
    //           isAnonymous: isAnonymous,
    //           messages: [
    //             {
    //               userId: userId,
    //               notificationType: 'MENTION',
    //               feedId: feedId,
    //             }
    //           ]
    //         })
    //       })
    //     }
    //     console.log(params)
    
    //     await lambda.invoke(params).promise()
    //   }    

    // }

    if (parentId == null) {

      query = `
        select 
          f.feed_type_cd, ui.user_id, ui.user_login_id
        from 
          oi_feed_info f
          join oi_user_info ui on f.reg_user_id = ui.user_id
        where
          f.feed_id = ${feedId};
      `
      result = await mysql.execute(query)
      let feedType = result[0].feed_type_cd

      log = feedType == 'HELP' ? `{accountId} 님의 도와줘에 🗨댓글 투척` : `{accountId} 님의 피드에 🗨댓글 투척`
      accountId = result[0].user_login_id
      linkInfo = {
        key: 'accountId',
        value: accountId,
        color: '#FF4800',
        targetType: 'USER',
        targetId: result[0].user_id
      }

    } else {

      query = `
        select 
          ui.user_id, ui.user_login_id
        from 
          oi_feed_comment fc
          join oi_user_info ui on fc.reg_user_id = ui.user_id
        where
          fc.comment_id = ${parentId};
      `
      result = await mysql.execute(query)    


      log = `{accountId} 님의 댓글에 🗨답글 투척`
      accountId = result[0].user_login_id
      linkInfo = {
        key: 'accountId',
        value: accountId,
        color: '#FF4800',
        targetType: 'USER',
        targetId: result[0].user_id
      }
    }

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: 3,
          type: 'COMMENT',
          targetType: 'FEED',
          targetId: feedId,
          actionId: commentId,
          log: log,
          linkInfo: linkInfo == null ? null : [linkInfo],
          accountId: accountId
        })
      })
    }
    console.log(logParams)

    mysql.end()

    await lambda.invoke(logParams).promise()

    return response.result(200, comment)
  
  } catch(e) {

    console.error(e)
    
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
