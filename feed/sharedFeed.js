'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

const crypto = require('crypto')

/**
 * @author Karl <karl@ohing.net> 
 * @description 공유용 피드 조회
 * @method GET sharedFeed
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let encryptedId = event.pathParameters.encryptedId
    let textParts = encryptedId.split('-')
    let iv = Buffer.from(textParts.shift(), 'hex')
    let encryptedText = Buffer.from(textParts.join('-'), 'hex')

    console.log('aaaa', process.env.shareSecret)

    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(process.env.shareSecret), iv)
    let decrypted = decipher.update(encryptedText)
    let feedId = Buffer.concat([decrypted, decipher.final()]).toString()
    
    let query = `
      select
        f.feed_id, f.feed_type_cd feed_type, f.feed_content,
        f.feed_title help_title, f.category_cd help_category,
        f.anony_yn is_anonymous, f.encoded_yn encoded, f.reg_dt,
        case when f.anony_yn = 'Y' then null else ui.user_id end user_id,
        case when f.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id,
        case when f.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url,
        case when f.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end profile_file_name,
        exists(select user_id from oi_like_log where target_id = ${feedId} and like_target_type_cd = 'FEED' and user_id = ${userId}) liked,
        fpa.like_cnt like_count,
        fpa.comment_cnt comment_count,
        fpa.view_cnt view_count,
        fpa.share_cnt share_count
      from
        oi_feed_info f
        join oi_user_info ui on f.reg_user_id = ui.user_id
        join oi_feed_popul_aggr fpa on f.feed_id = fpa.feed_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where
        f.feed_id = ${feedId};
    `
    console.log(query)
    let result = await mysql.execute(query)

    console.log(result)

    if (result.length == 0) {
      mysql.end()
      return response.result(404, null, '게시물을 찾을 수 없습니다.')
    }

    let feed = result[0]
    feed.liked = feed.liked != 0
    feed.is_anonymous = feed.is_anonymous == 'Y'
    feed.encoded = feed.encoded == 'Y'

    query = `
      select 
        (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id
          where feed_id = fmi.feed_id and media_seq = fmi.media_seq and ui.profile_open_type_cd != 'CLOSE') like_count,
        'N' liked,
        fmi.feed_id, fmi.media_seq,
        fi.media_type_cd type, org_file_nm media_url, substring_index(org_file_nm, '/', -1) media_file_name, width, height, play_time
      from
        oi_feed_media_info fmi
        join oi_file_info fi on fmi.file_id = fi.file_id
      where
        fmi.feed_id = ${feedId};
    `
    result = await mysql.execute(query)
    let medias = result.map(row => {
      row.liked = row.liked == 'Y'
      return row
    })
    if (medias.length > 0) {
      feed.medias = medias
    }

    query = `
      select 
        ti.tag_val
      from
        oi_feed_tag ft
        join oi_tag_info ti on ft.tag_id = ti.tag_id
      where
        ft.feed_id = ${feedId}
      order by
        ft.tag_seq asc; 
    `
    result = await mysql.execute(query)
    feed.tags = result.map(row => {
      return row.tag_val
    })

    query = `
      select
        lui.link_id, lui.link_url, lui.link_type_cd link_type, lui.thumbnail_url, lui.site_nm site_name, lui.title, lui.content
      from
        oi_link_url_info lui
          join oi_feed_link_url_mapping lum on lui.link_id = lum.link_id
      where
        lum.feed_id = ${feedId}
      order by 
        lum.link_seq asc
      limit 1;
    `
    result = await mysql.execute(query)
    if (result.length > 0)
    feed.link = result[0]

    query = `
      select
        cri.room_id, cri.title, cri.room_pwd_use_yn needs_password, cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_cnt, 
          ui.user_id, ui.user_login_id account_id,
          ufi.org_file_nm profile_image_url, substring_index(ufi.org_file_nm, '/', -1) profile_file_name, 
          rfi.org_file_nm room_image_url, substring_index(rfi.org_file_nm, '/', -1) room_file_name
      from
        oi_feed_chat_room_invite fcri
        join oi_chat_room_info cri on fcri.room_id = cri.room_id
          join oi_user_info ui on cri.reg_user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
          left join oi_file_info ufi on upi.file_id = ufi.file_id
          left join oi_file_info rfi on cri.room_image_id = rfi.file_id
      where
        fcri.feed_id = ${feedId}
        and cri.room_type_cd = 'OPEN'
        and cri.room_status_cd = 'ACTIVE'
      limit
        1;
    `
    result = await mysql.execute(query)
    if (result.length > 0) {

      let invite = result[0]
      invite.needs_password = invite.needs_password == 'Y'

      let query = `
        select
          tag_val
        from
          oi_chat_room_tag crt
            join oi_tag_info ti on crt.tag_id = ti.tag_id
        where
          room_id = ${invite.room_id}
        order by 
          crt.tag_seq asc;
      `
      result = await mysql.execute(query)
      invite.tags = result.map(row => {
        return row.tag_val
      })

      feed.invite = invite
    }

    query = `
      select
        count(*) count
      from 
        oi_feed_view_log
      where
        feed_id = ${feedId};
    `
    result = await mysql.execute(query)
    feed.view_count = result[0].count

    mysql.end()

    console.log(feed)

    return response.result(200, feed)
  
  } catch(e) {

    console.error(e)    
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
