'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 댓글/답글 수정
 * @method get getComments
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId
    let commentId = event.pathParameters.commentId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 댓글을 수정하실 수 없습니다.')
    }

    query = `
      select 
        comment_id
      from 
        oi_feed_comment 
      where 
        feed_id = ${feedId}
        and comment_id = ${commentId} 
        and reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(403, null, '수정할 수 없습니다.')
    }

    let parameters = JSON.parse(event.body || '{}') || {}
    let content = (parameters.content || '').trim()
    let isAnonymous = parameters.isAnonymous

    if (content.length == 0 || typeof(isAnonymous) != 'boolean') {
      mysql.end()
      return response.result(400)
    }

    content = await mysql.escape(content)

    query = `
      update
        oi_feed_comment
      set
        content = ${content}, anony_yn = '${isAnonymous ? 'Y' : 'N'}', upd_dt = now()
      where
        feed_id = ${feedId}
        and comment_id = ${commentId}
    `
    await mysql.execute(query)

    query = `
      select
        (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id
          where like_target_type_cd = 'COMT' and target_id = fc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
        if ((select count(*) from oi_like_log where like_target_type_cd = 'COMT' and target_id = fc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
        fc.comment_id, fc.parent_comment_id parent_id, fc.content, fc.anony_yn is_anonymous, fc.reg_dt,
        ui.user_id,
        case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id,
        case when fc.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url,
        case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end profile_file_name
      from 
        oi_feed_comment fc
        left join oi_user_info ui on fc.reg_user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where
        fc.comment_id = ${commentId};
    `
    console.log(query)
    result = await mysql.execute(query)
    let comment = result[0]

    mysql.end()

    comment.liked = comment.liked == 'Y'
    comment.is_anonymous = comment.is_anonymous == 'Y'
    comment.is_online = true

    return response.result(200, comment)
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
