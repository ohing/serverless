'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 댓글/답글 삭제
 * @method get getComments
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId
    let commentId = event.pathParameters.commentId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 댓글을 삭제하실 수 없습니다.')
    }

    query = `
      select 
        feed_id 
      from 
        oi_feed_info 
      where 
        feed_id = ${feedId} 
        and reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    let notMyFeed = result.length == 0

    query = `
      select 
        comment_id, parent_comment_id 
      from 
        oi_feed_comment 
      where 
        comment_id = ${commentId} 
        and reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result.length == 0 && notMyFeed) {
      mysql.end()
      return response.result(403, null, '삭제할 수 없습니다.')
    }

    let parentId = result[0].parent_comment_id

    let remainComment = null
    let deletedParentId = null

    if (parentId == null) {
      query = `
        select 
          count(*) count 
        from 
          oi_feed_comment 
        where 
          parent_comment_id = ${commentId};
      `
      result = await mysql.execute(query)
      if (result[0].count > 0) {

        query = `
          update 
            oi_feed_comment 
          set 
            content = '삭제된 댓글입니다.', del_yn = 'Y', upd_dt = now() 
          where 
            comment_id = ${commentId};
        `
        await mysql.execute(query)

        query = `
          select
            (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id and lui.user_status_cd = 'ACTIVE' and lui.profile_open_type_cd != 'CLOSE'
              where like_target_type_cd = 'COMT' and target_id = fc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
            if ((select count(*) from oi_like_log where like_target_type_cd = 'COMT' and target_id = fc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
            fc.comment_id, fc.content, fc.anony_yn is_anonymous, fc.reg_dt,
            case 
              when fc.del_yn = 'Y' then null
              else ui.user_id
            end user_id,
            case 
              when fc.del_yn = 'Y' then ''
              else case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end
            end account_id,
            case 
              when fc.del_yn = 'Y' then null
              else case when fc.anony_yn = 'Y' then null else fi.org_file_nm end
            end profile_image_url,
            case 
              when fc.del_yn = 'Y' then null
              else case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end
            end profile_file_name
          from 
            oi_feed_comment fc
            left join oi_user_info ui on fc.reg_user_id = ui.user_id
            left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
            left join oi_file_info fi on pi.file_id = fi.file_id
          where
            fc.comment_id = ${commentId};
        `
        result = await mysql.execute(query)

        if (result.length > 0) {
          remainComment = result[0]
          remainComment.replies = []
          remainComment.previousReplyCount = 0
          remainComment.liked = remainComment.liked == 'Y'
          remainComment.is_anonymous = remainComment.is_anonymous == 'Y'
          remainComment.is_deleted = true
        }

      } else {

        query = `
          delete from 
            oi_feed_comment 
          where 
            comment_id = ${commentId};
        `
        await mysql.execute(query)
      }
    } else {

      query = `
        delete from 
          oi_feed_comment 
        where 
          comment_id = ${commentId};
      `
      await mysql.execute(query)

      query = `
        select
          count(*) count
        from
          oi_feed_comment
        where
          parent_comment_id = ${parentId};
      `
      result = await mysql.execute(query)
      if (result[0].count == 0) {
        query = `
          delete from
            oi_feed_comment 
          where
            comment_id = ${parentId} 
            and del_yn = 'Y';
        `
        await mysql.execute(query)

        deletedParentId = parentId
      }
    }
    
    query = `
      update
        oi_feed_popul_aggr
      set
        comment_cnt = (select count(*) from oi_feed_comment fc join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fc.feed_id = ${feedId} and fc.del_yn = 'N')
      where
        feed_id = ${feedId};
    `
    await mysql.execute(query)

    mysql.end()

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: -3,
          type: 'DELETECOMT',
          actionId: commentId,
          log: '댓글 삭제/제재'
        })
      })
    }
    console.log(logParams)

    await lambda.invoke(logParams).promise()

    return response.result(200, {remainComment: remainComment, deletedParentId: deletedParentId})
  
  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
