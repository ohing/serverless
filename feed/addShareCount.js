'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 공유 카운트 증가
 * @method POST addShareCount
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)
  
  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId

    let query = `
      update
        oi_feed_popul_aggr
      set
        share_cnt = share_cnt + 1
      where
        feed_id = ${feedId};
    `
    await mysql.execute(query)

    query = `
      select
        share_cnt
      from
        oi_feed_popul_aggr
      where
        feed_id = ${feedId};
    `
    let result = await mysql.execute(query)
    let count = result.length > 0 ? result[0].share_cnt : 0

    return response.result(200, {shareCount: count})
  
  } catch(e) {

    console.error(e)    
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
