'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 대상 유저 도와줘 목록
 * @method GET getTargetHelps
 * @returns  
*/

module.exports = async event => {

  try {

    let userId = event.userId
    let parameters = event.parameters

    let targetUserId = parameters.targetUserId
    if (targetUserId == null) {
      targetUserId = userId
    }

    let defaultCondition = targetUserId == '0' ? `
      f.anony_yn = 'Y' and f.encoded_yn = 'Y'
    ` : `
      (${targetUserId == userId ? `f.reg_user_id = ${targetUserId}` : `f.reg_user_id = ${targetUserId} and f.anony_yn = 'N'`}) and f.encoded_yn = 'Y'
    `

    let rowCount = parameters.rowCount == null ? 20 : parseInt(parameters.rowCount)
    let lastId = parameters.lastId

    let idCondition = lastId == null ? '' : `and f.feed_id < ${lastId}`

    // readable = await redis.readable(3)

    // let onlineKey = `alive:${targetUserId}`
    // let isOnline = await readable.get(onlineKey) || false

    let query = `
      select
        f.feed_id, f.feed_type_cd feed_type, f.category_cd help_category, f.feed_title help_title, f.anony_yn is_anonymous, f.encoded_yn encoded, f.reg_dt,
        case when f.anony_yn = 'Y' and f.reg_user_id != ${userId} then null else ui.user_id end user_id,
        case when f.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id, 
        (select count(*) from oi_like_log where target_id = f.feed_id and user_id = ${userId} and like_target_type_cd = 'FEED') liked,
        fpa.like_cnt like_count,
        fpa.comment_cnt comment_count,
        fpa.view_cnt view_count,
        fpa.share_cnt share_count
      from
        oi_feed_info f
        join oi_user_info ui on f.reg_user_id = ui.user_id
        join oi_feed_popul_aggr fpa on f.feed_id = fpa.feed_id
      where
        f.feed_type_cd = 'HELP'
        and ${defaultCondition}
        and f.encoded_yn = 'Y'
        and f.feed_except_yn = 'N'
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        ${idCondition}
      order by
        f.feed_id desc
      limit 
        ${rowCount}
    `
    let result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end() 
      return response.result(200, [])
    }

    let feedIds = []
    let feedIndexMap = {}

    let index = 0

    let helps = result.map(row => {

      row.is_anonymous = row.is_anonymous == 'Y'
      row.encoded = row.encoded == 'Y'
      row.liked = row.liked != 0

      row.medias = []

      feedIds.push(row.feed_id)
      feedIndexMap[row.feed_id] = index

      index++

      return row
    })

    query = `
      select 
        (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id
          where feed_id = fmi.feed_id and media_seq = fmi.media_seq and ui.profile_open_type_cd != 'CLOSE') like_count,
        (select count(*) from oi_feed_media_like_log where feed_id = fmi.feed_id and media_seq = fmi.media_seq and user_id = ${userId}) liked,
        fmi.feed_id, fmi.media_seq,
        fi.media_type_cd type, fi.org_file_nm media_url, substring_index(fi.org_file_nm, '/', -1) media_file_name, fi.width, fi.height, fi.play_time
      from
        oi_feed_media_info fmi
          join oi_file_info fi on fmi.file_id = fi.file_id
      where
        fmi.feed_id in (${feedIds.join(',')})
      order by
        fmi.feed_id asc, fmi.media_seq asc;
    `
    result = await mysql.execute(query)

    for (let media of result) {

      media.liked = media.liked > 0

      let feedId = media.feed_id

      let index = feedIndexMap[feedId]
      if (index != null) {
        helps[index].medias.push(media)
      }
    }

    mysql.end()

    return response.result(200, helps)
  
  } catch(e) {

    console.error(e)    
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
