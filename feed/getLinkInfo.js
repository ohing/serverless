'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const urlRegex = require('url-regex');
const meta = require('html-metadata-parser');
const response = require('@ohing/ohingresponse')

const { URL } = require('url');


/**
 * @author Karl <karl@ohing.net> 
 * @description 링크정보 조회
 * @method GET getLinkInfo
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId
    
    const parameters = JSON.parse(event.body || '{}') || {}

    const medias = parameters.medias || []
    const feedContent = (parameters.feedContent || '').trim()

    if (medias.length == 0 && feedContent.length == 0) {
      return response.result(400)
    }

    let query = `
      select
        feed_title, feed_type_cd, feed_content, category_cd, anony_yn, encoded_yn
      from
        oi_feed_info
      where
        feed_id = ${feedId};
    `
    let result = await mysql.execute(query)

    if (result.length == 0) {
      return response.result(404, null, '대상을 찾을 수 없습니다.')
    }

    if (result[0].encoded_yn == 'N') {
      return response.result(403, null, '인코딩 완료 후 수정할 수 있습니다.')
    }

    let feedType = result[0].feed_type_cd

    let updates = []
    let sets = []

    if (feedType == 'HELP') {
      let helpTitle = parameters.helpTitle
      if (helpTitle != null && helpTitle.length > 0 && result[0].feed_title != helpTitle) {
        updates.push('feed_title')
        sets.push(`feed_title = '${helpTitle}'`)
      }
      let helpCategoryCode = parameters.helpCategoryCode
      if (helpCategoryCode != null && helpCategoryCode.length > 0 && result[0].category_cd != helpCategoryCode) {
        updates.push('category_cd')
        sets.push(`category_cd = '${helpCategoryCode}'`)
      }
      let isAnonymous = parameters.isAnonymous
      if (isAnonymous != null && typeof isAnonymous == 'boolean' && result[0].anony_yn) {
        updates.push('anony_yn')
        sets.push(`anony_yn = '${isAnonymous ? 'Y' : 'N'}'`)
      }
    }

    if (feedContent != result[0].feed_content) {
      updates.push('feed_content')
      sets.push(`feed_content = '${feedContent}'`)

      
    }

    await mysql.beginTransaction()

    result = await mysql.execute(query)
    let feed = result[0]
    feed.liked = feed.liked == 'Y'
    feed.is_anonymous = feed.is_anonymous == 'Y'
    feed.encoded = feed.encoded == 'Y'

    if (medias.length > 0) {

      let fileInsertValues = []
      medias.forEach(async media => {

        if (media.type == null || !(media.type == 'IMAGE' || media.type == 'AGIF' || media.type == 'MOVIE') || media.key == null) {
          await mysql.rollback()
          mysql.end()
          return response.result(400)
        }
        let width = media.width || 600
        let height = media.height || 600
        let playTime = media.playTime || '00:00'
        fileInsertValues.push(`( '${media.type}', '${media.key}', ${width}, ${height}, '${playTime}', '${media.type == 'MOVIE' ? 'N': 'Y'}' )`)
      })

      let valuesString = fileInsertValues.join(',')

      let query = `
        insert into oi_file_info (
          media_type_cd, org_file_nm, width, height, play_time, encoded_yn
        ) values
          ${valuesString};
      `
      console.log(query)
      let result = await mysql.execute(query)
      let insertId = result.insertId

      let mediaInsertValues = []
      for (let i = 0; i < medias.length; i++) {
        let fileId = i + insertId
        mediaInsertValues.push(`( ${feedId}, ${i+1}, ${fileId}, ${userId} )`)
      }

      valuesString = mediaInsertValues.join(',')

      query = `
        insert into oi_feed_media_info (
          feed_id, media_seq, file_id, reg_user_id
        ) values
          ${valuesString};
      `
      await mysql.execute(query)

      query = `
        select 
          0 like_count, 'N' liked,
          fmi.feed_id, fmi.media_seq,
          fi.media_type_cd type, org_file_nm file_nm, width, height, play_time
        from
          oi_feed_media_info fmi
          join oi_file_info fi on fmi.file_id = fi.file_id
        where
          fmi.feed_id = ${feedId};
      `
      feed.medias = await mysql.execute(query)

    } else {
      
      feed.medias = []
    }

    const tags = parameters.tags || []

    if (tags.length > 0) {

      let tagValues = []
      let trimmedValues = []

      tags.forEach(tag => {
        let trimmed = tag.trim()
        if (trimmed.length > 0) {
          tagValues.push(`( '${trimmed}', 1 )`)
          trimmedValues.push(`'${trimmed}'`)
        }
      })

      let valuesString = tagValues.join(',')

      let query = `
        insert into oi_tag_info (
          tag_val, tag_cnt
        ) values
          ${valuesString}
        on duplicate key update
          tag_cnt = values(tag_cnt) + 1;
      `

      await mysql.execute(query)

      query = `
        select
          tag_id, tag_val
        from 
          oi_tag_info
        where
          tag_val in (${trimmedValues.join(',')});
      `
      result = await mysql.execute(query)

      let tagInfos = tags.map(tag => {
        return {tag_val: tag}
      })
      for (let i = 0; i < tagInfos.length; i++) {
        let filtered = result.filter(row => {
          return row.tag_val == tagInfos[i].tag_val
        })
        tagInfos[i].tag_id = filtered[0].tag_id
      }
      
      let tagIds = tagInfos.map(tagInfo => {
        return tagInfo.tag_id
      })
      
      let seq = 0
      let values = tagIds.map(tagId => { 
        seq++
        return `( ${feedId}, ${tagId}, ${seq} )`
      })
      valuesString = values.join(',')
      
      query = `
        insert into oi_feed_tag (
          feed_id, tag_id, tag_seq
        ) values 
        ${valuesString};
      `
      await mysql.execute(query)

      query = `
        select 
          ti.tag_val
        from
          oi_feed_tag ft
          join oi_tag_info ti on ft.tag_id = ti.tag_id
        where
          ft.feed_id = ${feedId}
        order by
          ft.tag_seq asc; 
      `
      result = await mysql.execute(query)
      feed.tags = result.map(row => {
        return row.tag_val
      })

    } else {

      feed.tags = []
    }
    
    if (feedContent.length > 0) {

      let linkAllowed = false
      let inviteAllowed = false

      let urls = feedContent.match(urlRegex()) || []

      for (let i = 0; i < urls.length; i++) {
        let url = urls[i]

        if (inviteAllowed && linkAllowed) {
          break
        }

        let parsedUrl = new URL(url)
        console.log(parsedUrl)

        if (parsedUrl.protocol == 'ohing:' && inviteAllowed == false) {
          let paths = parsedUrl.pathname.split('/')
          if (paths.length == 3) {

            let type = paths[1]
            let id = parseInt(paths[2])

            if (type == 'room') {

              let query = `
                select
                  room_id 
                from
                  oi_chat_room_info
                where
                  room_id = ${id} and room_type_cd = 'OPEN';
              `
              let result = await mysql.execute(query)
              if (result.length > 0) {

                query = `
                  insert into oi_feed_chat_room_invite (
                    feed_id, room_id, invite_seq, reg_dt, reg_user_id
                  ) values (
                    ${feedId}, ${id}, 1, now(), ${userId}
                  );
                `
                await mysql.execute(query)
              }
            }
          }
          
          continue
        }

        if (linkAllowed) {
          continue
        }

        let query = `
          select link_id from oi_link_url_info where link_url = '${url}';
        `
        let result = await mysql.execute(query)
        let linkId
        if (result.length == 0) {

          let result = await meta.parser(url)
          console.log(JSON.stringify(result, null, 2))

          let linkType
          switch (parsedUrl.hostname) {
            case 'youtu.be':
            case 'youtube.com':
            case 'www.youtube.com':
              linkType = 'YTUBE'
              break;

            case 'instagram.com':
            case 'www.instagram.com':
              linkType = 'INSTAGRAM'
              break

            case 'facebook.com':
            case 'www.facebook.com':
              linkType = 'FACEBOOK'
              break
          
            default:
              linkType = 'PAGE'
              break;
          }

          let imageUrl = 'null'
          let siteName = 'null'
          let title = 'null'
          let content = 'null'

          if (result.og != null) {

            let og = result.og

            if (og.image != null && og.image.length > 0) {
              imageUrl = `'${og.image}'`
            } else if (og.images.length > 0) {
              let image = og.images[0]
              imageUrl = `'${image.url}'`
            }

            if (og.site_name != null) {
              siteName = `'${og.site_name}'`
            }

            if (og.title != null) {
              title = `'${og.title}'`
            }

            if (og.description != null) {
              content = `'${og.description}'`
            }
          }

          query = `
            insert into oi_link_url_info (
              link_url, link_type_cd, thumbnail_url, site_nm, title, content
            ) values (
              '${url}', '${linkType}', ${imageUrl}, ${siteName}, ${title}, ${content}
            );
          `

          result = await mysql.execute(query)
          linkId = result.insertId
          
        } else {
          linkId = result[0].link_id
        }

        if (linkId != null) {
          let query = `
            insert into oi_feed_link_url_mapping (
              feed_id, link_seq, link_id
            ) values (
              ${feedId}, ${i}, ${linkId}
            );
          `
          await mysql.execute(query)
        }
      }
    }
    
    query = `
      select
        lui.link_id, lui.link_url, lui.link_type_cd link_type, lui.thumbnail_url, lui.site_nm site_name, lui.title, lui.content
      from
        oi_link_url_info lui
        join oi_feed_link_url_mapping lum on lui.link_id = lum.link_id
      where
        lum.feed_id = ${feedId}
      order by 
        lum.link_seq asc
      limit 1;
    `
    result = await mysql.execute(query)

    if (result.length > 0) {
      feed.link = result[0]
    }

    query = `
      select
        cri.room_id, cri.title, cri.room_pwd_use_yn needs_password, cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, 
          ui.user_id, ui.user_login_id account_id,
          ufi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
          rfi.org_file_nm room_image_url, substring_index(fi.org_file_nm, '/', -1) room_file_name
      from
        oi_feed_chat_room_invite fcri
        join oi_chat_room_info cri on fcri.room_id = cri.room_id
          join oi_user_info ui on cri.reg_user_id = ui.user_id
          left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
          left join oi_file_info ufi on upi.file_id = ufi.file_id
          left join oi_file_info rfi on cri.room_image_id = rfi.file_id
      where
        fcri.feed_id = ${feedId}
        and cri.room_type_cd = 'OPEN'
        and cri.room_status_cd = 'ACTIVE'
      limit
        1;
    `
    result = await mysql.execute(query)
    if (result.length > 0) {

      let invite = result[0]
      invite.needs_password = invite.needs_password == 'Y'

      let query = `
        select
          tag_val
        from
          oi_chat_room_tag crt
            join oi_tag_info ti on crt.tag_id = ti.tag_id
        where
          room_id = ${invite.room_id}
        order by 
          crt.tag_seq asc;
      `
      result = await mysql.execute(query)
      invite.tags = result.map(row => {
        return row.tag_val
      })

      feed.invite = invite
    }
    
    await mysql.commit()
    mysql.end()

    return response.result(200, feed)
  
  } catch(e) {

    console.error(e)

    await mysql.rollback()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
