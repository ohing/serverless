'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 피드 조회
 * @method GET viewFeed
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId

    // let parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    const withComments = parameters.withComments == null ? 0 : parseInt(parameters.withComments)

    let query = `
      select
        f.feed_id, f.feed_type_cd feed_type, f.feed_content,
        f.feed_title help_title, f.category_cd help_category,        
        f.anony_yn is_anonymous, f.encoded_yn encoded, f.reg_dt,
        f.feed_except_yn,
        case when f.anony_yn = 'Y' and f.reg_user_id != ${userId} then null else ui.user_id end user_id,
        case when f.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id,
        case when f.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url,
        case when f.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end profile_file_name,
        exists(select user_id from oi_like_log where target_id = ${feedId} and like_target_type_cd = 'FEED' and user_id = ${userId}) liked,
        fpa.like_cnt like_count,
        fpa.comment_cnt comment_count,
        fpa.view_cnt view_count,
        fpa.share_cnt share_count
      from
        oi_feed_info f
        join oi_user_info ui on f.reg_user_id = ui.user_id
        join oi_feed_popul_aggr fpa on f.feed_id = fpa.feed_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where
        f.feed_id = ${feedId}
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE';
    `
    console.log(query)
    let result = await mysql.execute(query)

    console.log(result)

    if (result.length == 0) {
      mysql.end()
      return response.result(404, null, '게시물을 찾을 수 없습니다.')
    }

    let feed = result[0]

    if (feed.feed_except_yn == 'Y') {
      mysql.end()
      return response.result(4444, null, '차단된 게시물입니다.')
    }
    delete feed.feed_except_yn
    feed.liked = feed.liked != 0
    feed.is_anonymous = feed.is_anonymous == 'Y'
    feed.encoded = feed.encoded == 'Y'

    query = `
      select 
        (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id
          where feed_id = fmi.feed_id and media_seq = fmi.media_seq and ui.profile_open_type_cd != 'CLOSE') like_count,
        if ((select count(*) from oi_feed_media_like_log where feed_id = fmi.feed_id and media_seq = fmi.media_seq and user_id = ${userId}) > 0, 'Y', 'N') liked,
        fmi.feed_id, fmi.media_seq,
        fi.media_type_cd type, org_file_nm media_url, substring_index(org_file_nm, '/', -1) media_file_name, width, height, play_time
      from
        oi_feed_media_info fmi
        join oi_file_info fi on fmi.file_id = fi.file_id
      where
        fmi.feed_id = ${feedId};
    `
    result = await mysql.execute(query)
    let medias = result.map(row => {
      row.liked = row.liked == 'Y'
      return row
    })
    if (medias.length > 0) {
      feed.medias = medias
    } else {
      feed.medias = []
    }

    query = `
      select 
        ti.tag_val
      from
        oi_feed_tag ft
        join oi_tag_info ti on ft.tag_id = ti.tag_id
      where
        ft.feed_id = ${feedId}
      order by
        ft.tag_seq asc; 
    `
    result = await mysql.execute(query)
    feed.tags = result.map(row => {
      return row.tag_val
    })

    query = `
      select
        lui.link_id, lui.link_url, lui.link_type_cd link_type, lui.thumbnail_url, lui.site_nm site_name, lui.title, lui.content
      from
        oi_link_url_info lui
          join oi_feed_link_url_mapping lum on lui.link_id = lum.link_id
      where
        lum.feed_id = ${feedId}
      order by 
        lum.link_seq asc
      limit 1;
    `
    result = await mysql.execute(query)
    if (result.length > 0) {
      feed.link = result[0]
    }

    query = `
      select
        cri.room_id, cri.title, cri.room_pwd_use_yn needs_password, cri.max_user_cnt max_user_count, cri.join_user_cnt join_user_count, 
          ui.user_id, ui.user_login_id account_id,
          ufi.org_file_nm profile_image_url, substring_index(ufi.org_file_nm, '/', -1) profile_file_name, 
          rfi.org_file_nm room_image_url, substring_index(rfi.org_file_nm, '/', -1) room_file_name
      from
        oi_feed_chat_room_invite fcri
        join oi_chat_room_info cri on fcri.room_id = cri.room_id
        join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info ufi on upi.file_id = ufi.file_id
        left join oi_file_info rfi on cri.room_image_id = rfi.file_id
      where
        fcri.feed_id = ${feedId}
        and cri.room_type_cd = 'OPEN'
        and cri.room_status_cd = 'ACTIVE'
      limit
        1; 
    `
    console.log(query)
    result = await mysql.execute(query)
    if (result.length > 0) {

      let invite = result[0]
      invite.needs_password = invite.needs_password == 'Y'

      let query = `
        select
          tag_val
        from
          oi_chat_room_tag crt
            join oi_tag_info ti on crt.tag_id = ti.tag_id
        where
          room_id = ${invite.room_id}
        order by 
          crt.tag_seq asc;
      `
      result = await mysql.execute(query)
      invite.tags = result.map(row => {
        return row.tag_val
      })

      feed.invite = invite
    }

    if (userId != feed.reg_user_id) {
      query = `
        insert into oi_feed_view_log (
          feed_id, user_id, view_dt
        ) values (
          ${feedId}, ${userId}, now()
        ) on duplicate key update
          view_dt = now();
      `
      await mysql.execute(query)

      query = `
        update
          oi_feed_popul_aggr
        set
          view_cnt = (
            select 
              count(*)
            from 
              oi_feed_view_log
            where
              feed_id = ${feedId}
          )
        where
          feed_id = ${feedId};
      `
      await mysql.execute(query)
    }

    query = `
      select
        count(*) count
      from 
        oi_feed_view_log
      where
        feed_id = ${feedId};
    `
    result = await mysql.execute(query)
    feed.view_count = result[0].count

    if (withComments == 1) {

      let query = `
        select
          (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id
            where like_target_type_cd = 'COMT' and target_id = fc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
          if ((select count(*) from oi_like_log where like_target_type_cd = 'COMT' and target_id = fc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
          fc.comment_id, fc.del_yn is_deleted, fc.content, fc.anony_yn is_anonymous, fc.reg_dt,
          case when fc.del_yn = 'Y' or (fc.anony_yn ='Y' and fc.reg_user_id != ${userId}) then null else ui.user_id end user_id,
          case 
            when fc.del_yn = 'Y' then ''
            else case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end
          end account_id,
          case 
            when fc.del_yn = 'Y' then null
            else case when fc.anony_yn = 'Y' then null else fi.org_file_nm end
          end profile_image_url,
          case 
            when fc.del_yn = 'Y' then null
            else case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end
          end profile_file_name
        from 
          oi_feed_comment fc
          left join oi_user_info ui on fc.reg_user_id = ui.user_id
          left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
          left join oi_file_info fi on pi.file_id = fi.file_id
        where
          fc.feed_id = ${feedId}
          and ui.profile_open_type_cd != 'CLOSE'
          and fc.parent_comment_id is null
        order by
          fc.comment_id desc
        limit 
          15;
      `
      console.log(query)
      let comments = await mysql.execute(query)
      let previousCount = 0

      if (comments.length > 0) {

        comments = comments.map(comment => {
          comment.replies = []
          comment.previous_reply_count = 0
          comment.liked = comment.liked == 'Y'
          comment.is_anonymous = comment.is_anonymous == 'Y'
          comment.is_deleted = comment.is_deleted == 'Y'
          return comment
        })
    
        comments.reverse()
    
        console.log(comments)
    
        query = `
          select
            count(*) count
          from
            oi_feed_comment fc
            join oi_user_info ui on fc.reg_user_id = ui.user_id
          where
            fc.feed_id = ${feedId}
            and fc.parent_comment_id is null
            and fc.comment_id < ${comments[0].comment_id}
            and ui.user_status_cd = 'ACTIVE' 
            and ui.profile_open_type_cd != 'CLOSE';
        `
        let result = await mysql.execute(query)
        if (result.length > 0) {
          previousCount = result[0].count
    
          let commentIds = comments.map(comment => {
            return comment.comment_id
          })
          let commentIdString = commentIds.join(',')
          
          query = `
            select 
              fc.parent_comment_id, count(*) count 
            from 
              oi_feed_comment fc
              join oi_user_info ui on fc.reg_user_id = ui.user_id
            where 
              fc.parent_comment_id in (${commentIdString}) 
              and ui.user_status_cd = 'ACTIVE' 
              and ui.profile_open_type_cd != 'CLOSE'
            group by 
              fc.parent_comment_id;
          `
          let countResult = await mysql.execute(query)

          countResult.forEach(row => {
            for (let i = 0; i < comments.length; i++) {
              if (comments[i].comment_id == row.parent_comment_id) {
                comments[i].previous_reply_count = row.count
                break
              }
            }
          })

          console.log(comments)
    
          query = `
            select
              (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id
                where like_target_type_cd = 'COMT' and target_id = fc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
              if ((select count(*) from oi_like_log where like_target_type_cd = 'COMT' and target_id = fc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
              fc.comment_id, 'N' is_deleted, fc.parent_comment_id parent_id, fc.content, fc.anony_yn is_anonymous, fc.reg_dt,
              case when fc.anony_yn = 'Y' and fc.reg_user_id != ${userId} then null else ui.user_id end user_id,
              case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id,
              case when fc.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url,
              case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end profile_file_name
            from
              oi_feed_comment fc
              join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.profile_open_type_cd != 'CLOSE'
              left join (
          select fc.comment_id, fc.parent_comment_id, fc.reg_user_id,
          ui.profile_open_type_cd
          from oi_feed_comment fc
          join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.profile_open_type_cd != 'CLOSE'
        ) fc2 on fc.parent_comment_id = fc2.parent_comment_id and fc.comment_id < fc2.comment_id
              left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
              left join oi_file_info fi on pi.file_id = fi.file_id
            where 
              fc.parent_comment_id in (${commentIdString})
            group by 
              fc.parent_comment_id, fc.comment_id
            having 
              count(fc.comment_id) < 3
            order by
              fc.parent_comment_id, fc.comment_id desc
            ;
          `
          result = await mysql.execute(query)
    
          result.forEach(row => {
            row.liked = row.liked == 'Y'
            row.is_anonymous = row.is_anonymous == 'Y'
            row.is_deleted = row.is_deleted == 'Y'
            for (let i = 0; i < comments.length; i++) {
              if (comments[i].comment_id == row.parent_id && comments[i].replies.length < 3) {
                comments[i].replies.unshift(row)
                comments[i].previous_reply_count--
                break
              }
            }
          })
        }

        feed.previous_comment_count = previousCount
        feed.comments = comments
      } else {
        feed.comments = []
      }
    } else {
      feed.comments = []
    }

    mysql.end()

    let readable = await redis.readable(3)
    let onlineKey = `alive:${feed.reg_user_id}`
    feed.is_online = await readable.get(onlineKey) == "true"

    var onlines = {}

    if (feed.comments.length > 0) {
      feed.comments.forEach(comment => {
        onlines[comment.user_id] = false
        comment.replies.forEach(reply => {
          onlines[reply.user_id] = false
        })
      })
  
      keys = Object.keys(onlines)
      for (let i = 0; i < keys.length; i++) {
        let key = keys[i]
        let onlineKey = `alive:${key}`
        let isOnline = await readable.get(onlineKey)
        onlines[key] = isOnline == "true" || isOnline == true
      }
  
      for (let i = 0; i < feed.comments.length; i++) {
        let comment = feed.comments[i]
        feed.comments[i].is_online = onlines[comment.user_id]
        for (let j = 0; j < comment.replies.length; j++) {
          let reply = comment.replies[j]
          feed.comments[i].replies[j].is_online = onlines[reply.user_id]
        }
      }
    } else {
      delete feed.comments
    }

    redis.end()

    console.log(feed)

    return response.result(200, feed)
  
  } catch(e) {

    console.error(e)    
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
