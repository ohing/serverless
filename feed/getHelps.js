'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const search = require(`@ohing/ohingsearch`)
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse');

/**
 * @author Karl <karl@ohing.net> 
 * @description 도와줘 목록
 * @method GET getHelps
 * @returns  
*/

module.exports = async event => {

  try {

    let userId = event.userId
    let parameters = event.parameters
    
    let order = parameters.order || 'Recent'
    let rowCount = parameters.rowCount == null ? 20 : parseInt(parameters.rowCount)
    let helpCategoryCode = (parameters.helpCategoryCode || '').trim()

    let lastId = parameters.lastId

    let searchText = (parameters.searchText || '').trim()
    let searchInfo = search.parse(searchText)
    let searchCondition = search.searchCondition(searchInfo, 'HELP')

    let query
    let result

    query = `
      update 
        oi_user_info
      set
        upd_dt = now()
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    let categoryCode = helpCategoryCode.length == 0 ? 'ALL' : helpCategoryCode

    let categoryCondition = helpCategoryCode.length == 0 ? '' : `
      and f.category_cd = '${helpCategoryCode}'
    `

    let redisKey = (order == 'Popular' || order == 'View') ? `${order.toLowerCase()}:${userId}:${categoryCode.toLowerCase()}` : null
    let writable = (order == 'Popular' || order == 'View') ? await redis.writable(6) : null

    if (redisKey != null && lastId == null) {

      let countSelect = order == 'Popular' ? `
        z.like_count+z.comment_count popular
      ` : `
        z.view_count
      `

      let orderSelect = order == 'Popular' ? `
        fpa.like_cnt like_count, fpa.comment_cnt comment_count
      ` : `
        fpa.view_cnt view_count
      `

      let orderCondition = order == 'Popular' ? `
        popular desc,
      ` : `
        view_count desc,
      `
      
      query = `
        select 
          y.feed_id, @rownum:=@rownum+1
        from
          (SELECT @rownum:=0) r,
          (select 
            z.feed_id, ${countSelect}
          from 
            (select
              f.feed_id,
              f.encoded_yn encoded,
              ${orderSelect}
            from
              oi_feed_info f
              join oi_user_info ui on f.reg_user_id = ui.user_id
              join oi_feed_popul_aggr fpa on f.feed_id = fpa.feed_id
              left join oi_user_relation ur on ui.user_id = ur.user_id and ur.relation_user_id = ${userId} and ur.relation_type_cd = '02'
              left join oi_user_relation ur2 on ui.user_id = ur2.relation_user_id and ur.user_id = ${userId} and ur.relation_type_cd = '02'
            where
              f.feed_type_cd = 'HELP'
              ${categoryCondition}
              and f.encoded_yn = 'Y'
              and f.feed_except_yn = 'N'
              and ui.user_status_cd = 'ACTIVE'
              and ui.profile_open_type_cd != 'CLOSE'
              and ur.user_id is null 
              and ur2.user_id is null
            group by 
              f.feed_id
            ) z
          where
            1=1
          order by
            ${orderCondition}
            z.feed_id desc
          limit 
            300
          ) y;
      `

      console.log(query)
      result = await mysql.execute(query)

      let feedIds = [0,[]]

      for (let row of result) {
        let lastIndex = feedIds.length-1 
        if (feedIds[lastIndex].length < 30) {
          feedIds[lastIndex].push(row.feed_id)
        } else {
          feedIds.push(feedIds[lastIndex][29])
          feedIds.push([row.feed_id])
        }
      }

      await writable.hmset(redisKey, feedIds)
    }

    switch (order) {
      case 'Recent': {

        let idCondition = lastId == null ? '' : `and f.feed_id < ${lastId}`
        query = `
          select
            f.feed_id, f.feed_type_cd feed_type, f.category_cd help_category, f.feed_title help_title, f.feed_content, f.anony_yn is_anonymous, f.reg_dt,
            f.encoded_yn encoded,
            case when f.anony_yn = 'Y' and f.reg_user_id != ${userId} then null else ui.user_id end user_id,
            case when f.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id, 
            (select count(*) from oi_like_log where target_id = f.feed_id and user_id = ${userId} and like_target_type_cd = 'FEED') liked,
            fpa.like_cnt like_count,
            fpa.comment_cnt comment_count,
            fpa.view_cnt view_count,
            fpa.share_cnt share_count
          from
            oi_feed_info f
            join oi_user_info ui on f.reg_user_id = ui.user_id
            join oi_feed_popul_aggr fpa on f.feed_id = fpa.feed_id
            left join oi_user_school_history sh on ui.user_id = sh.user_id
            left join oi_feed_tag ft on f.feed_id = ft.feed_id
            left join oi_tag_info ti on ft.tag_id = ti.tag_id
          where
            f.feed_type_cd = 'HELP'
            -- and (f.encoded_yn = 'Y' or f.reg_user_id = ${userId})
            and f.encoded_yn = 'Y'
            and f.feed_except_yn = 'N'
            and ui.user_status_cd = 'ACTIVE'
            and ui.profile_open_type_cd != 'CLOSE'
            ${searchCondition}
            ${idCondition}
            ${categoryCondition}
          group by 
            f.feed_id
          
          order by
            f.feed_id desc
          limit 
            ${rowCount}
        `
        console.log(query)
        result = await mysql.execute(query)
        
        break
      }

      case 'Popular': {

        lastId = lastId || 0

        let feedIds = await writable.hget(redisKey, lastId)
        redis.end()

        query = `

          select 
            f.feed_id, f.feed_type_cd feed_type, f.category_cd help_category, f.feed_title help_title, f.feed_content, 
            f.anony_yn is_anonymous, f.reg_dt, f.encoded_yn encoded,
            case when f.anony_yn = 'Y' and f.reg_user_id != ${userId} then null else ui.user_id end user_id,
            case when f.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id, 
            (select count(*) from oi_like_log where target_id = f.feed_id and user_id = ${userId} and like_target_type_cd = 'FEED' limit 1) liked,
            fpa.like_cnt like_count,
            fpa.comment_cnt comment_count,
            fpa.like_cnt + fpa.comment_cnt popular,
            fpa.view_cnt view_count,
            fpa.share_cnt share_count
          from 
            oi_feed_info f
            join oi_user_info ui on f.reg_user_id = ui.user_id
            join oi_feed_popul_aggr fpa on f.feed_id = fpa.feed_id
          where
            f.feed_id in (${feedIds})
            ${searchCondition}
          order by 
            field(f.feed_id, ${feedIds});
        `
        console.log(query)
        result = await mysql.execute(query)

        break
      }

      case 'View': {

        lastId = lastId || 0

        let feedIds = await writable.hget(redisKey, lastId)
        redis.end()

        query = `
          select 
            f.feed_id, f.feed_type_cd feed_type, f.category_cd help_category, f.feed_title help_title, f.feed_content, 
            f.anony_yn is_anonymous, f.reg_dt, f.encoded_yn encoded,
            case when f.anony_yn = 'Y' and f.reg_user_id != ${userId} then null else ui.user_id end user_id,
            case when f.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id, 
            (select count(*) from oi_like_log where target_id = f.feed_id and user_id = ${userId} and like_target_type_cd = 'FEED' limit 1) liked,
            fpa.like_cnt like_count,
            fpa.comment_cnt comment_count,
            fpa.view_cnt view_count,
            fpa.share_cnt share_count
          from 
            oi_feed_info f
            join oi_user_info ui on f.reg_user_id = ui.user_id
            join oi_feed_popul_aggr fpa on f.feed_id = fpa.feed_id
          where
            f.feed_id in (${feedIds})
            ${searchCondition}
          order by 
            field(f.feed_id, ${feedIds});
        `
        console.log(query)
        result = await mysql.execute(query)

        break
      }

      default: {
        return response.result(400)
      }
    }

    if (result.length == 0) {
      mysql.end()
      return response.result(200, [])
    }

    let feedIds = []
    let feedIndexMap = {}

    let index = 0

    let helps = result.map(row => {

      row.is_anonymous = row.is_anonymous == 'Y'
      row.encoded = row.encoded == 'Y'
      row.liked = row.liked != 0

      row.medias = []

      feedIds.push(row.feed_id)
      feedIndexMap[row.feed_id] = index

      index++

      return row
    })

    console.log(feedIndexMap)

    query = `
      select 
        (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id
          where feed_id = fmi.feed_id and media_seq = fmi.media_seq and ui.profile_open_type_cd != 'CLOSE') like_count,
        (select count(*) from oi_feed_media_like_log where feed_id = fmi.feed_id and media_seq = fmi.media_seq and user_id = ${userId}) liked,
        fmi.feed_id, fmi.media_seq,
        fi.media_type_cd type, fi.org_file_nm media_url, substring_index(fi.org_file_nm, '/', -1) media_file_name, fi.width, fi.height, fi.play_time
      from
        oi_feed_media_info fmi
          join oi_file_info fi on fmi.file_id = fi.file_id
      where
        fmi.feed_id in (${feedIds.join(',')})
      order by
        fmi.feed_id asc, fmi.media_seq asc;
    `
    console.log(query)
    result = await mysql.execute(query)

    for (let media of result) {

      media.liked = media.liked > 0

      let feedId = media.feed_id

      let index = feedIndexMap[feedId]
      if (index != null) {
        helps[index].medias.push(media)
      }
    }

    mysql.end()

    return response.result(200, helps)
  
  } catch(e) {

    console.error(e)    
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
