'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 대상 유저 피드+도와줘 목록
 * @method GET getTargetAllFeeds
 * @returns  
*/

module.exports = async event => {

  try {

    let userId = event.userId
    let parameters = event.parameters

    let targetUserId = parameters.targetUserId
    if (targetUserId == null) {
      targetUserId = userId
    }

    let defaultCondition = targetUserId == '0' ? `
      f.anony_yn = 'Y' and f.encoded_yn = 'Y'
    ` : `
      (${targetUserId == userId ? `f.reg_user_id = ${targetUserId}` : `f.reg_user_id = ${targetUserId} and f.anony_yn = 'N'`}) and f.encoded_yn = 'Y'
    `

    let rowCount = parameters.rowCount == null ? 20 : parseInt(parameters.rowCount)
    let startId = parameters.startId
    let firstId = parameters.firstId
    let lastId = parameters.lastId
    let idCondition = ''

    if (startId != null) {
      idCondition = `and f.feed_id <= ${startId}`
    } else {
      if (firstId != null) {
        idCondition = `and f.feed_id > ${firstId}`
      } else if (lastId != null) {
        idCondition = `and f.feed_id < ${lastId}`
      }
    }

    let readable = await redis.readable(3)

    let onlineKey = `alive:${targetUserId}`
    let isOnline = await readable.get(onlineKey) || false

    let query = `
      select
        f.feed_id, f.feed_type_cd feed_type, f.category_cd help_category, f.feed_title help_title, 
        f.feed_content, f.anony_yn is_anonymous, f.encoded_yn encoded, f.reg_dt,
        case when f.anony_yn = 'Y' and f.reg_user_id != ${userId} then null else ui.user_id end user_id,
        case when f.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id,
        case when f.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url,
        case when f.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end profile_file_name,
        (select count(*) from oi_like_log where target_id = f.feed_id and user_id = ${userId} and like_target_type_cd = 'FEED') liked,
        fpa.like_cnt like_count,
        fpa.comment_cnt comment_count,
        fpa.view_cnt view_count,
        fpa.share_cnt share_count,
        lui.link_id, lui.link_url, lui.link_type_cd link_type, lui.thumbnail_url, lui.site_nm site_name, lui.title link_title, lui.content link_content,
        cri.room_id, cri.title room_title, cri.room_pwd_use_yn room_needs_password, 
        cri.max_user_cnt room_max_user_count, cri.join_user_cnt room_join_user_count, 
        
        case 
          when cui.chat_profile_id is null then ui2.user_login_id 
          else cpi.nick_nm 
        end room_master_account_id,
        case 
          when cui.chat_profile_id is null then ufi.org_file_nm
          else fi2.org_file_nm
        end room_master_profile_image_url,
        case 
          when cui.chat_profile_id is null then substring_index(ufi.org_file_nm, '/', -1)
          else substring_index(fi2.org_file_nm, '/', -1)
        end room_master_profile_file_name,
        
        rfi.org_file_nm room_image_url, substring_index(rfi.org_file_nm, '/', -1) room_file_name
      from
        oi_feed_info f
        join oi_user_info ui on f.reg_user_id = ui.user_id
        join oi_feed_popul_aggr fpa on f.feed_id = fpa.feed_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_feed_link_url_mapping flum on f.feed_id = flum.feed_id and flum.link_seq = 1
        left join oi_link_url_info lui on flum.link_id = lui.link_id
        left join oi_feed_chat_room_invite fcri on fcri.feed_id = f.feed_id
        left join oi_chat_room_info cri on fcri.room_id = cri.room_id
        left join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'
        left join oi_user_info ui2 on cui.user_id = ui2.user_id

        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
        
        left join oi_user_profile_image upi2 on ui2.user_id = upi2.user_id and upi2.image_type_cd = 'PF'
        left join oi_file_info ufi on upi2.file_id = ufi.file_id
        left join oi_file_info rfi on cri.room_image_id = rfi.file_id
      where
        ${defaultCondition}
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        ${idCondition}
      group by
        feed_id
      order by
        f.feed_id desc
      limit 
        ${rowCount};
    `
    console.log('aaa', query)
    let result = await mysql.execute(query)

    let feeds = []

    if (result.length == 0) {
      mysql.end()
      return response.result(200, feeds)
    }

    let feedIds = []
    let feedMap = {}

    for (let row of result) {
      feedMap[row.feed_id] = row
      feedIds.push(row.feed_id)
    }
    
    for (let row of result) {

      row.is_anonymous = row.is_anonymous == 'Y'
      row.encoded = row.encoded == 'Y'
      row.liked = row.liked != 0

      query = `
        select 
          ti.tag_val
        from 
          oi_feed_tag ft
          join oi_tag_info ti on ft.tag_id = ti.tag_id
        where
          ft.feed_id = ${row.feed_id}
        order by
          ft.tag_seq asc;
      `
      result = await mysql.execute(query)
      row.tags = result.map(row => {
        return row.tag_val
      })

      if (row.link_url != null) {
        row.link = {
          link_id: row.link_id,
          link_url: row.link_url,
          link_type: row.link_type,
          thumbnail_url: row.thumbnail_url,
          site_name: row.site_name,
          title: row.link_title,
          content: row.link_content
        }
      }

      delete row.link_id
      delete row.link_url
      delete row.link_type
      delete row.thumbnail_url
      delete row.site_name
      delete row.link_title
      delete row.link_content

      if (row.room_id != null) {

        query = `
          select 
            ti.tag_val
          from 
            oi_chat_room_tag crt
              join oi_tag_info ti on crt.tag_id = ti.tag_id
          where
            room_id = ${row.room_id}
          order by
            crt.tag_seq asc;
        `
        let tagResult = await mysql.execute(query)
        let tags = tagResult.map(tagRow => {
          return tagRow.tag_val
        })

        row.invite = {
          room_id: row.room_id,
          title: row.room_title,
          needs_password: row.room_needs_password == 'Y',
          max_user_count: row.room_max_user_count,
          join_user_count: row.room_join_user_count,
          user_id: row.room_master_user_id,
          account_id: row.room_master_account_id,
          profile_image_url: row.room_master_profile_image_url,
          profile_file_name: row.room_master_profile_file_name,
          room_image_url: row.room_image_url, 
          room_file_name: row.room_file_name,
          tags: tags
        }
      }

      delete row.room_id
      delete row.room_title
      delete row.room_needs_password
      delete row.room_max_user_count
      delete row.room_join_user_count
      delete row.room_master_user_id
      delete row.room_master_account_id
      delete row.room_master_profile_image_url
      delete row.room_master_profile_file_name
      delete row.room_image_url
      delete row.room_file_name

      row.comments = []
      row.medias = []
      
      feeds.push(row)
    }

    query = `
      select 
        (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id
          where feed_id = fmi.feed_id and media_seq = fmi.media_seq and ui.profile_open_type_cd != 'CLOSE') like_count,
        (select count(*) from oi_feed_media_like_log where feed_id = fmi.feed_id and media_seq = fmi.media_seq and user_id = ${userId}) liked,
        fmi.feed_id, fmi.media_seq,
        fi.media_type_cd type, fi.org_file_nm media_url, substring_index(fi.org_file_nm, '/', -1) media_file_name, fi.width, fi.height, fi.play_time
      from
        oi_feed_media_info fmi
        join oi_file_info fi on fmi.file_id = fi.file_id
      where
        fmi.feed_id in (${feedIds.join(',')})
      order by
        fmi.feed_id asc, fmi.media_seq asc;
    `
    console.log(query)
    result = await mysql.execute(query)

    for (let media of result) {

      media.liked = media.liked > 0

      let feedId = media.feed_id

      // console.log('media', media, feedId, feedMap[feedId] != null)

      if (feedMap[feedId] != null) {
        feedMap[feedId].medias.push(media)
      }
    }

    query = `
      select 
        fc.feed_id, fc.comment_id, fc.content, fc.anony_yn is_anonymous, fc.reg_dt, 
        ui.user_id t_user_id,
        case 
          when fc.del_yn = 'Y' then null
          else ui.user_id
        end user_id,
        case 
          when fc.del_yn = 'Y' then ''
          else case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end
        end account_id,
        case 
          when fc.del_yn = 'Y' then null
          else case when fc.anony_yn = 'Y' then null else fi.org_file_nm end
        end profile_image_url,
        case 
          when fc.del_yn = 'Y' then null
          else case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end
        end profile_file_name,
        fcr.comment_id r_comment_id, fcr.content r_content, fcr.anony_yn r_is_anonymous, fcr.reg_dt r_reg_dt,
        uir.user_id tr_user_id,
        case when fcr.anony_yn = 'Y' then null else uir.user_id end r_user_id,
        case when fcr.anony_yn = 'Y' then '익명사용자' else uir.user_login_id end r_account_id,
        case when fcr.anony_yn = 'Y' then null else fir.org_file_nm end r_profile_image_url,
        case when fcr.anony_yn = 'Y' then null else substring_index(fir.org_file_nm, '/', -1) end r_profile_file_name
      from 
        oi_feed_comment fc
        join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.profile_open_type_cd != 'CLOSE'
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_feed_comment fcr on fcr.comment_id = (select max(comment_id) from oi_feed_comment where parent_comment_id = fc.comment_id)
        left join oi_user_info uir on fcr.reg_user_id = uir.user_id and uir.profile_open_type_cd != 'CLOSE'
        left join oi_user_profile_image upir on uir.user_id = upir.user_id and upir.image_type_cd = 'PF'
        left join oi_file_info fir on upir.file_id = fir.file_id
      where
        fc.comment_id in (
          select
            fc.comment_id
          from
            oi_feed_comment fc
            join oi_user_info fcui on fc.reg_user_id = fcui.user_id and fcui.user_status_cd = 'ACTIVE' and fcui.profile_open_type_cd != 'CLOSE'
            left join oi_feed_comment fc2 on fc.feed_id = fc2.feed_id and fc.comment_id < fc2.comment_id and fc2.parent_comment_id is null
          where
            fc.feed_id in (${feedIds.join(',')})
            and fc.del_yn = 'N'
            and fc.parent_comment_id is null
            and fc2.feed_id is null
        )
        and fc.parent_comment_id is null;
    `
    console.log(query)
    result = await mysql.execute(query)

    for (let i = 0; i < result.length; i++) {

      let commentInfo = result[i]
      let feedId = commentInfo.feed_id

      if (feedMap[feedId] != null) {

        let onlineKey = `alive:${commentInfo.t_user_id}`
        let isOnline = await readable.get(onlineKey) || false

        let comment = {
          feed_id: commentInfo.feed_id,
          comment_id: commentInfo.comment_id,
          content: commentInfo.content,
          is_anonymous: commentInfo.is_anonymous == 'Y',
          is_deleted: false,
          reg_dt: commentInfo.reg_dt,
          user_id: commentInfo.user_id, 
          account_id: commentInfo.account_id,
          profile_image_url: commentInfo.profile_image_url,
          profile_file_name: commentInfo.profile_file_name,
          is_online: isOnline,
          replies: []
        }
        
        if (commentInfo.r_comment_id != null) {

          let onlineKey = `alive:${commentInfo.tr_user_id}`
          let isOnline = await readable.get(onlineKey) || false

          comment.replies.push({
            feed_id: commentInfo.feed_id,
            comment_id: commentInfo.r_comment_id,
            parent_id: commentInfo.comment_id,
            content: commentInfo.r_content,
            is_anonymous: commentInfo.r_is_anonymous == 'Y',
            is_deleted: false,
            reg_dt: commentInfo.r_reg_dt,
            user_id: commentInfo.r_user_id, 
            account_id: commentInfo.r_account_id,
            profile_image_url: commentInfo.r_profile_image_url,
            profile_file_name: commentInfo.r_profile_file_name,
            is_online: isOnline
          })
        }

        feedMap[feedId].comments.push(comment)
      } 
    }

    redis.end()
    mysql.end()

    feeds = Object.values(feedMap)
    feeds.sort((lhs, rhs) => {
      return lhs.feed_id < rhs.feed_id ? 1 : lhs.feed_id > rhs.feed_id ? -1 : 0
    })

    return response.result(200, feeds)
  
  } catch(e) {

    console.error(e)    
    redis.end()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
