'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()


/**
 * @author Karl <karl@ohing.net> 
 * @description 피드 삭제
 * @method DELETE deleteFeed
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 글을 삭제하실 수 없습니다.')
    }

    query = `
      select 
        feed_type_cd
      from
        oi_feed_info
      where
        feed_id = ${feedId};
    `
    result = await mysql.execute(query)
    let feedType = result[0].feed_type_cd

    await mysql.beginTransaction()

    query = `
      delete from
        oi_feed_info 
      where
        feed_id = ${feedId};
    `
    await mysql.execute(query)
    
    query = `
      select
        file_id
      from
        oi_feed_media_info
      where
        feed_id = ${feedId};
    `
    result = await mysql.execute(query)

    if (result.length > 0) {

      query = `
        delete from
          oi_feed_media_info
        where
          feed_id = ${feedId};
      `
      await mysql.execute(query)

      let fileIds = result.map(row => {
        return row.file_id
      })
      let fileIdsString = fileIds.join(',')

      query = `
        select
          org_file_nm
        from 
          oi_file_info
        where
          file_id in (${fileIdsString});
      `
      result = await mysql.execute(query)
      let keys = result.map(row => {
        return row.org_file_nm
      })

      // let params = {
      //   FunctionName: `api-new-batch-${process.env.stage}-deleteS3Objects`,
      //   InvocationType: "Event",
      //   Payload: JSON.stringify({
      //     body: JSON.stringify({
      //       bucket: process.env.mediaBucket,
      //       keys: keys
      //     })
      //   })
      // }
      // await lambda.invoke(params).promise()

      query = `
        delete from
          oi_file_info
        where
          file_id in (${fileIdsString});
      `
      await mysql.execute(query)
    }

    query = `
      delete from
        oi_feed_tag
      where
        feed_id = ${feedId};
    `
    await mysql.execute(query)

    query = `
      delete from
        oi_feed_chat_room_invite
      where
        feed_id = ${feedId};
    `
    await mysql.execute(query)

    query = `
      delete from
        oi_feed_link_url_mapping
      where
        feed_id = ${feedId};
    `
    await mysql.execute(query)

    await mysql.commit()
    mysql.end()

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: -2,
          type: 'DELETEFEED',
          actionId: feedId,
          log: '게시글 삭제'
        })
      })
    }
    console.log(logParams)

    await lambda.invoke(logParams).promise()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)

    await mysql.rollback()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
