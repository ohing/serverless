'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 답글 목록
 * @method get getReplies
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId
    let parentId = event.pathParameters.commentId
    
    const parameters = event.queryStringParameters || {}
    
    const rowCount = parameters.rowCount || 3
    let firstId = parameters.firstId || 2147483647
    
    let query = `
      select
        (select count(*) from oi_like_log ll join oi_user_info lui on ll.user_id = lui.user_id and lui.user_status_cd = 'ACTIVE' and lui.profile_open_type_cd != 'CLOSE'
          where like_target_type_cd = 'COMT' and target_id = fc.comment_id and lui.profile_open_type_cd != 'CLOSE') like_count,
        if ((select count(*) from oi_like_log where like_target_type_cd = 'COMT' and target_id = fc.comment_id and user_id = ${userId}) > 0, 'Y', 'N') liked,
        fc.comment_id, fc.parent_comment_id parent_id, fc.content, fc.anony_yn is_anonymous, fc.reg_dt,
        case when fc.anony_yn = 'Y' then null else ui.user_id end user_id,
        case when fc.anony_yn = 'Y' then '익명사용자' else ui.user_login_id end account_id,
        case when fc.anony_yn = 'Y' then null else fi.org_file_nm end profile_image_url,
        case when fc.anony_yn = 'Y' then null else substring_index(fi.org_file_nm, '/', -1) end profile_file_name 
      from 
        oi_feed_comment fc
        join oi_user_info ui on fc.reg_user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where
        fc.feed_id = ${feedId}
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and fc.parent_comment_id = ${parentId}
        and comment_id < ${firstId}
      order by
        fc.comment_id desc
      limit 
        ${rowCount};
    `
    let comments = await mysql.execute(query)
    comments = comments.map(comment => {
      comment.replies = []
      comment.replyCount = 0
      comment.liked = comment.liked == 'Y'
      comment.is_anonymous = comment.is_anonymous == 'Y'
      return comment
    })

    if (comments.length == 0) {
      mysql.end()
      return response.result(200, {previous_count: 0, replies: []})
    }

    comments.reverse()

    query = `
      select
        count(*) count
      from
        oi_feed_comment fc
        join oi_user_info ui on fc.reg_user_id = ui.user_id
      where
        feed_id = ${feedId}
        and parent_comment_id = ${parentId}
        and comment_id < ${comments[0].comment_id}
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE';
    `
    let result = await mysql.execute(query)
    let previousCount = 0
    if (result.length > 0) {
      previousCount = result[0].count
    }
    
    mysql.end()

    let readable = await redis.readable(3)
    
    var onlines = {}

    comments.forEach(comment => {
      onlines[comment.user_id] = false
    })

    let keys = Object.keys(onlines)
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i]
      let onlineKey = `alive:${key}`
      let isOnline = await readable.get(onlineKey)
      onlines[key] = isOnline == "true" || isOnline == true
    }

    for (let i = 0; i < comments.length; i++) {
      let comment = comments[i]
      comments[i].is_online = onlines[comment.user_id]
    }

    redis.end()

    return response.result(200, {previous_count: previousCount, replies: comments})
  
  } catch(e) {

    console.error(e)

    await mysql.rollback()
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
