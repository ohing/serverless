'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const urlRegex = require('url-regex');
const meta = require('html-metadata-parser');
const response = require('@ohing/ohingresponse')

const { URL } = require('url');

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()


/**
 * @author Karl <karl@ohing.net> 
 * @description 피드 수정
 * @method PATCH modifyFeed
 * @returns  
*/

module.exports.handler = async event => {
  
  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let feedId = event.pathParameters.feedId
    
    const parameters = JSON.parse(event.body || '{}') || {}

    let medias = parameters.medias
    let feedContent = parameters.feedContent

    if ((medias || []).length == 0 && (feedContent || '').trim().length == 0) {
      return response.result(400)
    }

    let query
    let result
    let valueParameters

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 글을 수정하실 수 없습니다.')
    }

    query = `
      select
        feed_title, feed_type_cd, feed_content, category_cd, anony_yn, encoded_yn, feed_except_yn
      from
        oi_feed_info
      where
        feed_id = ${feedId};
    `
    result = await mysql.execute(query)

    if (result.length == 0) {
      return response.result(404, null, '대상을 찾을 수 없습니다.')
    }

    if (result[0].feed_except_yn == 'Y') {
      return response.result(4444, null, '차단된 게시물입니다.')
    }

    if (result[0].encoded_yn == 'N') {
      return response.result(403, null, '인코딩 완료 후 수정할 수 있습니다.')
    }

    let feedType = result[0].feed_type_cd

    let sets = []

    if (feedType == 'HELP') {
      let helpTitle = parameters.helpTitle
      if (helpTitle != null && helpTitle.length > 0 && result[0].feed_title != helpTitle) {
        helpTitle = await mysql.escape(helpTitle)
        sets.push(`feed_title = ${helpTitle}`)
      }
      let helpCategoryCode = parameters.helpCategoryCode
      if (helpCategoryCode != null && helpCategoryCode.length > 0 && result[0].category_cd != helpCategoryCode) {
        sets.push(`category_cd = '${helpCategoryCode}'`)
      }
      let isAnonymous = parameters.isAnonymous
      if (isAnonymous != null && typeof isAnonymous == 'boolean' && result[0].anony_yn) {
        sets.push(`anony_yn = '${isAnonymous ? 'Y' : 'N'}'`)
      }
    }

    await mysql.beginTransaction()

    let invite = null

    if (feedContent != null) {
      feedContent = feedContent.trim()

      if (feedContent != result[0].feed_content) {
        feedContent = await mysql.escape(feedContent)
        sets.push(`feed_content = ${feedContent}`)
      }

      query = `
        delete from 
          oi_feed_link_url_mapping
        where
          feed_id = ${feedId};
      `
      await mysql.execute(query)
      
      query = `
        delete from 
          oi_feed_chat_room_invite
        where
          feed_id = ${feedId};
      `      
      await mysql.execute(query)

      if (feedContent.length > 0) {

        let linkAllowed = false
        let inviteAllowed = false

        let feedContentText = feedContent.replace(/\\n/g, ' ').replace(/\n/g, ' ')

        let urls = feedContentText.match(urlRegex()) || []
        console.log(urls)

        for (let i = 0; i < urls.length; i++) {
          let url = urls[i]
  
          if (inviteAllowed && linkAllowed) {
            break
          }
  
          let parsedUrl = new URL(url)
          console.log(parsedUrl)
  
          if (parsedUrl.protocol == 'ohing:' && inviteAllowed == false) {
            let paths = parsedUrl.pathname.split('/')

            if (paths.length == 4 && paths[3].startsWith('EOL')) {

              let type = paths[1]
              let id = parseInt(paths[2])
  
              if (type == 'room') {
  
                query = `
                  select
                    cri.room_id, cri.title,
                    case
                      when length(cri.room_pwd) is null or length(cri.room_pwd) = 0 then 'N'
                      else 'Y'
                    end needs_password,
                    max_user_cnt max_user_count, join_user_cnt join_user_count,
                    cui.user_id, ui.user_login_id account_id,
                    fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
                    fi2.org_file_nm room_image_url, substring_index(fi2.org_file_nm, '/', -1) room_file_name
                  from
                    oi_chat_room_info cri
                    join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.master_yn = 'Y'                  
                    join oi_user_info ui on cui.user_id = ui.user_id
                    left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
                    left join oi_file_info fi on upi.file_id = fi.file_id
                    left join oi_file_info fi2 on cri.room_image_id = fi2.file_id
                  where
                    cri.room_id = ${id} and cri.room_type_cd = 'OPEN';
                `
                result = await mysql.execute(query)

                console.log('1111')
                if (result.length > 0) {

                  console.log('2222')
  
                  let roomInfo = result[0]

                  query = `
                    insert into oi_feed_chat_room_invite (
                      feed_id, room_id, invite_seq, reg_dt, reg_user_id
                    ) values (
                      ${feedId}, ${id}, 1, now(), ${userId}
                    );
                  ` 

                  await mysql.execute(query)
                  query = `
                    select 
                      ti.tag_val
                    from 
                      oi_chat_room_tag crt
                          join oi_tag_info ti on crt.tag_id = ti.tag_id
                    where
                      crt.room_id = ${id}
                    order by 
                      crt.tag_seq asc;              
                  `
                  result = await mysql.execute(query)
                  roomInfo.tags = result.map(row => {
                    return row.tag_val
                  })
                  invite = roomInfo 

                  await mysql.execute(query)
                  inviteAllowed = true
                }
              }
            }
            
            continue
          }
  
          if (linkAllowed) {
            continue
          }
  
          query = `
            select link_id from oi_link_url_info where link_url = ?;
          `
          console.log(query)
          result = await mysql.execute(query, [url])
          let linkId
          if (result.length == 0) {

            let isTiktok = false

            let linkType
            switch (parsedUrl.hostname) {
              case 'youtu.be':
              case 'youtube.com':
              case 'www.youtube.com':
                linkType = 'YTUBE'
                break;

              case 'instagram.com':
              case 'www.instagram.com':
                linkType = 'INSTAGRAM'
                break

              case 'facebook.com':
              case 'www.facebook.com':
                linkType = 'FACEBOOK'
                break

              case 'vt.tiktok.com':
              case 'www.tiktok.com':
              case 'tiktok.com':
                linkType = 'TIKTOK'
                isTiktok = true
                break
            
              default:
                linkType = 'PAGE'
                break;
            }

            if (isTiktok) {
              // result = await module.tiktokMeta(url)
              continue
            } else {
              try {
                result = await meta.parser(url)
              } catch (e) {
                console.log(e)
              }
            }

            if (result) {
              let imageUrl = null
              let siteName = null
              let title = null
              let content = null

              if (result.og != null) {

                let og = result.og

                if (og.image != null && og.image.length > 0) {
                  imageUrl = og.image
                } else if (og.images.length > 0) {
                  let image = og.images[0]
                  imageUrl = image.url
                }

                if (og.site_name != null) {
                  siteName = og.site_name
                }

                if (og.title != null) {
                  title = og.title
                }

                if (og.description != null) {
                  content = og.description
                }
              }

              if ((imageUrl || '').length == 0) {
                continue
              }

              if (siteName == null) {
                siteName = parsedUrl.hostname
              }

              query = `
                insert into oi_link_url_info (
                  link_url, link_type_cd, thumbnail_url, site_nm, title, content
                ) values (
                  ?, ?, ?, ?, ?, ?
                );
              `

              valueParameters = [url, linkType, imageUrl, siteName, title, content]

              result = await mysql.execute(query, valueParameters)
              linkId = result.insertId
            }
            
          } else {
            linkId = result[0].link_id
          }

          if (linkId != null) {
            query = `
              insert into oi_feed_link_url_mapping (
                feed_id, link_seq, link_id
              ) values (
                ${feedId}, 1, ${linkId}
              );
            `
            await mysql.execute(query)

            linkAllowed = true
          }
        }
      }
    }

    if (sets.length > 0) {
      let setsString = sets.join(', ')
      query = `
        update 
          oi_feed_info
        set
          ${setsString}
        where
          feed_id = ${feedId};
      `
      await mysql.execute(query) 
    }

    if (medias != null) {

      let fileIds = []

      if (medias.length > 0) {        

        let needsEncoding = false
        
        for (let i = 0; i < medias.length; i++) {

          let media = medias[i]
          
          if (media.mediaSeq == null && (media.type == null || !(media.type == 'IMAGE' || media.type == 'AGIF' || media.type == 'MOVIE') || media.key == null)) {
            await mysql.rollback()
            mysql.end()
            return response.result(400)
          }

          let seq = media.mediaSeq

          if (seq != null) {
            
            query = `
              select 
                file_id
              from
                oi_feed_media_info
              where
                feed_id = ${feedId}
                and media_seq = ${seq};
            `
            result = await mysql.execute(query)
            if (result.length > 0) {
              fileIds.push(result[0].file_id)
            }

          } else {

            if (media.type == 'MOVIE') {
              needsEncoding = true
            }

            let width = media.width || 600
            let height = media.height || 600
            let playTime = media.playTime || '00:00'

            let mediaKey = media.key
            let finalKey = mediaKey

            if (media.type == 'MOVIE') {
              if (!mediaKey.includes('/') && mediaKey.includes('.')) {
                let fileNames = mediaKey.split('.')
                let fileName = fileNames[0]
                let fileExtension = fileNames[1]
                finalKey = `mov/${fileName}/mp4/${fileName}.${fileExtension}`
              } else if (mediaKey.includes('source')) {
                let paths = mediaKey.split('/')
                let fileNames = paths[paths.length-1].split('.')
                let fileName = fileNames[0]
                let fileExtension = fileNames[1]
                finalKey = `mov/${fileName}/mp4/${fileName}.${fileExtension}`
              }
            } else {
              if (!mediaKey.includes('/') && mediaKey.includes('.')) {
                let fileNames = mediaKey.split('.')
                let fileName = fileNames[0]
                let fileExtension = fileNames[1]
                finalKey = `img/${fileName}/${fileName}.${fileExtension}`
              }
            }

            query = `
              insert into oi_file_info (
                media_type_cd, org_file_nm, width, height, play_time, del_yn, encoded_yn
              ) values (
                '${media.type}', '${finalKey}', ${width}, ${height}, '${playTime}', 'N', '${media.type == 'MOVIE' ? 'N': 'Y'}'
              );
            `
            result = await mysql.execute(query)
            fileIds.push(result.insertId)
          }
        }

        if (needsEncoding) {
          sets.push(`encoded_yn = 'Y'`)
        }
      }

      query = `
        delete from
          oi_feed_media_info
        where
          feed_id = ${feedId};
      `
      await mysql.execute(query)

      console.log(fileIds)

      if (fileIds.length > 0) {

        let values = []
        for (let i = 0; i < fileIds.length; i++) {
          let fileId = fileIds[i]
          values.push(`( ${feedId}, ${i+1}, ${fileId}, ${userId} )`)
        }
        let valuesString = values.join(', ')

        query = `
          insert into oi_feed_media_info (
            feed_id, media_seq, file_id, reg_user_id
          ) values 
            ${valuesString}
        `
        await mysql.execute(query)
      }
    }

    let tags = parameters.tags

    if (tags != null) {

      query = `
        delete from
          oi_feed_tag
        where
          feed_id = ${feedId};
      `
      await mysql.execute(query)

      if (tags.length > 0) {

        let tagValues = []
        let trimmedValues = []

        for (let tag of tags) {
          let trimmed = tag.trim()
          if (trimmed.length > 0) {
            let escaped = await mysql.escape(trimmed)
            tagValues.push(`( ${escaped}, 1 )`)
            trimmedValues.push(`${escaped}`)
          }
        }

        let valuesString = tagValues.join(',')

        query = `
          insert into oi_tag_info (
            tag_val, tag_cnt
          ) values
            ${valuesString}
          on duplicate key update
            tag_cnt = values(tag_cnt) + 1;
        `

        await mysql.execute(query)

        query = `
          select
            tag_id, tag_val
          from 
            oi_tag_info
          where
            tag_val in (${trimmedValues.join(',')});
        `
        result = await mysql.execute(query)

        let tagInfos = tags.map(tag => {
          return {tag_val: tag}
        })
        for (let i = 0; i < tagInfos.length; i++) {
          let filtered = result.filter(row => {
            return row.tag_val == tagInfos[i].tag_val
          })
          tagInfos[i].tag_id = filtered[0].tag_id
        }
        
        let tagIds = tagInfos.map(tagInfo => {
          return tagInfo.tag_id
        })
        
        let seq = 0
        let values = tagIds.map(tagId => { 
          seq++
          return `( ${feedId}, ${tagId}, ${seq} )`
        })
        valuesString = values.join(',')
        
        query = `
          insert into oi_feed_tag (
            feed_id, tag_id, tag_seq
          ) values 
          ${valuesString};
        `
        await mysql.execute(query)

        query = `
          select 
            ti.tag_val
          from
            oi_feed_tag ft
            join oi_tag_info ti on ft.tag_id = ti.tag_id
          where
            ft.feed_id = ${feedId}
          order by
            ft.tag_seq asc; 
        `
        await mysql.execute(query)
      }
    }

    query = `
      update
        oi_feed_popul_aggr
      set
        like_cnt = (
          select              
            (select count(*) from oi_like_log ll join oi_user_info ui on ll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where ll.target_id = ${feedId} and ll.like_target_type_cd = 'FEED') +
            (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fmll.feed_id = ${feedId}) count
        )
      where
        feed_id = ${feedId};
    `
    console.log(query)
    await mysql.execute(query)
    
    await mysql.commit()
    mysql.end()
    
    let params = {
      FunctionName: `api-new-feed-${process.env.stage}-viewFeed`,
      Payload: JSON.stringify({
        headers: event.headers,
        pathParameters: event.pathParameters
      })
    }
    result = await lambda.invoke(params).promise()

    console.log(result.Payload)

    let payload = JSON.parse(result.Payload)
    let feed = JSON.parse(payload.body)

    feed.is_online = true
    feed.invite = invite

    return response.result(200, feed)
  
  } catch(e) {

    console.error(e)

    await mysql.rollback()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
