'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description event push test
 * @method POST event
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}
    let title = parameters.title
    let body = parameters.body

    if (body == null) {
      return response.result(400)
    }

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          messages: [
            {
              type: 'Event',
              dataType: 'Event',
              topic: 'OHINGNOTICEIOS',
              title: title,
              body: body
            },
            {
              type: 'Event',
              dataType: 'Event',
              topic: 'OHINGNOTICEANDROID',
              title: title,
              body: body
            },
            {
              type: 'Event',
              dataType: 'Event',
              topic: 'notification-ios',
              title: title,
              body: body
            },
            {
              type: 'Event',
              dataType: 'Event',
              topic: 'notification-android',
              title: title,
              body: body
            }
          ]
        })
      })
    }
    console.log(params)

    await lambda.invoke(params).promise()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}