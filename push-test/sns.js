'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()


/**
 * @author Karl <karl@ohing.net> 
 * @description sns push test
 * @method POST sns
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}
    let targetUserId = parameters.targetUserId
    let userId = parameters.userId
    let type = parameters.type
    let dataType = parameters.dataType

    if (targetUserId == null || userId == null || type == null || type == dataType) {
      return response.result(400)
    }

    let feedId = parameters.feedId
    let content = parameters.content
    let tags = parameters.tags
    let schoolName = parameters.schoolName

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          targetUserIds: [targetUserId],
          messages: [
            {
              userId: userId,
              type: type,
              dataType: dataType,
              feedId: feedId,
              content: content,
              tags: tags,
              schoolName: schoolName
            }
          ]
        })
      })
    }
    console.log(params)

    await lambda.invoke(params).promise()
    
    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}