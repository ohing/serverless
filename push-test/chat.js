'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const camelcase = require('camelcase-keys')
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description chat push test
 * @method POST chat
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let parameters = JSON.parse(event.body || '{}') || {}
    let type = parameters.type
    let roomId = parameters.roomId
    let messageId = parameters.messageId

    if (type == null || roomId == null || messageId == null) {
      console.log('Here?')
      return response.result(400)
    }

    let targetUserId = parameters.targetUserId

    let query = `
      select 
        cm.msg_id message_id, cm.msg_type_cd type, cm.room_id, cm.send_user_id user_id, cm.send_user_nick_nm nickname, cm.send_profile_image_url profile_image_url,
        cpi.chat_profile_id,
        cm.send_dt_int timestamp, cm.msg_content text, fi3.org_file_nm media_key,
        cm.target_user_id, cm.target_user_nick_nm target_user_nickname,
        cm.invite_room_id
      from
        oi_chat_msg cm
        left join oi_user_info ui on cm.send_user_id = ui.user_id
        left join oi_chat_profile_info cpi on cm.send_user_id = cpi.user_id
        left join oi_file_info fi3 on cm.file_id = fi3.file_id
      where
        msg_id = ${messageId};
    `
    let result = await mysql.execute(query)
    mysql.end()

    if (result.length == 0) {
      return response.result(404)
    }
    let message = result[0]

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendChatMessage`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          type: 'SendMessage',
          roomId: roomId,
          targetUserId: targetUserId,
          chatMessage: camelcase(message, {deep:true})
        })
      })
    }
    console.log(params)

    await lambda.invoke(params).promise()
    
    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}