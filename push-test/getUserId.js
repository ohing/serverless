'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 푸시 테스트에 사용할 userId 가져오기
 * @method GET getUserId
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    let paramters = event.queryStringParameters || {}
    let accountId = paramters.accountId

    let query = `
      select 
        user_id
      from
        oi_user_info
      where
        user_login_id = '${accountId}'; 
    `
    let result = await mysql.execute(query)
    
    if (result.length == 0) {
      return response.result(404)
    }

    mysql.end()

    return response.result(200, result[0])
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}