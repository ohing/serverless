#!/bin/sh

fullscreen_size=$( osascript -e 'tell application "Finder" to get bounds of window of desktop' )
echo $fullscreen_size
sizes=$(echo $fullscreen_size | tr "," "\n")
i=0

topMargin=24

for size in $sizes
do
  if [ ${i} -eq 2 ]; then 
  width=$(($size / 4))
  elif [ ${i} -eq 3 ]; then
  fullHeight=$(( $size - $topMargin ))
  height=$(( $fullHeight / 4 ))
  fi
  i=$(($i + 1))
done

osascript -e 'tell app "Terminal"
  activate
  do script "cd /Users/karl/Documents/Serverless/account && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/batch && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/chat/friend && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/chat/message && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/chat/profile && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/chat/room && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/common && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/feed && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/friend && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/rank && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/search && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/setting && sls deploy && exit"
  do script "cd /Users/karl/Documents/Serverless/user && sls deploy && exit"
end tell'

osascript -e "tell app \"Terminal\"
  set the bounds of the window 13 to {0, $topMargin, $width, $(($topMargin + $height))}
  set the bounds of the window 12 to {$width, $topMargin, $(($width * 2)), $(($topMargin + $height))}
  set the bounds of the window 11 to {$(($width * 2)), $topMargin, $(($width * 3)), $(($topMargin + $height))}
  set the bounds of the window 10 to {$(($width * 3)), $topMargin, $(($width * 4)), $(($topMargin + $height))}
  set the bounds of the window 9 to {0, $(($topMargin + $height)), $width, $(($topMargin + $height * 2))}
  set the bounds of the window 8 to {$width, $(($topMargin + $height)), $(($width * 2)), $(($topMargin + $height * 2))}
  set the bounds of the window 7 to {$(($width * 2)), $(($topMargin + $height)), $(($width * 3)), $(($topMargin + $height * 2))}
  set the bounds of the window 6 to {$(($width * 3)), $(($topMargin + $height)), $(($width * 4)), $(($topMargin + $height * 2))}
  set the bounds of the window 5 to {0, $(($topMargin + $height * 2)), $width, $(($topMargin + $height * 3))}
  set the bounds of the window 4 to {$width, $(($topMargin + $height * 2)), $(($width * 2)), $(($topMargin + $height * 3))}
  set the bounds of the window 3 to {$(($width * 2)), $(($topMargin + $height * 2)), $(($width * 3)), $(($topMargin + $height * 3))}
  set the bounds of the window 2 to {$(($width * 3)), $(($topMargin + $height * 2)), $(($width * 4)), $(($topMargin + $height * 3))}
  set the bounds of the window 1 to {0, $(($topMargin + $height * 3)), $width, $(($topMargin + $height * 4))}
end tell"