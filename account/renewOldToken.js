'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`);
const response = require(`@ohing/ohingresponse`);

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 과거버전 토큰 갱신 
 * @method POST renewOldToken
*/
module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400);
    }

    const parameters = JSON.parse(event.body || '{}');
    const oldToken = parameters.oldToken;

    if (oldToken == null) {
      return response.result(400);
    }

    let query;
    let result;

    query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname, ui.user_status_cd, 
        ui.user_email email, ui.user_hp_no phone_number,
        case 
          when pi.user_id is null then 'img/notification-default/notification-default.png'
          else fi.org_file_nm
        end profile_image_url,
          case 
          when pi.user_id is null then 'notification-default.png'
          else substring_index(fi.org_file_nm, '/', -1)
        end profile_file_name
      from 
        oi_user_info ui
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where 
        ui.login_auth_no = ?
        and ui.user_status_cd = 'ACTIVE';
    `;
    console.log(query);
    result = await mysql.execute(query, [oldToken]);

    if (result.length == 0) {
      mysql.end();
      return response.result(404, null);
    }

    let userInfo = result[0];

    const userId = userInfo.user_id;

    query = `
      select  
        ti.tag_id, ti.tag_val
      from 
        oi_tag_info ti
        join oi_user_tag ut on ti.tag_id = ut.tag_id
      where
        ut.user_id = ${userId}
      order by
        ut.tag_seq asc;
    `;
    result = await mysql.execute(query);

    userInfo.tags = result.map(row => {
      return row.tag_val;
    });

    let tagIds = result.map(row => {
      return row.tag_id;
    });

    query = `
      select 
        school_hist_cd, school_id, school_nm, school_short_nm, grade, class classe
      from
        oi_user_school_history
      where
        user_id = ${userId}
      order by
        field(school_hist_cd, 'CURR') desc;
    `;
    result = await mysql.execute(query);
    result = result.map(row => {
      row.is_current = row.school_hist_cd == 'CURR';
      delete row.school_hist_cd;
      return row;
    });

    userInfo.schools = result;

    const accessToken = authorizer.createOldToken();

    let fcmSteps = [];

    query = `
      select
        push_auth_key, login_yn
      from
        oi_user_device_info
      where
        user_id = ${userId} 
        and push_auth_key is not null and push_auth_key  <> ''
        ;
    `;
    result = await mysql.execute(query);

    const tokens = result.map(row => { return row.push_auth_key; });
    const loginTokens = result.filter(row => { return row.login_yn = 'Y'; });

    fcmSteps.push({
      type: 'unsubscribe',
      tokens: tokens,
      topics: []
    });

    if (loginTokens.length > 0) {

      let fcmToken = loginTokens[0].push_auth_key;

      let platformTopic = osType == 'iOS' ? 'notification-ios' : 'notification-android';

      const tags = tagIds.map(tagId => { return `tag-${tagId}`; });

      query = `
        select 
          ri.room_id
        from
          oi_chat_room_info ri 
          join oi_chat_user_info ui on ri.room_id = ui.room_id
        where
          ui.user_id = ${userId} and ri.room_type_cd = 'OPEN' and ui.alarm_yn = 'Y';
      `;
      result = await mysql.execute(query);
      const chattingRooms = result.map(element => { return `openchat-${element.room_id}`; });

      let topics = ['notification', platformTopic].concat(tags).concat(chattingRooms);

      fcmSteps.push({
        type: 'subscribe',
        tokens: [fcmToken],
        topics: topics
      });
    }

    if (fcmSteps.length > 0) {

      await lambda.invoke({
        FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            steps: fcmSteps
          })
        })
      }).promise();
    }

    query = `
      delete from
        oi_user_device_info
      where
        user_id = ${userId} and imei != '${imei}';
    `;
    await mysql.execute(query);
    await authorizer.setAccessToken(accessToken, userInfo.user_id);

    mysql.end();
    redis.end();

    userInfo.access_token = accessToken;
    return response.result(200, userInfo);

  } catch (e) {

    console.error(e);

    mysql.end();
    redis.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
