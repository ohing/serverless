'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()
const eventBridge = new AWS.EventBridge();


/**
 * @author Karl <karl@ohing.net> 
 * @description 리뷰작성확인
 * @method POST review
*/
module.exports.handler = async event => {

  console.log(event)
  
  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query = `
      update 
        oi_user_info
      set
        review_yn = 'Y'
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    return response.result(200, {})

  } catch(e) {

    console.error(e)

    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
