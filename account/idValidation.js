'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 아이디 유효성 및 가입여부 체크
 * @method POST idValidation
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body || '{}')
    let id = parameters.id
    if (id == null) {
      return response.result(400)
    }

    id = id.trim()

    if (id.length < 5 || id.length > 12) {
      return response.result(200, {isValid: false}, '아이디는 5~12자로 입력해 주세요.')
    }

    const isValid = id.match(/^[_a-z0-9.]*$/) != null

    if (!isValid) {
      return response.result(200, {isValid: false}, '아이디는 영문소문자, 숫자, 특수문자중 언더바(_)와 마침표(.)만 사용가능합니다.')
    }

    let query = `
      select
        user_id
      from
        oi_user_info
      where
        user_login_id = '${id}'
      limit
        1
    `

    let result = await mysql.execute(query)

    mysql.end()

    if (result.length > 0) {
      return response.result(200, {isValid: false}, '이미 사용중인 아이디입니다.')
    } else {
      return response.result(200, {isValid: true}, '사용 가능한 아이디입니다.')
    }

    mysql.end()

    return response.result(200, result)

  } catch(e) {

    console.error(e)

    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
