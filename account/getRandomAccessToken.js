'use strict';

const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description 로그인된 회원 중 아무나 토큰 가져오기
 * @method GET getRandomAccessToken
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    let readable = await redis.readable(3)

    let key = null
    let nextCursor = '0'

    while (key == null) {

      let matches = await readable.scan(nextCursor, 'match', 'user-access-token*', 'count', '100')
      if (matches[1].length == 0) {
        if (matches[0] == '0') {
          break
        }
        nextCursor = matches[0]
        continue
      }

      key = matches[1][0]
      break
    }

    let token = await readable.get(key)
    redis.end()

    return response.result(200, {access_token: token})

  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
