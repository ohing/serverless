'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`);

/**
 * @author Karl <karl@ohing.net> 
 * @description 비밀번호찾기 코드 체크
 * @method POST findIdValidation
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400)
    }

    const parameters = JSON.parse(event.body || '{}')
    let accountId = parameters.accountId
    let code = parameters.code
    if (accountId == null || code == null) {
      return response.result(400)
    }

    let query
    
    query = `
      select user_id, user_login_id account_id, auth_no from oi_user_info where user_login_id = '${accountId}';
    `

    let result = await mysql.execute(query)
    if (result.length == 0) {
      return response.result(404, null, '회원 정보를 찾을 수 없습니다.')
    }
    
    if (code != result[0].auth_no) {
      return response.result(403, null, '인증번호가 일치하지 않습니다.')
    }
    
    mysql.end()

    return response.result(200, {account_id: accountId})

  } catch(e) {

    console.error(e)

    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
