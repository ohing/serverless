'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require(`@ohing/ohingresponse`);

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();
// const eventBridge = new AWS.EventBridge();


/**
 * @author Karl <karl@ohing.net> 
 * @description 로그아웃
 * @method POST signOut
*/
module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    const parameters = JSON.parse((event.body || '{}')) || {};
    const reason = parameters.reason || '';

    let query = `
      select
        push_auth_key
      from
        oi_user_device_info
      where
        user_id = ${userId} 
        and push_auth_key is not null and push_auth_key  <> '';
    `;
    let result = await mysql.execute(query);

    const tokens = result.map(row => { return row.push_auth_key; });

    await lambda.invoke({
      FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          steps: [
            {
              type: 'unsubscribe',
              tokens: tokens,
              topics: []
            }
          ]
        })
      })
    }).promise();

    query = `
      delete from
        oi_user_device_info
      where
        user_id = ${userId};
    `;
    await mysql.execute(query);

    await authorizer.deleteAccessToken(accessToken);

    query = `
      update
        oi_user_info
      set
        user_status_cd = 'WDSTANDBY', exp_reason_content = '${reason}'
      where
        user_id = ${userId};
    `;
    await mysql.execute(query);

    mysql.end();

    let params = {
      FunctionName: `api-new-account-${process.env.stage}-backupForQuit`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId
        })
      })
    };
    await lambda.invoke(params).promise();

    // let date = new Date()
    // let tomorrow = new Date(date.getTime() + (process.env.stage == 'dev' ? 600000 : 172800000))
    // let cron = `${tomorrow.getUTCMinutes()} ${tomorrow.getUTCHours()} ${tomorrow.getUTCDate()} ${tomorrow.getUTCMonth()+1} ? ${tomorrow.getUTCFullYear()}`
    // let ruleName = `Quit-Process-${userId}`

    // params = {
    //   Name: ruleName,
    //   ScheduleExpression: `cron(${cron})`,
    //   RoleArn: 'arn:aws:iam::862667126176:role/Serverless'
    // }

    // result = await eventBridge.putRule(params).promise()
    // let ruleArn = result.RuleArn

    // params = {
    //   Rule: ruleName,
    //   Targets: [
    //     {
    //       Id: ruleName,
    //       Arn: `arn:aws:lambda:ap-northeast-2:862667126176:function:api-new-account-${process.env.stage}-deleteForQuit`,
    //       Input: JSON.stringify({
    //         pathParameters: {
    //           userId: userId
    //         }
    //       })
    //     }
    //   ]
    // }

    // await eventBridge.putTargets(params).promise()

    // params = {
    //   Action: 'lambda:InvokeFunction', 
    //   FunctionName: `api-new-account-${process.env.stage}-deleteForQuit`,
    //   Principal: 'events.amazonaws.com',
    //   SourceArn: ruleArn, 
    //   StatementId: ruleName
    // }

    // await lambda.addPermission(params).promise()

    return response.result(200, {}, '탈퇴 처리가 완료되었습니다. 다음에 또 볼 수 있기를...');

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
