'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require(`@ohing/ohingresponse`);

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 회원가입
 * @method PUT updateFCMToken
*/
module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const parameters = JSON.parse(event.body || '{}');
    const fcmToken = parameters.fcmToken;

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    let query = `
      select
        push_auth_key
      from
        oi_user_device_info
      where
        user_id = ${userId} 
        and push_auth_key is not null and push_auth_key  <> '';

    `;
    let result = await mysql.execute(query);
    const tokens = result.map(row => { return row.push_auth_key; });

    let fcmSteps = [];
    fcmSteps.push({
      type: 'unsubscribe',
      tokens: tokens,
      topics: []
    });

    if (fcmToken != null) {

      if (!tokens.includes(fcmToken)) {
        fcmSteps.push({
          type: 'unsubscribe',
          tokens: [fcmToken],
          topics: []
        });
      }

      query = `
        insert into oi_user_device_info (
          imei, user_id, os_type_cd, app_ver, push_auth_key, login_yn
        ) values (
          '${imei}', ${userId}, '${osType.toUpperCase()}', '${appVersion}', '${fcmToken}', 'Y'
        ) on duplicate key update 
          user_id = ${userId}, os_type_cd = '${osType.toUpperCase()}', app_ver = '${appVersion}', push_auth_key = '${fcmToken}', login_yn = 'Y', upd_dt = now();
      `;
      await mysql.execute(query);

      query = `
        update 
          oi_user_device_info
        set 
          push_auth_key = null, upd_dt = now()
        where
          push_auth_key = '${fcmToken}' and imei != '${imei}'
      `;
      await mysql.execute(query);

      let platformTopic = osType == 'iOS' ? 'notification-ios' : 'notification-android';

      query = `
        select
          ti.tag_id
        from
          oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
        where 
          ut.user_id = ${userId};
      `;
      result = await mysql.execute(query);
      const tags = result.map(element => { return `tag-${element.tag_id}`; });

      query = `
        select 
          ri.room_id
        from
          oi_chat_room_info ri join oi_chat_user_info ui on ri.room_id = ui.room_id
        where
          ui.user_id = ${userId} and ri.room_type_cd = 'OPEN' and ui.alarm_yn = 'Y';
      `;
      result = await mysql.execute(query);
      const chattingRooms = result.map(element => { return `openchat-${element.room_id}`; });

      let topics = ['notification', platformTopic].concat(tags).concat(chattingRooms);

      fcmSteps.push({
        type: 'subscribe',
        tokens: [fcmToken],
        topics: topics
      });

    } else {

      query = `
        update
          oi_user_device_info
        set
          push_auth_key = null
        where
          imei = '${imei}';
      `;
    }

    mysql.end();

    if (fcmSteps.length > 0) {
      await lambda.invoke({
        FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            steps: fcmSteps
          })
        })
      }).promise();
    }

    return response.result(200, {});

  } catch (e) {

    console.error(e);
    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
