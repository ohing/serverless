'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const response = require(`@ohing/ohingresponse`);

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 찾기를 통한 비밀번호 변경
 * @method POST changePassword
*/
module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400);
    }

    const parameters = JSON.parse(event.body || '{}');
    let accountId = parameters.accountId;
    // let email = parameters.email
    // let phoneNumber = parameters.phoneNumber
    let code = parameters.code;
    const password = parameters.password;
    if (accountId == null || code == null || password == null) {
      return response.result(400);
    }

    let query = `
      select user_id, user_login_id account_id, auth_no from oi_user_info where user_login_id = '${accountId}' limit 1;
    `;

    // if (email != null) {
    //   query = 
    // } else {
    //   phoneNumber = phoneNumber.replace(/-|\s/g,'')
    //   query = `
    //     select user_id, user_login_id account_id, auth_no from oi_user_info where user_hp_no = '${phoneNumber}' limit 1;
    //   `
    // }

    let result = await mysql.execute(query);
    if (result.length == 0) {
      return response.result(404);
    }

    if (code != result[0].auth_no) {
      mysql.end();
      return response.result(403, null, '인증번호가 일치하지 않습니다.');
    }

    let userId = result[0].user_id;

    let passwordHash = authorizer.createOldPasswordHash(password);

    query = `
      update
        oi_user_info
      set 
        user_pwd = '${passwordHash}', auth_no = null
      where
        user_id = ${userId};
    `;
    await mysql.execute(query);

    query = `
      select
        push_auth_key
      from
        oi_user_device_info
      where
        user_id = ${userId} and login_yn = 'Y'
        and push_auth_key is not null and push_auth_key  <> '';
    `;
    result = await mysql.execute(query);
    if (result.length > 0) {
      const fcmToken = result[0].push_auth_key;

      await lambda.invoke({
        FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            steps: [
              {
                type: 'unsubscribe',
                tokens: [fcmToken],
                topics: []
              }
            ]
          })
        })
      }).promise();
    }

    query = `
      update
        oi_user_device_info
      set
        push_auth_key = null, login_yn = 'N'
      where
        user_id = ${userId};
    `;
    await mysql.execute(query);

    mysql.end();

    authorizer.deleteAccessTokenFrom(userId);

    return response.result(200, { accountId: accountId });

  } catch (e) {

    console.error(e);

    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
