'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const sms = require(`@ohing/ohingsms-${process.env.stage}`)
const response = require(`@ohing/ohingresponse`);

const fs = require('fs')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const ses = new AWS.SES({apiVersion: '2010-12-01'})

/**
 * @author Karl <karl@ohing.net> 
 * @description 비밀번호 찾기
 * @method POST findPassword
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400)
    }

    const parameters = JSON.parse(event.body || '{}')
    let accountId = parameters.accountId
    let method = parameters.method
    if (accountId == null || method == null) {
      return response.result(400)
    }

    let query = `
      select 
        user_id, user_email, user_hp_no
      from
        oi_user_info
      where
        user_login_id = '${accountId}'
      limit 1;
    `
    
    let result = await mysql.execute(query)
    if (result.length == 0) {
      return response.result(404)
    }
    const userId = result[0].user_id
    const email = result[0].user_email
    const phoneNumber = result[0].user_hp_no

    const authNumber = Math.floor(Math.random() * (1000000 - 100000)) + 100000;

    query = `
      update 
        oi_user_info
      set 
        auth_no = '${authNumber}'
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    mysql.end()

    if (method == 'email') {
      let subject = '[오잉]인증번호가 발송되었습니다.'
      let bodyBuffer = fs.readFileSync('authEmail.html')
      let body = bodyBuffer.toString()
      body = body.replace('authNo', authNumber)

      let params = {
        Destination: {
          ToAddresses: [
            email
          ]
        },
        Message: {
          Body: {
            Html: {
              Charset: 'UTF-8',
              Data: body
            }
          },
          Subject: {
            Charset: 'UTF-8',
            Data: subject
          }
        },
        Source: 'service@ohing.net'
      }

      await ses.sendEmail(params).promise()

    } else {
      let message = `[오잉]인증번호 [${authNumber}]를 입력하세요.`
      await sms.send(phoneNumber, message)
    } 

    return response.result(200, {authNumber: authNumber.toString()})

  } catch(e) {

    console.error(e)

    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
