'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 본인인증 체크포인트. 본인인증 시작 후 취소한 건수 등을 체크하기 위해 만듦.
 * @method PUT authCheckPoint
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei

    if (imei == null) {
      return response.result(400)
    }

    const parameters = JSON.parse(event.body || '{}')

    let authType = (parameters.authType || '').trim()
    let txId = (parameters.txId || '').trim()

    if (authType.length == 0 || txId.length == 0) {
      return response.result(400)
    }

    let query
    let result
    
    query = `
      insert into oi_user_self_auth_info (
        imei, auth_type_cd, comm_cd, di
      ) values (
        '${imei}', 'self', '${authType}', '${txId}'
      );
    `
    await mysql.execute(query)

    mysql.end()
    
    return response.result(200, {})

  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
