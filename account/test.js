module.idValidation = async () => {
  const request = {
    headers: {
      'os-type': 'iOS'
    },
    body: JSON.stringify({
      id: 'hotpotato'
    })
  }
  return await require('./idValidation.js').handler(request)
}

module.saveAuthInfo = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F'
    },
    body: JSON.stringify({"imei":"1234","virtualNumber":"8516260999123","name":"신용","ageCode":"7","genderCode":"1","birthday":"19851201","nationalInfo":"","strRequestNumber":"FW13_1628048305379","di":"MC0GCCqGSIb3DQIJAyEAXKTbn9QXxb5W6dKXp2gC1Ibe87bE1gijrx8WDBSb0vE=","ci":"IcqLY+B2PsP5USOF5yLq7tTqhympSwz/jRd6cuBO6qJxI9y/jkqc47bN4C7vgSqJq22LU871gV2Pyx900AYcVA==","ciUpdate":"1","authType":"IPIN"})
  }
  return await require('./saveAuthInfo.js').handler(request)
}

module.getAuthId = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '1234'
    }
  }
  return await require('./getAuthId.js').handler(request)
}

module.signUp = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F'
    },
    body: JSON.stringify({
      "authId": 255,
      "accountId": "karl98",
      "password": "user1234",
      "nickname": "karl99",
      "email": "karl@ohing.net",
      "regionCode": "100278",
      "tags": ["공산당","싫어요"],
      "schoolName": "이것은학교",
      "grade": "3",
      "agreements": [
        {
          "termsId": 1,
          "agreed": true
        },
        {
          "termsId": 2,
          "agreed": true
        },
        {
          "termsId": 3,
          "agreed": true
        },
        {
          "termsId": 4,
          "agreed": true
        },
        {
          "termsId": 5,
          "agreed": true
        },{
          "termsId": 6,
          "agreed": true
        }
      ]
    })
  }
  return await require('./signUp.js').handler(request)
}

module.signIn = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F'
    },
    body: JSON.stringify({
      accountId: 'karl02',
      password: 'ch082323!',
    })
  }
  return await require('./signIn.js').handler(request)
}

module.updateFCMToken = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'd301e246-3d6c-4833-8a94-eb1701458d4e'
    },
    body: JSON.stringify({
      fcmToken: 'f55RuFsIhUxekJHZSupY6j:APA91bHxUEPFUXIkdScyGaVHRTogfoHggoVnKS_t2b2hD95WCaooEAOjnzyKgKLYIja6m9Qz5EINbjHhW4AdMO-Jx3gBt3Ixrw3yWX9xGSOQCO-gKiCkwRTAoEg8x99X2KOPPON8-8IO'
    })
  }
  return await require('./updateFCMToken.js').handler(request)
}

module.findId = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F'
    },
    body: JSON.stringify({
      phoneNumber: '010-2910-7431'
    })
  }
  return await require('./findId.js').handler(request)
}

module.findPasswordAccount = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F'
    },
    body: JSON.stringify({
      accountId: ''
    })
  }
  return await require('./findPasswordAccount.js').handler(request)
}

module.changePassword = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F'
    },
    body: JSON.stringify({
      accountId: 'karl02',
      code: '',
      password: 'ch082323!'
    })
  }
  return await require('./changePassword.js').handler(request)
}

module.signOut = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'dd8b257d-0cd2-4e8a-beff-69b01745f203'
    }
  }
  return await require('./signOut.js').handler(request)
}

module.quit = async () => {
  const request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c1bc9344-8355-47ac-a9fa-0104a1725e41'
    },
    body: JSON.stringify({
      "reason": "그냥"
    })
  }
  return await require('./quit.js').handler(request)
}

module.backupForQuit = async () => {
  const request = {
    headers: {
    },
    body: JSON.stringify({
      "userId": 7
    })
  }
  return await require('./backupForQuit.js').handler(request)
}

module.deleteForQuit = async () => {
  const request = {
    headers: {
    },
    pathParameters: {
      "userId": 20755
    }
  }
  return await require('./deleteForQuit.js').handler(request)
}

module.getRandomAccessToken = async () => {
  const request = {
  }
  return await require('./getRandomAccessToken.js').handler(request)
}


test = async () => {

  const environment = require('../environment.js')
  environment.setEnvironment()
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()