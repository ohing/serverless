'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`);


/**
 * @author Karl <karl@ohing.net> 
 * @description 비밀번호 찾기용 계정 확인
 * @method POST findPasswordAccount
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400)
    }

    const parameters = JSON.parse(event.body || '{}')
    let accountId = parameters.accountId
    if (accountId == null) {
      return response.result(400)
    }

    let query = `
      select user_email, user_hp_no from oi_user_info where user_login_id = '${accountId}';
    `
    let result = await mysql.execute(query)
    mysql.end()

    if (result.length == 0) {
      return response.result(404)
    }

    let info = result[0]

    let phoneNumber = info.user_hp_no
    let values = phoneNumber.replace(/\D[^\.]/g, "");
    let length = phoneNumber.length
    phoneNumber = `${values.slice(0,3)}-${values.slice(3,length-4).replace(/\d{2}$/, '**')}-${values.slice(length-4)}`
    let email = info.user_email
    values = email.split('@')
    length = values[0].length
    email = `${values[0].substring(0, length-2) + '**'}@${values[1]}`

    info.account_id = accountId
    info.user_hp_no = phoneNumber
    info.user_email = email

    return response.result(200, info)

  } catch(e) {

    console.error(e)

    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
