'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require(`@ohing/ohingresponse`)

/**
 * @author Karl <karl@ohing.net> 
 * @description getInit의 alarm 부분만 분리
 * @method GET getNotificationBadges
 * @returns  
*/
module.exports.handler = async event => {

  console.log(event)

  try {
    
    const accessToken = event.headers[access-token]
    const userInfo = authorizer.payload(accessToken)

    if (userInfo == null) {
      mysql.end()
      return response.result(401)
    }



    mysql.end()

    return response.result(200, {notificationCount: 0, chattingCount: 0})

  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
