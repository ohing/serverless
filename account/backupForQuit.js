'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)
const camelcase = require('camelcase-keys')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3();
const lambda = new AWS.Lambda();


/**
 * @author Karl <karl@ohing.net> 
 * @description 탈퇴자에 대한 데이터 백업
 * @method POST backupForQuit
*/
module.exports.handler = async event => {

  console.log(event)
  
  try {

    const parameters = JSON.parse(event.body || '{}') || {}
    const userId = parameters.userId

    if (userId == null) {
      return response.result(400)
    }

    let query
    let result

    // 백업 저장

    let data = {"description": "탈퇴자 백업 데이터입니다. 현재 버전에서는 기본적인 유저 정보, 본인인증 정보, 게시물(및 포함된 미디어), 댓글, 채팅 메시지들이 포함되어 있습니다."}

    query = `
      select 
        user_id, user_login_id account_id, user_nick_nm nickname, user_real_nm name, user_email email, user_hp_no phone_number, 
        region_cd, sex, birth_y, birth_md, profile_open_type_cd open_type, self_intro_content, parent_hp_no parent_phone_number, parent_nm parent_name, parent_birth_ymd, 
        last_login_dt last_login_at
      from 
        oi_user_info 
      where 
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    data.userInfo = result[0]
    
    query = `
      select 
        auth_type_cd auth_type, comm_cd provider, user_hp_no phone_number, sex, birth_ymd, ci, di, reg_dt auth_at
      from 
        oi_user_self_auth_info 
      where 
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    data.authentications = result

    query = `
      select 
        f.feed_id, f.feed_title title, f.feed_type_cd feed_type, f.feed_content, f.category_cd, 
          f.anony_yn is_anonymous, f.feed_except_yn is_blocked, f.encoded_yn is_encoded, f.reg_dt created_at,
          concat(
          '[',
          (
          select
            group_concat(concat('"', ti.tag_val, '"') separator ',')
          from 
            oi_feed_tag ft
            join oi_tag_info ti on ft.tag_id = ti.tag_id
          where 
            ft.feed_id = f.feed_id
          order by
            ft.tag_seq asc
          ),
          ']'
        ) tags,
          concat(
          '[',
          (
          select
            group_concat(concat('{"file_name":"', substring_index(fi.org_file_nm, '/', -1), '","media_type":"', fi.media_type_cd, '"}') separator ',')
          from 
            oi_feed_media_info fmi
            join oi_file_info fi on fmi.file_id = fi.file_id
          where 
            fmi.feed_id = f.feed_id
          order by
            fmi.media_seq asc
          ),
          ']'
        ) medias
      from 
        oi_feed_info f
      where
        f.reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    data.feeds = result.map(row => {
      row.is_anonymous = row.is_anonymous == 'Y'
      row.is_blocked = row.is_blocked == 'Y'
      row.is_encoded = row.is_encoded == 'Y'      
      return row
    })

    query = `
      select 
        comment_id, feed_id, parent_comment_id, content, anony_yn is_anonymous, del_yn is_deleted, except_yn is_excepted, reg_dt created_at
      from 
        oi_feed_comment
      where
        reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    data.comments = result.map(row => {
      row.is_anonymous = row.is_anonymous == 'Y'
      row.is_deleted = row.is_deleted == 'Y'
      row.is_excepted = row.is_excepted == 'Y'
      return row
    })

    query = `
      select 
        msg_id message_id, room_id, msg_type_cd message_type, msg_content content, send_dt_int timestamp,
          substring_index(send_profile_image_url, '/', -1) profile_file_name, 
          send_user_nick_nm nickname,
          target_user_id, target_user_nick_nm, media_info, invite_room_info, encoded_yn is_encoded
      from 
        oi_chat_msg
      where
        send_user_id = ${userId};
    `
    result = await mysql.execute(query)
    data.messages = result.map(row => {
      row.is_encoded = row.is_encoded == 'Y'
      return row
    })

    let date = new Date()
    let ymd = `${date.getUTCFullYear()}-${date.getUTCMonth()+1}-${date.getUTCDate()}`

    let camelcased = camelcase(data, {deep: true})

    let jsonString = JSON.stringify(camelcased)
    let buffer = Buffer.from(jsonString)

    const bucket = `ohing-backup-${process.env.stage}`
    const key = `account/${ymd}/${userId}.json`

    await s3.putObject({
      Bucket: bucket,
      ContentType: 'application/json',
      ContentLanguage: 'utf-8',
      ContentDisposition: 'attachment',
      Body: buffer,
      Key: key
    }).promise()

    // 삭제하다 중간에 문제생기면 안되니께 트랜잭션..
    await mysql.beginTransaction()

    // 삭제

    let applyingTagIds = []
    let needsRankerRefresh = false

    //////  피드   //////

    // 삭제 전, 영향을 준 모든 피드 아이디 조회.
    query = `
      select 
        f.feed_id
      from
        oi_feed_info f
          left join oi_feed_comment fc on f.feed_id = fc.feed_id
          left join oi_like_log ll on f.feed_id = ll.target_id and ll.like_target_type_cd = 'FEED'
          left join oi_feed_media_like_log fmll on f.feed_id = fmll.feed_id
      where
        f.reg_user_id != ${userId}
        and (
          (fc.reg_user_id = ${userId} and fc.del_yn = 'N')
              or ll.user_id = ${userId}
              or fmll.user_id = ${userId}
        )
      group by
        feed_id;
    `
    result = await mysql.execute(query)

    let applyingFeedIds = result.map(row => {
      return row.feed_id
    })

    // 피드 답글 및 답글 없는 댓글 바로 삭제
    query = `
      select 
        fc1.comment_id
      from 
        oi_feed_comment fc1
          left join oi_feed_comment fc2 on fc1.comment_id = fc2.parent_comment_id
      where
        fc1.reg_user_id = ${userId}
        and (
          fc1.parent_comment_id is not null
          or fc2.feed_id is null
        )
      group by 
        fc1.comment_id
    `
    result = await mysql.execute(query)

    if (result.length > 0) {

      let commentIds = result.map(row => {
        return row.comment_id
      })

      // 삭제할 댓글에 좋아요한 기록 삭제
      query = `
        delete from 
          oi_like_log
        where 
          target_id in (${commentIds.join(',')})
          and like_target_type_cd = 'COMT'
      `
      await mysql.execute(query)

      query = `
        delete from
          oi_feed_comment
        where
          comment_id in (
            ${commentIds.join(',')}
          );
      `
      await mysql.execute(query)
    }

    // 피드 답글 있는 댓글 갱신
    query = `
      select 
        fc1.comment_id
      from 
        oi_feed_comment fc1
        left join oi_feed_comment fc2 on fc1.comment_id = fc2.parent_comment_id
      where
        fc1.reg_user_id = ${userId}
        and fc1.parent_comment_id is null
        and fc2.feed_id is not null
      group by 
        fc1.comment_id
    `
    result = await mysql.execute(query)

    if (result.length > 0) {

      let commentIds = result.map(row => {
        return row.comment_id
      })

      // 삭제할 댓글에 좋아요한 기록 삭제
      query = `
        delete from 
          oi_like_log
        where 
          target_id in (${commentIds.join(',')})
          and like_target_type_cd = 'COMT'
      `
      await mysql.execute(query)

      query = `
        update 
          oi_feed_comment 
        set 
          content = '삭제된 댓글입니다.', del_yn = 'Y', reg_user_id = null, upd_dt = now() 
        where 
          comment_id in (
            ${commentIds.join(',')}
          );
      `
      await mysql.execute(query)
    }

    // 피드 / 댓글 좋아요 삭제
    query = `
      delete from
        oi_like_log
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    // 미디어 좋아요 삭제
    query = `
      delete from 
        oi_feed_media_like_log
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    // 피드 조회 기록 삭제는 안함
    // query = `
    //   delete from
    //     oi_feed_view_log
    //   where
    //     user_id = ${userId};
    // `
    // await mysql.execute(query)

    // 피드 미디어 파일 삭제
    query = `
      select 
        fi.file_id
      from
        oi_file_info fi
        left join oi_feed_media_info fmi on fi.file_id = fmi.file_id
      where 
        fmi.reg_user_id = ${userId};
    `
    result = await mysql.execute(query)

    if (result.length > 0) {

      let fileIds = result.map(row => {
        return row.file_id
      })

      query = `
        delete from 
          oi_file_info
        where
          file_id in (
            ${fileIds.join(',')}
          )
      `
      await mysql.execute(query) 
    }

    // 작성한 피드 목록
    query = `
      select 
        feed_id
      from
        oi_feed_info
      where
        reg_user_id = ${userId}
    `
    result = await mysql.execute(query) 

    if (result.length > 0) {

      let feedIds = result.map(row => {
        return row.feed_id
      })

      // 작성한 피드 통계 삭제
      query = `
        delete from 
          oi_feed_popul_aggr
        where
          feed_id in (${feedIds.join(',')});
      `
      await mysql.execute(query) 

      // 피드 미디어 좋아요 삭제
      query = `
        delete from
          oi_feed_media_like_log
        where
          feed_id in (${feedIds.join(',')});
      `
      await mysql.execute(query) 

      // 피드 좋아요 삭제
      query = `
        delete from
          oi_like_log
        where
          target_id in (${feedIds.join(',')})
          and like_target_type_cd = 'FEED';
      `
      await mysql.execute(query) 

      // 피드 미디어 삭제
      query = `
        delete from
          oi_feed_media_info
        where
          reg_user_id = ${userId};
      `
      await mysql.execute(query)

      // 피드 태그 목록
      query = `
        select 
          f.feed_id, ti.tag_id
        from
          oi_feed_info f
          join oi_feed_tag ft on f.feed_id = ft.feed_id
          join oi_tag_info ti on ft.tag_id = ti.tag_id
        where
          f.reg_user_id = ${userId}
        group by
          f.feed_id;
      `
      result = await mysql.execute(query)

      if (result.length > 0) {

        let feedIds = []
        let tagIds = []

        for (let row of result) {
          feedIds.push(row.feed_id)
          if (!tagIds.includes(row.tag_id)) {
            tagIds.push(row.tag_id)
          }
        }

        applyingTagIds = applyingTagIds.concat(tagIds)

        // 피드 태그 삭제
        query = `
          delete from
            oi_feed_tag
          where
            feed_id in (${feedIds.join(',')});
        `
        await mysql.execute(query)
      }

      // 피드 삭제
      query = `
        delete from
          oi_feed_info
        where
          reg_user_id = ${userId};
      `
      await mysql.execute(query)
    }


    // 영향을 준 피드의 통계정보 갱신
    
    if (applyingFeedIds.length > 0) {
      query = `
        update 
          oi_feed_popul_aggr fpa
        set
          fpa.like_cnt = (
            (select count(*) from oi_like_log ll join oi_user_info ui on ll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where ll.like_target_type_cd = 'FEED' and ll.target_id = fpa.feed_id)+
            (select count(*) from oi_feed_media_like_log fmll join oi_user_info ui on fmll.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fmll.feed_id = fpa.feed_id)
          ),
          fpa.view_cnt = (select count(*) from oi_feed_view_log where feed_id = fpa.feed_id),
          fpa.comment_cnt = (select count(*) from oi_feed_comment fc join oi_user_info ui on fc.reg_user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE' where fc.feed_id = fpa.feed_id and fc.del_yn = 'N')
        where
          fpa.feed_id in (${applyingFeedIds.join(',')});
      `
      await mysql.execute(query)
    }

    /////////////////// 

    //////  채팅   //////

    // 채팅공지 답글 및 답글 없는 댓글 목록
    query = `
      select 
        fc1.comment_id
      from 
        oi_chat_notice_comment fc1
        left join oi_chat_notice_comment fc2 on fc1.comment_id = fc2.parent_comment_id
      where
        fc1.reg_user_id = ${userId}
        and (
          fc1.parent_comment_id is not null
          or fc2.chat_notice_id is null
        )
      group by 
        fc1.comment_id
    `
    result = await mysql.execute(query)

    if (result.length > 0) {

      let commentIds = result.map(row => {
        return row.comment_id
      })

      // 삭제할 댓글에 좋아요한 기록 삭제
      query = `
        delete from 
          oi_like_log
        where 
          target_id in (${commentIds.join(',')})
          and like_target_type_cd = 'CNCOMT'
      `
      await mysql.execute(query)

      // 채팅공지 답글 및 답글 없는 댓글 삭제
      query = `
        delete from
          oi_chat_notice_comment
        where
          comment_id in (
            ${commentIds.join(',')}
          );
      `
      await mysql.execute(query)
    }

    // 채팅공지 답글 있는 댓글 목록
    query = `
      select 
        fc1.comment_id
      from 
        oi_chat_notice_comment fc1
        left join oi_chat_notice_comment fc2 on fc1.comment_id = fc2.parent_comment_id
      where
        fc1.reg_user_id = ${userId}
        and fc1.parent_comment_id is null
        and fc2.chat_notice_id is not null
      group by 
        fc1.comment_id
    `
    result = await mysql.execute(query)

    if (result.length > 0) {

      let commentIds = result.map(row => {
        return row.comment_id
      })

      // 삭제할 댓글에 좋아요한 기록 삭제
      query = `
        delete from 
          oi_like_log
        where 
          target_id in (${commentIds.join(',')})
          and like_target_type_cd = 'CNCOMT'
      `
      await mysql.execute(query)

      // 채팅공지 답글 있는 댓글 갱신
      query = `
        update 
          oi_chat_notice_comment 
        set 
          content = '삭제된 댓글입니다.', del_yn = 'Y', reg_user_id = null, upd_dt = now() 
        where 
          comment_id in (
            ${commentIds.join(',')}
          );
      `
      await mysql.execute(query)
    }

    // 공지사항댓글에 좋아요한 것 모두 삭제
    query = `
      delete from
        oi_like_log
      where
        like_target_type_cd = 'CNCOMT'
        and user_id = ${userId};
    `
    await mysql.execute(query)

    // 작성한 공지사항에 달린 댓글 아이디들
    query = `
      select 
        cnc.comment_id
      from
        oi_chat_notice_info cni
        join oi_chat_notice_comment cnc on cni.chat_notice_id = cnc.chat_notice_id
      where 
        cni.reg_user_id = ${userId};
    `
    result = await mysql.execute(query)
    if (result.length > 0) {

      let commentIds = result.map(row => {
        return row.comment_id
      })
      
      // 삭제할 공지사항에 달린 댓글에 좋아요한 기록 삭제
      query = `
        delete from 
          oi_like_log
        where 
          target_id in (${commentIds.join(',')})
          and like_target_type_cd = 'CNCOMT'
      `
      await mysql.execute(query)

      // 작성한 공지사항에 달린 댓글 모두 삭제
      query = `
        delete from 
          oi_chat_notice_comment
        where 
          comment_id in (${commentIds.join(',')})
      `
      await mysql.execute(query)
    }

    // 작성한 공지사항 모두 삭제
    query = `
      delete from 
        oi_chat_notice_info 
      where 
        reg_user_id = ${userId};
    `
    await mysql.execute(query)

    // 방장인 방 목록
    query = `
      select 
        cri.room_id, cri.room_status_cd
      from 
        oi_chat_user_info cui
        join oi_chat_room_info cri on cui.room_id = cri.room_id
      where 
        cui.user_id = ${userId}
        and cui.master_yn = 'Y';
    `
    result = await mysql.execute(query)

    if (result.length > 0) {

      for (let row of result) {

        let roomId = row.room_id
        let status = row.room_status_cd

        if (status == 'ACTIVE') {

          // 방장 물려주기
          query = `
            update 
              oi_chat_user_info
            set 
              master_yn = 'Y'
            where
              room_id = ${roomId} and use_yn = 'Y' and master_yn = 'N'
            order by
              join_dt asc
            limit 1
          `
          await mysql.execute(query)
        }

      }
    }

    // 채팅 메시지 삭제
    query = `
      delete from
        oi_chat_msg
      where
        send_user_id = ${userId};
    `
    await mysql.execute(query)

    // 내가 속한 모든 채팅방 아이디
      query = `
        select
          room_id
        from 
          oi_chat_user_info
        where
          user_id = ${userId};
      `
      result = await mysql.execute(query)

      // 내가 속한 모든 채팅방 나가기
      query = `
        delete from
          oi_chat_user_info
        where
          user_id = ${userId};
      `
      await mysql.execute(query)

      // 나간 채팅방들 카운트 재조정
      for (let row of result) {
        
        query  = `
          update 
            oi_chat_room_info
          set
            join_user_cnt = (
              select 
                count(*) count
              from 
                oi_chat_user_info
              where
                room_id = ${row.room_id}
                and use_yn = 'Y'
            )
          where
            room_id = ${row.room_id};
        `
        await mysql.execute(query)
      }

    /////////////////// 


    //////  유저   //////

    // 유저간의 관계 모두 삭제
    query = `
      delete from
        oi_user_relation
      where
        user_id = ${userId}
        or relation_user_id = ${userId};
    `
    await mysql.execute(query)

    // 활동로그 모두 삭제
    query = `
      delete from
        oi_user_action_log
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    // 학교기록 삭제
    query = `
      delete from
        oi_user_school_history
      where
        user_id = ${userId};
    `
    await mysql.execute(query)
    
    // 프로필이미지 파일아이디 목록
    query = `
      select 
        file_id
      from
        oi_user_profile_image
      where
        user_id = ${userId}
    `
    result = await mysql.execute(query)
    if (result.length > 0) {

      let fileIds = result.map(row => {
        return row.file_id
      })

      // 프로필이미지 파일 삭제
      query = `
        delete from
          oi_file_info
        where
          file_id in (${fileIds.join(',')});
      `
      await mysql.execute(query)

      // 프로필이미지 삭제
      query = `
        delete from
          oi_user_profile_image
        where
          user_id = ${userId}
      `
      await mysql.execute(query)
    }

    // 기기정보 삭제
    query = `
      delete from
        oi_user_device_info
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    // 일일기록 삭제
    query = `
      delete from
        oi_user_daily_stats
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    // 유저 태그 목록
    query = `
      select
        tag_id
      from
        oi_user_tag
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)

    if (result.length > 0) {

      let tagIds = result.map(row => {
        return row.tag_id
      })

      applyingTagIds = applyingTagIds.concat(tagIds)

      // 유저 태그 삭제
      query = `
        delete from 
          oi_user_tag
        where 
          user_id = ${userId};
      `
    }

    // 오늘 랭커인지 확인
    query = `
      select 
        user_id
      from
        oi_user_rank
      where
        rank_ymd = date_format(now(), '%Y%m%d');
    `
    result = await mysql.execute(query)
    if (result.length > 0) {
      needsRankerRefresh = true
    }

    /////////////////// 

    if (applyingTagIds.length > 0) {

      let tagIds = applyingTagIds.filter((item, pos) => {
        return applyingTagIds.indexOf(item) === pos
      })

      // 삭제된 태그들 카운팅
      query = `
        update
          oi_tag_info ti
        set
          tag_cnt = (
            (select count(*) from oi_user_tag ut where ut.tag_id = ti.tag_id) + 
            (select count(*) from oi_feed_tag ft where ft.tag_id = ti.tag_id) +
            (select count(*) from oi_chat_room_tag crt where crt.tag_id = ti.tag_id)
          )
        where
          ti.tag_id in (${tagIds.join(',')})
      `
      await mysql.execute(query)
    }

    // 탈퇴상태로 변경
    query = `
      update
        oi_user_info
      set
        user_status_cd = 'WITHDRAW'
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    await mysql.commit()

    mysql.end()

    if (needsRankerRefresh) {
      // 랭커 갱신
      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-makeRanking`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({})
        })
      }
      console.log(params)

      await lambda.invoke(params).promise()
    }

    return response.result(200, {})

  } catch(e) {

    console.error(e)

    await mysql.rollback()
    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
