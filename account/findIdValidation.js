'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`);

/**
 * @author Karl <karl@ohing.net> 
 * @description 아이디찾기 코드 체크
 * @method POST findIdValidation
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400)
    }

    const parameters = JSON.parse(event.body || '{}')
    let email = parameters.email
    let phoneNumber = parameters.phoneNumber
    let code = parameters.code
    if ((email == null && phoneNumber == null) || code == null) {
      return response.result(400)
    }

    let query

    let where
    
    if (email != null) {
      where = `
        where user_email = '${email}'
      `
      
    } else {
      phoneNumber = phoneNumber.replace(/-|\s/g,'')
      where = `
        where user_hp_no = '${phoneNumber}'
      `
    }

    query = `
      select user_id, user_login_id account_id, auth_no from oi_user_info ${where};
    `
    console.log(query)

    let result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404, null, '회원 정보를 찾을 수 없습니다.')
    }
    
    if (code != result[0].auth_no) {
      mysql.end()
      return response.result(403, null, '인증번호가 일치하지 않습니다.')
    }

    let userId = result[0].user_id

    query = `
      update 
        oi_user_info
      set
        auth_no = null
      ${where};
    `
    await mysql.execute(query)
    
    mysql.end()

    let ids = result.map(row => {
      return {
        accountId: row.account_id
      }
    })

    return response.result(200, ids)

  } catch(e) {

    console.error(e)

    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
