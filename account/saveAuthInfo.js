'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`);

/**
 * @author Karl <karl@ohing.net> 
 * @description 인증결과 저장
 * @method POST saveAuthInfo
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body || '{}')
    let authType = (parameters.authType || '').trim()
    let imei = (parameters.imei || '').trim()
    let name = (parameters.name || '').trim()
    let birthday = (parameters.birthday || '').trim()
    let localCode = '01'
    let phoneNumber = parameters.phoneNumber == null ? 'null' : `'${parameters.phoneNumber}'`
    let gender = (parameters.genderCode == '1' || parameters.genderCode == 'M') ? 'M' : 'F'
    let ci = (parameters.ci || '').trim()
    let di = (parameters.di || '').trim()
    
    if (authType.length == 0 || imei.length == 0 || name.length == 0 || birthday.length != 8 || ci.length == 0 || di.length == 0) {
      return response.result(400)
    }

    let query = `
      select 
        count(*) count 
      from 
        oi_user_info 
      where 
        ci = '${ci}';
    `
    let result = await mysql.execute(query)
    // if (result[0].count > 2) {
    //   return response.result(200, {authId: null})
    // }

    let txId = (parameters.txId || '').trim()

    let authId

    if (txId.length > 0) {

      query = `
        select
          auth_id
        from
          oi_user_self_auth_info
        where 
          imei = '${imei}'
          and comm_cd = '${authType}'
          and di = '${txId}'
        limit
          1;
      `
      result = await mysql.execute(query)
      if (result.length > 0) {

        authId = result[0].auth_id

        query = `
          update 
            oi_user_self_auth_info
          set
            auth_type_cd = 'self', comm_cd = '${authType}', local_cd = '${localCode}', user_real_nm = '${name}', user_hp_no = ${phoneNumber},
            sex = '${gender}', birth_ymd = '${birthday}', ci = '${ci}', di = '${di}'
          where
            auth_id = ${authId};
        `
        console.log(query)
        await mysql.execute(query)
      }

    }

    if (authId == null) {

      query = `
        insert into oi_user_self_auth_info (
          auth_type_cd, imei, comm_cd, local_cd, user_real_nm, user_hp_no, sex, birth_ymd, ci, di
        ) values (
          'self', '${imei}', '${authType}', '${localCode}', '${name}', ${phoneNumber}, '${gender}', '${birthday}', '${ci}', '${di}'
        );
      `
      console.log(query)
      result = await mysql.execute(query)

      authId = result.insertId
    }

    mysql.end()
    
    return response.result(200, {authId: authId})

  } catch(e) {

    console.error(e)

    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
