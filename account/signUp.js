'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`)

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 회원가입
 * @method PUT signUp
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    const authId = parameters.authId
    const accountId = parameters.accountId
    const password = parameters.password
    let nickname = parameters.nickname
    const email = parameters.email
    const regionCode = parameters.regionCode
    let schoolId = parameters.schoolId
    let schoolName = parameters.schoolName
    let grade = parameters.grade
    let class_ = parameters.class_
    let tags = parameters.tags || []
    const agreements = parameters.agreements
    const fcmToken = parameters.fcmToken

    const isValid = accountId.match(/^[_a-z0-9.]*$/) != null

    if (!isValid) {
      return response.result(400, null, '아이디는 영문소문자, 숫자, 특수문자중 언더바(_)와 마침표(.)만 사용가능합니다.')
    }

    if (password.length < 4) {
      return response.result(400, null, '비밀번호가 너무 짧습니다.')
    }

    if (authId == null || accountId == null || password == null || nickname == null || email == null || agreements == null || agreements.length == 0) {
      return response.result(400)
    }

    nickname = await mysql.escape(nickname)

    let query = `
      select 
        *
      from 
        oi_user_self_auth_info
      where
        auth_id = ${authId} 
        and user_id is null
        and auth_type_cd = 'self';
    `
    let result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(404, null, '인증 정보가 없습니다.')
    }

    let authInfo = result[0]

    let birthday = authInfo.birth_ymd
    let birthDate = new Date(birthday.substring(0,4), parseInt(birthday.substring(4,6))-1, birthday.substring(6,8))
    let koreanDate = new Date(birthDate.getTime() + 32400000)
    let koreanToday = new Date((new Date()).getTime() + 32400000)
    let age = koreanToday.getFullYear() - koreanDate.getFullYear()
    
    if (koreanDate.getMonth() > koreanToday.getMonth() || (koreanDate.getMonth() == koreanToday.getMonth() && koreanDate.getDate() > koreanToday.getDate())) {
      age--
    }

    let parentPhoneNumber
    let parentName
    let parentBirthday

    if (age < 14) {
      query = `
        select
          *
        from
          oi_user_self_auth_info
        where
          auth_type_cd = 'parents' and imei = '${imei}'
          and reg_dt < now() && reg_dt > timestampadd(minute, -30, now())
        limit
          1;
      `
      result = await mysql.execute(query)

      if (result.length == 0) {
        mysql.end()
        return response.result(404, null, '부모님 인증 정보가 없습니다.')
      }

      let parentAuthInfo = result[0]

      parentPhoneNumber = `'${parentAuthInfo.user_hp_no}'`
      parentName = `'${parentAuthInfo.user_real_nm}'`
      parentBirthday = `'${parentAuthInfo.birth_ymd}'`

    } else {

      parentPhoneNumber = 'null'
      parentName = 'null'
      parentBirthday = 'null'
    }

    query = `
      select
        user_id 
      from
        oi_user_info
      where
        user_login_id = '${accountId}'
      limit
        1;
    `
    result = await mysql.execute(query)
    if (result.length > 0) {
      mysql.end()
      return response.result(409, null, '이미 사용중인 아이디입니다.')
    }

    let schoolShortName
    let schoolIdValue
    let gradeValue
    let classValue

    if (schoolId != null) {
      query = `
        select
          *
        from
          oi_school_info
        where
          school_id = '${schoolId}'
      `
      result = await mysql.execute(query)

      if (result.length == 0) {
        mysql.end()
        return response.result(403, null, '학교 정보를 찾을 수 없습니다.')
      }

      schoolIdValue = `'${schoolId}'`

      schoolName = result[0].school_nm
      schoolShortName = result[0].school_short_nm

    } else if (schoolName != null) {

      schoolIdValue = 'null'
      schoolShortName = schoolName
    }

    if (grade != null) {
      gradeValue = `'${grade}'`
    } else {
      gradeValue = `''`
    }

    if (class_ != null) {
      classValue = `'${class_}'`
    } else {
      classValue = `''`
    }

    await mysql.beginTransaction()

    let token = authorizer.createOldToken()
    let passwordHash = authorizer.createOldPasswordHash(password)

    let birthY = birthday.substring(0, 4)
    let birthMd = birthday.substring(4, 8)

    query = `
      insert into oi_user_info(
        user_type_cd, user_login_id, user_pwd, user_nick_nm, user_real_nm, user_email, user_hp_no, region_cd, sex,
        birth_y, birth_md, profile_open_type_cd, parent_hp_no, parent_nm, parent_birth_ymd, ci, di,
        user_status_cd, login_auth_no, last_login_dt
      ) values (
        'USER', '${accountId}', '${passwordHash}', ${nickname}, '${authInfo.user_real_nm}', '${email}', '${authInfo.user_hp_no}', '${regionCode}', '${authInfo.sex}',
        '${birthY}', '${birthMd}', 'OPEN', ${parentPhoneNumber}, ${parentName}, ${parentBirthday}, '${authInfo.ci}', '${authInfo.di}',
        'ACTIVE', '${token}', now()
      );
    `
    console.log(query)
    result = await mysql.execute(query)
    const userId = result.insertId

    let schools = []

    if (schoolShortName != null) {

      let escapedSchoolName = await mysql.escape(schoolName)
      let escapedSchoolShortName = await mysql.escape(schoolShortName)

      query = `
        insert into oi_user_school_history (
          user_id, school_hist_cd, school_id, school_nm, school_short_nm, grade, class
        ) values (
          ${userId}, 'CURR', ${schoolIdValue}, ${escapedSchoolName}, ${escapedSchoolShortName}, ${gradeValue}, ${classValue}
        );
      `
      await mysql.execute(query)

      schools.push({
        schoolId: schoolIdValue,
        schoolNm: schoolName, 
        schoolShortName: schoolShortName, 
        isCurrent: true
      })
    }
    
    if (tags.length > 0) {

      if (tags.length > 20) {
        tags = tags.slice(0, 20)
      }

      let values = []
      let tagValues = []

      for (let tag of tags) {
        let trimmed = tag.trim()
        if (trimmed.length > 0) {
          let escaped = await mysql.escape(trimmed)
          values.push(`( ${escaped}, 1 )`)
          tagValues.push(`${escaped}`)
        } 
      }

      let valuesString = values.join(', ')

      query = `
        insert into oi_tag_info (
          tag_val, tag_cnt
        ) values 
          ${valuesString}
        on duplicate key update 
          tag_cnt = values(tag_cnt) + 1;
      `
      console.log(query)
      await mysql.execute(query)

      query = `
        select
          tag_id, tag_val
        from 
          oi_tag_info
        where
          tag_val in (${tagValues.join(',')});
      `
      result = await mysql.execute(query)

      console.log(result)

      let tagInfos = tags.map(tag => {
        return {tag_val: tag}
      })
      for (let i = 0; i < tagInfos.length; i++) {
        let filtered = result.filter(row => {
          return row.tag_val == tagInfos[i].tag_val
        })
        if (filtered.length > 0) {
          tagInfos[i].tag_id = filtered[0].tag_id
        }
      }
      
      let tagIds = tagInfos.map(tagInfo => {
        return tagInfo.tag_id
      })

      let seq = 0
      values = tagIds.map(tagId => { 
        seq++
        return `( ${userId}, ${tagId}, ${seq} )`
      })
      valuesString = values.join(',')

      query = `
        insert into oi_user_tag (
          user_id, tag_id, tag_seq
        ) values 
          ${valuesString};
      `
      console.log(query)
      await mysql.execute(query)
    }

    let values = agreements.map(agreement => {
      return `(${userId}, ${agreement.termsId}, now(), '${agreement.agreed ? 'Y' : 'N'}')`
    })
    let valuesString = values.join(', ')

    query = `
      insert into oi_terms_agree_log (
        user_id, terms_id, terms_check_dt, agree_yn
      ) values 
        ${valuesString};
    `
    console.log(query)
    await mysql.execute(query)

    query = `
      insert into oi_notification_setting (
        user_id
      ) values (
        ${userId}
      );
    `
    await mysql.execute(query)

    if (fcmToken != null) {

      let fcmSteps = []

      query = `
        insert into oi_user_device_info (
          imei, user_id, os_type_cd, app_ver, push_auth_key, login_yn
        ) values (
          '${imei}', ${userId}, '${osType.toUpperCase()}', '${appVersion}', '${fcmToken}', 'Y'
        ) on duplicate key update 
          user_id = ${userId}, os_type_cd = '${osType.toUpperCase()}', app_ver = '${appVersion}', push_auth_key = '${fcmToken}', login_yn = 'Y', upd_dt = now();
      `
      await mysql.execute(query)

      fcmSteps.push({
        type: 'unsubscribe',
        tokens: [fcmToken],
        topics: []
      })
      
      let platformTopic = osType == 'iOS' ? 'notification-ios' : 'notification-android'

      query = `
        select
          ti.tag_id
        from
          oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
        where 
          ut.user_id = ${userId};
      `
      result = await mysql.execute(query)

      const tagTopics = userInfo.tags.map(element => {return `tag-${element.tag_id}`})
      let topics = ['notification', platformTopic].concat(tagTopics)

      fcmSteps.push({
        type: 'subscribe',
        tokens: [fcmToken],
        topics: topics
      })

      await lambda.invoke({
        FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            steps: fcmSteps
          })
        })  
      }).promise()

    } else {
      
      query = `
        insert into oi_user_device_info (
          imei, user_id, os_type_cd, app_ver, push_auth_key, login_yn
        ) values (
          '${imei}', ${userId}, '${osType.toUpperCase()}', '${appVersion}', null, 'Y'
        ) on duplicate key update 
          user_id = ${userId}, os_type_cd = '${osType.toUpperCase()}', app_ver = '${appVersion}', push_auth_key = null, login_yn = 'Y', upd_dt = now();
      `
      await mysql.execute(query)
    }

    query = `
      update
        oi_user_self_auth_info
      set
        user_id = ${userId}
      where 
        auth_id = ${authId};
    `
    await mysql.execute(query)

    query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        ui.user_email email, ui.user_hp_no phone_number,
        'img/notification-default/notification-default.png' profile_image_url, 
        'notification-default.png' profile_file_name
      from 
        oi_user_info ui
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where 
        ui.user_id = ${userId};
    `
    result = await mysql.execute(query)

    let userInfo = result[0]

    query = `
      select  
        tag_val
      from 
        oi_tag_info ti
        join oi_user_tag ut on ti.tag_id = ut.tag_id
      where
        ut.user_id = ${userId}
      order by
        ut.tag_seq asc;
    `
    result = await mysql.execute(query)

    userInfo.tags = result.map(row => { 
      return row.tag_val
    })

    userInfo.schools = schools
    

    await mysql.commit()
    mysql.end()

    const accessToken = authorizer.createOldToken()
    await authorizer.setAccessToken(accessToken, userId)

    userInfo.access_token = accessToken

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: 20,
          type: 'SIGNUP',
          log: '오잉 가입을 환영합니다👏'
        })
      })
    }
    await lambda.invoke(logParams).promise()

    return response.result(200, userInfo)

  } catch(e) {

    console.error(e)
    await mysql.rollback()
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
