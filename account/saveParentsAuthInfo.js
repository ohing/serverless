'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`);

/**
 * @author Karl <karl@ohing.net> 
 * @description 부모인증결과 저장
 * @method POST saveParentsAuthInfo
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const parameters = JSON.parse(event.body || '{}')
    let authType = (parameters.authType || '').trim()
    let imei = (parameters.imei || '').trim()
    let name = (parameters.name || '').trim()
    let birthday = (parameters.birthday || '').trim()
    let localCode = '01'
    let phoneNumber = parameters.phoneNumber == null ? 'null' : `'${parameters.phoneNumber}'`
    let gender = (parameters.genderCode == '1' || parameters.genderCode == 'M') ? 'M' : 'F'
    let ci = (parameters.ci || '').trim()
    let di = (parameters.di || '').trim()
    
    if (authType.length == 0 || imei.length == 0 || name.length == 0 || birthday.length != 8 || ci.length == 0 || di.length == 0) {
      return response.result(400)
    }

    let query = `
      select 
        count(*) count 
      from 
        oi_user_info 
      where 
        ci = '${ci}';
    `
    let result = await mysql.execute(query)
    // if (result[0].count > 2) {
    //   return response.result(200, {authId: null})
    // }

    query = `
      insert into oi_user_self_auth_info (
        auth_type_cd, imei, comm_cd, local_cd, user_real_nm, user_hp_no, sex, birth_ymd, ci, di
      ) values (
        'parents', '${imei}', '${authType}', '${localCode}', '${name}', ${phoneNumber}, '${gender}', '${birthday}', '${ci}', '${di}'
      );
    `
    console.log(query)

    await mysql.execute(query)

    query = `
      select
        auth_id, birth_ymd
      from
        oi_user_self_auth_info
      where
        user_id is null
        and imei = '${imei}'
        and auth_type_cd = 'self'
      order by
        auth_id desc
      limit 1;
    `
    result = await mysql.execute(query)

    let authId = null;

    if (result.length > 0) {

      let parentsBirthday = result[0].birth_ymd
      let birthYear = parseInt(birthday.substring(0, 4))
      let parentsBirthYear = parseInt(parentsBirthday.substring(0, 4))

      console.log(birthYear, parentsBirthYear)

      if ((parentsBirthYear - birthYear) > 17) {
        authId = result[0].auth_id
      }
    }

    mysql.end()
    
    return response.result(200, {authId: authId})

  } catch(e) {

    console.error(e)

    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
