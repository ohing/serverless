'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`);
const response = require(`@ohing/ohingresponse`);

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 로그인
 * @method POST signIn
*/
module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400);
    }

    const parameters = JSON.parse(event.body || '{}');
    const accountId = parameters.accountId;
    const password = parameters.password;

    if (accountId == null || password == null) {
      return response.result(400);
    }

    let passwordHash = authorizer.createOldPasswordHash(password);

    let query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname, ui.user_status_cd, 
        ui.user_email email, ui.user_hp_no phone_number,
        case 
          when pi.user_id is null then 'img/notification-default/notification-default.png'
          else fi.org_file_nm
        end profile_image_url,
          case 
          when pi.user_id is null then 'notification-default.png'
          else substring_index(fi.org_file_nm, '/', -1)
        end profile_file_name
      from 
        oi_user_info ui
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
      where 
        ui.user_login_id = '${accountId}' and ui.user_pwd = '${passwordHash}';
    `;
    console.log(query);
    let result = await mysql.execute(query);

    if (result.length == 0) {
      mysql.end();
      return response.result(404, null, '일치하는 정보가 없습니다.');
    }

    if (result[0].user_status_cd == 'WDSTANDBY') {
      mysql.end();
      return response.result(4001, null, '탈퇴 신청이 접수되어 대기중입니다. 복구를 원하시는 경우 고객센터로 문의해 주세요.');
    }

    let userInfo = result[0];

    const userId = userInfo.user_id;

    let blackListDB = await redis.readable(4);
    const blackListKey = `blackList:userId:${userId}`;
    const isBlackList = await blackListDB.get(blackListKey);
    console.log(isBlackList);
    if (isBlackList) {
      mysql.end();
      return response.result(4444, null, '이용할 수 없습니다.');
    }

    query = `
      select  
        ti.tag_id, ti.tag_val
      from 
        oi_tag_info ti
        join oi_user_tag ut on ti.tag_id = ut.tag_id
      where
        ut.user_id = ${userId}
      order by
        ut.tag_seq asc;
    `;
    result = await mysql.execute(query);

    userInfo.tags = result.map(row => {
      return row.tag_val;
    });

    let tagIds = result.map(row => {
      return row.tag_id;
    });

    query = `
      select 
        school_hist_cd, school_id, school_nm, school_short_nm, grade, class classe
      from
        oi_user_school_history
      where
        user_id = ${userId}
      order by
        field(school_hist_cd, 'CURR') desc;
    `;
    result = await mysql.execute(query);
    result = result.map(row => {
      row.is_current = row.school_hist_cd == 'CURR';
      delete row.school_hist_cd;
      return row;
    });

    userInfo.schools = result;

    const accessToken = authorizer.createOldToken();

    let fcmSteps = [];

    query = `
      select
        push_auth_key
      from
        oi_user_device_info
      where
        user_id = ${userId} 
        and push_auth_key is not null and push_auth_key  <> '';
    `;
    result = await mysql.execute(query);

    const tokens = result.map(row => { return row.push_auth_key; });

    fcmSteps.push({
      type: 'unsubscribe',
      tokens: tokens,
      topics: []
    });

    const fcmToken = parameters.fcmToken;
    if (fcmToken != null) {

      query = `
        insert into oi_user_device_info (
          imei, user_id, os_type_cd, app_ver, push_auth_key, login_yn
        ) values (
          '${imei}', ${userId}, '${osType.toUpperCase()}', '${appVersion}', '${fcmToken}', 'Y'
        ) on duplicate key update 
          user_id = ${userId}, os_type_cd = '${osType.toUpperCase()}', app_ver = '${appVersion}', push_auth_key = '${fcmToken}', login_yn = 'Y', upd_dt = now();
      `;
      await mysql.execute(query);

      if (!tokens.includes(fcmToken)) {
        fcmSteps.push({
          type: 'unsubscribe',
          tokens: [fcmToken],
          topics: []
        });
      }

      let platformTopic = osType == 'iOS' ? 'notification-ios' : 'notification-android';

      const tags = tagIds.map(tagId => { return `tag-${tagId}`; });

      query = `
        select 
          ri.room_id
        from
          oi_chat_room_info ri 
          join oi_chat_user_info ui on ri.room_id = ui.room_id
        where
          ui.user_id = ${userId} and ri.room_type_cd = 'OPEN' and ui.alarm_yn = 'Y';
      `;
      result = await mysql.execute(query);
      const chattingRooms = result.map(element => { return `openchat-${element.room_id}`; });

      let topics = ['notification', platformTopic].concat(tags).concat(chattingRooms);

      fcmSteps.push({
        type: 'subscribe',
        tokens: [fcmToken],
        topics: topics
      });

    } else {

      query = `
        insert into oi_user_device_info (
          imei, user_id, os_type_cd, app_ver, push_auth_key, login_yn
        ) values (
          '${imei}', ${userId}, '${osType.toUpperCase()}', '${appVersion}', null, 'Y'
        ) on duplicate key update 
          user_id = ${userId}, os_type_cd = '${osType.toUpperCase()}', app_ver = '${appVersion}', push_auth_key = null, login_yn = 'Y', upd_dt = now();
      `;
      await mysql.execute(query);
    }

    if (fcmSteps.length > 0) {

      await lambda.invoke({
        FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            steps: fcmSteps
          })
        })
      }).promise();
    }

    query = `
      delete from
        oi_user_device_info
      where
        user_id = ${userId} and imei != '${imei}';
    `;
    await mysql.execute(query);
    await authorizer.setAccessToken(accessToken, userInfo.user_id);

    mysql.end();
    redis.end();

    userInfo.access_token = accessToken;
    return response.result(200, userInfo);

  } catch (e) {

    console.error(e);

    mysql.end();
    redis.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};
