'use strict';

const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require(`@ohing/ohingresponse`);

/**
 * @author Karl <karl@ohing.net> 
 * @description 인증결과 Id 조회
 * @method GET getAuthId
*/
module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']

    if (imei == null || osType == null || appVersion == null) {
      return response.result(400)
    }

    let expireMinutes = process.env.stage == 'dev' ? 60 : 1

    let query = `
      select 
        auth_id, sex, user_real_nm real_name
      from 
        oi_user_self_auth_info 
      where 
        imei = '${imei}'
          and auth_type_cd = 'self'
          and reg_dt > date_add(now(), interval -${expireMinutes} minute)
      order by 
        auth_id desc
      limit 
        1;
    `

    let result = await mysql.execute(query)

    mysql.end()

    console.log(result)

    if (result.length == 0) {
      return response.result(404)
    }

    const authId = result[0].auth_id
    const sex = result[0].sex
    const name = result[0].real_name

    return response.result(200, {authId: authId, sex: sex, realName: name})

  } catch(e) {

    console.error(e)

    mysql.end()
    
    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
