'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 프로필 수정
 * @method PATCH updateProfile
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let nickname = parameters.nickname
    let regionCode = parameters.regionCode
    let selfIntroduction = parameters.selfIntroduction

    if (nickname == null && regionCode == null && selfIntroduction == null) {
      return response.result(400)
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    // if (openType == 'CLOSE') {
    //   mysql.end()
    //   return response.result(403, null, '비공개 회원은 프로필 정보를 수정하실 수 없습니다.')
    // }

    let sets = []

    if (nickname != null || nickname.length > 0) {
      sets.push(`user_nick_nm = '${nickname}'`)
    }

    if (regionCode != null || regionCode.length > 0) {
      sets.push(`region_cd = ${regionCode}`)
    }

    if (selfIntroduction != null) {
      sets.push(`self_intro_content = '${selfIntroduction}'`)
    }

    query = `
      update
        oi_user_info
      set
        ${sets.join(', ')}
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
