'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const s3 = new AWS.S3();
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 프로필 이미지 수정
 * @method PUT updateProfileImage
 * @returns  
*/

module.fileCheck = async (key) => {
  try {
    let params = {
      Bucket: process.env.mediaBucket,
      Key: profileKey
    }
    console.log(params) 

    let result = await s3.headObject(params).promise()
    return result.ETag != null

  } catch (e) {
    return false
  }
}

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    // if (openType == 'CLOSE') {
    //   mysql.end()
    //   return response.result(403, null, '비공개 회원은 프로필 정보를 수정하실 수 없습니다.')
    // }

    const parameters = JSON.parse(event.body || '{}') || {}
    let profileKey = parameters.key

    if (profileKey == null) {
      return response.result(400)
    }

    if (profileKey.length > 0) {

      if (!profileKey.includes('/') && profileKey.includes('.')) {
        let fileNames = profileKey.split('.')
        let fileName = fileNames[0]
        let fileExtension = fileNames[1]
        profileKey = `img/${fileName}/${fileName}.${fileExtension}`
      }

      // let checkKey1
      // let checkKey2

      // if (!profileKey.includes('/') && profileKey.includes('.')) {
      //   let fileNames = profileKey.split('.')
      //   let fileName = fileNames[0]
      //   let fileExtension = fileNames[1]
      //   checkKey1 = `img/${fileName}/${fileName}.${fileExtension}`
      //   checkKey2 = `img/source/${fileName}.${fileExtension}`
      // }

      const width = parameters.width
      const height = parameters.height

      if (width == null || height == null) {
        return response.result(400)
      }

      // for (;;) {
      //   if (module.fileCheck(profileKey)) {
      //     break
      //   }
      //   if (checkKey1 == null) {
      //     response.result(404, null, '이미지가 존재하지 않습니다.')
      //   }

      //   if (profileKey == checkKey1) {
      //     profileKey = checkKey2
      //   } else if (profileKey == checkKey2) {
      //     response.result(404, null, '이미지가 존재하지 않습니다.')
      //   } else {
      //     profileKey = checkKey1
      //   }
      // }

      query = `
        select 
          fi.file_id, fi.org_file_nm
        from
          oi_user_profile_image pi 
            join oi_file_info fi on pi.file_id = fi.file_id
        where
          pi.user_id = ${userId}
          and pi.image_type_cd = 'PF';
      `
      result = await mysql.execute(query)
      if (result.length > 0) {
        let lastKey = result[0].org_file_nm
        if (lastKey != profileKey) {

          // let params = {
          //   FunctionName: `api-new-batch-${process.env.stage}-deleteS3Object`,
          //   InvocationType: "Event",
          //   Payload: JSON.stringify({
          //     body: JSON.stringify({
          //       bucket: process.env.mediaBucket,
          //       key: lastKey
          //     })
          //   })
          // }
          // await lambda.invoke(params).promise()
  
          let fileId = result[0].file_id
  
          query = `
            delete from
              oi_file_info
            where
              file_id = ${fileId};
          `
          await mysql.execute(query)
        }
      }

      console.log(profileKey)

      query = `
        insert into oi_file_info (
          media_type_cd, org_file_nm, width, height
        ) values (
          'IMAGE', '${profileKey}', ${width}, ${height}
        );
      `
      result = await mysql.execute(query)
      let fileId = result.insertId

      query = `
        insert into oi_user_profile_image (
          user_id, image_type_cd, file_id
        ) values (
          ${userId}, 'PF', ${fileId}
        ) on duplicate key update
          file_id = ${fileId};
      `
      await mysql.execute(query)

    } else {

      query = `
        select 
          fi.file_id, fi.org_file_nm
        from
          oi_user_profile_image pi 
            join oi_file_info fi on pi.file_id = fi.file_id
        where
          pi.user_id = ${userId}
          and pi.image_type_cd = 'PF';
      `
      result = await mysql.execute(query)
      if (result.length > 0) {
        let lastKey = result[0].org_file_nm
        let params = {
          FunctionName: `api-new-batch-${process.env.stage}-deleteS3Object`,
          InvocationType: "Event",
          Payload: JSON.stringify({
            body: JSON.stringify({
              bucket: process.env.mediaBucket,
              key: lastKey
            })
          })
        }
        await lambda.invoke(params).promise()

        let fileId = result[0].file_id

        query = `
          delete from
            oi_file_info
          where
            file_id = ${fileId};
        `
        await mysql.execute(query)
      }

      query = `
        delete from
          oi_user_profile_image
        where
          user_id = ${userId}
          and image_type_cd = 'PF'
      `
      await mysql.execute(query)
    }

    query = `
      select 
        relation_user_id
      from
        oi_user_relation
      where
        user_id = ${userId}
        and accpt_dt is not null;
    `
    result = await mysql.execute(query)

    let targetUserIds = result.map(row => {
      return row.relation_user_id
    })

    mysql.end()

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          targetUserIds: targetUserIds,
          messages: [
            {
              userId: userId,
              notificationType: 'PFIMAGE',
            }
          ]
        })
      })
    }
    console.log(params)

    await lambda.invoke(params).promise()

    let logParams = {
      FunctionName: `api-new-batch-${process.env.stage}-logActivity`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId,
          mileage: 5,
          type: 'PFIMAGE',
          targetType: 'USER',
          targetId: userId,
          log: '✨샤방~ 프로필 사진을 업로드 하였습니다.'
        })
      })
    }
    console.log(logParams)

    await lambda.invoke(logParams).promise()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
