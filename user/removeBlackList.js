'use strict';

const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 블랙리스트 설정
 * @method DELETE removeBlackList
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {
    
    let targetUserId = event.pathParameters.targetUserId
    let ci = event.queryStringParameters.ci

    let writable = await redis.writable(4)
    let key = `blackList:ci:${ci}`
    console.log(key)
    await writable.del(key)
    
    redis.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
