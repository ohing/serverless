'use strict';

const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 블랙리스트 여부 확인
 * @method DELETE isBlackList
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {
    
    let ci = event.queryStringParameters.ci

    let readable = await redis.readable(4)
    let key = `blackList:ci:${ci}`
    console.log(key)
    let blackListInfo = await readable.get(key)
    console.log(blackListInfo)

    redis.end()

    return response.result(200, {isBlackList: blackListInfo != null})
  
  } catch(e) {

    console.error(e)

    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
