'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 찜 목록
 * @method GET getFavoriteList
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let parameters = event.queryStringParameters || {}

    const rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    const lastAccountId = parameters.lastAccountId || ''
    const searchText = parameters.searchText
    let searchTextCondition = searchText == null ? '' : `
      and (
        ui.user_login_id like '%${searchText}%' 
        or ui.user_nick_nm like '%${searchText}%'
        or ti.tag_val like '%${searchText}%'
      )
    `

    let query = `
      select
        ui.user_id, ui.user_login_id account_id, user_nick_nm nickname,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        case when ur0.user_id is not null and ur0.accpt_dt is not null then 1 else 0 end following,
        case when ur0.user_id is not null and ur0.accpt_dt is null then 1 else 0 end requested
      from
        oi_user_relation ur
        left join oi_user_info ui on ur.user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_user_tag ut on ui.user_id = ut.user_id
        left join oi_tag_info ti on ut.tag_id = ti.tag_id
        left join oi_user_relation ur0 on ur.user_id = ur0.user_id and ur.relation_user_id = ur0.relation_user_id and ur0.relation_type_cd = '01'
      where
        ur.relation_user_id = ${userId}
        and ur.relation_type_cd = '05'
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and ui.user_login_id > '${lastAccountId}'
        and ur0.accpt_dt is null
        ${searchTextCondition}
      group by
        ui.user_id
      order by
        ui.user_login_id asc
      limit ${rowCount};
    `
    console.log(query)

    let result = await mysql.execute(query)

    let userIds = []
    let userMap = {}

    result.forEach(row => {
      
      row.following = row.following != 0
      row.requested = row.requested != 0
      row.jjim = true
      row.tags = []

      userIds.push(row.user_id)
      userMap[row.user_id] = row

      return row
    })

    let users
    console.log(userMap)

    if (result.length > 0) {
      query = `
        select
          ut.user_id, ti.tag_val
        from
          oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
        where
          ut.user_id in (${userIds.join(',')})
        order by 
          ut.user_id asc,
          ut.tag_seq asc;
      `
      result = await mysql.execute(query)

      result.forEach(row => {

        let tag = row.tag_val
  
        if (tag != null && tag.length > 0) {
          userMap[row.user_id].tags.push(tag)
        }
      })

      users = Object.values(userMap)
      users.sort((lhs, rhs) => {
        return lhs.account_id > rhs.account_id ? 1 : lhs.account_id < rhs.account_id ? -1 : 0
      })

      let onlineKeys = users.map(user => {
        return `alive:${user.user_id}`
      })
      
      let readable = await redis.readable(3)
      let onlines = await readable.mget(onlineKeys) 
      redis.end()

      for (let i = 0; i < onlines.length; i++) {
        users[i].is_online = onlines[i] != null
      }

    } else {
      users = []
    }

    mysql.end()

    return response.result(200, users)
  
  } catch(e) {

    console.error(e)
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}