'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const search = require(`@ohing/ohingsearch`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 상대방 차단
 * @method GET getBlockList
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }
    
    // const parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    const rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    const lastAccountId = parameters.lastAccountId || ''

    let searchText = (parameters.searchText || '').trim()
    let searchInfo = search.parse(searchText)
    let searchCondition = search.searchCondition(searchInfo, 'USER')

    let query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name
      from
        oi_user_relation ur
        join oi_user_info ui on ur.user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_user_tag ut on ui.user_id = ut.user_id
        left join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ur.relation_user_id = ${userId}
        and ur.relation_type_cd = '02'
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and ui.user_login_id > '${lastAccountId}'
        ${searchCondition}
      order by
        ui.user_login_id asc
      limit ${rowCount};

    `
    let result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(200, [])
    }

    let blocks = result.map(row => {
      row.tags = []
      return row
    })
    let userIds = blocks.map(block => {
      return block.user_id
    })
    let userIdsString = userIds.join(',')

    let blockMap = {}
    blocks.forEach(block => {
      blockMap[block.user_id] = block
    })
    
    query = `
      select
        ut.user_id, ti.tag_val
      from
        oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ut.user_id in (${userIdsString})
      order by 
		    ut.user_id asc,
        ut.tag_seq asc;
    `
    result = await mysql.execute(query)
    mysql.end()

    result.forEach(row => {

      let tag = row.tag_val

      if (tag != null && tag.length > 0) {
        blockMap[row.user_id].tags.push(tag)
      }
    })

    mysql.end()

    blocks = Object.values(blockMap)
    blocks.sort((lhs, rhs) => {
      return lhs.account_id > rhs.account_id ? 1 : lhs.account_id < rhs.account_id ? -1 : 0
    })

    let onlineKeys = blocks.map(block => {
      return `alive:${block.user_id}`
    })
    
    let readable = await redis.readable(3)
    let onlines = await readable.mget(onlineKeys) 
    redis.end()

    for (let i = 0; i < onlines.length; i++) {
      blocks[i].is_online = onlines[i] != null
    }

    redis.end()

    return response.result(200, blocks)
  
  } catch(e) {

    console.error(e)
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
