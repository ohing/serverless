'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 팔로우 요청
 * @method PUT followRequest
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const targetUserId = event.pathParameters.targetUserId

    if (userId == targetUserId) {
      return response.result(403, null, '스스로에게 요청할 수 없습니다.')
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 팔로우 요청하실 수 없습니다.')
    }

    query = `
      select
        profile_open_type_cd, user_status_cd
      from
        oi_user_info
      where
        user_id = ${targetUserId};
    `
    result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404, null, '회원 정보를 찾을 수 없습니다.')
    }

    const status = result[0].user_status_cd

    if (status != 'ACTIVE') {
      mysql.end()
      return response.result(404, null, '회원 정보를 찾을 수 없습니다.')
    }

    openType = result[0].profile_open_type_cd

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(404, null, '회원 정보를 찾을 수 없습니다.')
    }
 
    let following = false
    let requested = false

    if (openType == 'OPEN') {
      query = `
        insert into oi_user_relation (
          user_id, relation_user_id, relation_type_cd, req_dt, accpt_dt
        ) values (
          ${targetUserId}, ${userId}, '01', now(), now()
        ) on duplicate key update
          req_dt = now(), accpt_dt = now();
      `
      result = await mysql.execute(query)
      following = true

      query = `
        delete from
          oi_user_relation
        where
          user_id = ${targetUserId} and relation_user_id = ${userId} and relation_type_cd = '05';
      `
      await mysql.execute(query)
      
    } else if (openType == 'FRIEND') {
      query = `
        insert into oi_user_relation (
          user_id, relation_user_id, relation_type_cd, req_dt
        ) values (
          ${targetUserId}, ${userId}, '01', now()
        ) on duplicate key update
          req_dt = now()
      `
      result = await mysql.execute(query)
      requested = true
      
    } else {
      mysql.end()
      return response.result(403, null, '비공개 회원은 팔로우할 수 없습니다.')
    }

    mysql.end()

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          targetUserIds: [targetUserId],
          messages: [
            {
              userId: userId,
              notificationType: following ? 'FOLLOW' : 'FOLLOWREQ', 
            }
          ]
        })
      })
    }
    console.log(params)

    await lambda.invoke(params).promise()

    return response.result(200, {following: following, followRequested: requested})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
