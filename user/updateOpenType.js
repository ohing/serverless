'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 공개상태 변경
 * @method PUT updateOpenType
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    const openType = parameters.openType

    if (openType == null || !(openType == "OPEN" || openType == "FRIEND" || openType == "CLOSE")) {
      return response.result(400)
    }

    let query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `

    let result = await mysql.execute(query)
    let currentOpenType = result[0].open_type

    if (currentOpenType == openType) {
      return response.result(200, {openType: openType})
    }

    if (currentOpenType == 'CLOSE' || openType == 'CLOSE') {

      let writable = await redis.writable(6)

      let openTypeChangeKey = `open:changed:${userId}`

      let possibleTime = await writable.get(openTypeChangeKey)
      let currentTime = (new Date()).getTime() / 1000

      console.log(possibleTime, currentTime)

      if (possibleTime != null) {
        // let date = new Date(possibleTime)
        
        // let month = (date.getMonth()+1).toString().padStart(2, '0')
        // let day = date.getDate().toString().padStart(2, '0')
        // let hour = date.getHours().toString().padStart(2, '0')
        // let minute = date.getMinutes().toString().padStart(2, '0')
        // let month = date.getMonth()+1
        // let day = date.getDate()
        // let hour = date.getHours()
        // let minute = date.getMinutes()
        // return response.result(403, null, `아직 변경하실 수 없습니다. 변경 가능 시간: ${year}년 ${month}월 ${day}일 ${hour}시 ${minute}분`)

        let seconds = possibleTime - currentTime
        let timeString
        if (seconds < 60) {
          timeString = `${seconds}초`
        } else {
          let minutes = parseInt(seconds / 60)
          timeString = `${minutes}분`
        }

        mysql.end()
        redis.end()

        console.log('시간제한')
        return response.result(403, null, `프로필 공개범위 재변경은 ${timeString} 이후 가능합니다.`)
      }

      possibleTime = currentTime + 3600

      await writable.set(openTypeChangeKey, possibleTime)
      await writable.expire(openTypeChangeKey, 3600)

      redis.end()
    }
    
    query = `
      update
        oi_user_info
      set
        profile_open_type_cd = '${openType}'
      where
        user_id = ${userId};
    `
    console.log(query)
    await mysql.execute(query)

    mysql.end()

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-applyOpenType`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          userId: userId
        })
      })
    }

    await lambda.invoke(params).promise()

    return response.result(200, {openType: openType})
  
  } catch(e) {

    console.error(e)
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
