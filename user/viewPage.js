'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 유저 페이지 조회
 * @method GET viewPage
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const targetUserId = event.pathParameters.targetUserId

    let query = `
      select
        profile_open_type_cd, user_status_cd
      from
        oi_user_info
      where
        user_id = ${targetUserId};
    `
    let result = await mysql.execute(query)
    if (result.length == 0) {
      mysql.end()
      return response.result(404, null, '회원 정보를 찾을 수 없습니다.')
    }

    let openType = result[0].profile_open_type_cd
    let status = result[0].user_status_cd

    let finalResult = {
      status: status,
      openType: openType
    }

    let following = false
    let followed = false
    let requested = false
    let blocking = false
    let blocked = false

    if (targetUserId != userId) {

      let message

      if (status != 'ACTIVE') {
        message = status == 'BLOCK' ? '차단된 회원입니다.' : '탈퇴한 회원입니다.'
      } else if (openType == 'CLOSE') {
        message = '비공개 회원입니다.'
      }

      query = `
        select
          user_id, relation_type_cd, accpt_dt
        from
          oi_user_relation
        where
          (user_id = ${targetUserId} and relation_user_id = ${userId}) or (user_id = ${userId} and relation_user_id = ${targetUserId})
          and relation_type_cd in ('01', '02');
      `
      result = await mysql.execute(query)

      console.log(result)

      result.forEach(row => {
        if (row.user_id == userId) {
          if (row.relation_type_cd == '02') {
            blocked = true
          } else if (row.relation_type_cd == '01') {
            if (row.accpt_dt != null) {
              followed = true
            }
          }
        } else {
          if (row.relation_type_cd == '02') {
            blocking = true
          } else if (row.relation_type_cd == '01') {
            if (row.accpt_dt != null) {
              following = true
            } else {
              requested = true
            }
          }
        }
      })

      if (blocking) {
        finalResult.status = 'BLOCKING'
        message = '차단된 회원입니다.'
      }

      if (blocked) {
        finalResult.openType = 'CLOSE'
        message = '비공개 회원입니다.'
      }

      if (message != null) {
        mysql.end()
        return response.result(200, finalResult, message)
      }
    }

    if (targetUserId == userId || finalResult.openType == 'OPEN') {
      finalResult.user_info = await module.userInfo(mysql, targetUserId, userId)
    } else {
      if (following) {
        finalResult.user_info = await module.userInfo(mysql, targetUserId, userId)
      } else {
        query = `
          select 
            following.count following_count, follower.count follower_count, user_info.*, feed.count feed_count
          from
            (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.relation_user_id = ui.user_id where 
              ur.user_id = ${targetUserId} and ur.relation_type_cd = '01' and ur.accpt_dt is not null and ui.profile_open_type_cd != 'CLOSE') following,
            (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.user_id = ui.user_id where 
              ur.relation_user_id = ${targetUserId} and ur.relation_type_cd = '01' and ur.accpt_dt is not null and ui.profile_open_type_cd != 'CLOSE') follower,
            (select count(*) count from oi_feed_info where reg_user_id = ${targetUserId}) feed,
            (select 
              ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname, ui.region_cd, ui.self_intro_content self_introduction, 
              concat(ui.birth_y, '.', left(ui.birth_md, 2), '.', right(ui.birth_md, 2)) birthday, sex,
              fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
              fi2.org_file_nm background_image_url, substring_index(fi2.org_file_nm, '/', -1) background_file_name,
              ur.user_id jjim
            from 
              oi_user_info ui
              left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
              left join oi_file_info fi on pi.file_id = fi.file_id
              left join oi_user_profile_image pi2 on ui.user_id = pi2.user_id and pi2.image_type_cd = 'BG'
              left join oi_file_info fi2 on pi2.file_id = fi2.file_id
              left join oi_user_relation ur on ui.user_id = ur.user_id and ur.relation_user_id = ${userId} and ur.relation_type_cd = '05'
            where 
              ui.user_id = ${targetUserId}
            ) user_info;
        `
        result = await mysql.execute(query)
        if (result.length > 0) {
          finalResult.user_info = result[0]
        }
      }
    }

    if (finalResult.user_info) {

      // 태그
      query = `
        select
          ti.tag_val
        from
          oi_tag_info ti
          join oi_user_tag ut on ti.tag_id = ut.tag_id
        where
          ut.user_id = ${targetUserId}
        order by
          ut.tag_seq asc;
      `
      result = await mysql.execute(query)
      finalResult.user_info.tags = result.map(row => {
        return row.tag_val
      })

      if (finalResult.user_info.profile_image_url == '') {
        finalResult.user_info.profile_image_url = null
      }
      if (finalResult.user_info.profile_image_url == '') {
        finalResult.user_info.profile_image_url = null
      }
      if (finalResult.user_info.grade == '') {
        finalResult.user_info.grade = null
      }
      if (finalResult.user_info.class_ == '') {
        finalResult.user_info.class_ = null
      }

      finalResult.user_info.following = following
      finalResult.user_info.followed = followed
      finalResult.user_info.requested = requested
      
      finalResult.user_info.jjim = finalResult.user_info.jjim != null

      // 온라인 상태
      let readable = await redis.readable(3)
      let onlineKey = `alive:${targetUserId}`
      let isOnline = await readable.get(onlineKey) || false
      finalResult.user_info.is_online = isOnline
      redis.end()

      finalResult.room_id = (blocking || blocked) ? null : await module.roomId(targetUserId, userId)

      console.log('roomId', finalResult.room_id)

      // 학교
      query = `
        select 
          school_hist_cd, school_id, school_nm, school_short_nm, grade, class classe
        from
          oi_user_school_history
        where
          user_id = ${targetUserId}
        order by
          field(school_hist_cd, 'CURR') desc;
      `
      result = await mysql.execute(query)
      result = result.map(row => {
        row.is_current = row.school_hist_cd == 'CURR'
        delete row.school_hist_cd
        return row
      })

      finalResult.schools = result

      // 팔로워 
      query = `
        select 
          ur.relation_user_id user_id, fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name
        from
          oi_user_relation ur 
          inner join oi_user_relation ur2 on ur.relation_user_id = ur2.user_id
          join oi_user_info ui on ur.relation_user_id = ui.user_id
          left join oi_user_profile_image pi on ur.relation_user_id = pi.user_id and pi.image_type_cd = 'PF'
          left join oi_file_info fi on pi.file_id = fi.file_id
        where
          ur.user_id = ${targetUserId}
          and ur.relation_type_cd = '01'
          and ur.accpt_dt is not null
          and ur2.relation_user_id = ${targetUserId}
          and ur2.relation_type_cd = '01'
          and ur2.accpt_dt is not null
        group by
          ur.user_id, ur.relation_user_id
        order by
          ui.user_nick_nm asc
        limit 
          11;
      `
      let followers = await mysql.execute(query)

      if (result.length >= 10) {

        finalResult.followers = followers.slice(0, 10)
        finalResult.hasMoreFollowers = followers.length = 11

      } else {

        let needsMore = 10-followers.length
        let notInUserIdsString = ''

        if (followers.length > 0) {

          let notInUserIds = followers.map(follower => {
            return follower.user_id
          })
          notInUserIdsString = `not in (${notInUserIds.join(',')})`
        }

        query = `
          select
            ur.relation_user_id user_id, fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
            count(*) follower_cnt
          from
            oi_user_relation ur
            inner join oi_user_relation ur2 on ur.relation_user_id = ur2.user_id  
            join oi_user_info ui on ur.relation_user_id = ui.user_id
            left join oi_user_profile_image pi on ur.relation_user_id = pi.user_id and pi.image_type_cd = 'PF'
            left join oi_file_info fi on pi.file_id = fi.file_id
          where
            ur2.relation_type_cd = '01'
            and ur2.accpt_dt is not null
            and ur.user_id = ${targetUserId}
            and ur.relation_user_id ${notInUserIdsString}
            and ur.relation_type_cd = '01'
            and ur.accpt_dt is not null
          group by
            ur.user_id, ur.relation_user_id
          order by
            follower_cnt desc, 
            ui.user_nick_nm asc
          limit 
            ${needsMore+1};
        `
        result = await mysql.execute(query)
        result = result.map(row => {
          delete row.follower_cnt
          return row
        })

        followers = followers.concat(result)
        finalResult.hasMoreFollowers = followers.length == 11
        finalResult.followers = followers.slice(0, 10)
      }

      // 미디어

      let anonymousCondition = targetUserId == userId ? '' : `
        and f.anony_yn = 'N' 
      `

      query = `
        select
          media.*, count(media.media_seq) media_count
        from
          (select
            mi.feed_id, mi.media_seq, fi.media_type_cd, fi.org_file_nm media_url, substring_index(fi.org_file_nm, '/', -1) media_file_name,
            mi.reg_dt
          from 
            oi_feed_media_info mi
            join oi_feed_info f on mi.feed_id = f.feed_id
            join oi_file_info fi on mi.file_id = fi.file_id
          where
            mi.reg_user_id = ${targetUserId} 
            ${anonymousCondition}
            and mi.media_seq = 1
            and fi.encoded_yn = 'Y'
          order by
            mi.reg_dt desc
          limit
            12) media
          left join oi_feed_media_info mi on media.feed_id = mi.feed_id
        group by
          media.feed_id
        order by
          media.feed_id desc;
      `
      result = await mysql.execute(query)
      let medias = result.map(row => {
        delete row.media_seq
        return row
      })

      finalResult.medias = medias
    }

    query = `
      insert into oi_user_profile_view_log (
        user_id, request_user_id, request_dt
      ) values (
        ${targetUserId}, ${userId}, now()
      ) on duplicate key update
      request_dt = now();
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, finalResult)
  
  } catch(e) {

    console.error(e)
    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}

module.userInfo = async (mysql, targetUserId, userId) => {
  try {
    let query = `
      select 
        following.count following_count, follower.count follower_count, user_info.*, feed.count feedCount
      from
        (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.relation_user_id = ui.user_id where 
          ur.user_id = ${targetUserId} and ur.relation_type_cd = '01' and ur.accpt_dt is not null and ui.profile_open_type_cd != 'CLOSE') follower,
        (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.relation_user_id = ui.user_id where 
          ur.user_id = ${targetUserId} and ur.relation_type_cd = '01' and ur.accpt_dt is null and ui.profile_open_type_cd != 'CLOSE') requested,
        (select count(*) count from oi_user_relation ur join oi_user_info ui on ur.user_id = ui.user_id where 
          ur.relation_user_id = ${targetUserId} and ur.relation_type_cd = '01' and ur.accpt_dt is not null and ui.profile_open_type_cd != 'CLOSE') following,
        (select count(*) count from oi_feed_info where reg_user_id = ${targetUserId}) feed,
        (select 
          ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname, ui.region_cd, ui.self_intro_content self_introduction, 
          concat(ui.birth_y, '.', left(ui.birth_md, 2), '.', right(ui.birth_md, 2)) birthday, sex,
          fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
          fi2.org_file_nm background_image_url, substring_index(fi2.org_file_nm, '/', -1) background_file_name,
          ur.user_id jjim
        from 
          oi_user_info ui
          left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
          left join oi_file_info fi on pi.file_id = fi.file_id
          left join oi_user_profile_image pi2 on ui.user_id = pi2.user_id and pi2.image_type_cd = 'BG'
          left join oi_file_info fi2 on pi2.file_id = fi2.file_id
          left join oi_user_relation ur on ui.user_id = ur.user_id and ur.relation_user_id = ${userId} and ur.relation_type_cd = '05'
        where 
          ui.user_id = ${targetUserId}
        ) user_info;
    `
    console.log('qqq', query)
    let result = await mysql.execute(query)
    console.log(result)
    if (result.length > 0) {
      return result[0]
    }
  } catch (e) {
    console.error(e)
  }

  return null
}

module.roomId = async (targetUserId, userId) => {

  try {
    let query = `
      select 
        cri.room_id
      from 
        oi_chat_user_info cui
        join oi_chat_user_info cui2 on cui.room_id = cui2.room_id
        join oi_chat_room_info cri on cui.room_id = cri.room_id
      where
        cui.user_id = ${userId}
        and cui2.user_id = ${targetUserId}
        and cri.room_type_cd = 'ONE';
    `
    console.log(query)
    let result = await mysql.execute(query)
    console.log(result)

    if (result.length > 0) {
      return result[0].room_id
    } else {

      query = `
        insert into oi_chat_room_info (
          room_type_cd, max_user_cnt, join_user_cnt, reg_user_id
        ) values (
          'ONE', 0, 2, ${userId}
        );
      `
      
      result = await mysql.execute(query)
      let roomId = result.insertId

      query = `
        insert into oi_chat_user_info (
          room_id, user_id, join_dt, master_yn, use_yn
        ) values (
          ${roomId}, ${userId}, now(), 'Y', 'N'
        ), (
          ${roomId}, ${targetUserId}, now(), 'N', 'N'
        );
      `
      await mysql.execute(query)

      return roomId

    }
  } catch (e) {
    console.error(e)
  }

  return null
}