'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 찜
 * @method PUT favorite
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const targetUserId = event.pathParameters.targetUserId

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 상대를 찜 하실 수 없습니다.')
    }
    
    query = `
      select 
        user_status_cd status, profile_open_type_cd open_type, ur.user_id following
      from
        oi_user_info ui
        left join oi_user_relation ur on ui.user_id = ur.user_id and ur.relation_user_id = ${userId} and ur.relation_type_cd = '01' and ur.accpt_dt is not null
      where
        ui.user_id = ${targetUserId};
    `
    result = await mysql.execute(query)
    let status = result[0].status
    openType = result[0].open_type

    if (status != 'ACTIVE' || openType == 'CLOSE') {
      mysql.end()
      return response.result(200, {success: false}, '비공개 상태이거나 탈퇴한 회원은 찜 하실 수 없습니다.') 
    }

    let following = result[0].following != null

    if (following) {
      mysql.end()
      return response.result(200, {success: false}, '팔로우 중인 회원은 찜 하실 수 없습니다.') 
    }

    query = `
      insert ignore into oi_user_relation (
        user_id, relation_user_id, relation_type_cd, req_dt
      ) values (
        ${targetUserId}, ${userId}, '05', now() 
      );
    `
    await mysql.execute(query)

    mysql.end()

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          targetUserIds: [targetUserId],
          messages: [
            {
              userId: userId,
              notificationType: 'JJIM',
            }
          ]
        })
      })
    }
    console.log(params)

    await lambda.invoke(params).promise()

    return response.result(200, {success: true})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}