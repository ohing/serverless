'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 학교 목록 조회
 * @method GET getSchools
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const targetUserId = event.pathParameters.targetUserId

    let query = `
      select 
        sh.school_id, sh.school_nm, sh.school_short_nm, sh.grade, sh.class classe, sh.school_hist_cd is_current
      from
        oi_user_school_history sh
      where
        sh.user_id = ${targetUserId}
      order by 
        field(sh.school_hist_cd, 'CURR') desc; 
    `
    let result = await mysql.execute(query)
    result = result.map(row => {
      row.is_current = row.is_current == 'CURR'
      return row
    })

    mysql.end()

    return response.result(200, result)
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}