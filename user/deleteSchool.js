'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 학교 기록 삭제
 * @method delete deleteSchool
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    const isCurrent = parameters.isCurrent

    if (isCurrent == null) {
      return response.result(400)
    }

    let currentCode = isCurrent ? 'CURR' : 'BEFO'

    let query
    let result
    
    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    // if (openType == 'CLOSE') {
    //   mysql.end()
    //   return response.result(403, null, '비공개 회원은 프로필 정보를 수정하실 수 없습니다.')
    // }

    query = `
      delete from 
        oi_user_school_history
      where
        school_hist_cd = '${currentCode}' and user_id = ${userId};
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}