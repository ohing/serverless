'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`);
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable();
const fcm = require(`@ohing/ohingfcm-${process.env.stage}`);
const response = require('@ohing/ohingresponse');

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-northeast-2' });
const lambda = new AWS.Lambda();

/**
 * @author Karl <karl@ohing.net> 
 * @description 관심사 태그 변경
 * @method put updateTags
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event);

  try {

    const imei = event.headers.imei;
    const osType = event.headers['os-type'];
    const appVersion = event.headers['app-ver'];
    const accessToken = event.headers['access-token'];

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400);
    }

    const userId = await authorizer.userId(accessToken);
    if (userId == null) {
      return response.result(401);
    }

    const parameters = JSON.parse(event.body || '{}') || {};
    const tags = parameters.tags;

    if (tags == null || !Array.isArray(tags) || tags.length == 0) {
      return response.result(400, null, '적어도 하나의 관심사를 입력해 주세요.');
    }

    let query;
    let result;

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `;
    result = await mysql.execute(query);
    let openType = result[0].open_type;

    // if (openType == 'CLOSE') {
    //   mysql.end()
    //   return response.result(403, null, '비공개 회원은 프로필 정보를 수정하실 수 없습니다.')
    // }

    if (tags.length > 20) {
      tags = tags.slice(0, 20);
    }

    let values = [];
    for (let tag of tags) {
      values.push(`
        ( ?, 1 )
      `);
    }
    let valuesString = values.join(',');

    query = `
      insert into oi_tag_info (
        tag_val, tag_cnt
      ) values 
        ${valuesString}
      on duplicate key update
        tag_cnt = values(tag_cnt)+1
    `;
    await mysql.execute(query, tags);

    let tagValues = [];
    let tagValueParameters = [];

    for (let tag of tags) {
      tagValues.push('?');
    }

    tagValues = tagValues.join(',');

    query = `
      select
        tag_id, tag_val
      from
        oi_tag_info
      where
        tag_val in (${tagValues});
    `;
    console.log(query);
    result = await mysql.execute(query, tags);

    let tagInfos = tags.map(tag => {
      return { tag_val: tag };
    });

    for (let i = 0; i < tagInfos.length; i++) {
      let filtered = result.filter(row => {
        return row.tag_val == tagInfos[i].tag_val;
      });
      tagInfos[i].tag_id = filtered[0].tag_id;
    }

    let tagIds = tagInfos.map(tagInfo => {
      return tagInfo.tag_id;
    });

    query = `
      delete from
        oi_user_tag
      where
        user_id = ${userId};
    `;
    await mysql.execute(query);

    let seq = 0;
    values = tagIds.map(tagId => {
      seq++;
      return `( ${userId}, ${tagId}, ${seq} )`;
    });
    valuesString = values.join(',');

    query = `
      insert into oi_user_tag (
        user_id, tag_id, tag_seq
      ) values 
        ${valuesString};
    `;
    await mysql.execute(query);

    query = `
      select
        push_auth_key
      from oi_user_device_info
      where 
        user_id = ${userId} and login_yn = 'Y'
        and push_auth_key is not null and push_auth_key  <> ''
      limit 
        1;
    `;
    result = await mysql.execute(query);
    console.log(result);

    query = `
      select 
        relation_user_id
      from
        oi_user_relation
      where
        user_id = ${userId}
        and accpt_dt is not null;
    `;
    result = await mysql.execute(query);

    let targetUserIds = result.map(row => {
      return row.relation_user_id;
    });

    mysql.end();

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          targetUserIds: targetUserIds,
          messages: [
            {
              userId: userId,
              notificationType: 'PROFILE',
            }
          ]
        })
      })
    };
    console.log(params);

    await lambda.invoke(params).promise();

    mysql.end();

    if (result.length > 0) {
      let fcmToken = result[0].push_auth_key;
      const subscriptions = await fcm.subscriptions(fcmToken);

      let subscribedTopics = subscriptions.filter(subscription => {
        return subscription.startsWith('tag-');
      });

      let topics = tagIds.map(tagId => {
        return `tag-${tagId}`;
      });

      let unsubscribes = subscribedTopics.filter(topic => {
        return !topics.includes(topic);
      });

      let subscribes = topics.filter(topic => {
        return !subscribedTopics.includes(topic);
      });

      let fcmSteps = [];

      if (unsubscribes.length > 0) {
        fcmSteps.push({
          type: 'unsubscribe',
          tokens: [fcmToken],
          topics: unsubscribes
        });
      }

      if (subscribes.length > 0) {
        fcmSteps.push({
          type: 'subscribe',
          tokens: [fcmToken],
          topics: subscribes
        });
      }

      if (fcmSteps.length > 0) {
        await lambda.invoke({
          FunctionName: `api-new-batch-${process.env.stage}-processFCMSubscribe`,
          InvocationType: "Event",
          Payload: JSON.stringify({
            body: JSON.stringify({
              steps: fcmSteps
            })
          })
        }).promise();
      }
    }

    return response.result(200, {});

  } catch (e) {

    console.error(e);
    mysql.end();

    if (e.statusCode != null) {
      return e;
    } else {
      return response.result(403, e);
    }
  }
};