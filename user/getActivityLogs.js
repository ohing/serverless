'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 활동내역 로그 조회
 * @method get getActivityLogs
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    // let parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }
    
    const rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    let lastId = parameters.lastId
    let searchText = (parameters.searchText || '').trim().toLowerCase()
    let lastIdCondition = ''
    if (lastId != null) {
      lastIdCondition = `and log_id < ${lastId}`
    }
    let searchTextCondition = ''
    if (searchText.length > 0) {
      searchTextCondition = `
        and (
          ui.user_login_id like '%${searchText}%'
          or ual.action_log like '%${searchText}%'
        )
      `
    }

    let baseUrl = process.env.stage == 'dev' ? 'https://d2ro0vwgwjzuou.cloudfront.net/icon/activity/' : 'https://d257x8iyw0ju12.cloudfront.net/icon/activity/'

    let query = `
      select 
        ual.log_id, ual.mileage, ual.log_type_cd log_type, ual.target_type_cd target_type, ual.target_id, ual.action_log,
        ual.link_info, ual.reg_dt created_at,
        concat(
          '${baseUrl}',
          case 
            when ual.log_type_cd = 'SIGNUP' or ual.log_type_cd = 'PFIMAGE' or ual.log_type_cd = 'BGIMAGE' or ual.log_type_cd = 'OLDBIE' then 'account.png'
            when ual.log_type_cd like 'RP%' then 'report.png'
            when ual.log_type_cd = 'CHGMASTER' or ual.log_type_cd like '%CHAT' then 'chat.png'
            when ual.log_type_cd = 'WRITEFEED' or ual.log_type_cd = 'WRITEHELP' or ual.log_type_cd = 'DELETEFEED' then 'feed.png'
            when ual.log_type_cd = 'BANNERTAP' then 'ad.png'
            when ual.log_type_cd like '%LIKE' then 'like.png'
            when ual.log_type_cd = 'COMMENT' or ual.log_type_cd = 'DELETECOMT' then 'comment.png'
            else null
          end) icon_image_url
      from
        oi_user_action_log ual
        join oi_user_info ui on ual.user_id = ui.user_id
      where
        ual.user_id = ${userId}
        ${searchTextCondition}
        ${lastIdCondition}
      order by 
        log_id desc
      limit
        ${rowCount};
    `
    console.log(query)
    let result = await mysql.execute(query)

    mysql.end()

    return response.result(200, result)
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}