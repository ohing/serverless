'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const search = require(`@ohing/ohingsearch`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 팔로잉 리스트
 * @method get getFollowings
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const targetUserId = event.pathParameters.targetUserId

    // const parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }
    
    const rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    const lastAccountId = parameters.lastAccountId || ''

    let searchText = (parameters.searchText || '').trim()
    let searchInfo = search.parse(searchText)
    let searchCondition = search.searchCondition(searchInfo, 'USER')

    let roomId = parameters.roomId

    let query
    let result

    let exceptUserIds = []

    if (roomId != null) {

      query = `
        select 
          user_id 
        from 
          oi_chat_user_info
        where
          room_id = ${roomId}
          and user_id != ${userId};
      `
      result = await mysql.execute(query)

      if (result.length > 0) {
        exceptUserIds = result.map(row => {
          return row.user_id
        })
      }
    }
    console.log('exceptUserIds', exceptUserIds)
    
    query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        ur4.user_id jjim
      from
        oi_user_relation ur
        left join oi_user_info ui on ur.user_id = ui.user_id
        left join oi_user_school_history sh on ui.user_id = sh.user_id and sh.school_hist_cd = 'CURR'
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_user_relation ur4 on ur4.user_id = ur.user_id and ur4.relation_user_id = ${userId} and ur4.relation_type_cd = '05'
        left join oi_user_tag ut on ui.user_id = ut.user_id
        left join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ur.relation_user_id = ${targetUserId}
        and ur.relation_type_cd = '01'
        and ur.accpt_dt is not null
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and ui.user_login_id > '${lastAccountId}'
        ${searchCondition}
      group by
        ui.user_id
      order by
        ui.user_login_id asc
      limit ${rowCount};
    `
    console.log(query)
    result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      return response.result(200, [])
    }
    let followings = result.map(row => {
      row.tags = []
      row.following = false
      row.requested = false
      row.jjim = row.jjim != null
      return row
    })
    let userIds = followings.map(following => {
      return following.user_id
    })
    let userIdsString = userIds.join(',')

    let followingMap = {}
    followings.forEach(following => {
      followingMap[following.user_id] = following
    })

    query = `
      select
        user_id, accpt_dt
      from
        oi_user_relation
      where 
        relation_user_id = ${userId}
        and relation_type_cd = '01'
        and user_id in (${userIdsString})
    `
    result = await mysql.execute(query)

    result.forEach(row => {
      followingMap[row.user_id].following = row.accpt_dt != null
      followingMap[row.user_id].requested = row.accpt_dt == null
    })

    query = `
      select
        ut.user_id, ti.tag_val
      from
        oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ut.user_id in (${userIdsString})
      order by 
		    ut.user_id asc,
        ut.tag_seq asc;
    `
    result = await mysql.execute(query)
    mysql.end()

    result.forEach(row => {

      let tag = row.tag_val

      if (tag != null && tag.length > 0) {
        followingMap[row.user_id].tags.push(tag)
      }
    })

    followings = Object.values(followingMap)
    followings.sort((lhs, rhs) => {
      return lhs.account_id > rhs.account_id ? 1 : lhs.account_id < rhs.account_id ? -1 : 0
    })

    let onlineKeys = followings.map(user => {
        return `alive:${user.user_id}`
      })
      
    let readable = await redis.readable(3)
    let onlines = await readable.mget(onlineKeys) 
    redis.end()

    for (let i = 0; i < onlines.length; i++) {
      followings[i].is_online = onlines[i] != null
      
      let followingUserId = followings[i].user_id
      followings[i].included = exceptUserIds.includes(followingUserId)
    }
    
    return response.result(200, followings)
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}