'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 상대방 차단
 * @method PUT block
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const targetUserId = event.pathParameters.targetUserId

    if (userId == targetUserId) {
      return response.result(403, null, '스스로에게 요청할 수 없습니다.')
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 상대를 차단하실 수 없습니다.')
    }

    query = `
      insert ignore into oi_user_relation (
        user_id, relation_user_id, relation_type_cd, req_dt
      ) values (
        ${targetUserId}, ${userId}, '02', now()
      )
    `
    await mysql.execute(query)

    query = `
      delete from
        oi_user_relation 
      where
        relation_type_cd in ('01', '05', '06')
        and (
          user_id = ${userId} and relation_user_id = ${targetUserId}
          or user_id = ${targetUserId} and relation_user_id = ${userId}
        );
    `
    await mysql.execute(query)

    query = `
      select 
        cri.room_id
      from 
        oi_chat_user_info cui
        join oi_chat_user_info cui2 on cui.room_id = cui2.room_id
        join oi_chat_room_info cri on cui.room_id = cri.room_id
      where
        cui.user_id = ${userId}
        and cui2.user_id = ${targetUserId}
        and cri.room_type_cd = 'ONE';
    `
    result = await mysql.execute(query)
    for (let row of result) {
      let roomId = row.room_id

      query = `
        delete from
          oi_chat_user_info
        where
          room_id = ${roomId}
          and user_id = ${userId};
      `
      await mysql.execute(query)

      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-sendUpdatedRoomInfo`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            roomId: roomId,
          })
        })
      }

      await lambda.invoke(params).promise()
    }

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
