'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 현재 / 이전 학교 기록 동시 적용
 * @method put updateSchoolBulk
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }
    
    const parameters = JSON.parse(event.body || '{}') || {}

    let currentSchoolId = (parameters.currentSchoolId || '').trim()
    let currentSchoolName = (parameters.currentSchoolName || '').trim()
    let schoolShortName
    let currentGrade = (parameters.currentGrade || '').trim()
    let currentClasse = (parameters.currentClasse || '').trim()
    if (currentGrade.length > 0) {
      currentGrade = `${parseInt(currentGrade)}` || ''
    }
    if (currentClasse.length > 0) {
      currentClasse = `${parseInt(currentClasse)}` || ''
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 프로필 정보를 변경하실 수 없습니다.')
    }

    if (currentSchoolId.length > 0 || currentSchoolName.length > 0 || currentGrade.length > 0 || currentClasse.length > 0) {
      
      if (currentSchoolId.length > 0) {
        currentSchoolId = `'${currentSchoolId}'`
        query = `
          select
            school_nm, school_short_nm
          from
            oi_school_info
          where
            school_id = ${currentSchoolId};
        `
        result = await mysql.execute(query)
        if (result.length == 0) {
          mysql.end()
          return response.result(404, null, '학교 정보를 찾을 수 없습니다.')
        }
        currentSchoolName = await mysql.escape(result[0].school_nm)
        schoolShortName = await mysql.escape(result[0].school_short_nm)
      } else {
        currentSchoolId = 'null'
        currentSchoolName = await mysql.escape(currentSchoolName)
        schoolShortName = currentSchoolName
      }

      query = `
        insert into	oi_user_school_history (
            school_hist_cd, user_id, school_id, school_nm, school_short_nm, grade, class
        ) values (
          'CURR', ${userId}, ${currentSchoolId}, ${currentSchoolName}, ${schoolShortName},
          '${currentGrade}', '${currentClasse}'
        ) on duplicate key update
          school_id = ${currentSchoolId}, school_nm = ${currentSchoolName},
          school_short_nm = ${schoolShortName}, grade = '${currentGrade}', class = '${currentClasse}';
      `
      console.log(query)
      await mysql.execute(query)

    } else {

      query = `
        delete from
          oi_user_school_history
        where
          school_hist_cd = 'CURR'
          and user_id = ${userId}
      `
      await mysql.execute(query)
    }

    let previousSchoolId = (parameters.previousSchoolId || '').trim()
    let previousSchoolName = (parameters.previousSchoolName || '').trim()

    let previousGrade = (parameters.previousGrade || '').trim()
    let previousClasse = (parameters.previousClasse || '').trim()

    if (previousGrade.length > 0) {
      previousGrade = `${parseInt(previousGrade)}` || ''
    }
    if (previousClasse.length > 0) {
      previousClasse = `${parseInt(previousClasse)}` || ''
    }

    if (previousSchoolId.length > 0 || previousSchoolName.length > 0 || previousGrade.length > 0 || previousClasse.length > 0) {

      if (previousSchoolId.length > 0) {
        previousSchoolId = `'${previousSchoolId}'`
        query = `
          select
            school_nm, school_short_nm
          from
            oi_school_info
          where
            school_id = ${previousSchoolId};
        `
        result = await mysql.execute(query)
        if (result.length == 0) {
          mysql.end()
          return response.result(404, null, '학교 정보를 찾을 수 없습니다.')
        }
        previousSchoolName = await mysql.escape(result[0].school_nm)
        schoolShortName = await mysql.escape(result[0].school_short_nm)
      } else {
        previousSchoolId = 'null'
        previousSchoolName = await mysql.escape(previousSchoolName)
        schoolShortName = previousSchoolName
      }

      query = `
        insert into	oi_user_school_history (
            school_hist_cd, user_id, school_id, school_nm, school_short_nm, grade, class
        ) values (
          'BEFO', ${userId}, ${previousSchoolId}, ${previousSchoolName}, ${schoolShortName},
          '${previousGrade}', '${previousClasse}'
        ) on duplicate key update
          school_id = ${previousSchoolId}, school_nm = ${previousSchoolName},
          school_short_nm = ${schoolShortName}, grade = '${previousGrade}', class = '${previousClasse}';
      `
      await mysql.execute(query)

    } else {

      query = `
        delete from
          oi_user_school_history
        where
          school_hist_cd = 'BEFO'
          and user_id = ${userId}
      `
      await mysql.execute(query)
    }
    
    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}