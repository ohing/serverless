'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 팔로우 수락/거절
 * @method POST followAccept
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const targetUserId = event.pathParameters.targetUserId

    if (userId == targetUserId) {
      return response.result(403, null, '스스로에게 요청할 수 없습니다.')
    } 

    const parameters = JSON.parse(event.body || '{}') || {}
    const accept = parameters.accept

    if (accept == null || typeof accept != 'boolean') {
      return response.result(400)
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 팔로우 요청을 처리하실 수 없습니다.')
    }
    
    if (accept) {

      query = `
        update
          oi_user_relation
        set
          accpt_dt = now()
        where
          user_id = ${userId} and relation_user_id = ${targetUserId} and relation_type_cd = '01';
      `
      await mysql.execute(query)

      query = `
        delete from
          oi_user_relation
        where
          user_id = ${userId} and relation_user_id = ${targetUserId} and relation_type_cd = '05';
      `
      await mysql.execute(query)

    } else {

      query = `
        delete from
          oi_user_relation
        where
          user_id = ${userId} and relation_user_id = ${targetUserId} and relation_type_cd = '01';
      `
      await mysql.execute(query)
    }

    mysql.end()

    if (accept) {

      let params = {
        FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
        InvocationType: "Event",
        Payload: JSON.stringify({
          body: JSON.stringify({
            targetUserIds: [targetUserId],
            messages: [
              {
                userId: userId,
                notificationType: 'FOLLOWACPT',
              }
            ]
          })
        })
      }
      console.log(params)

      await lambda.invoke(params).promise()
    }

    return response.result(200, {followed: accept})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
