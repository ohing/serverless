'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 활동내역 카운트
 * @method get getActivityCounts
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query = `
    select
      *
    from
      (
        select 
          sum(mileage) point
        from 
          oi_user_action_log
        where
          user_id = ${userId}
      ) mileage,
        (
      select
        count(*) feed_count
      from
        oi_feed_info
      where
        reg_user_id = ${userId}
        and feed_type_cd = 'FEED'
        and encoded_yn = 'Y'
      ) feed,
        (
      select
        count(*) help_count
      from
        oi_feed_info
      where
        reg_user_id = ${userId}
        and feed_type_cd = 'HELP'
        and encoded_yn = 'Y'
      ) help,
        (
      select 
        count(*) open_chat_count
      from 
        oi_chat_user_info cui 
        join oi_chat_room_info cri on cui.room_id = cri.room_id
      where
        cui.user_id = ${userId}
        and cri.room_type_cd = 'OPEN'
      ) open_chat,
      (
      select
        count(*) log_count
        from
        oi_user_action_log
      where
        user_id = ${userId}
      ) log;
    `
    console.log(query)
    let result = await mysql.execute(query)

    mysql.end()

    let countsInfo = result[0]
    countsInfo.introduceUrl = 'https://www.ohing.net'

    return response.result(200, result[0])
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}