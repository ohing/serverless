'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 메모 저장
 * @method put putMemo
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let targetUserId = event.pathParameters.targetUserId
    
    const parameters = JSON.parse(event.body || '{}') || {}
    let memo = (parameters.memo || '').trim()
    memo = await mysql.escape(memo)
    
    let query
    let result

    query = `
      insert into oi_user_memo (
        user_id, target_user_id, memo
      ) values (
        ${userId}, ${targetUserId}, ${memo}
      ) on duplicate key update
        memo = ${memo}, reg_dt = now();
    `
    result = await mysql.execute(query)

    mysql.end()

    return response.result(200, {}, '메모가 저장되었습니다.')
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}