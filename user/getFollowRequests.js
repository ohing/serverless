'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).readable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 팔로우 요청 리스트
 * @method get getFollowRequests
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    // const parameters = JSON.parse(event.body || '{}') || {}
    const parameters = event.queryStringParameters || {}

    let keys = Object.keys(parameters)
    for (let key of keys) {
      if (parameters[key].length == 0) {
        delete parameters[key]
      }
    }

    const rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    const lastAccountId = parameters.lastAccountId || ''
    const type = (parameters.type || '').trim()
    if (!(type == 'Request' || type == 'Requested')) {
      return response.result(400)
    }

    let relationJoin = type == 'Request' ? `
      join oi_user_info ui on ur.user_id = ui.user_id
    ` : `
      join oi_user_info ui on ur.relation_user_id = ui.user_id
    `

    let mainCondition = type == 'Request' ? `
      ur.relation_user_id = ${userId}  
    ` : `
      ur.user_id = ${userId}  
    `
    
    let query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        ur4.user_id jjim
      from
        oi_user_relation ur
        ${relationJoin}
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_user_relation ur4 on ur4.user_id = ui.user_id and ur4.relation_user_id = ${userId} and ur4.relation_type_cd = '05' 
      where
        ${mainCondition}
        and ur.relation_type_cd = '01'
        and ur.accpt_dt is null
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and ui.user_login_id > '${lastAccountId}'
      order by
        ui.user_login_id asc
      limit ${rowCount};
    `
    console.log(query)
    let result = await mysql.execute(query)

    if (result.length > 0) {

      let followers = result.map(row => {
        row.tags = []
        row.following = false
        row.requested = type == 'Request'
        row.jjim = row.jjim != null
        return row
      })
      
      let userIds = followers.map(follower => {
        return follower.user_id
      })
      let userIdsString = userIds.join(',')
  
      let followerMap = {}
      followers.forEach(follower => {
        followerMap[follower.user_id] = follower
      })

      if (type == 'Requested') {
        
        query = `
          select
            user_id, accpt_dt
          from
            oi_user_relation
          where 
            relation_user_id = ${userId}
            and relation_type_cd = '01'
            and user_id in (${userIdsString})
        `
        result = await mysql.execute(query)
    
        result.forEach(row => {
          followerMap[row.user_id].requested = row.accpt_dt == null
        })
      }
  
      query = `
        select
          ut.user_id, ti.tag_val
        from
          oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
        where
          ut.user_id in (${userIdsString})
        order by 
          ut.user_id asc,
          ut.tag_seq asc;
      `
      result = await mysql.execute(query)
      mysql.end()
  
      result.forEach(row => {
  
        let tag = row.tag_val
  
        if (tag != null && tag.length > 0) {
          followerMap[row.user_id].tags.push(tag)
        }
      })
  
      followers = Object.values(followerMap)
      followers.sort((lhs, rhs) => {
        return lhs.account_id > rhs.account_id ? 1 : lhs.account_id < rhs.account_id ? -1 : 0
      })

      let onlineKeys = followers.map(user => {
        return `alive:${user.user_id}`
      })
      
      let readable = await redis.readable(3)
      let onlines = await readable.mget(onlineKeys) 
      redis.end()

      for (let i = 0; i < onlines.length; i++) {
        followers[i].is_online = onlines[i] != null
      }

      return response.result(200, followers)

    } else {

      mysql.end()
      return response.result(200, [])
    }

  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}