'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

const AWS = require('aws-sdk');
AWS.config.update({region: 'ap-northeast-2'});
const lambda = new AWS.Lambda()

/**
 * @author Karl <karl@ohing.net> 
 * @description 자기소개 수정
 * @method PUT updateSelfIntro
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}
    let selfIntroduction = parameters.selfIntroduction

    if (selfIntroduction == null) {
      return response.result(400)
    }

    selfIntroduction = await mysql.escape(selfIntroduction)

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    // if (openType == 'CLOSE') {
    //   mysql.end()
    //   return response.result(403, null, '비공개 회원은 프로필 정보를 수정하실 수 없습니다.')
    // }
    
    query = `
      update
        oi_user_info
      set
        self_intro_content = ${selfIntroduction}
      where
        user_id = ${userId};
    `
    await mysql.execute(query)

    query = `
      select 
        relation_user_id
      from
        oi_user_relation
      where
        user_id = ${userId}
        and accpt_dt is not null;
    `
    result = await mysql.execute(query)

    let targetUserIds = result.map(row => {
      return row.relation_user_id
    })

    let params = {
      FunctionName: `api-new-batch-${process.env.stage}-sendPush`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          targetUserIds: targetUserIds,
          messages: [
            {
              userId: userId,
              type: 'Relation',
              dataType: 'ProfileEdit',
            }
          ]
        })
      })
    }
    console.log(params)

    await lambda.invoke(params).promise()

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
