module.updateProfileImage = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    body: JSON.stringify({
      key: "bdccc421-8e3a-48c2-b1b7-47f55bcbf034_1629872785101.jpg",
      width: 800,
      height: 1423
    })
  }
  return await require('./updateProfileImage.js').handler(request)
}

module.viewPage = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      targetUserId: 1
    }
  }
  return await require('./viewPage.js').handler(request)
}

module.getSchools = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      targetUserId: 5
    }
  }
  return await require('./getSchools.js').handler(request)
}

module.deleteSchool = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      targetUserId: 300,
      schoolHistId: 46
    }
  }
  return await require('./deleteSchool.js').handler(request)
}

module.addSchool = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      targetUserId: 300,
      schoolHistId: 46
    },
    body: JSON.stringify({
      schoolId: 'S090006937',
      // schoolName: '어쩌란말이고',
      grade: '3',
      isCurrent: true
    })    
  }
  return await require('./addSchool.js').handler(request)
}

module.modifySchool = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      targetUserId: 300,
      schoolHistId: 65539
    },
    body: JSON.stringify({
      schoolName: '어쩌란말이고',
      grade: '3',
      class: '2'
    })
  }
  return await require('./applySchool.js').handler(request)
}

module.updateTags = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    },
    pathParameters: {
      targetUserId: 300
    },
    body: JSON.stringify({
      tags: [
        "게임", "아이고고", "배가빵빵"
      ]
    })
  }
  return await require('./updateTags.js').handler(request)
}

module.getFollowers = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'b26ab0e0-c277-4a6b-9432-8df520eab222'
    },
    pathParameters: {
      targetUserId: 300,
    },
    queryStringParameters: {
      searchText: '홍길동'
    }
  }
  return await require('./getFollowers.js').handler(request)
}

module.getFollowings = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '7bd6b6b9-f5a3-412f-aead-0181eaf454ab'
    },
    pathParameters: {
      targetUserId: 300
    },
    queryStringParameters: {
      searchText: '홍길동',
      roomId: '3536'
    }
  }
  return await require('./getFollowings.js').handler(request)
}

module.getFollowRequests = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'b26ab0e0-c277-4a6b-9432-8df520eab222'
    },
    queryStringParameters: {
      type: 'Request'
    }
  }
  return await require('./getFollowRequests.js').handler(request)
}

module.getBlockList = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '2e821bef-3d55-4406-b724-5cac64521215'
    },
    queryStringParameters: {
      type: 'Requested'
    }
  }
  return await require('./getBlockList.js').handler(request)
}

module.followRequest = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c39830bf-4d43-4afb-9ecd-fbafab31c5c2'
    },
    pathParameters: {
      targetUserId: 300
    }
  }
  return await require('./followRequest.js').handler(request)
}

module.followAccept = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c39830bf-4d43-4afb-9ecd-fbafab31c5c2'
    },
    pathParameters: {
      targetUserId: 300
    },
    body: JSON.stringify({
      accept: false
    })
  }
  return await require('./followAccept.js').handler(request)
}

module.getFavoriteList = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c39830bf-4d43-4afb-9ecd-fbafab31c5c2'
    },
    queryStringParameters: {
       searchText: '게임'
    }
  }
  return await require('./getFavoriteList.js').handler(request)
}

module.favorite = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c39830bf-4d43-4afb-9ecd-fbafab31c5c2'
    },
    pathParameters: {
      targetUserId: '240'
    }
  }
  return await require('./favorite.js').handler(request)
}



module.getActivityCount = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    }
  }
  return await require('./getActivityCount.js').handler(request)
}

module.getActivityLogs = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': 'c5ee8735-95c6-4025-b6d0-156f285c5e47'
    }
  }
  return await require('./getActivityLogs.js').handler(request)
}

module.isBlackList = async () => {

  let request = {
    pathParameters: {
      targetUserId: 123
    },
    queryStringParameters: {
      ci: 'tPVsmST7uubBw9fCQ/qWBJoBIAvxzKUh0AmXU7RpW0WLqF1xo3GfIy8oz/CgtktORywpbrzAziLLoCfUoVOPZQ=='
    }
  }
  return await require('./isBlackList.js').handler(request)
}

module.setBlackList = async () => {

  let request = {
    pathParameters: {
      targetUserId: 196
    },
    queryStringParameters: {
      ci: 'cicicici'
    }
  }
  return await require('./setBlackList.js').handler(request)
}

module.removeBlackList = async () => {

  let request = {
    pathParameters: {
      targetUserId: 196
    },
    queryStringParameters: {
      ci: 'cicicici'
    }
  }
  return await require('./removeBlackList.js').handler(request)
}

test = async () => {
  
  const environment = require('../environment.js')
  environment.setEnvironment()
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()