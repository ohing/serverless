'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 학교 기록 적용
 * @method put updateSchool
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }
    
    const parameters = JSON.parse(event.body || '{}') || {}
    let isCurrent = parameters.isCurrent
    let schoolId = parameters.schoolId
    let schoolName = parameters.schoolName

    if (isCurrent == null || (schoolId == null && schoolName == null)) {
      return response.result(400)
    }

    schoolName = await mysql.escape(schoolName)

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 프로필 정보를 변경하실 수 없습니다.')
    }

    let schoolShortName
    if (schoolId != null) {
      schoolId = `'${schoolId}'`
      query = `
        select
          school_nm, school_short_nm
        from
          oi_school_info
        where
          school_id = ${schoolId};
      `
      result = await mysql.execute(query)
      if (result.length == 0) {
        mysql.end()
        return response.result(404, null, '학교 정보를 찾을 수 없습니다.')
      }
      schoolName = await mysql.escape(result[0].school_nm)
      schoolShortName = await mysql.escape(result[0].school_short_nm)
    } else {
      schoolId = 'null'
      schoolShortName = schoolName
    }

    let grade = await mysql.escape(parameters.grade || '')
    let classe = await mysql.escape(parameters.classe || '')

    let currentCode = isCurrent ? 'CURR' : 'BEFO'

    query = `
      insert into	oi_user_school_history (
          school_hist_cd, user_id, school_id, school_nm, school_short_nm, grade, class
      ) values (
        '${currentCode}', ${userId}, ${schoolId}, ${schoolName}, ${schoolShortName},
        ${grade}, ${classe}
      ) on duplicate key update
        school_id = ${schoolId}, school_nm = ${schoolName},
        school_short_nm = ${schoolShortName}, grade = ${grade}, class = ${classe};
    `
    await mysql.execute(query)

    mysql.end()

    return response.result(200, {})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}