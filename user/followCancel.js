'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
// const pushreserve = require(`@ohing/ohingpushreserve-${process.env.stage}`)
const response = require('@ohing/ohingresponse')

/**
 * @author Karl <karl@ohing.net> 
 * @description 팔로우 취소
 * @method DELETE followCancel
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const targetUserId = event.pathParameters.targetUserId

    if (userId == targetUserId) {
      return response.result(403, null, '스스로에게 요청할 수 없습니다.')
    }

    let query
    let result

    query = `
      select
        profile_open_type_cd open_type
      from
        oi_user_info
      where
        user_id = ${userId};
    `
    result = await mysql.execute(query)
    let openType = result[0].open_type

    if (openType == 'CLOSE') {
      mysql.end()
      return response.result(403, null, '비공개 회원은 팔로우를 취소하실 수 없습니다.')
    }
    
    query = `
      delete from 
        oi_user_relation
      where
        user_id = ${targetUserId} and relation_user_id = ${userId} and relation_type_cd = '01';
    `
    await mysql.execute(query)

    mysql.end()

    // await pushreserve.delete(userId, 'FollowRequest')
    // await pushreserve.delete(userId, 'Follow')
    // pushreserve.end()

    return response.result(200, {following: false, followRequested: false})
  
  } catch(e) {

    console.error(e)
    mysql.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
