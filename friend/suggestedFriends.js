'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 추천 친구
 * @method GET suggestedFriends
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let query = `
      select 
        ut2.user_id
      from 
        oi_user_tag ut 
        left join oi_user_tag ut2 on ut.tag_id = ut2.tag_id and ut.user_id != ut2.user_id
        left join oi_user_info ui on ut2.user_id = ui.user_id
        left join oi_user_relation ur on ur.user_id = ui.user_id and ur.relation_user_id = ${userId} and ur.relation_type_cd = '01'
        left join oi_user_relation ur2 on ui.user_id = ur2.relation_user_id and ur2.user_id = ${userId} and (ur2.relation_type_cd = '02') 
        left join oi_user_relation ur3 on ui.user_id = ur3.user_id and ur3.relation_user_id = ${userId} and (ur3.relation_type_cd = '02') 
      where
        ut.user_id = ${userId}
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and ui.user_type_cd = 'USER'
        and ur.user_id is null
        and ur2.user_id is null
        and ur3.user_id is null
      group by
        ut2.user_id
      order by
        count(ut2.user_id) desc,
          ui.upd_dt desc
      limit 10;
    `
    console.log(query)
    let result = await mysql.execute(query)

    let userIds = result.map(row => {
      return row.user_id
    })

    let regionCount = 0
    let sameAgeCount = 0

    if (userIds.length < 10) {

      let needsCount = 10 - userIds.length

      query = `
        select 
          region_cd
        from
          oi_user_info
        where
          user_id = ${userId};
      `
      result = await mysql.execute(query)

      if (result[0].region_cd == null) {

        regionCount = 0
        sameAgeCount = needsCount

      } else {

        regionCount = parseInt(needsCount / 2)
        sameAgeCount = needsCount - regionCount
      }
    }

    let notInCondition = userIds.length == 0 ? '' : `
      and ui2.user_id not in (${userIds.join(',')})
    `

    query = `
      select
        ui2.user_id
      from
        oi_user_info ui
        join oi_user_info ui2 on ui.birth_y = ui2.birth_y and ui.user_id != ui2.user_id
        left join oi_user_relation ur on ur.user_id = ui2.user_id and ur.relation_type_cd = '01' and ur.accpt_dt is not null
        left join oi_user_relation ur2 on ui.user_id = ur2.relation_user_id and ur2.user_id = ${userId} and (ur2.relation_type_cd = '02') 
        left join oi_user_relation ur3 on ui.user_id = ur3.user_id and ur3.relation_user_id = ${userId} and (ur3.relation_type_cd = '02') 
      where
        ui.user_id = ${userId}
        ${notInCondition}
        and ui2.user_status_cd = 'ACTIVE'
        and ui2.profile_open_type_cd != 'CLOSE'
        and ui2.user_type_cd = 'USER'
        and ur.user_id is null
        and ur2.user_id is null
        and ur3.user_id is null
      order by 
        ui2.upd_dt desc
      limit
        ${sameAgeCount};
    `
    console.log(query)
    result = await mysql.execute(query)
    userIds = userIds.concat(result.map(row => {
      return row.user_id
    }))

    if (regionCount > 0) {

      notInCondition = userIds.length == 0 ? '' : `
        and ui2.user_id not in (${userIds.join(',')})
      `
      query = `
        select
          ui2.user_id
        from
          oi_user_info ui
          join oi_user_info ui2 on ui.region_cd = ui2.region_cd and ui.user_id != ui2.user_id
          left join oi_user_relation ur on ur.user_id = ui2.user_id and ur.relation_type_cd = '01' and ur.accpt_dt is not null
          left join oi_user_relation ur2 on ui.user_id = ur2.relation_user_id and ur2.user_id = ${userId} and (ur2.relation_type_cd = '02') 
          left join oi_user_relation ur3 on ui.user_id = ur3.user_id and ur3.relation_user_id = ${userId} and (ur3.relation_type_cd = '02') 
        where
          ui.user_id = ${userId}
          ${notInCondition}
          and ui2.user_status_cd = 'ACTIVE'
          and ui2.profile_open_type_cd != 'CLOSE'
          and ui2.user_type_cd = 'USER'
          and ur.user_id is null
          and ur2.user_id is null
          and ur3.user_id is null
        order by 
          ui2.upd_dt desc
        limit
          ${regionCount};
      `
      result = await mysql.execute(query)
      userIds = userIds.concat(result.map(row => {
        return row.user_id
      }))
    }

    if (userIds.length == 0) {
      mysql.end()
      return response.result(200, [])
    }

    console.log('userIds', userIds);

    let indexMap = {}
    let users = []

    for (let i = 0; i < userIds.length; i++) {
      let userId = userIds[i]
      indexMap[userId] = i
      users.push({})
    }

    query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname, ui.self_intro_content,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        case when ur.user_id is not null and ur.accpt_dt is not null then 1 else 0 end following,
        case when ur.user_id is not null and ur.accpt_dt is null then 1 else 0 end requested,
        ur4.user_id jjim
      from
        oi_user_info ui
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_user_relation ur on ui.user_id = ur.user_id and ur.relation_type_cd = '01' and ur.relation_user_id = ${userId}
        left join oi_user_relation ur4 on ur4.user_id = ui.user_id and ur4.relation_user_id = ${userId} and ur4.relation_type_cd = '05'
      where
        ui.user_id in (${userIds.join(',')});
    `
    result = await mysql.execute(query)

    for (let i = 0; i < result.length; i++) {
      let row = result[i]
      
      row.following = row.following != 0
      row.requested = row.requested != 0
      row.jjim = row.jjim != null
      row.tags = []
      row.medias = []
      
      users[indexMap[row.user_id]] = row
    }

    query = `
      select
        ut.user_id, ti.tag_val
      from
        oi_user_tag ut 
        join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ut.user_id in (${userIds.join(',')})
      order by 
		    ut.user_id asc,
        ut.tag_seq asc;
    `
    result = await mysql.execute(query)

    result.forEach(row => {

      let tag = row.tag_val

      if (tag != null && tag.length > 0) {
        users[indexMap[row.user_id]].tags.push(tag)
      }
    })

    for (let user of users) {
      query = `
        select 
          fmi.reg_user_id, fmi.feed_id, fi.org_file_nm media_url, fi.media_type_cd media_type, substring_index(fi.org_file_nm, '/', -1) media_file_name
        from 
          oi_feed_media_info fmi
          join oi_feed_info f on fmi.feed_id = f.feed_id
          left join oi_file_info fi on fmi.file_id = fi.file_id
        where
          fmi.reg_user_id = ${user.user_id}
          and fmi.media_seq = 1
          and f.anony_yn = 'N'
          and f.encoded_yn = 'Y'
          and f.feed_except_yn = 'N'
        order by 
          fmi.feed_id desc
        limit 3;
      `
      console.log(query)
      result = await mysql.execute(query)

      user.medias = result
    }    
    
    mysql.end()

    let readable = await redis.readable(3)

    for (let i = 0; i < users.length; i++) {
      let onlineKey = `alive:${users[i].user_id}`
      console.log(onlineKey)
      users[i].is_online = await readable.get(onlineKey) || false
    }

    redis.end()
    mysql.end()

    return response.result(200, users)

  } catch (e) {

    console.error(e)

    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
