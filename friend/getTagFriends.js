'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 태그포함한 목록
 * @method GET getTagFriends
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let parameters = event.queryStringParameters || {}
    let tag = parameters.tag || ''

    if (tag.length == 0) {
      return response.result(400)
    }

    let rowCount = parameters.rowCount == null ? 30 : parseInt(parameters.rowCount)
    let lastId = parameters.lastId
    let lastIdCondition = lastId == null ? '' : `
      and ui.user_id < ${parseInt(lastId)}
    `

    let query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        case when ur.user_id is not null and ur.accpt_dt is not null then 1 else 0 end following,
        case when ur.user_id is not null and ur.accpt_dt is null then 1 else 0 end requested,
        ur4.user_id jjim
      from
        oi_tag_info ti
        join oi_user_tag ut on ti.tag_id = ut.tag_id
        join oi_user_info ui on ut.user_id = ui.user_id
        left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
        left join oi_file_info fi on pi.file_id = fi.file_id
        left join oi_user_relation ur on ur.user_id = ui.user_id and ur.relation_type_cd = '01' and ur.relation_user_id = ${userId}
        left join oi_user_relation ur2 on ui.user_id = ur2.relation_user_id and ur2.user_id = ${userId} and (ur2.relation_type_cd = '02') 
        left join oi_user_relation ur3 on ui.user_id = ur3.user_id and ur3.relation_user_id = ${userId} and (ur3.relation_type_cd = '02') 
        left join oi_user_relation ur4 on ur4.user_id = ui.user_id and ur4.relation_user_id = ${userId} and ur4.relation_type_cd = '05'
      where
        ti.tag_val = '${tag}'
        and ur2.user_id is null
        and ur3.user_id is null
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        ${lastIdCondition}
      group by
        ui.user_id
      order by
        ui.user_id desc
      limit 
        ${rowCount};
    `
    console.log(query)
    let result = await mysql.execute(query)

    if (result.length == 0) {
      mysql.end()
      redis.end()
      return response.result(200, {total_count: 0, data: []})
    }

    console.log(result.length)

    let userMap = {}
    let userIds = []

    result.forEach(row => {
      row.following = row.following != 0
      row.requested = row.requested != 0
      row.jjim = row.jjim != null
      row.tags = []
      userMap[row.user_id] = row
      userIds.push(row.user_id)
    })

    query = `
      select 
        count(*) total_count
      from (
        select 
          ui.user_id
        from
          oi_tag_info ti
          join oi_user_tag ut on ti.tag_id = ut.tag_id
          join oi_user_info ui on ut.user_id = ui.user_id
          left join oi_user_profile_image pi on ui.user_id = pi.user_id and pi.image_type_cd = 'PF'
          left join oi_file_info fi on pi.file_id = fi.file_id
          left join oi_user_relation ur on ur.user_id = ui.user_id and ur.relation_type_cd = '01' and ur.relation_user_id = ${userId}
          left join oi_user_relation ur2 on ui.user_id = ur2.relation_user_id and ur2.user_id = ${userId} and (ur2.relation_type_cd = '02') 
          left join oi_user_relation ur3 on ui.user_id = ur3.user_id and ur3.relation_user_id = ${userId} and (ur3.relation_type_cd = '02') 
          left join oi_user_relation ur4 on ur4.user_id = ui.user_id and ur4.relation_user_id = ${userId} and ur4.relation_type_cd = '05'
        where
          ti.tag_val = '${tag}'
          and ur2.user_id is null
          and ur3.user_id is null
          and ui.user_status_cd = 'ACTIVE'
          and ui.profile_open_type_cd != 'CLOSE'
        group by
          ui.user_id
      ) a
    `
    result = await mysql.execute(query)
    let totalCount = result[0].total_count

    query = `
      select
        ut.user_id, ti.tag_val
      from
        oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ut.user_id in (${userIds.join(',')})
      order by 
        ut.user_id asc,
        ut.tag_seq asc;
    `
    console.log(query)
    result = await mysql.execute(query)
    mysql.end()

    for (let row of result) {

      let tagValue = row.tag_val

      if (tagValue != null && tagValue.length > 0) {
        userMap[row.user_id].tags.push(tagValue)
      }
    }

    let users = Object.values(userMap)
    
    users.sort((lhs, rhs) => {
      return lhs.user_id > rhs.user_id ? -1 : lhs.user_id < rhs.user_id ? 1 : 0
    })

    let readable = await redis.readable(3)

    let onlineKeys = users.map(user => {
      return `alive:${user.user_id}`
    })

    let onlines = await readable.mget(onlineKeys)

    for (let i = 0; i < onlines.length; i++) {
      users[i].is_online = onlines[i] != null
    }

    redis.end()

    return response.result(200, {total_count: totalCount, data: users})
  
  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
