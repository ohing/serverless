module.find = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    body: JSON.stringify({
      region: [
        '100260'
      ] 
    })
  }
  return await require('./find.js').handler(request)
}

module.getTagFriends = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    queryStringParameters: {
      tag: '게임'
    }
  }
  return await require('./getTagFriends.js').handler(request)
}

module.getIdFriends = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    queryStringParameters: {
      accountId: 'karl01'
    }
  }
  return await require('./getIdFriends.js').handler(request)
}

module.suggestedFriends = async () => {
  let request = {
    headers: {
      'os-type': 'iOS',
      'app-ver': '3.0.0',
      'imei': '0E29614A-CBA2-40F2-9C86-F',
      'access-token': '6c982018-b509-4ca7-8234-1d7f19704d9e'
    },
    queryStringParameters: {
      accountId: 'karl01'
    }
  }
  return await require('./suggestedFriends.js').handler(request)
}



test = async () => {
  
  const environment = require('../environment.js')
  environment.setEnvironment()
  
  if (process.argv.length > 2) {

    const begin = new Date()
  
    let fname = process.argv[2]
    console.log(fname)
    if (fname.endsWith('.js')) {
      fname = fname.substring(0, fname.length-3)
    }
    const result = await module[fname]()

    console.log(result)
  
    console.log('duration', new Date() - begin)

  } else {

    console.log('no input')
  }
}

test()