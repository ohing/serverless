'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 친구찾기
 * @method GET find
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    const parameters = JSON.parse(event.body || '{}') || {}

    let sex = parameters.sex || ['M', 'F']
    if (Array.isArray(sex) && sex.length == 0) {
      sex = ['M', 'F']
    }
    let age = parameters.age || [0]
    if (Array.isArray(age) && age.length == 0) {
      age = [0]
    }
    let tags = parameters.tag || []
    let region = parameters.region || []

    let rowCount = parameters.rowCount || 30
    let lastNickname = parameters.lastNickname

    if (Array.isArray(sex) == false || Array.isArray(age) == false || Array.isArray(tags) == false || Array.isArray(region) == false) {
      return response.result(400)
    }

    // if (tag.length == 0) {
    //   let query = `
    //     select 
    //       ti.tag_val
    //     from 
    //       oi_tag_info ti
    //       join oi_user_tag ut on ti.tag_id = ut.tag_id
    //     where
    //       ut.user_id = ${userId}
    //     order by
    //       ut.tag_seq asc;
    //   `
    //   let result = await mysql.execute(query)
    //   tag = result.map(row => {
    //     return row.tag_val
    //   })
    // }

    // if (tag.length == 0) {
    //   mysql.end()
    //   return response.result(200, [])
    // }
    
    // if (region.lenth == 0) {
    //   return response.result(200, [])
    // }
    
    let regionStrings = region.map(regionValue => {
      return `'${regionValue}'`
    })

    let date = new Date()
    date = new Date(date.getTime() + 32400000)
    // let month = `${date.getMonth()+1}`.padStart(2, '0')
    // let day = `${date.getDate()}`.padStart(2, '0')
    // let md = `${month}${day}`

    let sexCondition = ''
    sex = sex.filter(sexValue => {
      return sexValue == 'M' || sexValue == 'F'
    })

    if (sex.length == 1) {
      sexCondition = `and ui.sex = '${sex[0]}'`
    }

    let regionCondition = regionStrings.length == 0 ? '' : `and ui.region_cd in (${regionStrings.join(',')})`
    
    if (age.includes(0)) {
      let query = `
        select
          birth_y, birth_md
        from
          oi_user_info
        where
          user_id = ${userId}
      `
      let result = await mysql.execute(query)
      let userAge = date.getFullYear() - result[0].birth_y + 1
      // if (md < result[0].birth_md) {
      //   userAge --
      // }
      if (!age.includes(userAge)) {
        age.push(userAge)
      }
    }

    let ageConditions = []

    age.forEach(ageValue => {

      if (ageValue > 0) {
        
        let year = date.getFullYear() - ageValue + 1

        // if (md == '1231') {
        //   ageConditions.push(
        //     `(ui.birth_y = '${year}')`
        //   )
        // } else {
        //   ageConditions.push(
        //     `((ui.birth_y = '${year-1}' and ui.birth_md >= '${md}') or (ui.birth_y = '${year}' and ui.birth_md < '${md}'))`
        //   )
        // }

        ageConditions.push(`ui.birth_y = '${year}'`)
      }
    })

    let ageCondition = ageConditions.length == 0 ? '' : `
      and (${ageConditions.join(' or ')})
    `

    let tagStrings = []

    for (let tag of tags) {
      let escaped = await mysql.escape(tag)
      tagStrings.push(escaped)
    }

    let nicknameCondition = lastNickname == null ? '' : `and ui.user_nick_nm > '${lastNickname}'`

    let tagCondition = tagStrings.length > 0 ? `and ti.tag_val in (${tagStrings.join(',')})` : ''
    
    let query = `
      select
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        case when ur.user_id is not null and ur.accpt_dt is not null then 1 else 0 end following,
        case when ur.user_id is not null and ur.accpt_dt is null then 1 else 0 end requested,
        ur4.user_id jjim
      from
        oi_user_info ui
        left join oi_user_tag ut on ui.user_id = ut.user_id
        left join oi_tag_info ti on ut.tag_id = ti.tag_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_user_relation ur on ui.user_id = ur.user_id and ur.relation_user_id = ${userId} and ur.relation_type_cd = '01'
        left join oi_user_relation ur2 on ui.user_id = ur2.relation_user_id and ur2.user_id = ${userId} and (ur2.relation_type_cd = '02') 
        left join oi_user_relation ur3 on ui.user_id = ur3.user_id and ur3.relation_user_id = ${userId} and (ur3.relation_type_cd = '02') 
        left join oi_user_relation ur4 on ur4.user_id = ui.user_id and ur4.relation_user_id = ${userId} and ur4.relation_type_cd = '05'
      where
        ui.user_id != ${userId}
        and ur2.user_id is null
        and ur3.user_id is null
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        ${sexCondition}
        ${regionCondition}
        ${ageCondition}
        ${tagCondition}
        ${nicknameCondition}
        and ui.user_id not in (
          select 
            ur.user_id
          from 
            oi_user_info ui
            left join oi_user_relation ur on ui.user_id = ur.user_id
          where
            ur.relation_user_id = ${userId}
            and (ur.relation_type_cd = '01' and ur.accpt_dt is not null)
        )
      group by 
        ui.user_id
      order by 
        ui.user_nick_nm asc
      limit ${rowCount}; 
    `

    console.log(query)
    
    let users = await mysql.execute(query)

    if (users.length == 0) {
      mysql.end()
      return response.result(200, [])
    }

    let userIds = users.map(user => {
      return user.user_id
    })

    let userMap = {}
    users.forEach(user => {
      user.following = user.following != 0
      user.requested = user.requested != 0
      user.jjim = user.jjim != null
      user.tags = []
      userMap[user.user_id] = user
    })

    query = `
      select
        ut.user_id, ti.tag_val
      from
        oi_user_tag ut 
        join oi_tag_info ti on ut.tag_id = ti.tag_id
      where
        ut.user_id in (${userIds.join(',')})
      order by 
		    ut.user_id asc,
        ut.tag_seq asc;
    `
    let result = await mysql.execute(query)
    mysql.end()

    result.forEach(row => {

      let tag = row.tag_val

      if (tag != null && tag.length > 0) {
        userMap[row.user_id].tags.push(tag)
      }
    })

    users = Object.values(userMap)
    users.sort((lhs, rhs) => {
      return lhs.nickname > rhs.nickname ? 1 : lhs.nickname < rhs.nickname ? -1 : 0
    })

    let readable = await redis.readable(3)

    for (let i = 0; i < users.length; i++) {
      let onlineKey = `alive:${users[i].user_id}`
      console.log(onlineKey)
      users[i].is_online = await readable.get(onlineKey) || false
    }
    
    redis.end()

    return response.result(200, users)
  
  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
