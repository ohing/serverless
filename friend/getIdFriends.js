'use strict';

const authorizer = require(`@ohing/ohingauthorizer-${process.env.databaseStage}`)
const mysql = require(`@ohing/ohingrds-${process.env.databaseStage}`).writable()
const redis = require(`@ohing/ohingredis-${process.env.databaseStage}`)
const response = require('@ohing/ohingresponse')


/**
 * @author Karl <karl@ohing.net> 
 * @description 아이디 검색 목록
 * @method GET getIdFriends
 * @returns  
*/

module.exports.handler = async event => {

  console.log(event)

  try {

    const imei = event.headers.imei
    const osType = event.headers['os-type']
    const appVersion = event.headers['app-ver']
    const accessToken = event.headers['access-token']

    if (imei == null || osType == null || appVersion == null || accessToken == null) {
      return response.result(400)
    }

    const userId = await authorizer.userId(accessToken)
    if (userId == null) {
      return response.result(401)
    }

    let parameters = event.queryStringParameters || {}
    let accountId = (parameters.accountId || '').trim()

    if (accountId.length == 0) {
      return response.result(400)
    }
    
    let query = `
      select 
        ui.user_id, ui.user_login_id account_id, ui.user_nick_nm nickname, ui.self_intro_content,
        fi.org_file_nm profile_image_url, substring_index(fi.org_file_nm, '/', -1) profile_file_name,
        case when ur.user_id is not null and ur.accpt_dt is not null then 1 else 0 end following,
        case when ur.user_id is not null and ur.accpt_dt is null then 1 else 0 end requested,
        ur4.user_id jjim
      from
        oi_user_info ui
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_user_relation ur on ur.user_id = ui.user_id and ur.relation_type_cd = '01' and ur.relation_user_id = ${userId}
        left join oi_user_relation ur2 on ui.user_id = ur2.relation_user_id and ur2.user_id = ${userId} and (ur2.relation_type_cd = '02') 
        left join oi_user_relation ur3 on ui.user_id = ur3.user_id and ur3.relation_user_id = ${userId} and (ur3.relation_type_cd = '02') 
        left join oi_user_relation ur4 on ur4.user_id = ui.user_id and ur4.relation_user_id = ${userId} and ur4.relation_type_cd = '05'
      where
        ui.user_id != ${userId}
        and ui.user_login_id like '%${accountId}%'
        and ui.user_status_cd = 'ACTIVE'
        and ui.profile_open_type_cd != 'CLOSE'
        and ur2.user_id is null
        and ur3.user_id is null
      group by 
		    ui.user_id
      order by 
        ui.user_login_id
      limit 
        50;
    `
    console.log(query)
    let result = await mysql.execute(query)

    if (result.length == 0) {
      return response.result(200, [])
    }

    let users = []

    let readable = await redis.readable(3)

    for (let userInfo of result) {

      userInfo.following = userInfo.following != 0
      userInfo.requested = userInfo.requested != 0
      userInfo.jjim = userInfo.jjim != null

      query = `
        select
          ti.tag_val
        from
          oi_user_tag ut join oi_tag_info ti on ut.tag_id = ti.tag_id
        where
          ut.user_id = ${userInfo.user_id}
        order by 
          ut.user_id asc,
          ut.tag_seq asc;
      `
      // console.log(query)
      result = await mysql.execute(query)

      userInfo.tags = result.map(row => {
        return row.tag_val
      })

      query = `
        select 	
          fmi.feed_id, fmi.media_seq,
          fi.org_file_nm media_url, substring_index(fi.org_file_nm, '/', -1) media_file_name,
          fi.media_type_cd media_type
        from
          oi_feed_info f
          join oi_feed_media_info fmi on f.feed_id = fmi.feed_id
          join oi_file_info fi on fmi.file_id = fi.file_id
        where
          f.reg_user_id = ${userInfo.user_id}
          and f.anony_yn = 'N'
          and f.encoded_yn = 'Y'
          and f.feed_except_yn = 'N'
          and media_seq = 1
        order by
          f.feed_id desc
        limit 3;
      `
      userInfo.medias = await mysql.execute(query)

      let onlineKey = `alive:${userInfo.user_id}`
      let online = await readable.get(onlineKey)

      userInfo.is_online = online != null

      users.push(userInfo)
    }

    mysql.end()
    redis.end()
    
    return response.result(200, users)
  
  } catch(e) {

    console.error(e)

    mysql.end()
    redis.end()

    if (e.statusCode != null) {
      return e
    } else {
      return response.result(403, e)
    }
  }
}
